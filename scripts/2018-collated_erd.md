













IC-FEDS DATABASE

ENTITY RELATIONSHIP DIAGRAMS

------------------------------------------------------------------------

Financial Examination Group
==========================================================

1.  Brokers Division
    ------------------------------------------



1.  ### Annual Submission and Examination of Financial Condition/Standing

![](up-ic-erd/image12.png)

















Summaries

id

name

account\_type

1

Cash Restricted - Clients' Money Account

Assets

2

Cash and Cash Equivalents

Assets

3

Receivable from Insurance Companies' Clients

Assets

4

Receivable from Ceding Company

Assets

5

Receivable from HMOs' Members

Assets

6

Commission Receivable

Assets

7

Other Receivables

Assets

8

Subscriptions Receivable

Assets

9

Prepayments

Assets

10

Financial Assets At Fair Value Through Profit or
Loss

Assets

11

Held to Collect (HTC) Investments

Assets

12

Financial Assets at Other Comprehensive Income

Assets

13

Derivative Assets Held for Hedging

Assets

14

Loans and Receivable

Assets

15

Investment in Associates

Assets

16

Investment in Joint Ventures

Assets

17

Investment Property

Assets

18

Property and Equipment

Assets

19

Post-Employment Defined Benefit Assets

Assets

20

Deferred Tax Asset

Assets

21

Intangible Assets

Assets

22

Other Assets

Assets

23

Payable to Insurance Companies

Liability

24

Payable to Reinsurer

Liability

25

Payable to Ceding Companies

Liability

26

Payable to Insured

Liability

27

Payable to HMO Provider

Liability

28

Other Payables

Liability

29

SSS/ECC/PAG-IBIG/PHILHEALTH Premium Contributions
Payable

Liability

30

Taxes Payable

Liability

31

Management Fee Payable

Liability

32

Notes Payables

Liability

33

Loan Payable

Liability

34

Finance Lease Liabilities

Liability

35

Post-Employment Benefits Obligation

Liability

36

Deferred Tax Liability

Liability

37

Derivative Liabilities Held for Hedging

Liability

38

Other Liabilities

Liability

39

Share Capital

Equity

40

Subscribed Share Capital

Equity

41

Capital Paid in Excess of Par

Equity

42

Retained Earnings

Equity

43

Treasury Share

Equity

44

Reserves

Equity

45

Contingency Surplus

Equity

46

Revaluation Surplus

Equity

47

Commission Income

Income

48

Interest Income

Income

49

Dividend Income

Income

50

Rental Income

Income

51

Gain/Loss on Sale of Investments

Income

52

Gain/Loss on Sale of Property and Equipment

Income

53

Miscellaneous Income

Income

54

Cost of Services

Expense

55

Administrative Expenses

Expense

56

Donation and Contribution

Expense

57

Miscellaneous Expense

Expense

58

Provision for Income Tax

Expense







Schedules

id

name

summary\_id

1

Clients' Money on Hand

1

2

Clients' Money in Bank

1

3

Money Market Instruments

2

4

Cash in Bank

2

5

Cash Equivalents

2

6

Receivable from Insurance Companies' Clients

3

7

Receivable from Ceding Company

3

8

Receivable from HMOs' Members

3

9

Commission Receivable from Insured

6

10

Commission Receivable from Insurance Companies

6

11

Commission Receivable from Reinsurer

6

12

Commission Receivable from Ceding Company

6

13

Commission Receivable - Others

6

14

Accounts Receivable

7

15

Notes Receivable

7

16

Interest Receivable

7

17

Dividend Receivable

7

18

Receivables - Others

7

19

Allowance for Impairment Losses

7

20

Subscriptions Receivable

8

21

Prepaid Rent

9

22

Prepaid Value-Added Tax (VAT)

9

23

Creditable Withholding Tax

9

24

Financial Assets At Fair Value Through Profit or
Loss

10

25

Held to Collect (HTC) Investments

11

26

Financial Assets at Other Comprehensive Income

12

27

Fair Value Hedge

13

28

Cash Flow Hedge

13

29

Hedges of a Net Investment in Foreign Operation

13

30

Loans and Receivable

14

31

Investment in Associates

15

32

Investment in Joint Ventures

16

33

Investment Property - Land

17

34

Investment Property - Building and Building
Improvements

17

35

Investment Property - Foreclosed Properties

17

36

Land

18

37

Building and Building Improvements

18

38

Leasehold Improvements

18

39

IT Equipment

18

40

Transportation Equipment

18

41

Office Furniture, Fixtures and Equipment

18

42

Property and Equipment under Lease

18

43

Other Equipment

18

44

Revaluation Increment

18

45

Post-Employment Defined Benefit Assets

19

46

Deferred Tax Asset

20

47

Intangible Assets

21

48

Other Assets

22

49

Payable to Insurance Companies

23

50

Payable to Reinsurer

24

51

Payable to Ceding Companies

25

52

Payable to Insured

26

53

Payable to HMO Provider

27

54

Accounts Payable

28

55

Lease Liability

28

56

Dividends Payable

28

57

Other Payables

28

58

SSS Premiums Payable

29

59

SSS Loans Payable

29

60

PAG-IBIG Premiums Payable

29

61

PAG-IBIG Loans Payable

29

62

Income Tax Payable

30

63

Withholding Tax Payable

30

64

Value-Added Tax (VAT) Payable

30

65

Other Taxes and Licenses Payable

30

66

Management Fee Payable

31

67

Notes Payables

32

68

Loan Payable

33

69

Finance Lease Liabilities

34

70

Post-Employment Benefits Obligation

35

71

Deferred Tax Liability

36

72

Fair Value Hedge

37

73

Cash Flow Hedge

37

74

Hedges of a Net Investment Foreign Operation

37

75

Deferred Income

38

76

Others

38

77

Preferred Shares

39

78

Common Shares

39

79

Subscribed Share Capital

40

80

Capital Paid in Excess of Par

41

81

Appropriated Retained Earnings

42

82

Unappropriated Retained Earnings

42

83

Treasury Share

43

84

Reserve for Financial Assets at Other Comprehensive
Income

44

85

Reserve for Cash Flow Hedge

44

86

Reserve for Hedge of a Net Investment in Foreign
Operations

44

87

Cumulative Foreign Currency Translation

44

88

Contingency Surplus

45

89

Revaluation Surplus

46

90

Commission Income from Insurance Companies

47

91

Commission Income from Insureds

47

92

Commission Income - Others

47

93

Interest Income

48

94

Dividend Income

49

95

Rental Income

50

96

Gain/Loss on Sale of Investments

51

97

Gain/Loss on Sale of Property and Equipment

52

98

Miscellaneous Income

53

99

Representation and Entertainment

54

100

Transportation and Travel Expenses

54

101

Management Fee Expense

54

102

Other Expenses

54

103

Salaries and Wages Benefits

55

104

Professional and Technical Development

55

105

Professional Fees

55

106

Taxes and Licenses

55

107

Rental Expense

55

108

Utilities Expense

55

109

Depreciation and Amortization

55

110

Repairs and Maintenance

55

111

Insurance Expense

55

112

Advertising and Promotions

55

113

Bank Charges

55

114

Donation and Contribution

56

115

Miscellaneous Expense

57

116

Provision for Income Tax - Final

58

117

Provision for Income Tax - Current

58

118

Provision for Income Tax - Deferred

58





Forms

id

schedule\_id

form\_name

1

1

Clients' Money on Hand - Premiums

2

1

Clients' Money on Hand - Claims

3

1

Clients' Money on Hand - HMO Fees

4

2

Clients' Money in Bank - Premiums

5

2

Clients' Money in Bank - Claims

6

2

Clients' Money in Bank - HMO Fees

7

3

Undeposited Collection

8

3

Petty Cash Fund

9

3

Revolving Fund

10

3

Other Funds

11

4

Cash in Bank - Current

12

4

Cash in Bank - Savings

13

5

Time Deposits

14

5

Money Market Instruments

15

5

Cash Equivalent - Others

16

6

Receivable from Insurance Companies' Clients

17

7

Receivable from Ceding Company

18

8

Receivable from HMOs' Members

19

9

Commission Receivable from Insured

20

10

Commission Receivable from Insurance Companies - Direct
Payment

21

10

Commission Receivable from Insurance Companies -
Indirect Payment

22

11

Commission Receivable from Reinsurer- Direct
Payment

23

11

Commission Receivable from Reinsurer - Indirect
Payment

24

12

Commission Receivable from Ceding Company

25

13

Commission Receivable - Others

26

14

Advances to Officers and Employees

27

14

Rental Receivable

28

15

Notes Receivable

29

16

Interest Receivable

30

17

Dividend Receivable

31

18

Receivables - Others

32

19

Allowance for Impairment Losses

33

20

Subscriptions Receivable

34

21

Prepaid Rent

35

22

Prepaid Value-Added Tax (VAT)

36

23

Creditable Withholding Tax

37

24

Financial Assets At Fair Value Through Profit or
Loss

38

25

Held to Collect (HTC) Investments

39

26

Financial Assets at Other Comprehensive Income

40

27

Fair Value Hedge

41

28

Cash Flow Hedge

42

29

Hedges of a Net Investment in Foreign Operation

43

30

Loans and Receivable

44

31

Investment in Associates

45

32

Investment in Joint Ventures

46

33

Investment Property - Land

47

34

Investment Property - Building

48

34

Investment Property - Building Improvements

49

35

Investment Property - Foreclosed Properties

50

36

Land

51

37

Building

52

37

Building Improvements

53

38

Leasehold Improvements

54

39

IT Equipment

55

40

Transportation Equipment

56

41

Office Furniture, Fixtures and Equipment

57

42

Property and Equipment under Lease

58

43

Other Equipment

59

44

Revaluation Increment

60

45

Post-Employment Defined Benefit Assets

61

46

Deferred Tax Asset

62

47

Intangible Assets

63

48

Other Assets

64

49

Payable to Insurance Companies

65

50

Payable to Reinsurer

66

51

Payable to Ceding Companies

67

52

Payable to Insured

68

53

Payable to HMO Provider

69

54

Accounts Payable

70

55

Lease Liability

71

56

Dividends Payable

72

57

Other Payables

73

58

SSS Premiums Payable

74

59

SSS Loans Payable

75

60

PAG-IBIG Premiums Payable

76

61

PAG-IBIG Loans Payable

77

62

Income Tax Payable

78

63

Withholding Tax Payable

79

64

Value-Added Tax (VAT) Payable

80

65

Other Taxes and Licenses Payable

81

66

Management Fee Payable

82

67

Notes Payables

83

68

Loan Payable

84

69

Finance Lease Liabilities

85

70

Post-Employment Benefits Obligation

86

71

Deferred Tax Liability

87

72

Fair Value Hedge

88

73

Cash Flow Hedge

89

74

Hedges of a Net Investment Foreign Operation

90

75

Deferred Income

91

76

Others

92

77

Preferred Shares

93

78

Common Shares

94

79

Subscribed Share Capital

95

80

Capital Paid in Excess of Par

96

81

Appropriated Retained Earnings

97

82

Unappropriated Retained Earnings

98

83

Treasury Share

99

84

Reserve for Financial Assets at Other Comprehensive
Income

100

85

Reserve for Cash Flow Hedge

101

86

Reserve for Hedge of a Net Investment in Foreign
Operations

102

87

Cumulative Foreign Currency Translation

103

88

Contingency Surplus

104

89

Revaluation Surplus

105

90

Commission Income from Insurance Companies

106

91

Commission Income from Insureds

107

92

Commission Income - Others

108

93

Interest Income

109

94

Dividend Income

110

95

Rental Income

111

96

Gain/Loss on Sale of Investments

112

97

Gain/Loss on Sale of Property and Equipment

113

98

Miscellaneous Income

114

99

Representation and Entertainment

115

100

Transportation and Travel Expenses

116

101

Management Fee Expense

117

102

Other Expenses

118

103

Salaries and Wages

119

103

13th Month/Bonuses/Incentives

120

103

SSS/EC/PAG-IBIG/PHILHEALTH Contributions

121

103

Post-Employment Benefit Expense

122

103

Other Employee Benefits

123

104

Professional and Technical Development

124

105

Professional Fees

125

106

Taxes and Licenses

126

107

Rental Expense

127

108

Utilities Expense

128

109

Depreciation and Amortization

129

110

Repairs and Maintenance - Labor

130

110

Repairs and Maintenance - Materials

131

111

Insurance Expense

132

112

Advertising and Promotions

133

113

Bank Charges

134

114

Donation and Contribution

135

115

Miscellaneous Expense

136

116

Provision for Income Tax - Final

137

117

Provision for Income Tax - Current

138

118

Provision for Income Tax - Deferred





Computed Attributes

id

form\_id

attribute

1

1

balance

2

2

balance

3

3

balance

4

4

account\_balance

5

5

translated\_balance

6

6

account\_balance

7

7

balance

8

8

balance

9

9

balance

10

10

balance

11

11

translated\_balance

12

12

translated\_balance

13

13

account\_balance

14

14

amount

15

15

amount

16

16

amount

17

17

amount

18

18

amount

19

19

amount

20

20

amount

21

21

amount

22

22

amount

23

23

amount

24

24

amount

25

25

amount

26

26

amount

27

27

amount

28

28

amount

29

29

amount

30

30

amount

31

31

amount

32

32

amount

33

33

amount

34

34

amount

35

35

amount

36

36

amount

37

37

total\_fair\_market\_value

38

38

total\_fair\_market\_value

39

39

total\_fair\_market\_value

40

40

amount

41

41

amount

42

42

amount

43

43

amount

44

44

amount

45

45

amount

46

46

fair\_value

47

47

fair\_value

48

48

fair\_value

49

49

fair\_value

50

50

fair\_value

51

51

fair\_value

52

52

fair\_value

53

53

net\_book\_value

54

54

net\_book\_value

55

55

net\_book\_value

56

56

net\_book\_value

57

57

net\_book\_value

58

58

net\_book\_value

59

59

net\_book\_value

60

60

present\_value\_of\_benefit\_obligation

61

61

amount

62

62

net\_book\_value

63

63

amount

64

64

amount

65

65

amount

66

66

amount

67

67

amount

68

68

amount

69

69

amount

70

70

amount

71

71

amount

72

72

amount

73

73

amount

74

74

amount

75

75

amount

76

76

amount

77

77

amount

78

78

amount

79

79

amount

80

80

amount

81

81

amount

82

82

amount

83

83

amount

84

84

amount

85

85

present\_value\_of\_benefit\_obligation

86

86

amount

87

87

amount

88

88

amount

89

89

amount

90

90

amount

91

91

amount

92

92

total\_share\_capital

93

93

total\_share\_capital

94

94

amount

95

95

amount

96

96

amount

97

97

amount

98

98

amount

99

99

amount

100

100

amount

101

101

amount

102

102

amount

103

103

amount

104

104

amount

105

105

amount

106

106

amount

107

107

amount

108

108

amount

109

109

amount

110

110

amount

111

111

amount

112

112

amount

113

113

amount

114

114

amount

115

115

amount

116

116

amount

117

117

amount

118

118

amount

119

119

amount

120

120

amount

121

121

amount

122

122

amount

123

123

amount

124

124

amount

125

125

amount

126

126

amount

127

127

amount

128

128

amount

129

129

amount

130

130

amount

131

131

amount

132

132

amount

133

133

Amount

134

134

amount

135

135

amount

136

136

amount

137

137

amount

138

138

amount



1.  ### Quarterly Submission and Monitoring of Client Money’s Compliance

![](up-ic-erd/image4.png)





Business Lines

id

name

1

Life Insurance

2

Fire Insurance

3

Marine Cargo

4

Marine Hull

5

Aviation

6

Fidelity and Surety

7

Motor Insurance

8

Health Insurance

9

HMO

10

Accident

11

Engineering

12

Miscellaneous



1.  HMO Division
    -------------------------------------



1.  ### Annual Submission and Examination of Financial Statements

![](up-ic-erd/image41.png)





SFP Chart of Accounts

id

level\_id

parent\_id

account\_name

1

0

-

ASSETS

2

1

1

Current Assets

3

2

2

Cash and Cash Equivalents

4

3

3

Cash on Hand

5

4

4

Undeposited Collections

6

4

4

Petty Cash Fund

7

4

4

Revolving Fund

8

4

4

Commission Fund

9

4

4

Other Funds

10

3

3

Cash in Banks

11

4

10

Current - Peso

12

4

10

Current - Foreign

13

4

10

Savings - Peso

14

4

10

Savings - Foreign

15

3

3

Cash Equivalents

16

3

10

Time Deposits

17

3

10

Money Market Instruments

18

3

10

Others

19

2

3

Short-term Cash lnvestments

20

2

2

Administrative Service Only (ASO) Cash Fund

21

2

2

Trade and Other Receivables

22

3

21

Trade Receivables

23

4

22

Membership Fee Receivable

24

4

22

Riders Fee Receivable

25

4

22

Deposit to Healthcare Providers

26

4

22

Due from ASO Accounts

27

4

22

Allowance for Bad Debts

28

3

21

Other Receivables

29

4

28

Notes Receivable

30

4

28

Interest Receivable

31

4

28

Advances to Officers and Employees

32

4

28

Due from Officers and Employees

33

4

28

Due from Related Parties

34

4

28

Others

35

4

28

Allowance for Bad Debts

36

2

2

Financial Assets at Fair Value Through Profit or
Loss

37

3

36

Securities Held for Trading

38

4

37

Trading Debt Securities - Government

39

4

37

Trading Debt Securities - Private

40

4

37

Trading Equity Securities

41

4

37

Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds

42

3

36

Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)

43

4

42

Debt Securities - Government

44

4

42

Debt Securities - Private

45

4

42

Equity Securities

46

4

42

Mutual Funds and Unit Investment Trusts

47

4

42

Real Estate Investment Trusts

48

4

42

Others

49

2

2

Held-to-Maturity (HTM) Investments - Current
Portion

50

3

49

HTM Debt Securities - Government - Current
Portion

51

4

50

Unamortized (Discount)/Premium

52

4

50

HTM Debt Securities - Private - Current Portion

53

4

50

Unamortized (Discount)/Premium

54

4

50

Allowance for Impairment Losses

55

3

49

Available-for-Sale (AFS) Financial Assets - Current
Portion

56

4

55

AFS Debt Securities - Government - Current
Portion

57

4

55

Unamortized (Discount)/Premium

58

4

55

AFS Debt Securities - Private - Current Portion

59

4

55

Unamortized (Discount)/Premium

60

4

55

AFS Equity Securities - Current Portion

61

4

55

Allowance for Impairment Losses

62

2

2

Mutual Funds and Unit Investment Trusts - Current
Portion

63

3

62

Real Estate Investment Trusts - Current Portion

64

4

64

Others - Current Portion

65

3

62

Prepayments

66

4

64

Supplies

67

3

62

Prepaid Commissions

68

2

2

Prepaid Rent

69

3

68

Prepaid Taxes

70

4

69

Others

71

3

68

Other Current Assets

72

4

71

Non-current Assets

73

3

68

Held-to-Maturity (HTM) Investments - Non-Current
Portion

74

3

68

HTM Debt Securities - Government - Non-Current
Portion

75

3

68

Unamortized (Discount)/Premium

76

3

68

HTM Debt Securities - Private - Non-Current
Portion

77

3

68

Unamortized (Discount)/Premium

78

2

2

Allowance for Impairment Losses

79

3

78

Available-for-Sale (AFS) Financial Assets - Non-Current
Portion

80

3

78

AFS Debt Securities - Government - Non-Current
Portion

81

3

78

Unamortized (Discount)/Premium

82

3

78

AFS Debt Securities - Private - Non-Current
Portion

83

3

78

Unamortized (Discount)/Premium

84

2

2

AFS Equity Securities - Non-Current Portion

85

1

1

Allowance for Impairment Losses

86

2

85

Mutual Funds and Unit Investment Trusts - Non-Current
Portion

87

3

86

Real Estate Investment Trusts - Non-Current
Portion

88

4

87

Others - Non-Current Portion

89

3

86

Investments in Subsidiaries, Associates and Joint
Ventures

90

4

89

Investment in Subsidiaries

91

3

86

Investment in Associates

92

2

85

Investment in Joint Ventures

93

3

92

Property and Equipment

94

4

93

Land - At Cost

95

3

92

Building and Building Improvements - At Cost

96

4

95

Accumulated Depreciation - Building and Building
Improvements

97

3

92

Leasehold Improvements - At Cost

98

3

92

Accumulated Depreciation - Leasehold
Improvements

99

3

92

IT Equipment - At Cost

100

3

92

Accumulated Depreciation - IT Equipment

101

3

92

Transportation Equipment - At Cost

102

2

85

Accumulated Depreciation - Transportation
Equipment

103

3

102

Office Furniture, Fixtures and Equipment - At
Cost

104

3

102

Accumulated Depreciation - Office Furniture, Fixtures
and Equipemnt

105

3

102

Medical Equipment

106

2

85

Accumulated Depreciation - Medical Equipment

107

3

106

Property and Equipment Under Finance Lease

108

3

106

Accumulated Depreciation - P&E under Finance
Lease

109

4

108

Revaluation Increment

110

3

106

Accumulated Depreciation - Revaluation Increment

111

4

110

Investment Property

112

3

106

Land - At Cost

113

4

112

Building and Building Improvements - At Cost

114

3

106

Accumulate Depreciation - Building and Building
Improvements

115

4

114

Accumulated Impairment Losses

116

3

106

Land - At Fair Value

117

4

117

Building and Building Improvements - At Fair
Value

118

3

106

Foreclosed Properties

119

4

118

Intangible Assets

120

3

106

Deferred Tax Assets

121

4

120

Other Non-Current Assets

122

3

106

Miscellaneous Deposits

123

4

122

Deferred Input VAT

124

2

85

Retirement Pension Asset

125

3

124

Others

126

3

124

LIABILITIES

127

4

126

Current Liabilities

128

3

124

HMO Agreement Liabilities

129

3

124

HMO Agreement Reserves

130

3

124

Claims Reserve

131

3

124

Due and Unpaid (D&U) Claims

132

2

85

In Course of Settlement (ICOS)

133

2

85

Resisted Claims

134

2

85

Incurred but not Reported (IBNR)

135

3

134

Claims Handling Expense

136

3

134

Membership Fee Reserves

137

3

134

Other Reserves

138

3

134

Deficiency Reserves

139

0

-

Agreement Reserves

140

1

139

ASO Reserves

141

2

140

Administrative Service Only (ASO) Funds

142

3

141

Unearned Administrative Fee Reserves (UAFR)

143

4

142

Fund Withdrawals

144

5

143

Liabilities Due to Insurance and Providers

145

5

143

Commission Payable

146

5

143

Accounts Payable

147

5

143

SSS Premium Payable

148

5

143

SSS Loans Payable

149

4

142

Pag-Ibig Premiums Payable

150

4

142

Pag-Ibig Loans Payable

151

5

150

PhilHealth Premiums Payable

152

5

150

Other Accounts Payable

153

3

141

Due to Related Parties

154

4

153

Accrued Expenses

155

4

153

Accrued Utilities

156

4

153

Accrued Services

157

2

140

Accrued Interest

158

2

140

Notes Payable - Current Portion

159

2

140

Dividends Payable

160

3

159

Taxes Payable

161

3

159

Value-added Tax (VAT) Payable

162

3

159

Withholding Taxes Payable

163

3

159

Income Tax Payable

164

3

159

Other Current Liabilities

165

3

159

Non-current Liabilities

166

2

140

Notes Payable - Non-current Portion

167

2

140

Pension Benefit Obligation

168

3

167

Deferred Tax Liability

169

3

167

Other Non-current Liabilities

170

3

167

EQUITY

171

2

140

Share Capital

172

2

140

Preferred Share Capital

173

2

140

Common Share Capital

174

3

173

Subscribed Share Capital

175

3

173

Subscribed Preferred - Shares

176

3

173

Subscription Receivable - Preferred Shares

177

2

140

Subscribed Common Share Capital

178

1

139

Subscription Receivable - Common Shares

179

2

178

Share Dividend Distributable

180

2

178

Capital Paid In Excess of Par

181

2

178

Retained Earnings (Deficit)

182

2

178

Retained Earnings - Appropriated

183

0

-

Retained Earnings - Unappropriated

184

1

183

Reserves

185

2

186

Deposit for Future Subscription

186

2

186

Revaluation Reserves

187

1

183

Net Unrealized Gains (Losses) on AFS Investments

188

2

187

Cumulative Translation Adjustment

189

2

187

Treasury Shares

190

2

187



191

2

187



192

1

183

3 Share Dividend Distributable

193

1

183

4. Capital Paid in Excess of Par

194

1

183

4 Retained Earnings (Deficit)

195

2

194

4.1 Retained Earnings - Appropriated

196

2

194

4.2 Retained Earnings - Unappropriated

197

1

183

5. Reserves

198

2

197

5.1 Deposit for Future Subscription

199

2

197

5.2 Revaluation Reserves

200

2

197

5.3 Net Unrealized Gains (Losses) on AFS
Investments

201

2

197

5.4 Cumulative Translation Adjustments

202

1

183

6. Treasure Share





SCI Chart of Accounts

id

level\_id

parent\_id

account\_name

1

0

-

Comprehensive Income

2

1

1

Other Comprehensive Income

3

1

1

Net Income After Taxes

4

2

2

Unrealized Gains (Losses) on Investments

5

2

2

Unrealized Foreign Exchange on Gain (Loss)

6

3

3

Provision for Income Taxes

7

3

3

Net Income Before Taxes

8

4

7

Income (Loss) from Operations

9

4

7

Other Income (Expenses)

10

5

9

Dividend Income

11

5

9

Interest Income

12

5

9

Gain(Loss) on Sale of Property and Equipment

13

5

9

Gain(Loss) On Sale of Investments

14

5

9

Foreign Exchange Gain (Loss)

15

5

9

Other Revenues

16

5

9

Interest Expense

17

5

8

Operating Expenses

18

5

8

Gross Profit

19

6

17

Commision Expenses

20

6

17

Salaries And Wages

21

6

17

SSS Contributions

22

6

17

Philhealth Contributions

23

6

17

Pag-Ibig Contribution

24

6

17

Employees Compensation And Maternity
Contributions

25

6

17

Hospitalization Contribution

26

6

17

Medical Supplies

27

6

17

Employee’s Welfare

28

6

17

Employee Benefits

29

6

17

Post-Employment Benefit Cost

30

6

17

Professional and Technical Development

31

6

17

Representation and Entertainment

32

6

17

Transportation and Travel Expenses

33

6

17

Investment Management Fees

34

6

17

Director's Fees and Allowances

35

6

17

Corporate Secretary's Fees

36

6

17

Auditors' Fees

37

6

17

Actuarial Fees

38

6

17

Service Fees

39

6

17

Legal Fees

40

6

17

Association Dues

41

6

17

Light and Water

42

6

17

Communication and Postage

43

6

17

Printing, Stationery and Supplies

44

6

17

Books and Periodicals

45

6

17

Advertising and Promotions

46

6

17

Contributions and Donations

47

6

17

Rental Expense

48

6

17

Insurance Expenses

49

6

17

Taxes and Licenses

50

6

17

Bank Charges

51

6

17

Repairs and Maintenance - Materials

52

6

17

Repairs and Maintenance - Labor

53

6

17

Depreciation and Amortization

54

6

17

Share in Profit/Loss of Associates and Joint
Ventures

55

6

17

Provision for Impairment Losses

56

6

17

Miscellaneous Expenses

57

6

18

Healthcare Benefits and Claims

58

6

18

Revenues Earned

59

7

57

Medical Services

60

7

57

Hospitalization

61

7

57

Hospital Professional Fees

62

7

57

Other Benefits and Claims

63

7

58

Membership Fees

64

7

58

Enrolment Fees

65

7

58

Administrative Fees



1.  ### Quarterly Submission and Examination of Interim Financial Statements

![](up-ic-erd/image20.png)





SFP Chart of Accounts

id

level\_id

parent\_id

account\_name

1

0

-

ASSETS

2

1

1

Current Assets

3

2

2

Cash and Cash Equivalents

4

3

3

Cash on Hand

5

4

4

Undeposited Collections

6

4

4

Petty Cash Fund

7

4

4

Revolving Fund

8

4

4

Commission Fund

9

4

4

Other Funds

10

3

3

Cash in Banks

11

4

10

Current - Peso

12

4

10

Current - Foreign

13

4

10

Savings - Peso

14

4

10

Savings - Foreign

15

3

3

Cash Equivalents

16

3

10

Time Deposits

17

3

10

Money Market Instruments

18

3

10

Others

19

2

3

Short-term Cash lnvestments

20

2

2

Administrative Service Only (ASO) Cash Fund

21

2

2

Trade and Other Receivables

22

3

21

Trade Receivables

23

4

22

Membership Fee Receivable

24

4

22

Riders Fee Receivable

25

4

22

Deposit to Healthcare Providers

26

4

22

Due from ASO Accounts

27

4

22

Allowance for Bad Debts

28

3

21

Other Receivables

29

4

28

Notes Receivable

30

4

28

Interest Receivable

31

4

28

Advances to Officers and Employees

32

4

28

Due from Officers and Employees

33

4

28

Due from Related Parties

34

4

28

Others

35

4

28

Allowance for Bad Debts

36

2

2

Financial Assets at Fair Value Through Profit or
Loss

37

3

36

Securities Held for Trading

38

4

37

Trading Debt Securities - Government

39

4

37

Trading Debt Securities - Private

40

4

37

Trading Equity Securities

41

4

37

Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds

42

3

36

Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)

43

4

42

Debt Securities - Government

44

4

42

Debt Securities - Private

45

4

42

Equity Securities

46

4

42

Mutual Funds and Unit Investment Trusts

47

4

42

Real Estate Investment Trusts

48

4

42

Others

49

2

2

Held-to-Maturity (HTM) Investments - Current
Portion

50

3

49

HTM Debt Securities - Government - Current
Portion

51

4

50

Unamortized (Discount)/Premium

52

4

50

HTM Debt Securities - Private - Current Portion

53

4

50

Unamortized (Discount)/Premium

54

4

50

Allowance for Impairment Losses

55

3

49

Available-for-Sale (AFS) Financial Assets - Current
Portion

56

4

55

AFS Debt Securities - Government - Current
Portion

57

4

55

Unamortized (Discount)/Premium

58

4

55

AFS Debt Securities - Private - Current Portion

59

4

55

Unamortized (Discount)/Premium

60

4

55

AFS Equity Securities - Current Portion

61

4

55

Allowance for Impairment Losses

62

2

2

Mutual Funds and Unit Investment Trusts - Current
Portion

63

3

62

Real Estate Investment Trusts - Current Portion

64

4

64

Others - Current Portion

65

3

62

Prepayments

66

4

64

Supplies

67

3

62

Prepaid Commissions

68

2

2

Prepaid Rent

69

3

68

Prepaid Taxes

70

4

69

Others

71

3

68

Other Current Assets

72

4

71

Non-current Assets

73

3

68

Held-to-Maturity (HTM) Investments - Non-Current
Portion

74

3

68

HTM Debt Securities - Government - Non-Current
Portion

75

3

68

Unamortized (Discount)/Premium

76

3

68

HTM Debt Securities - Private - Non-Current
Portion

77

3

68

Unamortized (Discount)/Premium

78

2

2

Allowance for Impairment Losses

79

3

78

Available-for-Sale (AFS) Financial Assets - Non-Current
Portion

80

3

78

AFS Debt Securities - Government - Non-Current
Portion

81

3

78

Unamortized (Discount)/Premium

82

3

78

AFS Debt Securities - Private - Non-Current
Portion

83

3

78

Unamortized (Discount)/Premium

84

2

2

AFS Equity Securities - Non-Current Portion

85

1

1

Allowance for Impairment Losses

86

2

85

Mutual Funds and Unit Investment Trusts - Non-Current
Portion

87

3

86

Real Estate Investment Trusts - Non-Current
Portion

88

4

87

Others - Non-Current Portion

89

3

86

Investments in Subsidiaries, Associates and Joint
Ventures

90

4

89

Investment in Subsidiaries

91

3

86

Investment in Associates

92

2

85

Investment in Joint Ventures

93

3

92

Property and Equipment

94

4

93

Land - At Cost

95

3

92

Building and Building Improvements - At Cost

96

4

95

Accumulated Depreciation - Building and Building
Improvements

97

3

92

Leasehold Improvements - At Cost

98

3

92

Accumulated Depreciation - Leasehold
Improvements

99

3

92

IT Equipment - At Cost

100

3

92

Accumulated Depreciation - IT Equipment

101

3

92

Transportation Equipment - At Cost

102

2

85

Accumulated Depreciation - Transportation
Equipment

103

3

102

Office Furniture, Fixtures and Equipment - At
Cost

104

3

102

Accumulated Depreciation - Office Furniture, Fixtures
and Equipment

105

3

102

Medical Equipment

106

2

85

Accumulated Depreciation - Medical Equipment

107

3

106

Property and Equipment Under Finance Lease

108

3

106

Accumulated Depreciation - P&E under Finance
Lease

109

4

108

Revaluation Increment

110

3

106

Accumulated Depreciation - Revaluation Increment

111

4

110

Investment Property

112

3

106

Land - At Cost

113

4

112

Building and Building Improvements - At Cost

114

3

106

Accumulated Depreciation - Building and Building
Improvements

115

4

114

Accumulated Impairment Losses

116

3

106

Land - At Fair Value

117

4

117

Building and Building Improvements - At Fair
Value

118

3

106

Foreclosed Properties

119

4

118

Intangible Assets

120

3

106

Deferred Tax Assets

121

4

120

Other Non-Current Assets

122

3

106

Miscellaneous Deposits

123

4

122

Deferred Input VAT

124

2

85

Retirement Pension Asset

125

3

124

Others

126

3

124

LIABILITIES

127

4

126

Current Liabilities

128

3

124

HMO Agreement Liabilities

129

3

124

HMO Agreement Reserves

130

3

124

Claims Reserve

131

3

124

Due and Unpaid (D&U) Claims

132

2

85

In Course of Settlement (ICOS)

133

2

85

Resisted Claims

134

2

85

Incurred but not Reported (IBNR)

135

3

134

Claims Handling Expense

136

3

134

Membership Fee Reserves

137

3

134

Other Reserves

138

3

134

Deficiency Reserves

139

0

-

Agreement Reserves

140

1

139

ASO Reserves

141

2

140

Administrative Service Only (ASO) Funds

142

3

141

Unearned Administrative Fee Reserves (UAFR)

143

4

142

Fund Withdrawals

144

5

143

Liabilities Due to Insurance and Providers

145

5

143

Commission Payable

146

5

143

Accounts Payable

147

5

143

SSS Premium Payable

148

5

143

SSS Loans Payable

149

4

142

Pag-Ibig Premiums Payable

150

4

142

Pag-Ibig Loans Payable

151

5

150

PhilHealth Premiums Payable

152

5

150

Other Accounts Payable

153

3

141

Due to Related Parties

154

4

153

Accrued Expenses

155

4

153

Accrued Utilities

156

4

153

Accrued Services

157

2

140

Accrued Interest

158

2

140

Notes Payable - Current Portion

159

2

140

Dividends Payable

160

3

159

Taxes Payable

161

3

159

Value-added Tax (VAT) Payable

162

3

159

Withholding Taxes Payable

163

3

159

Income Tax Payable

164

3

159

Other Current Liabilities

165

3

159

Non-current Liabilities

166

2

140

Notes Payable - Non-current Portion

167

2

140

Pension Benefit Obligation

168

3

167

Deferred Tax Liability

169

3

167

Other Non-current Liabilities

170

3

167

EQUITY

171

2

140

Share Capital

172

2

140

Preferred Share Capital

173

2

140

Common Share Capital

174

3

173

Subscribed Share Capital

175

3

173

Subscribed Preferred - Shares

176

3

173

Subscription Receivable - Preferred Shares

177

2

140

Subscribed Common Share Capital

178

1

139

Subscription Receivable - Common Shares

179

2

178

Share Dividend Distributable

180

2

178

Capital Paid In Excess of Par

181

2

178

Retained Earnings (Deficit)

182

2

178

Retained Earnings - Appropriated

183

0

-

Retained Earnings - Unappropriated

184

1

183

Reserves

185

2

186

Deposit for Future Subscription

186

2

186

Revaluation Reserves

187

1

183

Net Unrealized Gains (Losses) on AFS Investments

188

2

187

Cumulative Translation Adjustment

189

2

187

Treasury Shares

190

2

187



191

2

187



192

1

183

3 Share Dividend Distributable

193

1

183

4. Capital Paid in Excess of Par

194

1

183

4 Retained Earnings (Deficit)

195

2

194

4.1 Retained Earnings - Appropriated

196

2

194

4.2 Retained Earnings - Unappropriated

197

1

183

5. Reserves

198

2

197

5.1 Deposit for Future Subscription

199

2

197

5.2 Revaluation Reserves

200

2

197

5.3 Net Unrealized Gains (Losses) on AFS
Investments

201

2

197

5.4 Cumulative Translation Adjustments

202

1

183

6. Treasure Share





SCI Chart of Accounts

id

level\_id

parent\_id

account\_name

1

0

-

Comprehensive Income

2

1

1

Other Comprehensive Income

3

1

1

Net Income After Taxes

4

2

2

Unrealized Gains (Losses) on Investments

5

2

2

Unrealized Foreign Exchange on Gain (Loss)

6

3

3

Provision for Income Taxes

7

3

3

Net Income Before Taxes

8

4

7

Income (Loss) from Operations

9

4

7

Other Income (Expenses)

10

5

9

Dividend Income

11

5

9

Interest Income

12

5

9

Gain(Loss) on Sale of Property and Equipment

13

5

9

Gain(Loss) On Sale of Investments

14

5

9

Foreign Exchange Gain (Loss)

15

5

9

Other Revenues

16

5

9

Interest Expense

17

5

8

Operating Expenses

18

5

8

Gross Profit

19

6

17

Commision Expenses

20

6

17

Salaries And Wages

21

6

17

SSS Contributions

22

6

17

Philhealth Contributions

23

6

17

Pag-Ibig Contribution

24

6

17

Employees Compensation And Maternity
Contributions

25

6

17

Hospitalization Contribution

26

6

17

Medical Supplies

27

6

17

Employee’s Welfare

28

6

17

Employee Benefits

29

6

17

Post-Employment Benefit Cost

30

6

17

Professional and Technical Development

31

6

17

Representation and Entertainment

32

6

17

Transportation and Travel Expenses

33

6

17

Investment Management Fees

34

6

17

Director's Fees and Allowances

35

6

17

Corporate Secretary's Fees

36

6

17

Auditors' Fees

37

6

17

Actuarial Fees

38

6

17

Service Fees

39

6

17

Legal Fees

40

6

17

Association Dues

41

6

17

Light and Water

42

6

17

Communication and Postage

43

6

17

Printing, Stationery and Supplies

44

6

17

Books and Periodicals

45

6

17

Advertising and Promotions

46

6

17

Contributions and Donations

47

6

17

Rental Expense

48

6

17

Insurance Expenses

49

6

17

Taxes and Licenses

50

6

17

Bank Charges

51

6

17

Repairs and Maintenance - Materials

52

6

17

Repairs and Maintenance - Labor

53

6

17

Depreciation and Amortization

54

6

17

Share in Profit/Loss of Associates and Joint
Ventures

55

6

17

Provision for Impairment Losses

56

6

17

Miscellaneous Expenses

57

6

18

Healthcare Benefits and Claims

58

6

18

Revenues Earned

59

7

57

Medical Services

60

7

57

Hospitalization

61

7

57

Hospital Professional Fees

62

7

57

Other Benefits and Claims

63

7

58

Membership Fees

64

7

58

Enrolment Fees

65

7

58

Administrative Fees



1.  Life/ MBA/ Trust Division
    --------------------------------------------------



1.  ### Annual Submission and Examination of Financial Standing of Life Companies with Separate Presentation for Composite Insurance Companies



![](up-ic-erd/image2.png)

1.  ### Annual Submission and Examination of Mutual Benefit Association (MBA)



![](up-ic-erd/image2.png)                



1.  ### Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report



                Quarterly - RBC, AVR

                

![](up-ic-erd/image37.png)





Quarterly - SFP, SCI



![](up-ic-erd/image22.png)









SFP Chart of Accounts

id

level\_id

parent\_id

account\_name

1

0

-

Total Assets

2

0

1

Cash on Hand

3

0

1

Cash in Banks

4

0

1

Time Deposits

5

0

1

Premiums Due and Uncollected

6

0

1

Due from Ceding Companies, net

7

0

1

Funds Held By Ceding Companies, net

8

0

1

Amounts Recoverable from Reinsurers, net

9

0

1

Financial Assets at Fair Value Through Profit or
Loss

10

0

1

Held-to-Maturity (HTM) Investments

11

0

1

Loans and Receivables

12

0

1

Available-for-Sale (AFS) Financial Assets

13

0

1

Investments Income Due and Accrued

14

0

1

Accounts Receivable

15

0

1

Investments in Subsidiaries, Associates and Joint
Ventures

16

0

1

Segregated Fund Assets

17

0

1

Property and Equipment

18

0

1

Investment Property

19

0

1

Non-current Assets Held for Sale

20

0

1

Receivable from Life Insurance Pools

21

0

1

Subscription Receivable

22

0

1

Security Fund Contribution

23

0

1

Pension Asset

24

0

1

Derivative Assets Held for Hedging

25

0

1

Other Assets 

26

0

2

Undeposited Collections

27

0

2

Petty Cash Fund

28

0

2

Commission Fund

29

0

2

Policy Loan Fund

30

0

2

Documentary Stamps Fund

31

0

2

Other Funds

32

1

3

Current - Peso

33

1

3

Current - Foreign

34

1

3

Savings - Peso

35

1

3

Savings - Foreign

36

2

4

Peso Currency

37

2

4

Foreign Currency

38

4

6

Premiums Due from Ceding Companies - Treaty

39

4

6

Premiums Due from Ceding Companies - Facultative

40

4

6

Allowance for Impairment Losses

41

5

7

Funds Held By Ceding Companies

42

5

7

Allowance for Impairment Losses

43

6

8

Reinsurance Recoverable on Paid Losses - Treaty

44

6

8

Reinsurance Recoverable on Paid Losses -
Facultative

45

6

8

Reinsurance Recoverable on Unpaid Losses -
Treaty

46

6

8

Reinsurance Recoverable on Unpaid Losses -
Facultative

47

6

8

Allowance for Impairment Losses

48

7

9

Securities Held for Trading

49

7

9

Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)

50

7

9

Derivative Assets

51

8

10

HTM Debt Securities - Government

52

8

10

HTM Debt Securities - Private

53

9

11

Real Estate Mortgage Loans

54

9

11

Collateral Loans

55

9

11

Guaranteed Loans

56

9

11

Chattel Mortgage Loans

57

9

11

Policy Loans

58

9

11

Unearned interest Income

59

9

11

Notes Receivable

60

9

11

Housing Loans

61

9

11

Car Loans

62

9

11

Low Cost Housing

63

9

11

Purchase Money Mortgages

64

9

11

Sales Contract Receivables

65

9

11

Unquoted Debt Securities

66

9

11

Salary Loans

67

9

11

Other Loans Receivables

68

9

11

Allowance for Impairment Losses

69

10

12

AFS Debt Securities - Government

70

10

12

AFS Debt Securities - Private

71

10

12

AFS Equity Securities

72

10

12

Allowance for Impairment Losses

73

10

12

Mutual Funds and Unit Investment Trusts

74

10

12

Real Estate Investment Trusts

75

10

12

Accrued Dividends Receivable

76

10

12

Other Funds

77

11

13

Accrued Interest Income - Cash In Banks

78

11

13

Accrued Interest Income - Time Deposits

79

11

13

Accrued Interest Income - Financial Assets at
FVPL

80

11

13

Accrued Interest Income - AFS Financial Assets

81

11

13

Accrued Interest Income - HTM Investments

82

11

13

Accrued Interest Income - Loans and Receivables

83

12

14

Advances to Agents (Agents Accounts) / Employees

84

12

14

Operating Lease Receivables

85

12

14

Allowance for Impairment Losses

86

15

17

Land - At Cost

87

15

17

Building and Building Improvements - At Cost

88

15

17

Leasehold Improvements - At Cost

89

15

17

IT Equipment - At Cost

90

15

17

Transportation Equipment - At Cost

91

15

17

Office Furniture, Fixtures and Equipment - At
Cost

92

15

17

Property and Equipment Under Finance Lease

93

15

17

Revaluation Increment

94

15

17

Accumulated Impairment Losses

95

16

18

Land - At Cost

96

16

18

Building and Building Improvements - At Cost

97

16

18

Accumulated Impairment Losses

98

16

18

Land - At Fair Value

99

16

18

Building and Building Improvements - At Fair
Value

100

16

18

Foreclosed Properties

101

22

24

Fair Value Hedge

102

22

24

Cash Flow Hedge

103

22

24

Hedges of a Net Investment in Foreign Operation

104

22

48

Trading Debt Securities - Government

105

22

48

Trading Debt Securities - Private

106

22

48

Trading Equity Securities

107

22

48

Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds

108

23

49

Debt Securities - Government

109

23

49

Debt Securities - Private

110

23

49

Equity Securities

111

23

49

Mutual Funds and Unit Investment Trusts

112

23

49

Real Estate Investment Trusts

113

23

49

Other Funds

114

55

81

Real Estate Mortgage Loans

115

55

81

Collateral Loans

116

55

81

Guaranteed Loans

117

55

81

Chattel Mortgage Loans

118

55

81

Policy Loans

119

55

81

Notes Receivable

120

55

81

Housing Loans

121

55

81

Car Loans

122

55

81

Low Cost Housing Loans

123

55

81

Purchase Money Mortgages

124

55

81

Sales Contract Receivable

125

55

81

Unquoted Debt Securities

126

55

81

Salary Loans

127

55

81

Others

128

56

75

FVPL Equity Securities

129

56

75

DVPL Equity Securities

130

56

75

AFS Equity Securities

131

67

96

Accumulate Depreciation - Building and Building
Improvements

132

68

97

Accumulated Depreciation - Leasehold
Improvements

133

69

98

Accumulated Depreciation - IT Equipment

134

70

99

Accumulated Depreciation - Transportation
Equipment

135

71

100

Accumulated Depreciation – Office Furniture, Fixtures
and Equipment

136

72

101

Accumulated Depreciation

137

73

102

Accumulated Depreciation - Revaluation Increment

138

76

103

Accumulate Depreciation - Building and Building
Improvements

139

1

-

Total Liabilities

140

1

139

Aggregate Reserve for Life Policies

141

1

139

Aggregate Reserve for Accident and Health
Policies

142

1

139

Reserve for Supplementary Contracts Without Life
Contingencies

143

1

139

Policy and Contract Claims Payable

144

1

139

Due to Reinsurers

145

1

139

Funds Held for Reinsurers

146

1

139

Life Insurance Deposit/Applicant's Deposit

147

1

139

Segregated Fund Liabilities

148

1

139

Premium Deposit Fund

149

1

139

Remitances Unapplied Deposit

150

1

139

Premium Received in Advance

151

1

139

Policyholders' Dividends Due and Unpaid

152

1

139

Policyholders' Dividends Accumulations/ Dividends Held
on Deposit

153

1

139

Maturities and Surrenders Payables

154

1

139

Commissions Payable

155

1

139

Return Premiums Payable

156

1

139

Taxes Payable

157

1

139

Accounts Payable

158

1

139

Deposit for Real Estate Under Contract to Sell

159

1

139

Dividends Payable

160

1

139

Liability on Life insurance Pool Business

161

1

139

Financial Liabilities at Fair Value Through Profit or
Loss

162

1

139

Notes Payable

163

1

139

Finance Lease Liability

164

1

139

Pension Obligation

165

1

139

Accrual for Long-Term Employee Benefits

166

1

139

Deferred Tax Liability 

167

1

139

Provisions

168

1

139

Cash-Settled Share-Based Payment

169

1

139

Accrued Expenses

170

1

139

Other Liabilities

171

1

139

Derivative Liabilities Held for Hedging

172

27

143

Claims Due and Unpaid

173

27

143

Outstanding Claims Reserve

174

27

143

Claims Resisted

175

27

143

Claims Incurred but not yet Reported

176

28

144

Premiums Due to Reinsurers - Treaty

177

28

144

Premiums Due to Reinsurers - Facultative

178

29

145

Premiums Due to Reinsurers - Facultative

179

40

156

Experience refunds

180

40

156

Premiums Tax Payable

181

40

156

Documentary Stamps Tax Payable

182

40

156

Value-added Tax (VAT) Payable

183

40

156

Income Tax Payable

184

40

156

Withholding Tax Payable

185

40

156

Fire Service Tax Payable

186

40

156

Other Taxes and Licenses Payable

187

41

157

SSS Premiums Payable

188

41

157

SSS Loans Payable

189

41

157

Pag-ibig Premiums Payable

190

41

157

Pag-ibig Loans Payable

191

41

157

Operating Lease Liability

192

41

157

Other Accounts Payable

193

45

161

Financial Liabilities Held for Trading

194

45

161

Financial Liabilities Designated at Fair Value Through
Profit or Loss

195

45

161

Derivative Liabilities

196

53

169

Accrued Utilities

197

53

169

Accrued Services

198

53

169

Accrual for Unused Compensated Absences

199

54

170

Deferred Income

200

54

170

Agency Retirement Plan

201

54

170

Agency Group Hospitalization Plan

202

54

170

Agency Group Term Plan

203

54

170

Agency Cash Bond Deposit

204

55

171

Fair Value Hedge

205

55

171

Cash Flow Hedge

206

55

171

Hedges of a Net Investment in Foreign Operation

207

2

-

Total Networth

208

2

207

Capital Stock

209

2

207

Statutory Deposit

210

2

207

Capital Stock Subscribed

211

2

207

Contributed Surplus

212

2

207

Contingency Surplus / Home Office Inward
Remittances

213

2

207

Capital Paid In Excess of Par

214

2

207

Retained Earnings / Home Office Account

215

2

207

Cost of Share-Based Payment

216

2

207

Reserve Accounts

217

2

207

Reserve for Appraisal Increment - Property and
Equipment

218

2

207

Remeasurement Gains (Losses) on Retirement Pension
Asset (Obligation)

219

2

207

Treasury Stock

220

56

208

Preferred Stock

221

56

208

Common Stock

222

62

214

Retained Earnings - Appropriated for Negative
Reserve

223

62

214

Retained Earnings - Appropriated Others

224

62

214

Retained Earnings - Unappropriated

225

62

214

Retained Earnings - Transition Adjustments

226

64

216

Reserve for AFS Securities

227

64

216

Reserve for Cash Flow Hedge

228

64

216

Reserve for Hedge of a Net Investment in Foreign
Operation

229

64

216

Cumulative Foreign Currency Translation

230

64

216

Remeasurement on Life Insurance Reserves

231

64

216

Reserve for Investment in Associates













                Certificate Request



                ![](up-ic-erd/image29.png)

1.  Non-Life Division
    ------------------------------------------



1.  ### Annual Submission and Examination of Financial Standing of Non-Life Companies

### ![](up-ic-erd/image10.png)

1.  ### Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report



![](up-ic-erd/image39.png)

1.  ### Issuance of Certification on Financial Condition of Insurance Companies



![](up-ic-erd/image21.png)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



1.  Pre-Need Division
    -------------------------------------------



1.  ### Annual Submission and Examination of Pre-Need Companies

![](up-ic-erd/image8.png)





Requirement Checklist

id

requirement

1

Audited Financial Statements (AFS) duly stamped by the
Bureau of Internal Revenue (BIR)

2

Reconciliation Statement of The AFS versus the ATB
figures;

3

Detailed Reconciliation Statement of Trust Fund
Balances per Trustee Bank(s) versus AFS/AS;

4

Annual Pre-need Reserve Valuation Report

5

List of current members of board of directors and
officers, their respective addresses, position and committee
membership

6

Sworn Statement from the responsible officers of the
company stating that: "Any deficiency in Trust Funds has been duly
addressed, attaching the necessary documents as proofs thereof"

7

Sworn Statement of the company's insurer certifying the
coverage on the life insurance policies or guarantees on premium
payments assumed by the company, indicating the extent, term and
duration of such coverage/guarantees

8

Documents supporting the accounts held as corporate
assets

9

Annual Statements Showing the financial condition of
the pre-need company and all its exhibits and schedules

10

Exhibit 5 - Summary of Monthly Withdrawals from the
Trust Fund

11

Annual Statements of Trust Funds per type of plan and
all its exhibits and schedules

12

Complete details of Exhibits 6, Exhibit 7 and Exhibit
8

13

Adjusted Trial Balance (ATB)

14

Reconciliation Statement of the AFS versus the ATB
figures

15

Detailed Reconciliation Statement of Trust Fund
Balances per Trustee Bank(s) versus AFS/AS

16

PDF copy Audited Financial Statements (AFS) duly
Stamped by the bureau of Internal Revenue (BIR)







Additional Requirements

requirement\_id

addl\_requirement

4

Certification by the actuary and/or any accountable
officer of the company on the actuary and completeness of the in-force
files used in valuation of reserves

4

Certification on the prudent adequacy of the Pre-need
reserve that it shall provide at least the guaranteed contractual
benefits under each pre-need contract of the company

8

Certification of Custodian for Petty Cash Fund,
Revolving Fund

8

Bank reconciliation statements as of 31 December 20\_
and 31 January 20\_ using adjusted balance method, together with the
bank statements, passbooks and certificates of all current, saving and
time deposit account respectively

8

Official receipts, bank validated deposit slips and
bank statements to support year-end deposit in transit

8

Confirmation of Sales of investment in Bonds and
Treasury Bills (Government Securities) together with statement of
Securities Account of Btr-ROSS as 31 December 20\_

8

Statement of Holdings as of year-and from the company's
custodian bank for dollar denominated bonds

8

Statement of Accounts as of year-end from Philippine
Depository and Trust Corporation (PDTC) for scriptless stock
investment

8

Statement of Net Asset Value (NAV/pu) of Unit
Investment Trust Fund (UITF), Mutual Funds and related
investments

8

Report from a licensed real estate appraiser accredited
by the Philippine Association of Real Estate Appraisers to support any
increase or decrease in fair value or real estate properties

8

Document supporting investments in commercial
papers

8

Document supporting mortgage loans and other loans
accounts

8

BIR tax returns/payments for the year on Income Tax and
Value added Tax (VAT)

8

Summary of taxes paid including Documentary Stamps Tax
(DST), withholding taxes and all other Taxes

8

Other documents to support all other corporate assets
and liability accounts

9

Table of Contents showing the summary of Investment for
the year ended December 31, 20\_ and Certification



1.  ### Quarterly Submission and Monitoring of Interim Financial Statements

![](up-ic-erd/image28.png)





Report Types

id

report\_type

1

Interim Financial Statements

2

Sales Collections and Deposit to Trust Fund





Trust Fund Types

id

type

1

Life

2

Pension

3

Education

4

Main





Account Types

id

account\_type

1

Assets

2

Liabilities

3

Fund Equity







Accounts

id

account\_type

account\_name

1

1

Government Securities

2

1

Cash in Savings

3

1

Mutual Funds/UITF

4

1

Short Term Investments

5

1

Corporate Bonds

6

1

Mortgage Loans

7

1

Planholders' Loans

8

1

Stocks

9

1

Real Estate

10

1

Other Investments

11

1

Other Assets

12

2

Accrued Trust Fees

13

2

Accrued Taxes

14

2

Other Liabilities

15

3

Fund Balance

16

3

Additional Contribution

17

3

Withdrawals

18

3

Adjustments

19

3

Fund Balance

20

3

Retained Earnings

21

3

Net Income

22

3

Net Unrealized Income





Income Statement Attributes

id

attribute

1

Premiums

2

Increase/Decrease in Pre-Need Reserves

3

Increase/Decrease in Premium Reserves

4

Increase/Decrease in Other Pre-Need Reserves

5

Other Direct Income

6

Total Direct Income

7

Benefit Payments

8

Commissions Expenses

9

Other Direct Expenses

10

Total Direct Expenses

11

Gain/Loss

12

Bonds

13

Stocks

14

Real Estate

15

Purchase Money Mortgage

16

Mortgage Loans

17

Collateral Loans

18

Guaranteed Loans

19

Other Loans

20

Short-term Investments

21

Other Investments

22

Bank Deposits

23

Total Gain/Loss and Interest Earned

24

Depreciation

25

Investment Expenses

26

Taxes on Real Estate

27

Documentary Stamp Tax

28

Corporate Residence Certificate

29

Assessment, License and Fees

30

VAT & Fringe Benefit Tax

31

Final Taxes

32

Salaries and Wages

33

Allowance to Officers

34

Allowance to Employees

35

Pension, Retirement and Other similar Benefits

36

Rent, Light and Water

37

Other General Expenses

38

Net Income/Loss before Income Tax

39

Income Tax

40

Net Income/Loss for the Quarter





1.  ### Monthly Submission and Monitoring of Sales Reports, Collection Reports and Deposit to Trust Fund Reports

![](up-ic-erd/image24.png)





Plan Types

id

plan\_type

1

Life/Memorial Plan

2

Pension Plan

3

Education Plan





Categories

id

category

1

Fully Paid

2

Installment











Technical Services Group
=======================================================

1.  Statistics and Research Division
    ---------------------------------------------------------



1.  ### Preparation of Annual Data - Annual Report (selected sample tables)

                        Refer to the ERD
of all the Divisions of Financial Examination Group

1.  ### Preparation of Annual Data - Key Data

Refer to the ERD of all the Divisions of Financial
Examination Group

1.  ### Preparation of Annual Data - ASEAN Report

        Refer to the ERD of all the
Divisions of Financial Examination Group

1.  ### Preparation of Annual Data - Ranking

Refer to the ERD of all the Divisions of Financial
Examination Group

1.  ### Preparation of Annual Data - Assets Information Management Report (AIMP)

![](up-ic-erd/image17.png)





requirement\_list (look up table)

id

information\_name

information\_specific

information\_subtype

0

Basic Information

Certificate of Authenticity

none

1

Basic Information

Date of original Issue

none

2

Basic Information

Date of latest renewal

none

3

Basic Information

Taxpayer Name

none

4

Basic Information

Company/Trade Name

none

5

List of affiliated companies

Name of affiliated company

none

6

List of affiliated companies

TIN

none

7

List of affiliated companies

Address

none

8

Annual Statement for the Year

Total Assets

none

9

Annual Statement for the Year

Total Liabilities

none

10

Annual Statement for the Year

Total Networth

none

11

Amount of Net Increase/ Decrease in Reserves on Direct
Business

none

none

12

Other Information

Total number of policies issued

none

13

Other Information

Total amount of policy coverage

none

14

Other Information

Total premiums from direct business

none

15

Other Information

Net Investment income

none

16

Other Information

Total Interest income earned

none

17

Other Information

Total Dividends Earned

none

18

Other Information

Total Real Estate Income Earned

none

19

Other Information

Gains and Losses on Investments

none

20

Other Information

Total amount of Policy and Contract Claims

none

21

Basic Information

Certificate of Authenticity

none

22

Basic Information

Date of original Issue

none

23

Basic Information

Date of latest renewal

none

24

Basic Information

Taxpayer Name

none

25

List of affiliated companies

Name of affiliated company

none

26

List of affiliated companies

TIN

none

27

List of affiliated companies

Address

none

28

Annual Statement for the Year

Total Assets

none

29

Annual Statement for the Year

Total Liabilities

none

30

Annual Statement for the Year

Total Networth

none

31

Amount of Net Increase/ Decrease in Reserves on Direct
Business

none

none

32

Other Information

Total number of policies issued

Health and Accident

33

Other Information

Total number of policies issued

TPL/CLB on automobiles

34

Other Information

Total number of policies issued

Marine

35

Other Information

Total number of policies issued

Others

36

Other Information

Total premiums from direct business

Health and Accident

37

Other Information

Total premiums from direct business

TPL/CLB on automobiles

38

Other Information

Total premiums from direct business

Marine

39

Other Information

Total premiums from direct business

Others

40

Other Information

Net Investment income

none

41

Other Information

Total Interest income earned

none

42

Other Information

Total Dividends Earned

none

43

Other Information

Total Real Estate Income Earned

none

44

Other Information

Gains and Losses on Investments

none

45

Other Information

Total amount of Policy and Contract Claims

none

46

Other information that may be needed to satisfy the
objectives of AIMP

none

none





insurance\_sub\_sub\_type\_table (look up table)

id

insurance\_sub\_type\_id

sub\_sub\_type

0

Ordinary

Whole Life

1

Ordinary

Endowment

2

Ordinary

Term

3

Group

Permanent

4

Group

Term

5

Industrial

Whole Life

6

Industrial

Endowment

7

Industrial

Term

8

Health & Accident

Whole Life

9

Health & Accident

Endowment

10

Health & Accident

Term

11

None

Health & Accident

12

None

Variable Life

13

None

Micro Insurance

14

None

Migrant Workers



1.  ### Quarterly Submission of Compliance to Various Reportorial Requirements - Life, Non-Life, MBA



1.  Life

![](up-ic-erd/image19.png)





Life Financial Accounts Table

id

account name

parent id

1

Total Assets

-

2

Cash & Invested Assets

1

3

Net Life Insurance Premiums & Annuity Considerations
Due & Uncollected

1

4

Reinsurance Accounts Receivable

1

5

Variable Life - Separate Account Assets

1

6

Total Liabilities

-

7

Legal Policy Reserves

6

8

Policy & Contract Claims

6

9

Premium Deposits Fund

6

10

Reinsurance Accounts Payable

6

11

Variable Life Liabilities

6

12

Taxes Payable

6

13

Other Liabilities

6

14

Total Networth

-

15

Paid-Up Capital/ Statutory Deposit

14

16

Capital Paid in Excess of Par Value

14

17

Seed Capital on Variable Life

14

18

Contributed Surplus/ Home Office/ Inward
Remittances

14

19

Deposit for Future Subscription

14

20

Contingency Surplus

14

21

Investment Fluctuation Reserves

14

22

Unassigned/ Retained Earnings/ Home Office
Account

14





Life Investments Accounts Table

id

account name

parent id

1

Long Term Investments

-

2

Government Bonds

1

3

Corporate Bonds

1

4

Short-Term Investments

-

5

Government (Treasury Bills)

4

6

Corporate Investments

4

7

Stocks

-

8

Real Estate

-

9

Mortgage Loans

-

10

Guaranteed Loans

-

11

Other Loans

-

12

Mutual Funds

-

13

Unit Investment Trust Funds

-

14

Real Estate Investment Trusts

-

15

Time Deposits/Fixed Deposits

-

16

Other Investments

-

17

Proprietary Shares

16

18

Money Market Placement

16

19

etc\*

16

20

Others

-

21

Exchange Traded Fund

20

22

Securities Borrowing & Lending

20

23

etc\*

20





insurance\_type\_table

id

insurance type

insurance\_sub\_type

sub\_sub\_type

0

Life insurance

Ordinary Insurance

Whole Life

1

Life insurance

Ordinary Insurance

Endowment

2

Life insurance

Ordinary Insurance

Term

3

Life insurance

Group & Industrial

Permanent

4

Life insurance

Group & Industrial

Term

5

Life insurance

Accident

Individual

6

Life insurance

Accident

Term

7

Life insurance

Health

Individual

8

Life insurance

Health

Term

9

Microinsurance

none

none

10

Migrant Worker

none

none





business\_type\_table

id

business\_type

business\_sub\_type

0

beginning\_balance

none

1

new\_business

Issued

2

new\_business

Revived

3

new\_business

Increased

4

new\_business

Others

5

insurance\_terminated

none

6

in\_force\_at\_end\_quarter

none





business\_line\_table

id

type

subtype

line

0

New Business

First Year

Premiums and considerations, direct business

1

New Business

First Year

Reinsurance premiums assumed

2

New Business

First Year

Reinsurance premiums ceded

3

New Business

Single

Premiums and considerations, direct business

4

New Business

Single

Reinsurance premiums assumed

5

New Business

Single

Reinsurance premiums ceded

6

Renewal

none

Premiums and considerations, direct business

7

Renewal

none

Reinsurance premiums assumed

8

Renewal

none

Reinsurance premiums ceded





seperate accounts table

id

account name

parent id

1

Total Assets

-

2

A. Assets at Market Value

1

3

1. Bonds

2

4

2. Stocks

2

5

3. Fixed Deposit

2

6

4. Others

2

7

B. Net Investment Income due and accrued

1

8

C. Others

1

9

D. Debtors/ Accounts Receivables\*

1

10

Total Liabilities and Networth

-

11

Variable Life Liabilities

10

12

General Expenses Due and Accrued

10

13

Other Liabilities

10

14

Seed Capital on Variable Life

10





statement of change in assets accounts table

id

account name

parent id

1

Net Assets, beginning

-

2

Total Additions

-

3

Deposits, net of Withdrawals

2

4

Gross Investment Income

2

5

Interest on Bonds

2

6

Dividend Income

2

7

Interest on Deposits

2

8

Interest on Loans

2

9

Other Income

2

10

Total Deductions

-

11

Investment Expenses

10

12

Investment Management fees

10

13

Taxes

10

14

Other Expenses

10

15

Net Assets, end of the quarter

-



1.  Non-Life

![](up-ic-erd/image34.png)





Non Life Financial Accounts Table

id

account name

parent id

1

Total Assets

-

2

Cash & Invested Assets

1

3

Premiums Receivable

1

4

Reinsurance Accounts Receivable

1

5

Other Assets

1

6

Total Liabilities

-

7

Reserve for Unearned Premiums

6

8

Losses & Claims Payable

6

9

Catastrophe Loss Reserve

6

10

Reinsurance Accounts Payable

6

11

Taxes Payable

6

12

Other Liabilities

6

13

Total Networth

-

14

Paid-Up Capital/ Statutory Deposit

13

15

Capital Paid in Excess of Par Value

13

16

Contributed Surplus/ Home Office/ Inward
Remittances

13

17

Deposit for Future Subscription

13

18

Contingency Surplus

13

19

Investment Fluctuation Reserves

13

20

Other Assigned

13

21

Unassigned/ Retained Earnings/ Home Office
Account

13





Non Life Investments Accounts Table

id

account name

parent id

1

Bonds

-

2

Government Bonds

1

3

Corporate Bonds

1

4

Short-Term Investments

-

5

Government (Treasury Bills)

4

6

Corporate Investments

4

7

Stocks

-

8

Real Estate

-

9

Purchase Money Mortgages

-

10

Mortgage Loans on Real Estate

-

11

Collateral Loans

-

12

Guaranteed Loans

-

13

Other Loans

-

14

Mutual Funds

-

15

Unit Investment Trust Funds

-

16

Real Estate Investment Trusts

-

17

Time Deposits/Fixed Deposits

-

18

Other Investments

-

19

Proprietary Shares

18

20

Money Market Placement

18

21

etc\*

18

22

Others

-

23

Exchange Traded Fund

22

24

Securities Borrowing & Lending

22

25

etc\*

22





business\_type\_table

id

business\_type

business\_sub\_type

0

Fire

Regular

1

Fire

Microinsurance

2

Marine

None

3

Aviation

None

4

Motorcar

CMVL

5

Motorcar

Non-CMVL

6

Health

Regular

7

Health

Microinsurance

8

Health

Migrant Workers

9

Accident

Regular

10

Accident

Microinsurance

11

Accident

Migrant Workers

12

Engineering

None

13

Other Casualty

Regular

14

Other Casualty

Microinsurance

15

Other Casualty

Migrant Workers

16

Suretyship

None



1.  MBA

![](up-ic-erd/image27.png)





MBA Financial Accounts Table

id

account name

parent id

1

Total Assets

-

2

Cash and invested assets

1

3

Members' Fees & Dues Receivable

1

4

Members' Contributions Due and Uncollected

1

5

Net Premiums Due and Uncollected

1

6

Unremitted Members' Contributions, Dues and Fees

1

7

Unremitted Premiums

1

8

Amounts Recoverable from Reinsurers

1

9

Other Assets

1

10

Total Liabilities

-

11

Liability on Individual Equity Value

10

12

Basic Contingent Benefit Reserve

10

13

Optional Benefit Reserve

10

14

Claims Payable on Basic Contingent Benefit

10

15

Claims Payable on Optional Benefits

10

16

Amounts Due to Reinsurers

10

17

Other Liabilites

10

18

Total Fund Balance

-

19

Assigned Fund Balance

18

20

Funds Assigned for Guarranty Fund

19

21

Funds Assigned for Members' Benefits

19

22

Funds Assigned for Community Development

19

23

Others

19

24

Revaluation/ Fluctuation Reserve

18

25

Free and Unassigned Fund Balance

18





MBA Investments Accounts Table

id

account name

parent id

1

Long Term Investments

-

2

a. Government Bonds

1

3

b. Corporate Bonds

1

4

Short Term Investments

-

5

a. Government (Treasury Bills)

4

6

b. Corporate Investments

4

7

Stocks

-

8

Investment in Properties

-

9

Loans

-

10

a. Membership Certificate Loans

9

11

b. Policy Loans

9

12

c. Other Loans Receivable

9

13

Time Deposits/ Fixed Deposits

-

14

Other Investments

-

15

a. Proprietary Shares

14

16

b. Money Market Placements

14

17

c. Others

14





business\_type\_table

id

business\_type

0

micro business

1

non micro business





basic\_sub\_type\_table

id

type

sub\_type

0

Beginning Balance

None

1

New Business

New Issues

2

New Business

Reinstated

3

New Business

Others

4

Terminations

Deaths

5

Terminations

Surrenders

6

Terminations

Lapsed

7

Terminations

Matured

8

Terminations

Others





product\_type

id

product\_type

0

micro product

1

non micro product





basic\_member\_benefit\_type\_table

id

type

0

Member

1

Dependents



1.  ### Report on Compulsory Insurance Coverage for Agency-Hired Migrant Workers

![](up-ic-erd/image30.png)





type\_of\_insurance\_table

id

type

subtype

0

Life

Land Based

1

Life

Personal Accident

2

Life

Medical Reimbursement

3

Life

Others

4

Sea Based

Natural Death

5

Sea Based

Personal Accident

6

Sea Based

Medical Reimbursement

7

Sea Based

Others



1.  Rating Division
    ----------------------------------------



1.  ### Approval of Premium Rates on Fire/ Motor Car Insurance and Surety

![](up-ic-erd/image1.png)

1.  ### Submission and Monitoring of Data from Compulsory Third Party Liability (CTPL)

![](up-ic-erd/image36.png)





vehicle types ctpl

id

vehicle type

1

Private Cars

2

Light Medium Trucks

3

Heavy Trucks

4

AC Tourits Cars

5

Taxi, PUJ and Mini Bus

6

PUB and Tourist Bus

7

Motorcycles/Tricycles/Trailers

8

Private Cars\*

9

Light Medium Trucks\*

10

Heavy Trucks\*

11

AC Tourits Cars\*

12

Taxi, PUJ and Mini Bus\*

13

PUB and Tourist Bus\*

14

Motorcycles/Tricycles/Trailers\*



1.  ### Submission and Monitoring of Data from Passenger Personal Accident Insurance (PPAI)

![](up-ic-erd/image38.png)





vehicle types ppai

id

vehicle type

1

AUV

2

BUS

3

JEEP

4

SEDAN

5

TRUCK



1.  ### Off-site Examination of Non-Life Insurance Companies



1.  #### Motor Car Total Loss

![](up-ic-erd/image25.png)

1.  #### Bonds

![](up-ic-erd/image5.png)





bond class

id

class

1

1

2

2

3

3

4

4

5

5





rate of bonds

id

class

max

base\_rate

rate\_step

base\_over

penalty\_rate

1

1

15000

0.48

0.048

0

0

2

2

50000

0.96

0.048

312

0.288

3

3

100000

1.92

0.048

1104

0.288

4

4

150000

2.88

0.048

2376

0.288

5

5

200000

3.84

0.048

4128

0.288





bond type table

id

class

bond type

1

1

bidders bond

2

2

berthing bond to pay

3

2

bidders bond standing

4

2

cigarette stamps bond

5

2

ships side bond

6

2

tonnage bond to pay

7

2

wharfage bond to pay

8

3

administrator s bond

9

3

arrastre bond

10

3

attachment bond

11

3

bid, performance bond

12

3

broker s bond

13

3

countervailing bond

14

3

fidelity bond individual

15

3

firearm bond

16

3

fishpond bond

17

3

guardian s bond

18

3

importer s bond general performance

19

3

reconstituted title bond

20

3

replevin bond

21

4

advanced baggage bond

22

4

anti-dumping bond

23

4

common carrier bond

24

4

contract worker s bond

25

4

execution pending appeal bond

26

4

export bond, re \[other than motor vehicles\]

27

4

forestry bond

28

4

forestry bond \[BFD\]

29

4

guarantee bond \[CB/re-import\]

30

4

guarantee bond \[maintenance/warranty\]

31

4

heir s bond

32

4

injunction bond, plaintiff

33

4

land carrier bond for \[TH\]

34

4

manufacturers official bond

35

4

millers bond \[NDA\]

36

4

milling contract bond \[NFA\]

37

4

miner s official bond

38

4

payment bond \[for construction\]

39

4

penalty and forfeiture bond

40

4

receiver s bond

41

4

receiver s bond, to appoint

42

4

replacement bond for lost TW, checks, ets

43

4

stock broker s and dealer s bond

44

4

tax exemption bond

45

4

truck operator s \[T\] bond for

46

4

valuation differences bond

47

4

warehouse bond, general

48

4

warehouse bond, for raw materials

49

4

warehouse bond, rice bonded \[BODT\]

50

4

warehouse bond, rice bonded \[NFA\]

51

5

attachment bond, to lift

52

5

bail bond

53

5

BOI omnibus bond

54

5

contract grower s bond

55

5

dealership bond

56

5

energy development bond \[BOED\]

57

5

export bond, re \[motor vehicles only\]

58

5

hauler s bond

59

5

immigration bond

60

5

indemnity bond, 3rd party, sheriffs

61

5

injunction bond, to lift

62

5

payment of taxes/duties bond

63

5

production of certificate of landing bond

64

5

quedans financing bond \[NFA\]

65

5

receivership bond, to lift

66

5

replevin bond, counter

67

5

supersedeas bond

68

5

surety bond, general

69

5

sweepstakes bond \[one year\]





special type bond table

id

bond\_type\_id

callable

max\_amt

penalty

1

11

1

300,000

0.55

2

11

0

300,000

0.5

3

68

1

500,000

0.6

4

68

0

500,000

0.55



1.  #### Adjuster’s Quarterly Report

![](up-ic-erd/image13.png)

1.  #### Fire Service Tax (FST) Remittances

![](up-ic-erd/image15.png)

1.  Investment Division
    --------------------------------------------



1.  ### Approval of Investments of Insurance Companies and Others (e.g. banks and investment houses)





![](up-ic-erd/image23.png)











                        



Type of Investment

id

type

0

Mutual Funds

1

UITF

2

Corporate Bonds

3

Foreign Currency Denominated Corporate Bonds or
Stocks

4

Preferred Stocks

5

Common Stocks

6

Purchase of Real Estate Properties

7

Valuation of Real Estate Properties

8

Income Producing Properties

9

EDP

10

Derivatives

11

Funds (Underlying for VUL Products)

12

Mortgage

13

Approval of IMA

14

Loan Facilities(Term Loan)

15

LTNCD

16

Commercial Paper

17

Salary Loan

18

Financial Assistance









Investment Requirements

id

type\_of\_investment\_id

requirement

investment\_cl\_id

0

0

SEC Approval

0

1

0

Prospectus/Fund Fact Sheet

0

2

1

BSP Approval

1

3

1

Prospectus/Fund Fact Sheet

1

4

2

Prospectus

2

5

2

Latest 3 Year Audited F/S

2

6

2

Credit Rating

3

7

2

SEC Approval

3

8

3

Prospectus

4

9

3

Latest 3 Year Audited F/S

4

10

3

Credit Rating

4

11

3

SEC Approval

4

12

4

Prospectus

5

13

4

Latest 3 Year Audited F/S

5

14

4

SEC Approval

5

15

4

PSE Listing Approval (IPO)

5

16

5

Prospectus

6

17

5

Latest 3 Year Audited F/S

6

18

6

TCT/CCT In Company's Name

7

19

6

Copy Of Absolute Deed Of Sale

7

20

6

All Other Terms And Conditions Of The Purchase

7

21

7

Appraisal Report By An Appraisal Company Duly
Accredited By SEC

8

22

7

Photocopy Of TCT/CCT

8

23

7

Latest Real Estate Tax Declaration

8

24

7

Latest Real Estate Tax Payment Official Receipt

8

25

8

TCT/CCT In Company's Name

10

26

8

Copy Of Absolute Deed Of Sale

10

27

8

All Other Terms And Conditions Of The Purchase

10

28

9

Copy Of Official Receipt/Proof Of Acquisition

11

29

9

Summary Of Purchased Equipment

11

30

10

Written Request For Approval Stating Objectives And
Proof That The Company Understands And Able To Manage Risks

12

31

10

Duly Accomplished Questionnaire From IC (Annex
A)

12

32

10

ISDA And CSA (If Applicable)

12

33

11

Prospectus/General Information/Features Of The
Funds

13

34

11

Pertinent Regulatory Approvals

13

35

11

List Of Products That Will Be Linked To Each
Fund

13

36

11

Surplus Shall In No Event Be Less Than Php 1.0
Million

13

37

12

Mortgage Loan Contract

14

38

12

Mortgage Loan Schedule

15

39

12

Appraisal Report By An Appraisal Company Duly
Accredited By SEC

15

40

13

Board Resolution Authorizing the Placements under
IMA

16

41

13

Pro-Forma Copy Of The IMA

16

42

13

Three (3) Year Audited Financial Statements Of The
Investment Or Fund Manager

16

43

13

Investment/Fund Manager Shall Be Duly Licensed And
Authorized Bank Of BSP

16

44

14

Issuer's Credit Rating

17

45

14

Certificate Of No Event Of Default

17

46

14

Three (3) Year Audited Financial Statements Of The
Borrower

17

47

15

Issuer's Credit Rating

18

48

15

BSP Approval

18

49

15

Three (3) Year Audited Financial Statements Of The
Borrower

19

50

16

Pre-Approval

20

51

16

Three (3) Year Audited Financial Statements Of The
Borrower

20

52

17

Board Resolution

20

53

17

Terms And Conditions

20

54

17

MOA

20

55

18

Board Resolution Approving The Financial
Assistance

21

56

18

Terms And Conditions

21









Circular Letters

id

circular\_num

letters

0

CL No. 2014-50



1

CL No. 2014-50



2

Section 206 (b)(4)



3

Section 211



4

CL No. 2014-19



5

Section 206 (b)(5)



6

Section 206 (b)(6)



7

Section 206 (b)(1)



8

CL No. 2016-16



9

Section 208 (a)



10

CL No. 2017-43



11

CL No. 2014-18



12

CL No. 2015-56



13

Section 243



14

Section 204 (j)(l)



15

CL No. 2014-21



16

CL No. 2015-41-A



17

Section 206 (b)(4)



18

Section 204 (g)



19

CL No. 2014-21



20

CL No. 2018-43



21

CL No. 2014-20





1.  ### Quarterly Submission and Monitoring of Statement of Paid Up Capital and Reserve Investment (SPUCRI)



No ERD because there are no data that is yet to be
submitted by the Investment Division

1.  ### Monthly Submission and Assessment of Investments Made, Sold, and Disposed



Investment Made and Sold - Monthly



![](up-ic-erd/image7.png)





        

        Investment Made and Sold - Quarterly
(MBA)



![](up-ic-erd/image11.png)

1.  Actuarial Division
    -------------------------------------------



1.  ### Approval of Insurance Products under Expeditious Approval Process



![](up-ic-erd/image42.png)

1.  ### Submission of Insurance Products under Non-Expeditious Approval Process



![](up-ic-erd/image31.png)

1.  Reinsurance Division
    ---------------------------------------------



1.  ### Submission and Monitoring of Facultative and Non-Facultative Placements

![](up-ic-erd/image40.png)





Requirements

name

description

Name of Insurer



Line of Business



Policy Number



Amount Ceded



Sum Insured



...

...













Report Types


report_type


Facultative and Non-Facultative Placements


Particulars of Reinsurance Treaties


Foreign Receipts and Remittances






1.  ### Submission and Monitoring of Particulars of Reinsurance Treaties

![](up-ic-erd/image26.png)











Report Types


report_type


Facultative and Non-Facultative Placements


Particulars of Reinsurance Treaties


Foreign Receipts and Remittances










Name of requirements

name

description

with\_particulars

with\_percentage\_to\_equity

Name of Company



1

0

Kind of Treaty



1

0

Resident Agent



1

0

Premium Reserved



1

1



1.  ### Submission and Monitoring of Foreign Receipts and Remittances

![](up-ic-erd/image18.png)











Report Types


report_type


Facultative and Non-Facultative Placements


Particulars of Reinsurance Treaties


Foreign Receipts and Remittances
















Risk Classifications


risk


Fire


Marine


Engineering






------------------------------------------------------------------------











Report Purposes


purpose


Premium


Loss


Recovery


Others






Legal Services Group
===================================================

1.  Regulation, Enforcement and Prosecution Division (REPD)
    --------------------------------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Administrative Complaints Filed Against Regulated Entities

![](up-ic-erd/image9.png)

1.  ### Data Storage, Retrieval and Reporting on Approved Non-life Insurance Policies, Riders, Clauses, Warranties or Endorsements

![](up-ic-erd/image14.png)

1.  Public Assistance and Mediation Division (PAMD)
    ------------------------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Complaints/Requests for Assistance Concerning Regulated Entities

![](up-ic-erd/image35.png)

1.  Licensing Division
    -------------------------------------------



1.  ### Recording and Retrieval of Basic Information of all Regulated Entities

![](up-ic-erd/image33.png)



1.  Conservatorship, Receivership, and Liquidation Division (CRL)
    --------------------------------------------------------------------------------------



1.  ### Registration, Monitoring and Reporting of Complaints/ Requests for Assistance Concerning Companies Placed under CRL

![](up-ic-erd/image6.png)

1.  ### Data Storage, Retrieval and Reporting on Companies under CRL

![](up-ic-erd/image16.png)

1.  Claims Adjudication Division (CAD)
    -----------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Complaints Lodged with CAD



![](up-ic-erd/image32.png)

1.  Suretyship Section
    -------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Requests Received and Certifications Issued

![](up-ic-erd/image3.png)





Collated Entity Relationship
Diagrams                                                            
           
