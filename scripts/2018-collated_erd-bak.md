<span class="c21"></span>

<span class="c34"></span>

<span class="c34"></span>

<span class="c34"></span>

<span class="c34"></span>

<span class="c34"></span>

<span class="c34"></span>

<span class="c150 c183">IC-FEDS DATABASE</span>

<span class="c150 c184">ENTITY RELATIONSHIP DIAGRAMS</span>

------------------------------------------------------------------------

<span class="c157 c150">Financial Examination Group</span>
==========================================================

1.  <span class="c150">Brokers Division</span>
    ------------------------------------------

<!-- -->

1.  ### <span class="c26">Annual Submission and Examination of Financial Condition/Standing</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 506.67px;">![](images/image12.png)</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.d37fd535cc60ad803b9514fdf541a08e85c9fbec"></span><span
id="t.0"></span>

<span class="c56">Summaries</span>

<span class="c0">id</span>

<span class="c0">name</span>

<span class="c0">account\_type</span>

<span class="c0">1</span>

<span class="c0">Cash Restricted - Clients' Money Account</span>

<span class="c0">Assets</span>

<span class="c0">2</span>

<span class="c0">Cash and Cash Equivalents</span>

<span class="c0">Assets</span>

<span class="c0">3</span>

<span class="c0">Receivable from Insurance Companies' Clients</span>

<span class="c0">Assets</span>

<span class="c0">4</span>

<span class="c0">Receivable from Ceding Company</span>

<span class="c0">Assets</span>

<span class="c0">5</span>

<span class="c0">Receivable from HMOs' Members</span>

<span class="c0">Assets</span>

<span class="c0">6</span>

<span class="c0">Commission Receivable</span>

<span class="c0">Assets</span>

<span class="c0">7</span>

<span class="c0">Other Receivables</span>

<span class="c0">Assets</span>

<span class="c0">8</span>

<span class="c0">Subscriptions Receivable</span>

<span class="c0">Assets</span>

<span class="c0">9</span>

<span class="c0">Prepayments</span>

<span class="c0">Assets</span>

<span class="c0">10</span>

<span class="c0">Financial Assets At Fair Value Through Profit or
Loss</span>

<span class="c0">Assets</span>

<span class="c0">11</span>

<span class="c0">Held to Collect (HTC) Investments</span>

<span class="c0">Assets</span>

<span class="c0">12</span>

<span class="c0">Financial Assets at Other Comprehensive Income</span>

<span class="c0">Assets</span>

<span class="c0">13</span>

<span class="c0">Derivative Assets Held for Hedging</span>

<span class="c0">Assets</span>

<span class="c0">14</span>

<span class="c0">Loans and Receivable</span>

<span class="c0">Assets</span>

<span class="c0">15</span>

<span class="c0">Investment in Associates</span>

<span class="c0">Assets</span>

<span class="c0">16</span>

<span class="c0">Investment in Joint Ventures</span>

<span class="c0">Assets</span>

<span class="c0">17</span>

<span class="c0">Investment Property</span>

<span class="c0">Assets</span>

<span class="c0">18</span>

<span class="c0">Property and Equipment</span>

<span class="c0">Assets</span>

<span class="c0">19</span>

<span class="c0">Post-Employment Defined Benefit Assets</span>

<span class="c0">Assets</span>

<span class="c0">20</span>

<span class="c0">Deferred Tax Asset</span>

<span class="c0">Assets</span>

<span class="c0">21</span>

<span class="c0">Intangible Assets</span>

<span class="c0">Assets</span>

<span class="c0">22</span>

<span class="c0">Other Assets</span>

<span class="c0">Assets</span>

<span class="c0">23</span>

<span class="c0">Payable to Insurance Companies</span>

<span class="c0">Liability</span>

<span class="c0">24</span>

<span class="c0">Payable to Reinsurer</span>

<span class="c0">Liability</span>

<span class="c0">25</span>

<span class="c0">Payable to Ceding Companies</span>

<span class="c0">Liability</span>

<span class="c0">26</span>

<span class="c0">Payable to Insured</span>

<span class="c0">Liability</span>

<span class="c0">27</span>

<span class="c0">Payable to HMO Provider</span>

<span class="c0">Liability</span>

<span class="c0">28</span>

<span class="c0">Other Payables</span>

<span class="c0">Liability</span>

<span class="c0">29</span>

<span class="c0">SSS/ECC/PAG-IBIG/PHILHEALTH Premium Contributions
Payable</span>

<span class="c0">Liability</span>

<span class="c0">30</span>

<span class="c0">Taxes Payable</span>

<span class="c0">Liability</span>

<span class="c0">31</span>

<span class="c0">Management Fee Payable</span>

<span class="c0">Liability</span>

<span class="c0">32</span>

<span class="c0">Notes Payables</span>

<span class="c0">Liability</span>

<span class="c0">33</span>

<span class="c0">Loan Payable</span>

<span class="c0">Liability</span>

<span class="c0">34</span>

<span class="c0">Finance Lease Liabilities</span>

<span class="c0">Liability</span>

<span class="c0">35</span>

<span class="c0">Post-Employment Benefits Obligation</span>

<span class="c0">Liability</span>

<span class="c0">36</span>

<span class="c0">Deferred Tax Liability</span>

<span class="c0">Liability</span>

<span class="c0">37</span>

<span class="c0">Derivative Liabilities Held for Hedging</span>

<span class="c0">Liability</span>

<span class="c0">38</span>

<span class="c0">Other Liabilities</span>

<span class="c0">Liability</span>

<span class="c0">39</span>

<span class="c0">Share Capital</span>

<span class="c0">Equity</span>

<span class="c0">40</span>

<span class="c0">Subscribed Share Capital</span>

<span class="c0">Equity</span>

<span class="c0">41</span>

<span class="c0">Capital Paid in Excess of Par</span>

<span class="c0">Equity</span>

<span class="c0">42</span>

<span class="c0">Retained Earnings</span>

<span class="c0">Equity</span>

<span class="c0">43</span>

<span class="c0">Treasury Share</span>

<span class="c0">Equity</span>

<span class="c0">44</span>

<span class="c0">Reserves</span>

<span class="c0">Equity</span>

<span class="c0">45</span>

<span class="c0">Contingency Surplus</span>

<span class="c0">Equity</span>

<span class="c0">46</span>

<span class="c0">Revaluation Surplus</span>

<span class="c0">Equity</span>

<span class="c0">47</span>

<span class="c0">Commission Income</span>

<span class="c0">Income</span>

<span class="c0">48</span>

<span class="c0">Interest Income</span>

<span class="c0">Income</span>

<span class="c0">49</span>

<span class="c0">Dividend Income</span>

<span class="c0">Income</span>

<span class="c0">50</span>

<span class="c0">Rental Income</span>

<span class="c0">Income</span>

<span class="c0">51</span>

<span class="c0">Gain/Loss on Sale of Investments</span>

<span class="c0">Income</span>

<span class="c0">52</span>

<span class="c0">Gain/Loss on Sale of Property and Equipment</span>

<span class="c0">Income</span>

<span class="c0">53</span>

<span class="c0">Miscellaneous Income</span>

<span class="c0">Income</span>

<span class="c0">54</span>

<span class="c0">Cost of Services</span>

<span class="c0">Expense</span>

<span class="c0">55</span>

<span class="c0">Administrative Expenses</span>

<span class="c0">Expense</span>

<span class="c0">56</span>

<span class="c0">Donation and Contribution</span>

<span class="c0">Expense</span>

<span class="c0">57</span>

<span class="c0">Miscellaneous Expense</span>

<span class="c0">Expense</span>

<span class="c0">58</span>

<span class="c0">Provision for Income Tax</span>

<span class="c0">Expense</span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.33c66a139c780b844067eb1a646384d969a9cc94"></span><span
id="t.1"></span>

<span class="c56">Schedules</span>

<span class="c0">id</span>

<span class="c0">name</span>

<span class="c0">summary\_id</span>

<span class="c0">1</span>

<span class="c0">Clients' Money on Hand</span>

<span class="c0">1</span>

<span class="c0">2</span>

<span class="c0">Clients' Money in Bank</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Money Market Instruments</span>

<span class="c0">2</span>

<span class="c0">4</span>

<span class="c0">Cash in Bank</span>

<span class="c0">2</span>

<span class="c0">5</span>

<span class="c0">Cash Equivalents</span>

<span class="c0">2</span>

<span class="c0">6</span>

<span class="c0">Receivable from Insurance Companies' Clients</span>

<span class="c0">3</span>

<span class="c0">7</span>

<span class="c0">Receivable from Ceding Company</span>

<span class="c0">3</span>

<span class="c0">8</span>

<span class="c0">Receivable from HMOs' Members</span>

<span class="c0">3</span>

<span class="c0">9</span>

<span class="c0">Commission Receivable from Insured</span>

<span class="c0">6</span>

<span class="c0">10</span>

<span class="c0">Commission Receivable from Insurance Companies</span>

<span class="c0">6</span>

<span class="c0">11</span>

<span class="c0">Commission Receivable from Reinsurer</span>

<span class="c0">6</span>

<span class="c0">12</span>

<span class="c0">Commission Receivable from Ceding Company</span>

<span class="c0">6</span>

<span class="c0">13</span>

<span class="c0">Commission Receivable - Others</span>

<span class="c0">6</span>

<span class="c0">14</span>

<span class="c0">Accounts Receivable</span>

<span class="c0">7</span>

<span class="c0">15</span>

<span class="c0">Notes Receivable</span>

<span class="c0">7</span>

<span class="c0">16</span>

<span class="c0">Interest Receivable</span>

<span class="c0">7</span>

<span class="c0">17</span>

<span class="c0">Dividend Receivable</span>

<span class="c0">7</span>

<span class="c0">18</span>

<span class="c0">Receivables - Others</span>

<span class="c0">7</span>

<span class="c0">19</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">7</span>

<span class="c0">20</span>

<span class="c0">Subscriptions Receivable</span>

<span class="c0">8</span>

<span class="c0">21</span>

<span class="c0">Prepaid Rent</span>

<span class="c0">9</span>

<span class="c0">22</span>

<span class="c0">Prepaid Value-Added Tax (VAT)</span>

<span class="c0">9</span>

<span class="c0">23</span>

<span class="c0">Creditable Withholding Tax</span>

<span class="c0">9</span>

<span class="c0">24</span>

<span class="c0">Financial Assets At Fair Value Through Profit or
Loss</span>

<span class="c0">10</span>

<span class="c0">25</span>

<span class="c0">Held to Collect (HTC) Investments</span>

<span class="c0">11</span>

<span class="c0">26</span>

<span class="c0">Financial Assets at Other Comprehensive Income</span>

<span class="c0">12</span>

<span class="c0">27</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">13</span>

<span class="c0">28</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">13</span>

<span class="c0">29</span>

<span class="c0">Hedges of a Net Investment in Foreign Operation</span>

<span class="c0">13</span>

<span class="c0">30</span>

<span class="c0">Loans and Receivable</span>

<span class="c0">14</span>

<span class="c0">31</span>

<span class="c0">Investment in Associates</span>

<span class="c0">15</span>

<span class="c0">32</span>

<span class="c0">Investment in Joint Ventures</span>

<span class="c0">16</span>

<span class="c0">33</span>

<span class="c0">Investment Property - Land</span>

<span class="c0">17</span>

<span class="c0">34</span>

<span class="c0">Investment Property - Building and Building
Improvements</span>

<span class="c0">17</span>

<span class="c0">35</span>

<span class="c0">Investment Property - Foreclosed Properties</span>

<span class="c0">17</span>

<span class="c0">36</span>

<span class="c0">Land</span>

<span class="c0">18</span>

<span class="c0">37</span>

<span class="c0">Building and Building Improvements</span>

<span class="c0">18</span>

<span class="c0">38</span>

<span class="c0">Leasehold Improvements</span>

<span class="c0">18</span>

<span class="c0">39</span>

<span class="c0">IT Equipment</span>

<span class="c0">18</span>

<span class="c0">40</span>

<span class="c0">Transportation Equipment</span>

<span class="c0">18</span>

<span class="c0">41</span>

<span class="c0">Office Furniture, Fixtures and Equipment</span>

<span class="c0">18</span>

<span class="c0">42</span>

<span class="c0">Property and Equipment under Lease</span>

<span class="c0">18</span>

<span class="c0">43</span>

<span class="c0">Other Equipment</span>

<span class="c0">18</span>

<span class="c0">44</span>

<span class="c0">Revaluation Increment</span>

<span class="c0">18</span>

<span class="c0">45</span>

<span class="c0">Post-Employment Defined Benefit Assets</span>

<span class="c0">19</span>

<span class="c0">46</span>

<span class="c0">Deferred Tax Asset</span>

<span class="c0">20</span>

<span class="c0">47</span>

<span class="c0">Intangible Assets</span>

<span class="c0">21</span>

<span class="c0">48</span>

<span class="c0">Other Assets</span>

<span class="c0">22</span>

<span class="c0">49</span>

<span class="c0">Payable to Insurance Companies</span>

<span class="c0">23</span>

<span class="c0">50</span>

<span class="c0">Payable to Reinsurer</span>

<span class="c0">24</span>

<span class="c0">51</span>

<span class="c0">Payable to Ceding Companies</span>

<span class="c0">25</span>

<span class="c0">52</span>

<span class="c0">Payable to Insured</span>

<span class="c0">26</span>

<span class="c0">53</span>

<span class="c0">Payable to HMO Provider</span>

<span class="c0">27</span>

<span class="c0">54</span>

<span class="c0">Accounts Payable</span>

<span class="c0">28</span>

<span class="c0">55</span>

<span class="c0">Lease Liability</span>

<span class="c0">28</span>

<span class="c0">56</span>

<span class="c0">Dividends Payable</span>

<span class="c0">28</span>

<span class="c0">57</span>

<span class="c0">Other Payables</span>

<span class="c0">28</span>

<span class="c0">58</span>

<span class="c0">SSS Premiums Payable</span>

<span class="c0">29</span>

<span class="c0">59</span>

<span class="c0">SSS Loans Payable</span>

<span class="c0">29</span>

<span class="c0">60</span>

<span class="c0">PAG-IBIG Premiums Payable</span>

<span class="c0">29</span>

<span class="c0">61</span>

<span class="c0">PAG-IBIG Loans Payable</span>

<span class="c0">29</span>

<span class="c0">62</span>

<span class="c0">Income Tax Payable</span>

<span class="c0">30</span>

<span class="c0">63</span>

<span class="c0">Withholding Tax Payable</span>

<span class="c0">30</span>

<span class="c0">64</span>

<span class="c0">Value-Added Tax (VAT) Payable</span>

<span class="c0">30</span>

<span class="c0">65</span>

<span class="c0">Other Taxes and Licenses Payable</span>

<span class="c0">30</span>

<span class="c0">66</span>

<span class="c0">Management Fee Payable</span>

<span class="c0">31</span>

<span class="c0">67</span>

<span class="c0">Notes Payables</span>

<span class="c0">32</span>

<span class="c0">68</span>

<span class="c0">Loan Payable</span>

<span class="c0">33</span>

<span class="c0">69</span>

<span class="c0">Finance Lease Liabilities</span>

<span class="c0">34</span>

<span class="c0">70</span>

<span class="c0">Post-Employment Benefits Obligation</span>

<span class="c0">35</span>

<span class="c0">71</span>

<span class="c0">Deferred Tax Liability</span>

<span class="c0">36</span>

<span class="c0">72</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">37</span>

<span class="c0">73</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">37</span>

<span class="c0">74</span>

<span class="c0">Hedges of a Net Investment Foreign Operation</span>

<span class="c0">37</span>

<span class="c0">75</span>

<span class="c0">Deferred Income</span>

<span class="c0">38</span>

<span class="c0">76</span>

<span class="c0">Others</span>

<span class="c0">38</span>

<span class="c0">77</span>

<span class="c0">Preferred Shares</span>

<span class="c0">39</span>

<span class="c0">78</span>

<span class="c0">Common Shares</span>

<span class="c0">39</span>

<span class="c0">79</span>

<span class="c0">Subscribed Share Capital</span>

<span class="c0">40</span>

<span class="c0">80</span>

<span class="c0">Capital Paid in Excess of Par</span>

<span class="c0">41</span>

<span class="c0">81</span>

<span class="c0">Appropriated Retained Earnings</span>

<span class="c0">42</span>

<span class="c0">82</span>

<span class="c0">Unappropriated Retained Earnings</span>

<span class="c0">42</span>

<span class="c0">83</span>

<span class="c0">Treasury Share</span>

<span class="c0">43</span>

<span class="c0">84</span>

<span class="c0">Reserve for Financial Assets at Other Comprehensive
Income</span>

<span class="c0">44</span>

<span class="c0">85</span>

<span class="c0">Reserve for Cash Flow Hedge</span>

<span class="c0">44</span>

<span class="c0">86</span>

<span class="c0">Reserve for Hedge of a Net Investment in Foreign
Operations</span>

<span class="c0">44</span>

<span class="c0">87</span>

<span class="c0">Cumulative Foreign Currency Translation</span>

<span class="c0">44</span>

<span class="c0">88</span>

<span class="c0">Contingency Surplus</span>

<span class="c0">45</span>

<span class="c0">89</span>

<span class="c0">Revaluation Surplus</span>

<span class="c0">46</span>

<span class="c0">90</span>

<span class="c0">Commission Income from Insurance Companies</span>

<span class="c0">47</span>

<span class="c0">91</span>

<span class="c0">Commission Income from Insureds</span>

<span class="c0">47</span>

<span class="c0">92</span>

<span class="c0">Commission Income - Others</span>

<span class="c0">47</span>

<span class="c0">93</span>

<span class="c0">Interest Income</span>

<span class="c0">48</span>

<span class="c0">94</span>

<span class="c0">Dividend Income</span>

<span class="c0">49</span>

<span class="c0">95</span>

<span class="c0">Rental Income</span>

<span class="c0">50</span>

<span class="c0">96</span>

<span class="c0">Gain/Loss on Sale of Investments</span>

<span class="c0">51</span>

<span class="c0">97</span>

<span class="c0">Gain/Loss on Sale of Property and Equipment</span>

<span class="c0">52</span>

<span class="c0">98</span>

<span class="c0">Miscellaneous Income</span>

<span class="c0">53</span>

<span class="c0">99</span>

<span class="c0">Representation and Entertainment</span>

<span class="c0">54</span>

<span class="c0">100</span>

<span class="c0">Transportation and Travel Expenses</span>

<span class="c0">54</span>

<span class="c0">101</span>

<span class="c0">Management Fee Expense</span>

<span class="c0">54</span>

<span class="c0">102</span>

<span class="c0">Other Expenses</span>

<span class="c0">54</span>

<span class="c0">103</span>

<span class="c0">Salaries and Wages Benefits</span>

<span class="c0">55</span>

<span class="c0">104</span>

<span class="c0">Professional and Technical Development</span>

<span class="c0">55</span>

<span class="c0">105</span>

<span class="c0">Professional Fees</span>

<span class="c0">55</span>

<span class="c0">106</span>

<span class="c0">Taxes and Licenses</span>

<span class="c0">55</span>

<span class="c0">107</span>

<span class="c0">Rental Expense</span>

<span class="c0">55</span>

<span class="c0">108</span>

<span class="c0">Utilities Expense</span>

<span class="c0">55</span>

<span class="c0">109</span>

<span class="c0">Depreciation and Amortization</span>

<span class="c0">55</span>

<span class="c0">110</span>

<span class="c0">Repairs and Maintenance</span>

<span class="c0">55</span>

<span class="c0">111</span>

<span class="c0">Insurance Expense</span>

<span class="c0">55</span>

<span class="c0">112</span>

<span class="c0">Advertising and Promotions</span>

<span class="c0">55</span>

<span class="c0">113</span>

<span class="c0">Bank Charges</span>

<span class="c0">55</span>

<span class="c0">114</span>

<span class="c0">Donation and Contribution</span>

<span class="c0">56</span>

<span class="c0">115</span>

<span class="c0">Miscellaneous Expense</span>

<span class="c0">57</span>

<span class="c0">116</span>

<span class="c0">Provision for Income Tax - Final</span>

<span class="c0">58</span>

<span class="c0">117</span>

<span class="c0">Provision for Income Tax - Current</span>

<span class="c0">58</span>

<span class="c0">118</span>

<span class="c0">Provision for Income Tax - Deferred</span>

<span class="c0">58</span>

<span class="c21"></span>

<span id="t.b75a600837a46b27e797976903fbde2d0dd2cc85"></span><span
id="t.2"></span>

<span class="c56">Forms</span>

<span class="c0">id</span>

<span class="c0">schedule\_id</span>

<span class="c0">form\_name</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Clients' Money on Hand - Premiums</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">Clients' Money on Hand - Claims</span>

<span class="c0">3</span>

<span class="c0">1</span>

<span class="c0">Clients' Money on Hand - HMO Fees</span>

<span class="c0">4</span>

<span class="c0">2</span>

<span class="c0">Clients' Money in Bank - Premiums</span>

<span class="c0">5</span>

<span class="c0">2</span>

<span class="c0">Clients' Money in Bank - Claims</span>

<span class="c0">6</span>

<span class="c0">2</span>

<span class="c0">Clients' Money in Bank - HMO Fees</span>

<span class="c0">7</span>

<span class="c0">3</span>

<span class="c0">Undeposited Collection</span>

<span class="c0">8</span>

<span class="c0">3</span>

<span class="c0">Petty Cash Fund</span>

<span class="c0">9</span>

<span class="c0">3</span>

<span class="c0">Revolving Fund</span>

<span class="c0">10</span>

<span class="c0">3</span>

<span class="c0">Other Funds</span>

<span class="c0">11</span>

<span class="c0">4</span>

<span class="c0">Cash in Bank - Current</span>

<span class="c0">12</span>

<span class="c0">4</span>

<span class="c0">Cash in Bank - Savings</span>

<span class="c0">13</span>

<span class="c0">5</span>

<span class="c0">Time Deposits</span>

<span class="c0">14</span>

<span class="c0">5</span>

<span class="c0">Money Market Instruments</span>

<span class="c0">15</span>

<span class="c0">5</span>

<span class="c0">Cash Equivalent - Others</span>

<span class="c0">16</span>

<span class="c0">6</span>

<span class="c0">Receivable from Insurance Companies' Clients</span>

<span class="c0">17</span>

<span class="c0">7</span>

<span class="c0">Receivable from Ceding Company</span>

<span class="c0">18</span>

<span class="c0">8</span>

<span class="c0">Receivable from HMOs' Members</span>

<span class="c0">19</span>

<span class="c0">9</span>

<span class="c0">Commission Receivable from Insured</span>

<span class="c0">20</span>

<span class="c0">10</span>

<span class="c0">Commission Receivable from Insurance Companies - Direct
Payment</span>

<span class="c0">21</span>

<span class="c0">10</span>

<span class="c0">Commission Receivable from Insurance Companies -
Indirect Payment</span>

<span class="c0">22</span>

<span class="c0">11</span>

<span class="c0">Commission Receivable from Reinsurer- Direct
Payment</span>

<span class="c0">23</span>

<span class="c0">11</span>

<span class="c0">Commission Receivable from Reinsurer - Indirect
Payment</span>

<span class="c0">24</span>

<span class="c0">12</span>

<span class="c0">Commission Receivable from Ceding Company</span>

<span class="c0">25</span>

<span class="c0">13</span>

<span class="c0">Commission Receivable - Others</span>

<span class="c0">26</span>

<span class="c0">14</span>

<span class="c0">Advances to Officers and Employees</span>

<span class="c0">27</span>

<span class="c0">14</span>

<span class="c0">Rental Receivable</span>

<span class="c0">28</span>

<span class="c0">15</span>

<span class="c0">Notes Receivable</span>

<span class="c0">29</span>

<span class="c0">16</span>

<span class="c0">Interest Receivable</span>

<span class="c0">30</span>

<span class="c0">17</span>

<span class="c0">Dividend Receivable</span>

<span class="c0">31</span>

<span class="c0">18</span>

<span class="c0">Receivables - Others</span>

<span class="c0">32</span>

<span class="c0">19</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">33</span>

<span class="c0">20</span>

<span class="c0">Subscriptions Receivable</span>

<span class="c0">34</span>

<span class="c0">21</span>

<span class="c0">Prepaid Rent</span>

<span class="c0">35</span>

<span class="c0">22</span>

<span class="c0">Prepaid Value-Added Tax (VAT)</span>

<span class="c0">36</span>

<span class="c0">23</span>

<span class="c0">Creditable Withholding Tax</span>

<span class="c0">37</span>

<span class="c0">24</span>

<span class="c0">Financial Assets At Fair Value Through Profit or
Loss</span>

<span class="c0">38</span>

<span class="c0">25</span>

<span class="c0">Held to Collect (HTC) Investments</span>

<span class="c0">39</span>

<span class="c0">26</span>

<span class="c0">Financial Assets at Other Comprehensive Income</span>

<span class="c0">40</span>

<span class="c0">27</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">41</span>

<span class="c0">28</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">42</span>

<span class="c0">29</span>

<span class="c0">Hedges of a Net Investment in Foreign Operation</span>

<span class="c0">43</span>

<span class="c0">30</span>

<span class="c0">Loans and Receivable</span>

<span class="c0">44</span>

<span class="c0">31</span>

<span class="c0">Investment in Associates</span>

<span class="c0">45</span>

<span class="c0">32</span>

<span class="c0">Investment in Joint Ventures</span>

<span class="c0">46</span>

<span class="c0">33</span>

<span class="c0">Investment Property - Land</span>

<span class="c0">47</span>

<span class="c0">34</span>

<span class="c0">Investment Property - Building</span>

<span class="c0">48</span>

<span class="c0">34</span>

<span class="c0">Investment Property - Building Improvements</span>

<span class="c0">49</span>

<span class="c0">35</span>

<span class="c0">Investment Property - Foreclosed Properties</span>

<span class="c0">50</span>

<span class="c0">36</span>

<span class="c0">Land</span>

<span class="c0">51</span>

<span class="c0">37</span>

<span class="c0">Building</span>

<span class="c0">52</span>

<span class="c0">37</span>

<span class="c0">Building Improvements</span>

<span class="c0">53</span>

<span class="c0">38</span>

<span class="c0">Leasehold Improvements</span>

<span class="c0">54</span>

<span class="c0">39</span>

<span class="c0">IT Equipment</span>

<span class="c0">55</span>

<span class="c0">40</span>

<span class="c0">Transportation Equipment</span>

<span class="c0">56</span>

<span class="c0">41</span>

<span class="c0">Office Furniture, Fixtures and Equipment</span>

<span class="c0">57</span>

<span class="c0">42</span>

<span class="c0">Property and Equipment under Lease</span>

<span class="c0">58</span>

<span class="c0">43</span>

<span class="c0">Other Equipment</span>

<span class="c0">59</span>

<span class="c0">44</span>

<span class="c0">Revaluation Increment</span>

<span class="c0">60</span>

<span class="c0">45</span>

<span class="c0">Post-Employment Defined Benefit Assets</span>

<span class="c0">61</span>

<span class="c0">46</span>

<span class="c0">Deferred Tax Asset</span>

<span class="c0">62</span>

<span class="c0">47</span>

<span class="c0">Intangible Assets</span>

<span class="c0">63</span>

<span class="c0">48</span>

<span class="c0">Other Assets</span>

<span class="c0">64</span>

<span class="c0">49</span>

<span class="c0">Payable to Insurance Companies</span>

<span class="c0">65</span>

<span class="c0">50</span>

<span class="c0">Payable to Reinsurer</span>

<span class="c0">66</span>

<span class="c0">51</span>

<span class="c0">Payable to Ceding Companies</span>

<span class="c0">67</span>

<span class="c0">52</span>

<span class="c0">Payable to Insured</span>

<span class="c0">68</span>

<span class="c0">53</span>

<span class="c0">Payable to HMO Provider</span>

<span class="c0">69</span>

<span class="c0">54</span>

<span class="c0">Accounts Payable</span>

<span class="c0">70</span>

<span class="c0">55</span>

<span class="c0">Lease Liability</span>

<span class="c0">71</span>

<span class="c0">56</span>

<span class="c0">Dividends Payable</span>

<span class="c0">72</span>

<span class="c0">57</span>

<span class="c0">Other Payables</span>

<span class="c0">73</span>

<span class="c0">58</span>

<span class="c0">SSS Premiums Payable</span>

<span class="c0">74</span>

<span class="c0">59</span>

<span class="c0">SSS Loans Payable</span>

<span class="c0">75</span>

<span class="c0">60</span>

<span class="c0">PAG-IBIG Premiums Payable</span>

<span class="c0">76</span>

<span class="c0">61</span>

<span class="c0">PAG-IBIG Loans Payable</span>

<span class="c0">77</span>

<span class="c0">62</span>

<span class="c0">Income Tax Payable</span>

<span class="c0">78</span>

<span class="c0">63</span>

<span class="c0">Withholding Tax Payable</span>

<span class="c0">79</span>

<span class="c0">64</span>

<span class="c0">Value-Added Tax (VAT) Payable</span>

<span class="c0">80</span>

<span class="c0">65</span>

<span class="c0">Other Taxes and Licenses Payable</span>

<span class="c0">81</span>

<span class="c0">66</span>

<span class="c0">Management Fee Payable</span>

<span class="c0">82</span>

<span class="c0">67</span>

<span class="c0">Notes Payables</span>

<span class="c0">83</span>

<span class="c0">68</span>

<span class="c0">Loan Payable</span>

<span class="c0">84</span>

<span class="c0">69</span>

<span class="c0">Finance Lease Liabilities</span>

<span class="c0">85</span>

<span class="c0">70</span>

<span class="c0">Post-Employment Benefits Obligation</span>

<span class="c0">86</span>

<span class="c0">71</span>

<span class="c0">Deferred Tax Liability</span>

<span class="c0">87</span>

<span class="c0">72</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">88</span>

<span class="c0">73</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">89</span>

<span class="c0">74</span>

<span class="c0">Hedges of a Net Investment Foreign Operation</span>

<span class="c0">90</span>

<span class="c0">75</span>

<span class="c0">Deferred Income</span>

<span class="c0">91</span>

<span class="c0">76</span>

<span class="c0">Others</span>

<span class="c0">92</span>

<span class="c0">77</span>

<span class="c0">Preferred Shares</span>

<span class="c0">93</span>

<span class="c0">78</span>

<span class="c0">Common Shares</span>

<span class="c0">94</span>

<span class="c0">79</span>

<span class="c0">Subscribed Share Capital</span>

<span class="c0">95</span>

<span class="c0">80</span>

<span class="c0">Capital Paid in Excess of Par</span>

<span class="c0">96</span>

<span class="c0">81</span>

<span class="c0">Appropriated Retained Earnings</span>

<span class="c0">97</span>

<span class="c0">82</span>

<span class="c0">Unappropriated Retained Earnings</span>

<span class="c0">98</span>

<span class="c0">83</span>

<span class="c0">Treasury Share</span>

<span class="c0">99</span>

<span class="c0">84</span>

<span class="c0">Reserve for Financial Assets at Other Comprehensive
Income</span>

<span class="c0">100</span>

<span class="c0">85</span>

<span class="c0">Reserve for Cash Flow Hedge</span>

<span class="c0">101</span>

<span class="c0">86</span>

<span class="c0">Reserve for Hedge of a Net Investment in Foreign
Operations</span>

<span class="c0">102</span>

<span class="c0">87</span>

<span class="c0">Cumulative Foreign Currency Translation</span>

<span class="c0">103</span>

<span class="c0">88</span>

<span class="c0">Contingency Surplus</span>

<span class="c0">104</span>

<span class="c0">89</span>

<span class="c0">Revaluation Surplus</span>

<span class="c0">105</span>

<span class="c0">90</span>

<span class="c0">Commission Income from Insurance Companies</span>

<span class="c0">106</span>

<span class="c0">91</span>

<span class="c0">Commission Income from Insureds</span>

<span class="c0">107</span>

<span class="c0">92</span>

<span class="c0">Commission Income - Others</span>

<span class="c0">108</span>

<span class="c0">93</span>

<span class="c0">Interest Income</span>

<span class="c0">109</span>

<span class="c0">94</span>

<span class="c0">Dividend Income</span>

<span class="c0">110</span>

<span class="c0">95</span>

<span class="c0">Rental Income</span>

<span class="c0">111</span>

<span class="c0">96</span>

<span class="c0">Gain/Loss on Sale of Investments</span>

<span class="c0">112</span>

<span class="c0">97</span>

<span class="c0">Gain/Loss on Sale of Property and Equipment</span>

<span class="c0">113</span>

<span class="c0">98</span>

<span class="c0">Miscellaneous Income</span>

<span class="c0">114</span>

<span class="c0">99</span>

<span class="c0">Representation and Entertainment</span>

<span class="c0">115</span>

<span class="c0">100</span>

<span class="c0">Transportation and Travel Expenses</span>

<span class="c0">116</span>

<span class="c0">101</span>

<span class="c0">Management Fee Expense</span>

<span class="c0">117</span>

<span class="c0">102</span>

<span class="c0">Other Expenses</span>

<span class="c0">118</span>

<span class="c0">103</span>

<span class="c0">Salaries and Wages</span>

<span class="c0">119</span>

<span class="c0">103</span>

<span class="c0">13th Month/Bonuses/Incentives</span>

<span class="c0">120</span>

<span class="c0">103</span>

<span class="c0">SSS/EC/PAG-IBIG/PHILHEALTH Contributions</span>

<span class="c0">121</span>

<span class="c0">103</span>

<span class="c0">Post-Employment Benefit Expense</span>

<span class="c0">122</span>

<span class="c0">103</span>

<span class="c0">Other Employee Benefits</span>

<span class="c0">123</span>

<span class="c0">104</span>

<span class="c0">Professional and Technical Development</span>

<span class="c0">124</span>

<span class="c0">105</span>

<span class="c0">Professional Fees</span>

<span class="c0">125</span>

<span class="c0">106</span>

<span class="c0">Taxes and Licenses</span>

<span class="c0">126</span>

<span class="c0">107</span>

<span class="c0">Rental Expense</span>

<span class="c0">127</span>

<span class="c0">108</span>

<span class="c0">Utilities Expense</span>

<span class="c0">128</span>

<span class="c0">109</span>

<span class="c0">Depreciation and Amortization</span>

<span class="c0">129</span>

<span class="c0">110</span>

<span class="c0">Repairs and Maintenance - Labor</span>

<span class="c0">130</span>

<span class="c0">110</span>

<span class="c0">Repairs and Maintenance - Materials</span>

<span class="c0">131</span>

<span class="c0">111</span>

<span class="c0">Insurance Expense</span>

<span class="c0">132</span>

<span class="c0">112</span>

<span class="c0">Advertising and Promotions</span>

<span class="c0">133</span>

<span class="c0">113</span>

<span class="c0">Bank Charges</span>

<span class="c0">134</span>

<span class="c0">114</span>

<span class="c0">Donation and Contribution</span>

<span class="c0">135</span>

<span class="c0">115</span>

<span class="c0">Miscellaneous Expense</span>

<span class="c0">136</span>

<span class="c0">116</span>

<span class="c0">Provision for Income Tax - Final</span>

<span class="c0">137</span>

<span class="c0">117</span>

<span class="c0">Provision for Income Tax - Current</span>

<span class="c0">138</span>

<span class="c0">118</span>

<span class="c0">Provision for Income Tax - Deferred</span>

<span class="c21"></span>

<span id="t.d12653e524264f3015ead2213a8e55ce70d972ad"></span><span
id="t.3"></span>

<span class="c56">Computed Attributes</span>

<span class="c0">id</span>

<span class="c0">form\_id</span>

<span class="c0">attribute</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">balance</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">balance</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">balance</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">account\_balance</span>

<span class="c0">5</span>

<span class="c0">5</span>

<span class="c0">translated\_balance</span>

<span class="c0">6</span>

<span class="c0">6</span>

<span class="c0">account\_balance</span>

<span class="c0">7</span>

<span class="c0">7</span>

<span class="c0">balance</span>

<span class="c0">8</span>

<span class="c0">8</span>

<span class="c0">balance</span>

<span class="c0">9</span>

<span class="c0">9</span>

<span class="c0">balance</span>

<span class="c0">10</span>

<span class="c0">10</span>

<span class="c0">balance</span>

<span class="c0">11</span>

<span class="c0">11</span>

<span class="c0">translated\_balance</span>

<span class="c0">12</span>

<span class="c0">12</span>

<span class="c0">translated\_balance</span>

<span class="c0">13</span>

<span class="c0">13</span>

<span class="c0">account\_balance</span>

<span class="c0">14</span>

<span class="c0">14</span>

<span class="c0">amount</span>

<span class="c0">15</span>

<span class="c0">15</span>

<span class="c0">amount</span>

<span class="c0">16</span>

<span class="c0">16</span>

<span class="c0">amount</span>

<span class="c0">17</span>

<span class="c0">17</span>

<span class="c0">amount</span>

<span class="c0">18</span>

<span class="c0">18</span>

<span class="c0">amount</span>

<span class="c0">19</span>

<span class="c0">19</span>

<span class="c0">amount</span>

<span class="c0">20</span>

<span class="c0">20</span>

<span class="c0">amount</span>

<span class="c0">21</span>

<span class="c0">21</span>

<span class="c0">amount</span>

<span class="c0">22</span>

<span class="c0">22</span>

<span class="c0">amount</span>

<span class="c0">23</span>

<span class="c0">23</span>

<span class="c0">amount</span>

<span class="c0">24</span>

<span class="c0">24</span>

<span class="c0">amount</span>

<span class="c0">25</span>

<span class="c0">25</span>

<span class="c0">amount</span>

<span class="c0">26</span>

<span class="c0">26</span>

<span class="c0">amount</span>

<span class="c0">27</span>

<span class="c0">27</span>

<span class="c0">amount</span>

<span class="c0">28</span>

<span class="c0">28</span>

<span class="c0">amount</span>

<span class="c0">29</span>

<span class="c0">29</span>

<span class="c0">amount</span>

<span class="c0">30</span>

<span class="c0">30</span>

<span class="c0">amount</span>

<span class="c0">31</span>

<span class="c0">31</span>

<span class="c0">amount</span>

<span class="c0">32</span>

<span class="c0">32</span>

<span class="c0">amount</span>

<span class="c0">33</span>

<span class="c0">33</span>

<span class="c0">amount</span>

<span class="c0">34</span>

<span class="c0">34</span>

<span class="c0">amount</span>

<span class="c0">35</span>

<span class="c0">35</span>

<span class="c0">amount</span>

<span class="c0">36</span>

<span class="c0">36</span>

<span class="c0">amount</span>

<span class="c0">37</span>

<span class="c0">37</span>

<span class="c0">total\_fair\_market\_value</span>

<span class="c0">38</span>

<span class="c0">38</span>

<span class="c0">total\_fair\_market\_value</span>

<span class="c0">39</span>

<span class="c0">39</span>

<span class="c0">total\_fair\_market\_value</span>

<span class="c0">40</span>

<span class="c0">40</span>

<span class="c0">amount</span>

<span class="c0">41</span>

<span class="c0">41</span>

<span class="c0">amount</span>

<span class="c0">42</span>

<span class="c0">42</span>

<span class="c0">amount</span>

<span class="c0">43</span>

<span class="c0">43</span>

<span class="c0">amount</span>

<span class="c0">44</span>

<span class="c0">44</span>

<span class="c0">amount</span>

<span class="c0">45</span>

<span class="c0">45</span>

<span class="c0">amount</span>

<span class="c0">46</span>

<span class="c0">46</span>

<span class="c0">fair\_value</span>

<span class="c0">47</span>

<span class="c0">47</span>

<span class="c0">fair\_value</span>

<span class="c0">48</span>

<span class="c0">48</span>

<span class="c0">fair\_value</span>

<span class="c0">49</span>

<span class="c0">49</span>

<span class="c0">fair\_value</span>

<span class="c0">50</span>

<span class="c0">50</span>

<span class="c0">fair\_value</span>

<span class="c0">51</span>

<span class="c0">51</span>

<span class="c0">fair\_value</span>

<span class="c0">52</span>

<span class="c0">52</span>

<span class="c0">fair\_value</span>

<span class="c0">53</span>

<span class="c0">53</span>

<span class="c0">net\_book\_value</span>

<span class="c0">54</span>

<span class="c0">54</span>

<span class="c0">net\_book\_value</span>

<span class="c0">55</span>

<span class="c0">55</span>

<span class="c0">net\_book\_value</span>

<span class="c0">56</span>

<span class="c0">56</span>

<span class="c0">net\_book\_value</span>

<span class="c0">57</span>

<span class="c0">57</span>

<span class="c0">net\_book\_value</span>

<span class="c0">58</span>

<span class="c0">58</span>

<span class="c0">net\_book\_value</span>

<span class="c0">59</span>

<span class="c0">59</span>

<span class="c0">net\_book\_value</span>

<span class="c0">60</span>

<span class="c0">60</span>

<span class="c0">present\_value\_of\_benefit\_obligation</span>

<span class="c0">61</span>

<span class="c0">61</span>

<span class="c0">amount</span>

<span class="c0">62</span>

<span class="c0">62</span>

<span class="c0">net\_book\_value</span>

<span class="c0">63</span>

<span class="c0">63</span>

<span class="c0">amount</span>

<span class="c0">64</span>

<span class="c0">64</span>

<span class="c0">amount</span>

<span class="c0">65</span>

<span class="c0">65</span>

<span class="c0">amount</span>

<span class="c0">66</span>

<span class="c0">66</span>

<span class="c0">amount</span>

<span class="c0">67</span>

<span class="c0">67</span>

<span class="c0">amount</span>

<span class="c0">68</span>

<span class="c0">68</span>

<span class="c0">amount</span>

<span class="c0">69</span>

<span class="c0">69</span>

<span class="c0">amount</span>

<span class="c0">70</span>

<span class="c0">70</span>

<span class="c0">amount</span>

<span class="c0">71</span>

<span class="c0">71</span>

<span class="c0">amount</span>

<span class="c0">72</span>

<span class="c0">72</span>

<span class="c0">amount</span>

<span class="c0">73</span>

<span class="c0">73</span>

<span class="c0">amount</span>

<span class="c0">74</span>

<span class="c0">74</span>

<span class="c0">amount</span>

<span class="c0">75</span>

<span class="c0">75</span>

<span class="c0">amount</span>

<span class="c0">76</span>

<span class="c0">76</span>

<span class="c0">amount</span>

<span class="c0">77</span>

<span class="c0">77</span>

<span class="c0">amount</span>

<span class="c0">78</span>

<span class="c0">78</span>

<span class="c0">amount</span>

<span class="c0">79</span>

<span class="c0">79</span>

<span class="c0">amount</span>

<span class="c0">80</span>

<span class="c0">80</span>

<span class="c0">amount</span>

<span class="c0">81</span>

<span class="c0">81</span>

<span class="c0">amount</span>

<span class="c0">82</span>

<span class="c0">82</span>

<span class="c0">amount</span>

<span class="c0">83</span>

<span class="c0">83</span>

<span class="c0">amount</span>

<span class="c0">84</span>

<span class="c0">84</span>

<span class="c0">amount</span>

<span class="c0">85</span>

<span class="c0">85</span>

<span class="c0">present\_value\_of\_benefit\_obligation</span>

<span class="c0">86</span>

<span class="c0">86</span>

<span class="c0">amount</span>

<span class="c0">87</span>

<span class="c0">87</span>

<span class="c0">amount</span>

<span class="c0">88</span>

<span class="c0">88</span>

<span class="c0">amount</span>

<span class="c0">89</span>

<span class="c0">89</span>

<span class="c0">amount</span>

<span class="c0">90</span>

<span class="c0">90</span>

<span class="c0">amount</span>

<span class="c0">91</span>

<span class="c0">91</span>

<span class="c0">amount</span>

<span class="c0">92</span>

<span class="c0">92</span>

<span class="c0">total\_share\_capital</span>

<span class="c0">93</span>

<span class="c0">93</span>

<span class="c0">total\_share\_capital</span>

<span class="c0">94</span>

<span class="c0">94</span>

<span class="c0">amount</span>

<span class="c0">95</span>

<span class="c0">95</span>

<span class="c0">amount</span>

<span class="c0">96</span>

<span class="c0">96</span>

<span class="c0">amount</span>

<span class="c0">97</span>

<span class="c0">97</span>

<span class="c0">amount</span>

<span class="c0">98</span>

<span class="c0">98</span>

<span class="c0">amount</span>

<span class="c0">99</span>

<span class="c0">99</span>

<span class="c0">amount</span>

<span class="c0">100</span>

<span class="c0">100</span>

<span class="c0">amount</span>

<span class="c0">101</span>

<span class="c0">101</span>

<span class="c0">amount</span>

<span class="c0">102</span>

<span class="c0">102</span>

<span class="c0">amount</span>

<span class="c0">103</span>

<span class="c0">103</span>

<span class="c0">amount</span>

<span class="c0">104</span>

<span class="c0">104</span>

<span class="c0">amount</span>

<span class="c0">105</span>

<span class="c0">105</span>

<span class="c0">amount</span>

<span class="c0">106</span>

<span class="c0">106</span>

<span class="c0">amount</span>

<span class="c0">107</span>

<span class="c0">107</span>

<span class="c0">amount</span>

<span class="c0">108</span>

<span class="c0">108</span>

<span class="c0">amount</span>

<span class="c0">109</span>

<span class="c0">109</span>

<span class="c0">amount</span>

<span class="c0">110</span>

<span class="c0">110</span>

<span class="c0">amount</span>

<span class="c0">111</span>

<span class="c0">111</span>

<span class="c0">amount</span>

<span class="c0">112</span>

<span class="c0">112</span>

<span class="c0">amount</span>

<span class="c0">113</span>

<span class="c0">113</span>

<span class="c0">amount</span>

<span class="c0">114</span>

<span class="c0">114</span>

<span class="c0">amount</span>

<span class="c0">115</span>

<span class="c0">115</span>

<span class="c0">amount</span>

<span class="c0">116</span>

<span class="c0">116</span>

<span class="c0">amount</span>

<span class="c0">117</span>

<span class="c0">117</span>

<span class="c0">amount</span>

<span class="c0">118</span>

<span class="c0">118</span>

<span class="c0">amount</span>

<span class="c0">119</span>

<span class="c0">119</span>

<span class="c0">amount</span>

<span class="c0">120</span>

<span class="c0">120</span>

<span class="c0">amount</span>

<span class="c0">121</span>

<span class="c0">121</span>

<span class="c0">amount</span>

<span class="c0">122</span>

<span class="c0">122</span>

<span class="c0">amount</span>

<span class="c0">123</span>

<span class="c0">123</span>

<span class="c0">amount</span>

<span class="c0">124</span>

<span class="c0">124</span>

<span class="c0">amount</span>

<span class="c0">125</span>

<span class="c0">125</span>

<span class="c0">amount</span>

<span class="c0">126</span>

<span class="c0">126</span>

<span class="c0">amount</span>

<span class="c0">127</span>

<span class="c0">127</span>

<span class="c0">amount</span>

<span class="c0">128</span>

<span class="c0">128</span>

<span class="c0">amount</span>

<span class="c0">129</span>

<span class="c0">129</span>

<span class="c0">amount</span>

<span class="c0">130</span>

<span class="c0">130</span>

<span class="c0">amount</span>

<span class="c0">131</span>

<span class="c0">131</span>

<span class="c0">amount</span>

<span class="c0">132</span>

<span class="c0">132</span>

<span class="c0">amount</span>

<span class="c0">133</span>

<span class="c0">133</span>

<span class="c0">Amount</span>

<span class="c0">134</span>

<span class="c0">134</span>

<span class="c0">amount</span>

<span class="c0">135</span>

<span class="c0">135</span>

<span class="c0">amount</span>

<span class="c0">136</span>

<span class="c0">136</span>

<span class="c0">amount</span>

<span class="c0">137</span>

<span class="c0">137</span>

<span class="c0">amount</span>

<span class="c0">138</span>

<span class="c0">138</span>

<span class="c0">amount</span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission and Monitoring of Client Money’s Compliance</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 430.67px;">![](images/image4.png)</span>

<span class="c21"></span>

<span id="t.dd9e045b9cc815b3e81be7ce41a27d2a442462fd"></span><span
id="t.4"></span>

<span class="c0">Business Lines</span>

<span class="c0">id</span>

<span class="c0">name</span>

<span class="c0">1</span>

<span class="c0">Life Insurance</span>

<span class="c0">2</span>

<span class="c0">Fire Insurance</span>

<span class="c0">3</span>

<span class="c0">Marine Cargo</span>

<span class="c0">4</span>

<span class="c0">Marine Hull</span>

<span class="c0">5</span>

<span class="c0">Aviation</span>

<span class="c0">6</span>

<span class="c0">Fidelity and Surety</span>

<span class="c0">7</span>

<span class="c0">Motor Insurance</span>

<span class="c0">8</span>

<span class="c0">Health Insurance</span>

<span class="c0">9</span>

<span class="c0">HMO</span>

<span class="c0">10</span>

<span class="c0">Accident</span>

<span class="c0">11</span>

<span class="c0">Engineering</span>

<span class="c0">12</span>

<span class="c0">Miscellaneous</span>

<span class="c21"></span>

1.  <span class="c75">HMO Division</span>
    -------------------------------------

<!-- -->

1.  ### <span class="c26">Annual Submission and Examination of Financial Statements</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 422.67px;">![](images/image41.png)</span>

<span class="c21"></span>

<span id="t.1a72f8a01f19be14ee73786c8e1d7f959c93e218"></span><span
id="t.5"></span>

<span class="c56">SFP Chart of Accounts</span>

<span class="c0">id</span>

<span class="c0">level\_id</span>

<span class="c0">parent\_id</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">ASSETS</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Current Assets</span>

<span class="c0">3</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Cash and Cash Equivalents</span>

<span class="c0">4</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash on Hand</span>

<span class="c0">5</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Undeposited Collections</span>

<span class="c0">6</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Petty Cash Fund</span>

<span class="c0">7</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Revolving Fund</span>

<span class="c0">8</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Commission Fund</span>

<span class="c0">9</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Other Funds</span>

<span class="c0">10</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash in Banks</span>

<span class="c0">11</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Current - Peso</span>

<span class="c0">12</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Current - Foreign</span>

<span class="c0">13</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Savings - Peso</span>

<span class="c0">14</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Savings - Foreign</span>

<span class="c0">15</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash Equivalents</span>

<span class="c0">16</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Time Deposits</span>

<span class="c0">17</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Money Market Instruments</span>

<span class="c0">18</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Others</span>

<span class="c0">19</span>

<span class="c0">2</span>

<span class="c0">3</span>

<span class="c0">Short-term Cash lnvestments</span>

<span class="c0">20</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Administrative Service Only (ASO) Cash Fund</span>

<span class="c0">21</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Trade and Other Receivables</span>

<span class="c0">22</span>

<span class="c0">3</span>

<span class="c0">21</span>

<span class="c0">Trade Receivables</span>

<span class="c0">23</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Membership Fee Receivable</span>

<span class="c0">24</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Riders Fee Receivable</span>

<span class="c0">25</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Deposit to Healthcare Providers</span>

<span class="c0">26</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Due from ASO Accounts</span>

<span class="c0">27</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Allowance for Bad Debts</span>

<span class="c0">28</span>

<span class="c0">3</span>

<span class="c0">21</span>

<span class="c0">Other Receivables</span>

<span class="c0">29</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Notes Receivable</span>

<span class="c0">30</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Interest Receivable</span>

<span class="c0">31</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Advances to Officers and Employees</span>

<span class="c0">32</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Due from Officers and Employees</span>

<span class="c0">33</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Due from Related Parties</span>

<span class="c0">34</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Others</span>

<span class="c0">35</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Allowance for Bad Debts</span>

<span class="c0">36</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Financial Assets at Fair Value Through Profit or
Loss</span>

<span class="c0">37</span>

<span class="c0">3</span>

<span class="c0">36</span>

<span class="c0">Securities Held for Trading</span>

<span class="c0">38</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Debt Securities - Government</span>

<span class="c0">39</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Debt Securities - Private</span>

<span class="c0">40</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Equity Securities</span>

<span class="c0">41</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds</span>

<span class="c0">42</span>

<span class="c0">3</span>

<span class="c0">36</span>

<span class="c0">Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)</span>

<span class="c0">43</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Debt Securities - Government</span>

<span class="c0">44</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Debt Securities - Private</span>

<span class="c0">45</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Equity Securities</span>

<span class="c0">46</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Mutual Funds and Unit Investment Trusts</span>

<span class="c0">47</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">48</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Others</span>

<span class="c0">49</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Held-to-Maturity (HTM) Investments - Current
Portion</span>

<span class="c0">50</span>

<span class="c0">3</span>

<span class="c0">49</span>

<span class="c0">HTM Debt Securities - Government - Current
Portion</span>

<span class="c0">51</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">52</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">HTM Debt Securities - Private - Current Portion</span>

<span class="c0">53</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">54</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">55</span>

<span class="c0">3</span>

<span class="c0">49</span>

<span class="c0">Available-for-Sale (AFS) Financial Assets - Current
Portion</span>

<span class="c0">56</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Debt Securities - Government - Current
Portion</span>

<span class="c0">57</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">58</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Debt Securities - Private - Current Portion</span>

<span class="c0">59</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">60</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Equity Securities - Current Portion</span>

<span class="c0">61</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">62</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Mutual Funds and Unit Investment Trusts - Current
Portion</span>

<span class="c0">63</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Real Estate Investment Trusts - Current Portion</span>

<span class="c0">64</span>

<span class="c0">4</span>

<span class="c0">64</span>

<span class="c0">Others - Current Portion</span>

<span class="c0">65</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Prepayments</span>

<span class="c0">66</span>

<span class="c0">4</span>

<span class="c0">64</span>

<span class="c0">Supplies</span>

<span class="c0">67</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Prepaid Commissions</span>

<span class="c0">68</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Prepaid Rent</span>

<span class="c0">69</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Prepaid Taxes</span>

<span class="c0">70</span>

<span class="c0">4</span>

<span class="c0">69</span>

<span class="c0">Others</span>

<span class="c0">71</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Other Current Assets</span>

<span class="c0">72</span>

<span class="c0">4</span>

<span class="c0">71</span>

<span class="c0">Non-current Assets</span>

<span class="c0">73</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Held-to-Maturity (HTM) Investments - Non-Current
Portion</span>

<span class="c0">74</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">HTM Debt Securities - Government - Non-Current
Portion</span>

<span class="c0">75</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">76</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">HTM Debt Securities - Private - Non-Current
Portion</span>

<span class="c0">77</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">78</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">79</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Available-for-Sale (AFS) Financial Assets - Non-Current
Portion</span>

<span class="c0">80</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">AFS Debt Securities - Government - Non-Current
Portion</span>

<span class="c0">81</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">82</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">AFS Debt Securities - Private - Non-Current
Portion</span>

<span class="c0">83</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">84</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">AFS Equity Securities - Non-Current Portion</span>

<span class="c0">85</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">86</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Mutual Funds and Unit Investment Trusts - Non-Current
Portion</span>

<span class="c0">87</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Real Estate Investment Trusts - Non-Current
Portion</span>

<span class="c0">88</span>

<span class="c0">4</span>

<span class="c0">87</span>

<span class="c0">Others - Non-Current Portion</span>

<span class="c0">89</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Investments in Subsidiaries, Associates and Joint
Ventures</span>

<span class="c0">90</span>

<span class="c0">4</span>

<span class="c0">89</span>

<span class="c0">Investment in Subsidiaries</span>

<span class="c0">91</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Investment in Associates</span>

<span class="c0">92</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Investment in Joint Ventures</span>

<span class="c0">93</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Property and Equipment</span>

<span class="c0">94</span>

<span class="c0">4</span>

<span class="c0">93</span>

<span class="c0">Land - At Cost</span>

<span class="c0">95</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">96</span>

<span class="c0">4</span>

<span class="c0">95</span>

<span class="c0">Accumulated Depreciation - Building and Building
Improvements</span>

<span class="c0">97</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Leasehold Improvements - At Cost</span>

<span class="c0">98</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Accumulated Depreciation - Leasehold
Improvements</span>

<span class="c0">99</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">IT Equipment - At Cost</span>

<span class="c0">100</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Accumulated Depreciation - IT Equipment</span>

<span class="c0">101</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Transportation Equipment - At Cost</span>

<span class="c0">102</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Accumulated Depreciation - Transportation
Equipment</span>

<span class="c0">103</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Office Furniture, Fixtures and Equipment - At
Cost</span>

<span class="c0">104</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Accumulated Depreciation - Office Furniture, Fixtures
and Equipemnt</span>

<span class="c0">105</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Medical Equipment</span>

<span class="c0">106</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Accumulated Depreciation - Medical Equipment</span>

<span class="c0">107</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Property and Equipment Under Finance Lease</span>

<span class="c0">108</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulated Depreciation - P&E under Finance
Lease</span>

<span class="c0">109</span>

<span class="c0">4</span>

<span class="c0">108</span>

<span class="c0">Revaluation Increment</span>

<span class="c0">110</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulated Depreciation - Revaluation Increment</span>

<span class="c0">111</span>

<span class="c0">4</span>

<span class="c0">110</span>

<span class="c0">Investment Property</span>

<span class="c0">112</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Land - At Cost</span>

<span class="c0">113</span>

<span class="c0">4</span>

<span class="c0">112</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">114</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulate Depreciation - Building and Building
Improvements</span>

<span class="c0">115</span>

<span class="c0">4</span>

<span class="c0">114</span>

<span class="c0">Accumulated Impairment Losses</span>

<span class="c0">116</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Land - At Fair Value</span>

<span class="c0">117</span>

<span class="c0">4</span>

<span class="c0">117</span>

<span class="c0">Building and Building Improvements - At Fair
Value</span>

<span class="c0">118</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Foreclosed Properties</span>

<span class="c0">119</span>

<span class="c0">4</span>

<span class="c0">118</span>

<span class="c0">Intangible Assets</span>

<span class="c0">120</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Deferred Tax Assets</span>

<span class="c0">121</span>

<span class="c0">4</span>

<span class="c0">120</span>

<span class="c0">Other Non-Current Assets</span>

<span class="c0">122</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Miscellaneous Deposits</span>

<span class="c0">123</span>

<span class="c0">4</span>

<span class="c0">122</span>

<span class="c0">Deferred Input VAT</span>

<span class="c0">124</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Retirement Pension Asset</span>

<span class="c0">125</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Others</span>

<span class="c0">126</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">LIABILITIES</span>

<span class="c0">127</span>

<span class="c0">4</span>

<span class="c0">126</span>

<span class="c0">Current Liabilities</span>

<span class="c0">128</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">HMO Agreement Liabilities</span>

<span class="c0">129</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">HMO Agreement Reserves</span>

<span class="c0">130</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Claims Reserve</span>

<span class="c0">131</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Due and Unpaid (D&U) Claims</span>

<span class="c0">132</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">In Course of Settlement (ICOS)</span>

<span class="c0">133</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Resisted Claims</span>

<span class="c0">134</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Incurred but not Reported (IBNR)</span>

<span class="c0">135</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Claims Handling Expense</span>

<span class="c0">136</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Membership Fee Reserves</span>

<span class="c0">137</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Other Reserves</span>

<span class="c0">138</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Deficiency Reserves</span>

<span class="c0">139</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Agreement Reserves</span>

<span class="c0">140</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">ASO Reserves</span>

<span class="c0">141</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Administrative Service Only (ASO) Funds</span>

<span class="c0">142</span>

<span class="c0">3</span>

<span class="c0">141</span>

<span class="c0">Unearned Administrative Fee Reserves (UAFR)</span>

<span class="c0">143</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Fund Withdrawals</span>

<span class="c0">144</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Liabilities Due to Insurance and Providers</span>

<span class="c0">145</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Commission Payable</span>

<span class="c0">146</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Accounts Payable</span>

<span class="c0">147</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">SSS Premium Payable</span>

<span class="c0">148</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">SSS Loans Payable</span>

<span class="c0">149</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Pag-Ibig Premiums Payable</span>

<span class="c0">150</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Pag-Ibig Loans Payable</span>

<span class="c0">151</span>

<span class="c0">5</span>

<span class="c0">150</span>

<span class="c0">PhilHealth Premiums Payable</span>

<span class="c0">152</span>

<span class="c0">5</span>

<span class="c0">150</span>

<span class="c0">Other Accounts Payable</span>

<span class="c0">153</span>

<span class="c0">3</span>

<span class="c0">141</span>

<span class="c0">Due to Related Parties</span>

<span class="c0">154</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Expenses</span>

<span class="c0">155</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Utilities</span>

<span class="c0">156</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Services</span>

<span class="c0">157</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Accrued Interest</span>

<span class="c0">158</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Notes Payable - Current Portion</span>

<span class="c0">159</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Dividends Payable</span>

<span class="c0">160</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Taxes Payable</span>

<span class="c0">161</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Value-added Tax (VAT) Payable</span>

<span class="c0">162</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Withholding Taxes Payable</span>

<span class="c0">163</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Income Tax Payable</span>

<span class="c0">164</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Other Current Liabilities</span>

<span class="c0">165</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Non-current Liabilities</span>

<span class="c0">166</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Notes Payable - Non-current Portion</span>

<span class="c0">167</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Pension Benefit Obligation</span>

<span class="c0">168</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">Deferred Tax Liability</span>

<span class="c0">169</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">Other Non-current Liabilities</span>

<span class="c0">170</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">EQUITY</span>

<span class="c0">171</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Share Capital</span>

<span class="c0">172</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Preferred Share Capital</span>

<span class="c0">173</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Common Share Capital</span>

<span class="c0">174</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscribed Share Capital</span>

<span class="c0">175</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscribed Preferred - Shares</span>

<span class="c0">176</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscription Receivable - Preferred Shares</span>

<span class="c0">177</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Subscribed Common Share Capital</span>

<span class="c0">178</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Subscription Receivable - Common Shares</span>

<span class="c0">179</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Share Dividend Distributable</span>

<span class="c0">180</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Capital Paid In Excess of Par</span>

<span class="c0">181</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Retained Earnings (Deficit)</span>

<span class="c0">182</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Retained Earnings - Appropriated</span>

<span class="c0">183</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Retained Earnings - Unappropriated</span>

<span class="c0">184</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">Reserves</span>

<span class="c0">185</span>

<span class="c0">2</span>

<span class="c0">186</span>

<span class="c0">Deposit for Future Subscription</span>

<span class="c0">186</span>

<span class="c0">2</span>

<span class="c0">186</span>

<span class="c0">Revaluation Reserves</span>

<span class="c0">187</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">Net Unrealized Gains (Losses) on AFS Investments</span>

<span class="c0">188</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0">Cumulative Translation Adjustment</span>

<span class="c0">189</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0">Treasury Shares</span>

<span class="c0">190</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0"></span>

<span class="c0">191</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0"></span>

<span class="c0">192</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">3 Share Dividend Distributable</span>

<span class="c0">193</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">4. Capital Paid in Excess of Par</span>

<span class="c0">194</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">4 Retained Earnings (Deficit)</span>

<span class="c0">195</span>

<span class="c0">2</span>

<span class="c0">194</span>

<span class="c0">4.1 Retained Earnings - Appropriated</span>

<span class="c0">196</span>

<span class="c0">2</span>

<span class="c0">194</span>

<span class="c0">4.2 Retained Earnings - Unappropriated</span>

<span class="c0">197</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">5. Reserves</span>

<span class="c0">198</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.1 Deposit for Future Subscription</span>

<span class="c0">199</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.2 Revaluation Reserves</span>

<span class="c0">200</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.3 Net Unrealized Gains (Losses) on AFS
Investments</span>

<span class="c0">201</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.4 Cumulative Translation Adjustments</span>

<span class="c0">202</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">6. Treasure Share</span>

<span class="c21"></span>

<span id="t.da4d6262adb5228a642d1c267a4b6feb4424ce6b"></span><span
id="t.6"></span>

<span class="c56">SCI Chart of Accounts</span>

<span class="c0">id</span>

<span class="c0">level\_id</span>

<span class="c0">parent\_id</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Comprehensive Income</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Other Comprehensive Income</span>

<span class="c0">3</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Net Income After Taxes</span>

<span class="c0">4</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Unrealized Gains (Losses) on Investments</span>

<span class="c0">5</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Unrealized Foreign Exchange on Gain (Loss)</span>

<span class="c0">6</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Provision for Income Taxes</span>

<span class="c0">7</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Net Income Before Taxes</span>

<span class="c0">8</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Income (Loss) from Operations</span>

<span class="c0">9</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Other Income (Expenses)</span>

<span class="c0">10</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Dividend Income</span>

<span class="c0">11</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Interest Income</span>

<span class="c0">12</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Gain(Loss) on Sale of Property and Equipment</span>

<span class="c0">13</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Gain(Loss) On Sale of Investments</span>

<span class="c0">14</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Foreign Exchange Gain (Loss)</span>

<span class="c0">15</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Other Revenues</span>

<span class="c0">16</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Interest Expense</span>

<span class="c0">17</span>

<span class="c0">5</span>

<span class="c0">8</span>

<span class="c0">Operating Expenses</span>

<span class="c0">18</span>

<span class="c0">5</span>

<span class="c0">8</span>

<span class="c0">Gross Profit</span>

<span class="c0">19</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Commision Expenses</span>

<span class="c0">20</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Salaries And Wages</span>

<span class="c0">21</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">SSS Contributions</span>

<span class="c0">22</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Philhealth Contributions</span>

<span class="c0">23</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Pag-Ibig Contribution</span>

<span class="c0">24</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employees Compensation And Maternity
Contributions</span>

<span class="c0">25</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Hospitalization Contribution</span>

<span class="c0">26</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Medical Supplies</span>

<span class="c0">27</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employee’s Welfare</span>

<span class="c0">28</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employee Benefits</span>

<span class="c0">29</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Post-Employment Benefit Cost</span>

<span class="c0">30</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Professional and Technical Development</span>

<span class="c0">31</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Representation and Entertainment</span>

<span class="c0">32</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Transportation and Travel Expenses</span>

<span class="c0">33</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Investment Management Fees</span>

<span class="c0">34</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Director's Fees and Allowances</span>

<span class="c0">35</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Corporate Secretary's Fees</span>

<span class="c0">36</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Auditors' Fees</span>

<span class="c0">37</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Actuarial Fees</span>

<span class="c0">38</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Service Fees</span>

<span class="c0">39</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Legal Fees</span>

<span class="c0">40</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Association Dues</span>

<span class="c0">41</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Light and Water</span>

<span class="c0">42</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Communication and Postage</span>

<span class="c0">43</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Printing, Stationery and Supplies</span>

<span class="c0">44</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Books and Periodicals</span>

<span class="c0">45</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Advertising and Promotions</span>

<span class="c0">46</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Contributions and Donations</span>

<span class="c0">47</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Rental Expense</span>

<span class="c0">48</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Insurance Expenses</span>

<span class="c0">49</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Taxes and Licenses</span>

<span class="c0">50</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Bank Charges</span>

<span class="c0">51</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Repairs and Maintenance - Materials</span>

<span class="c0">52</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Repairs and Maintenance - Labor</span>

<span class="c0">53</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Depreciation and Amortization</span>

<span class="c0">54</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Share in Profit/Loss of Associates and Joint
Ventures</span>

<span class="c0">55</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Provision for Impairment Losses</span>

<span class="c0">56</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Miscellaneous Expenses</span>

<span class="c0">57</span>

<span class="c0">6</span>

<span class="c0">18</span>

<span class="c0">Healthcare Benefits and Claims</span>

<span class="c0">58</span>

<span class="c0">6</span>

<span class="c0">18</span>

<span class="c0">Revenues Earned</span>

<span class="c0">59</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Medical Services</span>

<span class="c0">60</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Hospitalization</span>

<span class="c0">61</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Hospital Professional Fees</span>

<span class="c0">62</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Other Benefits and Claims</span>

<span class="c0">63</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Membership Fees</span>

<span class="c0">64</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Enrolment Fees</span>

<span class="c0">65</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Administrative Fees</span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission and Examination of Interim Financial Statements</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 422.67px;">![](images/image20.png)</span>

<span class="c21"></span>

<span id="t.b58447be6490b4c9ee882a28668e71cec7f955a8"></span><span
id="t.7"></span>

<span class="c56">SFP Chart of Accounts</span>

<span class="c0">id</span>

<span class="c0">level\_id</span>

<span class="c0">parent\_id</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">ASSETS</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Current Assets</span>

<span class="c0">3</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Cash and Cash Equivalents</span>

<span class="c0">4</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash on Hand</span>

<span class="c0">5</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Undeposited Collections</span>

<span class="c0">6</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Petty Cash Fund</span>

<span class="c0">7</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Revolving Fund</span>

<span class="c0">8</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Commission Fund</span>

<span class="c0">9</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">Other Funds</span>

<span class="c0">10</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash in Banks</span>

<span class="c0">11</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Current - Peso</span>

<span class="c0">12</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Current - Foreign</span>

<span class="c0">13</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Savings - Peso</span>

<span class="c0">14</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">Savings - Foreign</span>

<span class="c0">15</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Cash Equivalents</span>

<span class="c0">16</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Time Deposits</span>

<span class="c0">17</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Money Market Instruments</span>

<span class="c0">18</span>

<span class="c0">3</span>

<span class="c0">10</span>

<span class="c0">Others</span>

<span class="c0">19</span>

<span class="c0">2</span>

<span class="c0">3</span>

<span class="c0">Short-term Cash lnvestments</span>

<span class="c0">20</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Administrative Service Only (ASO) Cash Fund</span>

<span class="c0">21</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Trade and Other Receivables</span>

<span class="c0">22</span>

<span class="c0">3</span>

<span class="c0">21</span>

<span class="c0">Trade Receivables</span>

<span class="c0">23</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Membership Fee Receivable</span>

<span class="c0">24</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Riders Fee Receivable</span>

<span class="c0">25</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Deposit to Healthcare Providers</span>

<span class="c0">26</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Due from ASO Accounts</span>

<span class="c0">27</span>

<span class="c0">4</span>

<span class="c0">22</span>

<span class="c0">Allowance for Bad Debts</span>

<span class="c0">28</span>

<span class="c0">3</span>

<span class="c0">21</span>

<span class="c0">Other Receivables</span>

<span class="c0">29</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Notes Receivable</span>

<span class="c0">30</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Interest Receivable</span>

<span class="c0">31</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Advances to Officers and Employees</span>

<span class="c0">32</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Due from Officers and Employees</span>

<span class="c0">33</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Due from Related Parties</span>

<span class="c0">34</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Others</span>

<span class="c0">35</span>

<span class="c0">4</span>

<span class="c0">28</span>

<span class="c0">Allowance for Bad Debts</span>

<span class="c0">36</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Financial Assets at Fair Value Through Profit or
Loss</span>

<span class="c0">37</span>

<span class="c0">3</span>

<span class="c0">36</span>

<span class="c0">Securities Held for Trading</span>

<span class="c0">38</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Debt Securities - Government</span>

<span class="c0">39</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Debt Securities - Private</span>

<span class="c0">40</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Trading Equity Securities</span>

<span class="c0">41</span>

<span class="c0">4</span>

<span class="c0">37</span>

<span class="c0">Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds</span>

<span class="c0">42</span>

<span class="c0">3</span>

<span class="c0">36</span>

<span class="c0">Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)</span>

<span class="c0">43</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Debt Securities - Government</span>

<span class="c0">44</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Debt Securities - Private</span>

<span class="c0">45</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Equity Securities</span>

<span class="c0">46</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Mutual Funds and Unit Investment Trusts</span>

<span class="c0">47</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">48</span>

<span class="c0">4</span>

<span class="c0">42</span>

<span class="c0">Others</span>

<span class="c0">49</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Held-to-Maturity (HTM) Investments - Current
Portion</span>

<span class="c0">50</span>

<span class="c0">3</span>

<span class="c0">49</span>

<span class="c0">HTM Debt Securities - Government - Current
Portion</span>

<span class="c0">51</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">52</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">HTM Debt Securities - Private - Current Portion</span>

<span class="c0">53</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">54</span>

<span class="c0">4</span>

<span class="c0">50</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">55</span>

<span class="c0">3</span>

<span class="c0">49</span>

<span class="c0">Available-for-Sale (AFS) Financial Assets - Current
Portion</span>

<span class="c0">56</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Debt Securities - Government - Current
Portion</span>

<span class="c0">57</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">58</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Debt Securities - Private - Current Portion</span>

<span class="c0">59</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">60</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">AFS Equity Securities - Current Portion</span>

<span class="c0">61</span>

<span class="c0">4</span>

<span class="c0">55</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">62</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Mutual Funds and Unit Investment Trusts - Current
Portion</span>

<span class="c0">63</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Real Estate Investment Trusts - Current Portion</span>

<span class="c0">64</span>

<span class="c0">4</span>

<span class="c0">64</span>

<span class="c0">Others - Current Portion</span>

<span class="c0">65</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Prepayments</span>

<span class="c0">66</span>

<span class="c0">4</span>

<span class="c0">64</span>

<span class="c0">Supplies</span>

<span class="c0">67</span>

<span class="c0">3</span>

<span class="c0">62</span>

<span class="c0">Prepaid Commissions</span>

<span class="c0">68</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Prepaid Rent</span>

<span class="c0">69</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Prepaid Taxes</span>

<span class="c0">70</span>

<span class="c0">4</span>

<span class="c0">69</span>

<span class="c0">Others</span>

<span class="c0">71</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Other Current Assets</span>

<span class="c0">72</span>

<span class="c0">4</span>

<span class="c0">71</span>

<span class="c0">Non-current Assets</span>

<span class="c0">73</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Held-to-Maturity (HTM) Investments - Non-Current
Portion</span>

<span class="c0">74</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">HTM Debt Securities - Government - Non-Current
Portion</span>

<span class="c0">75</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">76</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">HTM Debt Securities - Private - Non-Current
Portion</span>

<span class="c0">77</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">78</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">79</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Available-for-Sale (AFS) Financial Assets - Non-Current
Portion</span>

<span class="c0">80</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">AFS Debt Securities - Government - Non-Current
Portion</span>

<span class="c0">81</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">82</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">AFS Debt Securities - Private - Non-Current
Portion</span>

<span class="c0">83</span>

<span class="c0">3</span>

<span class="c0">78</span>

<span class="c0">Unamortized (Discount)/Premium</span>

<span class="c0">84</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">AFS Equity Securities - Non-Current Portion</span>

<span class="c0">85</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">86</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Mutual Funds and Unit Investment Trusts - Non-Current
Portion</span>

<span class="c0">87</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Real Estate Investment Trusts - Non-Current
Portion</span>

<span class="c0">88</span>

<span class="c0">4</span>

<span class="c0">87</span>

<span class="c0">Others - Non-Current Portion</span>

<span class="c0">89</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Investments in Subsidiaries, Associates and Joint
Ventures</span>

<span class="c0">90</span>

<span class="c0">4</span>

<span class="c0">89</span>

<span class="c0">Investment in Subsidiaries</span>

<span class="c0">91</span>

<span class="c0">3</span>

<span class="c0">86</span>

<span class="c0">Investment in Associates</span>

<span class="c0">92</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Investment in Joint Ventures</span>

<span class="c0">93</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Property and Equipment</span>

<span class="c0">94</span>

<span class="c0">4</span>

<span class="c0">93</span>

<span class="c0">Land - At Cost</span>

<span class="c0">95</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">96</span>

<span class="c0">4</span>

<span class="c0">95</span>

<span class="c0">Accumulated Depreciation - Building and Building
Improvements</span>

<span class="c0">97</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Leasehold Improvements - At Cost</span>

<span class="c0">98</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Accumulated Depreciation - Leasehold
Improvements</span>

<span class="c0">99</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">IT Equipment - At Cost</span>

<span class="c0">100</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Accumulated Depreciation - IT Equipment</span>

<span class="c0">101</span>

<span class="c0">3</span>

<span class="c0">92</span>

<span class="c0">Transportation Equipment - At Cost</span>

<span class="c0">102</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Accumulated Depreciation - Transportation
Equipment</span>

<span class="c0">103</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Office Furniture, Fixtures and Equipment - At
Cost</span>

<span class="c0">104</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Accumulated Depreciation - Office Furniture, Fixtures
and Equipment</span>

<span class="c0">105</span>

<span class="c0">3</span>

<span class="c0">102</span>

<span class="c0">Medical Equipment</span>

<span class="c0">106</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Accumulated Depreciation - Medical Equipment</span>

<span class="c0">107</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Property and Equipment Under Finance Lease</span>

<span class="c0">108</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulated Depreciation - P&E under Finance
Lease</span>

<span class="c0">109</span>

<span class="c0">4</span>

<span class="c0">108</span>

<span class="c0">Revaluation Increment</span>

<span class="c0">110</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulated Depreciation - Revaluation Increment</span>

<span class="c0">111</span>

<span class="c0">4</span>

<span class="c0">110</span>

<span class="c0">Investment Property</span>

<span class="c0">112</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Land - At Cost</span>

<span class="c0">113</span>

<span class="c0">4</span>

<span class="c0">112</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">114</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Accumulated Depreciation - Building and Building
Improvements</span>

<span class="c0">115</span>

<span class="c0">4</span>

<span class="c0">114</span>

<span class="c0">Accumulated Impairment Losses</span>

<span class="c0">116</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Land - At Fair Value</span>

<span class="c0">117</span>

<span class="c0">4</span>

<span class="c0">117</span>

<span class="c0">Building and Building Improvements - At Fair
Value</span>

<span class="c0">118</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Foreclosed Properties</span>

<span class="c0">119</span>

<span class="c0">4</span>

<span class="c0">118</span>

<span class="c0">Intangible Assets</span>

<span class="c0">120</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Deferred Tax Assets</span>

<span class="c0">121</span>

<span class="c0">4</span>

<span class="c0">120</span>

<span class="c0">Other Non-Current Assets</span>

<span class="c0">122</span>

<span class="c0">3</span>

<span class="c0">106</span>

<span class="c0">Miscellaneous Deposits</span>

<span class="c0">123</span>

<span class="c0">4</span>

<span class="c0">122</span>

<span class="c0">Deferred Input VAT</span>

<span class="c0">124</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Retirement Pension Asset</span>

<span class="c0">125</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Others</span>

<span class="c0">126</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">LIABILITIES</span>

<span class="c0">127</span>

<span class="c0">4</span>

<span class="c0">126</span>

<span class="c0">Current Liabilities</span>

<span class="c0">128</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">HMO Agreement Liabilities</span>

<span class="c0">129</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">HMO Agreement Reserves</span>

<span class="c0">130</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Claims Reserve</span>

<span class="c0">131</span>

<span class="c0">3</span>

<span class="c0">124</span>

<span class="c0">Due and Unpaid (D&U) Claims</span>

<span class="c0">132</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">In Course of Settlement (ICOS)</span>

<span class="c0">133</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Resisted Claims</span>

<span class="c0">134</span>

<span class="c0">2</span>

<span class="c0">85</span>

<span class="c0">Incurred but not Reported (IBNR)</span>

<span class="c0">135</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Claims Handling Expense</span>

<span class="c0">136</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Membership Fee Reserves</span>

<span class="c0">137</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Other Reserves</span>

<span class="c0">138</span>

<span class="c0">3</span>

<span class="c0">134</span>

<span class="c0">Deficiency Reserves</span>

<span class="c0">139</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Agreement Reserves</span>

<span class="c0">140</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">ASO Reserves</span>

<span class="c0">141</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Administrative Service Only (ASO) Funds</span>

<span class="c0">142</span>

<span class="c0">3</span>

<span class="c0">141</span>

<span class="c0">Unearned Administrative Fee Reserves (UAFR)</span>

<span class="c0">143</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Fund Withdrawals</span>

<span class="c0">144</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Liabilities Due to Insurance and Providers</span>

<span class="c0">145</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Commission Payable</span>

<span class="c0">146</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">Accounts Payable</span>

<span class="c0">147</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">SSS Premium Payable</span>

<span class="c0">148</span>

<span class="c0">5</span>

<span class="c0">143</span>

<span class="c0">SSS Loans Payable</span>

<span class="c0">149</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Pag-Ibig Premiums Payable</span>

<span class="c0">150</span>

<span class="c0">4</span>

<span class="c0">142</span>

<span class="c0">Pag-Ibig Loans Payable</span>

<span class="c0">151</span>

<span class="c0">5</span>

<span class="c0">150</span>

<span class="c0">PhilHealth Premiums Payable</span>

<span class="c0">152</span>

<span class="c0">5</span>

<span class="c0">150</span>

<span class="c0">Other Accounts Payable</span>

<span class="c0">153</span>

<span class="c0">3</span>

<span class="c0">141</span>

<span class="c0">Due to Related Parties</span>

<span class="c0">154</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Expenses</span>

<span class="c0">155</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Utilities</span>

<span class="c0">156</span>

<span class="c0">4</span>

<span class="c0">153</span>

<span class="c0">Accrued Services</span>

<span class="c0">157</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Accrued Interest</span>

<span class="c0">158</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Notes Payable - Current Portion</span>

<span class="c0">159</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Dividends Payable</span>

<span class="c0">160</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Taxes Payable</span>

<span class="c0">161</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Value-added Tax (VAT) Payable</span>

<span class="c0">162</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Withholding Taxes Payable</span>

<span class="c0">163</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Income Tax Payable</span>

<span class="c0">164</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Other Current Liabilities</span>

<span class="c0">165</span>

<span class="c0">3</span>

<span class="c0">159</span>

<span class="c0">Non-current Liabilities</span>

<span class="c0">166</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Notes Payable - Non-current Portion</span>

<span class="c0">167</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Pension Benefit Obligation</span>

<span class="c0">168</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">Deferred Tax Liability</span>

<span class="c0">169</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">Other Non-current Liabilities</span>

<span class="c0">170</span>

<span class="c0">3</span>

<span class="c0">167</span>

<span class="c0">EQUITY</span>

<span class="c0">171</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Share Capital</span>

<span class="c0">172</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Preferred Share Capital</span>

<span class="c0">173</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Common Share Capital</span>

<span class="c0">174</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscribed Share Capital</span>

<span class="c0">175</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscribed Preferred - Shares</span>

<span class="c0">176</span>

<span class="c0">3</span>

<span class="c0">173</span>

<span class="c0">Subscription Receivable - Preferred Shares</span>

<span class="c0">177</span>

<span class="c0">2</span>

<span class="c0">140</span>

<span class="c0">Subscribed Common Share Capital</span>

<span class="c0">178</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Subscription Receivable - Common Shares</span>

<span class="c0">179</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Share Dividend Distributable</span>

<span class="c0">180</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Capital Paid In Excess of Par</span>

<span class="c0">181</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Retained Earnings (Deficit)</span>

<span class="c0">182</span>

<span class="c0">2</span>

<span class="c0">178</span>

<span class="c0">Retained Earnings - Appropriated</span>

<span class="c0">183</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Retained Earnings - Unappropriated</span>

<span class="c0">184</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">Reserves</span>

<span class="c0">185</span>

<span class="c0">2</span>

<span class="c0">186</span>

<span class="c0">Deposit for Future Subscription</span>

<span class="c0">186</span>

<span class="c0">2</span>

<span class="c0">186</span>

<span class="c0">Revaluation Reserves</span>

<span class="c0">187</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">Net Unrealized Gains (Losses) on AFS Investments</span>

<span class="c0">188</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0">Cumulative Translation Adjustment</span>

<span class="c0">189</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0">Treasury Shares</span>

<span class="c0">190</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0"></span>

<span class="c0">191</span>

<span class="c0">2</span>

<span class="c0">187</span>

<span class="c0"></span>

<span class="c0">192</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">3 Share Dividend Distributable</span>

<span class="c0">193</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">4. Capital Paid in Excess of Par</span>

<span class="c0">194</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">4 Retained Earnings (Deficit)</span>

<span class="c0">195</span>

<span class="c0">2</span>

<span class="c0">194</span>

<span class="c0">4.1 Retained Earnings - Appropriated</span>

<span class="c0">196</span>

<span class="c0">2</span>

<span class="c0">194</span>

<span class="c0">4.2 Retained Earnings - Unappropriated</span>

<span class="c0">197</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">5. Reserves</span>

<span class="c0">198</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.1 Deposit for Future Subscription</span>

<span class="c0">199</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.2 Revaluation Reserves</span>

<span class="c0">200</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.3 Net Unrealized Gains (Losses) on AFS
Investments</span>

<span class="c0">201</span>

<span class="c0">2</span>

<span class="c0">197</span>

<span class="c0">5.4 Cumulative Translation Adjustments</span>

<span class="c0">202</span>

<span class="c0">1</span>

<span class="c0">183</span>

<span class="c0">6. Treasure Share</span>

<span class="c21"></span>

<span id="t.da4d6262adb5228a642d1c267a4b6feb4424ce6b"></span><span
id="t.8"></span>

<span class="c56">SCI Chart of Accounts</span>

<span class="c0">id</span>

<span class="c0">level\_id</span>

<span class="c0">parent\_id</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Comprehensive Income</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Other Comprehensive Income</span>

<span class="c0">3</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Net Income After Taxes</span>

<span class="c0">4</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Unrealized Gains (Losses) on Investments</span>

<span class="c0">5</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">Unrealized Foreign Exchange on Gain (Loss)</span>

<span class="c0">6</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Provision for Income Taxes</span>

<span class="c0">7</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">Net Income Before Taxes</span>

<span class="c0">8</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Income (Loss) from Operations</span>

<span class="c0">9</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Other Income (Expenses)</span>

<span class="c0">10</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Dividend Income</span>

<span class="c0">11</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Interest Income</span>

<span class="c0">12</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Gain(Loss) on Sale of Property and Equipment</span>

<span class="c0">13</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Gain(Loss) On Sale of Investments</span>

<span class="c0">14</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Foreign Exchange Gain (Loss)</span>

<span class="c0">15</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Other Revenues</span>

<span class="c0">16</span>

<span class="c0">5</span>

<span class="c0">9</span>

<span class="c0">Interest Expense</span>

<span class="c0">17</span>

<span class="c0">5</span>

<span class="c0">8</span>

<span class="c0">Operating Expenses</span>

<span class="c0">18</span>

<span class="c0">5</span>

<span class="c0">8</span>

<span class="c0">Gross Profit</span>

<span class="c0">19</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Commision Expenses</span>

<span class="c0">20</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Salaries And Wages</span>

<span class="c0">21</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">SSS Contributions</span>

<span class="c0">22</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Philhealth Contributions</span>

<span class="c0">23</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Pag-Ibig Contribution</span>

<span class="c0">24</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employees Compensation And Maternity
Contributions</span>

<span class="c0">25</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Hospitalization Contribution</span>

<span class="c0">26</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Medical Supplies</span>

<span class="c0">27</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employee’s Welfare</span>

<span class="c0">28</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Employee Benefits</span>

<span class="c0">29</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Post-Employment Benefit Cost</span>

<span class="c0">30</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Professional and Technical Development</span>

<span class="c0">31</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Representation and Entertainment</span>

<span class="c0">32</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Transportation and Travel Expenses</span>

<span class="c0">33</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Investment Management Fees</span>

<span class="c0">34</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Director's Fees and Allowances</span>

<span class="c0">35</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Corporate Secretary's Fees</span>

<span class="c0">36</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Auditors' Fees</span>

<span class="c0">37</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Actuarial Fees</span>

<span class="c0">38</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Service Fees</span>

<span class="c0">39</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Legal Fees</span>

<span class="c0">40</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Association Dues</span>

<span class="c0">41</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Light and Water</span>

<span class="c0">42</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Communication and Postage</span>

<span class="c0">43</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Printing, Stationery and Supplies</span>

<span class="c0">44</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Books and Periodicals</span>

<span class="c0">45</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Advertising and Promotions</span>

<span class="c0">46</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Contributions and Donations</span>

<span class="c0">47</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Rental Expense</span>

<span class="c0">48</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Insurance Expenses</span>

<span class="c0">49</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Taxes and Licenses</span>

<span class="c0">50</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Bank Charges</span>

<span class="c0">51</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Repairs and Maintenance - Materials</span>

<span class="c0">52</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Repairs and Maintenance - Labor</span>

<span class="c0">53</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Depreciation and Amortization</span>

<span class="c0">54</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Share in Profit/Loss of Associates and Joint
Ventures</span>

<span class="c0">55</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Provision for Impairment Losses</span>

<span class="c0">56</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">Miscellaneous Expenses</span>

<span class="c0">57</span>

<span class="c0">6</span>

<span class="c0">18</span>

<span class="c0">Healthcare Benefits and Claims</span>

<span class="c0">58</span>

<span class="c0">6</span>

<span class="c0">18</span>

<span class="c0">Revenues Earned</span>

<span class="c0">59</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Medical Services</span>

<span class="c0">60</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Hospitalization</span>

<span class="c0">61</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Hospital Professional Fees</span>

<span class="c0">62</span>

<span class="c0">7</span>

<span class="c0">57</span>

<span class="c0">Other Benefits and Claims</span>

<span class="c0">63</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Membership Fees</span>

<span class="c0">64</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Enrolment Fees</span>

<span class="c0">65</span>

<span class="c0">7</span>

<span class="c0">58</span>

<span class="c0">Administrative Fees</span>

<span class="c21"></span>

1.  <span class="c75">Life/ MBA/ Trust Division</span>
    --------------------------------------------------

<!-- -->

1.  ### <span class="c146">Annual Submission and Examination of Financial Standing of Life Companies with Separate Presentation for Composite Insurance Companies</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 181.00px; height: 217.00px;">![](images/image2.png)</span>

1.  ### <span class="c26">Annual Submission and Examination of Mutual Benefit Association (MBA)</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 181.00px; height: 217.00px;">![](images/image2.png)</span><span>                </span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report</span>

<span class="c21"></span>

<span class="c21">                Quarterly - RBC, AVR</span>

<span class="c21">                </span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 373.33px;">![](images/image37.png)</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21">Quarterly - SFP, SCI</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 250.67px;">![](images/image22.png)</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.0ca9e9d007e5cf7a896d2d6ab585b880408997de"></span><span
id="t.9"></span>

<span class="c0">SFP Chart of Accounts</span>

<span class="c0">id</span>

<span class="c0">level\_id</span>

<span class="c0">parent\_id</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">-</span>

<span class="c0">Total Assets</span>

<span class="c0">2</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Cash on Hand</span>

<span class="c0">3</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Cash in Banks</span>

<span class="c0">4</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Time Deposits</span>

<span class="c0">5</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Premiums Due and Uncollected</span>

<span class="c0">6</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Due from Ceding Companies, net</span>

<span class="c0">7</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Funds Held By Ceding Companies, net</span>

<span class="c0">8</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Amounts Recoverable from Reinsurers, net</span>

<span class="c0">9</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Financial Assets at Fair Value Through Profit or
Loss</span>

<span class="c0">10</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Held-to-Maturity (HTM) Investments</span>

<span class="c0">11</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Loans and Receivables</span>

<span class="c0">12</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Available-for-Sale (AFS) Financial Assets</span>

<span class="c0">13</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Investments Income Due and Accrued</span>

<span class="c0">14</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Accounts Receivable</span>

<span class="c0">15</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Investments in Subsidiaries, Associates and Joint
Ventures</span>

<span class="c0">16</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Segregated Fund Assets</span>

<span class="c0">17</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Property and Equipment</span>

<span class="c0">18</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Investment Property</span>

<span class="c0">19</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Non-current Assets Held for Sale</span>

<span class="c0">20</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Receivable from Life Insurance Pools</span>

<span class="c0">21</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Subscription Receivable</span>

<span class="c0">22</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Security Fund Contribution</span>

<span class="c0">23</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Pension Asset</span>

<span class="c0">24</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Derivative Assets Held for Hedging</span>

<span class="c0">25</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">Other Assets </span>

<span class="c0">26</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Undeposited Collections</span>

<span class="c0">27</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Petty Cash Fund</span>

<span class="c0">28</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Commission Fund</span>

<span class="c0">29</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Policy Loan Fund</span>

<span class="c0">30</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Documentary Stamps Fund</span>

<span class="c0">31</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">Other Funds</span>

<span class="c0">32</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Current - Peso</span>

<span class="c0">33</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Current - Foreign</span>

<span class="c0">34</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Savings - Peso</span>

<span class="c0">35</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Savings - Foreign</span>

<span class="c0">36</span>

<span class="c0">2</span>

<span class="c0">4</span>

<span class="c0">Peso Currency</span>

<span class="c0">37</span>

<span class="c0">2</span>

<span class="c0">4</span>

<span class="c0">Foreign Currency</span>

<span class="c0">38</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">Premiums Due from Ceding Companies - Treaty</span>

<span class="c0">39</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">Premiums Due from Ceding Companies - Facultative</span>

<span class="c0">40</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">41</span>

<span class="c0">5</span>

<span class="c0">7</span>

<span class="c0">Funds Held By Ceding Companies</span>

<span class="c0">42</span>

<span class="c0">5</span>

<span class="c0">7</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">43</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Reinsurance Recoverable on Paid Losses - Treaty</span>

<span class="c0">44</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Reinsurance Recoverable on Paid Losses -
Facultative</span>

<span class="c0">45</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Reinsurance Recoverable on Unpaid Losses -
Treaty</span>

<span class="c0">46</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Reinsurance Recoverable on Unpaid Losses -
Facultative</span>

<span class="c0">47</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">48</span>

<span class="c0">7</span>

<span class="c0">9</span>

<span class="c0">Securities Held for Trading</span>

<span class="c0">49</span>

<span class="c0">7</span>

<span class="c0">9</span>

<span class="c0">Financial Assets Designated at Fair Value Through
Profit or Loss (FVPL)</span>

<span class="c0">50</span>

<span class="c0">7</span>

<span class="c0">9</span>

<span class="c0">Derivative Assets</span>

<span class="c0">51</span>

<span class="c0">8</span>

<span class="c0">10</span>

<span class="c0">HTM Debt Securities - Government</span>

<span class="c0">52</span>

<span class="c0">8</span>

<span class="c0">10</span>

<span class="c0">HTM Debt Securities - Private</span>

<span class="c0">53</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Real Estate Mortgage Loans</span>

<span class="c0">54</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Collateral Loans</span>

<span class="c0">55</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Guaranteed Loans</span>

<span class="c0">56</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Chattel Mortgage Loans</span>

<span class="c0">57</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Policy Loans</span>

<span class="c0">58</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Unearned interest Income</span>

<span class="c0">59</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Notes Receivable</span>

<span class="c0">60</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Housing Loans</span>

<span class="c0">61</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Car Loans</span>

<span class="c0">62</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Low Cost Housing</span>

<span class="c0">63</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Purchase Money Mortgages</span>

<span class="c0">64</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Sales Contract Receivables</span>

<span class="c0">65</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Unquoted Debt Securities</span>

<span class="c0">66</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Salary Loans</span>

<span class="c0">67</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Other Loans Receivables</span>

<span class="c0">68</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">69</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">AFS Debt Securities - Government</span>

<span class="c0">70</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">AFS Debt Securities - Private</span>

<span class="c0">71</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">AFS Equity Securities</span>

<span class="c0">72</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">73</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Mutual Funds and Unit Investment Trusts</span>

<span class="c0">74</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">75</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Accrued Dividends Receivable</span>

<span class="c0">76</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Other Funds</span>

<span class="c0">77</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - Cash In Banks</span>

<span class="c0">78</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - Time Deposits</span>

<span class="c0">79</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - Financial Assets at
FVPL</span>

<span class="c0">80</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - AFS Financial Assets</span>

<span class="c0">81</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - HTM Investments</span>

<span class="c0">82</span>

<span class="c0">11</span>

<span class="c0">13</span>

<span class="c0">Accrued Interest Income - Loans and Receivables</span>

<span class="c0">83</span>

<span class="c0">12</span>

<span class="c0">14</span>

<span class="c0">Advances to Agents (Agents Accounts) / Employees</span>

<span class="c0">84</span>

<span class="c0">12</span>

<span class="c0">14</span>

<span class="c0">Operating Lease Receivables</span>

<span class="c0">85</span>

<span class="c0">12</span>

<span class="c0">14</span>

<span class="c0">Allowance for Impairment Losses</span>

<span class="c0">86</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Land - At Cost</span>

<span class="c0">87</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">88</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Leasehold Improvements - At Cost</span>

<span class="c0">89</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">IT Equipment - At Cost</span>

<span class="c0">90</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Transportation Equipment - At Cost</span>

<span class="c0">91</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Office Furniture, Fixtures and Equipment - At
Cost</span>

<span class="c0">92</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Property and Equipment Under Finance Lease</span>

<span class="c0">93</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Revaluation Increment</span>

<span class="c0">94</span>

<span class="c0">15</span>

<span class="c0">17</span>

<span class="c0">Accumulated Impairment Losses</span>

<span class="c0">95</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Land - At Cost</span>

<span class="c0">96</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Building and Building Improvements - At Cost</span>

<span class="c0">97</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Accumulated Impairment Losses</span>

<span class="c0">98</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Land - At Fair Value</span>

<span class="c0">99</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Building and Building Improvements - At Fair
Value</span>

<span class="c0">100</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Foreclosed Properties</span>

<span class="c0">101</span>

<span class="c0">22</span>

<span class="c0">24</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">102</span>

<span class="c0">22</span>

<span class="c0">24</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">103</span>

<span class="c0">22</span>

<span class="c0">24</span>

<span class="c0">Hedges of a Net Investment in Foreign Operation</span>

<span class="c0">104</span>

<span class="c0">22</span>

<span class="c0">48</span>

<span class="c0">Trading Debt Securities - Government</span>

<span class="c0">105</span>

<span class="c0">22</span>

<span class="c0">48</span>

<span class="c0">Trading Debt Securities - Private</span>

<span class="c0">106</span>

<span class="c0">22</span>

<span class="c0">48</span>

<span class="c0">Trading Equity Securities</span>

<span class="c0">107</span>

<span class="c0">22</span>

<span class="c0">48</span>

<span class="c0">Mutual, Unit Investment Trust, Real Estate Investment
Trusts and Other Funds</span>

<span class="c0">108</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Debt Securities - Government</span>

<span class="c0">109</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Debt Securities - Private</span>

<span class="c0">110</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Equity Securities</span>

<span class="c0">111</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Mutual Funds and Unit Investment Trusts</span>

<span class="c0">112</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">113</span>

<span class="c0">23</span>

<span class="c0">49</span>

<span class="c0">Other Funds</span>

<span class="c0">114</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Real Estate Mortgage Loans</span>

<span class="c0">115</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Collateral Loans</span>

<span class="c0">116</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Guaranteed Loans</span>

<span class="c0">117</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Chattel Mortgage Loans</span>

<span class="c0">118</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Policy Loans</span>

<span class="c0">119</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Notes Receivable</span>

<span class="c0">120</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Housing Loans</span>

<span class="c0">121</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Car Loans</span>

<span class="c0">122</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Low Cost Housing Loans</span>

<span class="c0">123</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Purchase Money Mortgages</span>

<span class="c0">124</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Sales Contract Receivable</span>

<span class="c0">125</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Unquoted Debt Securities</span>

<span class="c0">126</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Salary Loans</span>

<span class="c0">127</span>

<span class="c0">55</span>

<span class="c0">81</span>

<span class="c0">Others</span>

<span class="c0">128</span>

<span class="c0">56</span>

<span class="c0">75</span>

<span class="c0">FVPL Equity Securities</span>

<span class="c0">129</span>

<span class="c0">56</span>

<span class="c0">75</span>

<span class="c0">DVPL Equity Securities</span>

<span class="c0">130</span>

<span class="c0">56</span>

<span class="c0">75</span>

<span class="c0">AFS Equity Securities</span>

<span class="c0">131</span>

<span class="c0">67</span>

<span class="c0">96</span>

<span class="c0">Accumulate Depreciation - Building and Building
Improvements</span>

<span class="c0">132</span>

<span class="c0">68</span>

<span class="c0">97</span>

<span class="c0">Accumulated Depreciation - Leasehold
Improvements</span>

<span class="c0">133</span>

<span class="c0">69</span>

<span class="c0">98</span>

<span class="c0">Accumulated Depreciation - IT Equipment</span>

<span class="c0">134</span>

<span class="c0">70</span>

<span class="c0">99</span>

<span class="c0">Accumulated Depreciation - Transportation
Equipment</span>

<span class="c0">135</span>

<span class="c0">71</span>

<span class="c0">100</span>

<span class="c0">Accumulated Depreciation – Office Furniture, Fixtures
and Equipment</span>

<span class="c0">136</span>

<span class="c0">72</span>

<span class="c0">101</span>

<span class="c0">Accumulated Depreciation</span>

<span class="c0">137</span>

<span class="c0">73</span>

<span class="c0">102</span>

<span class="c0">Accumulated Depreciation - Revaluation Increment</span>

<span class="c0">138</span>

<span class="c0">76</span>

<span class="c0">103</span>

<span class="c0">Accumulate Depreciation - Building and Building
Improvements</span>

<span class="c0">139</span>

<span class="c0">1</span>

<span class="c0">-</span>

<span class="c0">Total Liabilities</span>

<span class="c0">140</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Aggregate Reserve for Life Policies</span>

<span class="c0">141</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Aggregate Reserve for Accident and Health
Policies</span>

<span class="c0">142</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Reserve for Supplementary Contracts Without Life
Contingencies</span>

<span class="c0">143</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Policy and Contract Claims Payable</span>

<span class="c0">144</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Due to Reinsurers</span>

<span class="c0">145</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Funds Held for Reinsurers</span>

<span class="c0">146</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Life Insurance Deposit/Applicant's Deposit</span>

<span class="c0">147</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Segregated Fund Liabilities</span>

<span class="c0">148</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Premium Deposit Fund</span>

<span class="c0">149</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Remitances Unapplied Deposit</span>

<span class="c0">150</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Premium Received in Advance</span>

<span class="c0">151</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Policyholders' Dividends Due and Unpaid</span>

<span class="c0">152</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Policyholders' Dividends Accumulations/ Dividends Held
on Deposit</span>

<span class="c0">153</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Maturities and Surrenders Payables</span>

<span class="c0">154</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Commissions Payable</span>

<span class="c0">155</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Return Premiums Payable</span>

<span class="c0">156</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Taxes Payable</span>

<span class="c0">157</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Accounts Payable</span>

<span class="c0">158</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Deposit for Real Estate Under Contract to Sell</span>

<span class="c0">159</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Dividends Payable</span>

<span class="c0">160</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Liability on Life insurance Pool Business</span>

<span class="c0">161</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Financial Liabilities at Fair Value Through Profit or
Loss</span>

<span class="c0">162</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Notes Payable</span>

<span class="c0">163</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Finance Lease Liability</span>

<span class="c0">164</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Pension Obligation</span>

<span class="c0">165</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Accrual for Long-Term Employee Benefits</span>

<span class="c0">166</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Deferred Tax Liability </span>

<span class="c0">167</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Provisions</span>

<span class="c0">168</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Cash-Settled Share-Based Payment</span>

<span class="c0">169</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Accrued Expenses</span>

<span class="c0">170</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Other Liabilities</span>

<span class="c0">171</span>

<span class="c0">1</span>

<span class="c0">139</span>

<span class="c0">Derivative Liabilities Held for Hedging</span>

<span class="c0">172</span>

<span class="c0">27</span>

<span class="c0">143</span>

<span class="c0">Claims Due and Unpaid</span>

<span class="c0">173</span>

<span class="c0">27</span>

<span class="c0">143</span>

<span class="c0">Outstanding Claims Reserve</span>

<span class="c0">174</span>

<span class="c0">27</span>

<span class="c0">143</span>

<span class="c0">Claims Resisted</span>

<span class="c0">175</span>

<span class="c0">27</span>

<span class="c0">143</span>

<span class="c0">Claims Incurred but not yet Reported</span>

<span class="c0">176</span>

<span class="c0">28</span>

<span class="c0">144</span>

<span class="c0">Premiums Due to Reinsurers - Treaty</span>

<span class="c0">177</span>

<span class="c0">28</span>

<span class="c0">144</span>

<span class="c0">Premiums Due to Reinsurers - Facultative</span>

<span class="c0">178</span>

<span class="c0">29</span>

<span class="c0">145</span>

<span class="c0">Premiums Due to Reinsurers - Facultative</span>

<span class="c0">179</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Experience refunds</span>

<span class="c0">180</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Premiums Tax Payable</span>

<span class="c0">181</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Documentary Stamps Tax Payable</span>

<span class="c0">182</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Value-added Tax (VAT) Payable</span>

<span class="c0">183</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Income Tax Payable</span>

<span class="c0">184</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Withholding Tax Payable</span>

<span class="c0">185</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Fire Service Tax Payable</span>

<span class="c0">186</span>

<span class="c0">40</span>

<span class="c0">156</span>

<span class="c0">Other Taxes and Licenses Payable</span>

<span class="c0">187</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">SSS Premiums Payable</span>

<span class="c0">188</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">SSS Loans Payable</span>

<span class="c0">189</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">Pag-ibig Premiums Payable</span>

<span class="c0">190</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">Pag-ibig Loans Payable</span>

<span class="c0">191</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">Operating Lease Liability</span>

<span class="c0">192</span>

<span class="c0">41</span>

<span class="c0">157</span>

<span class="c0">Other Accounts Payable</span>

<span class="c0">193</span>

<span class="c0">45</span>

<span class="c0">161</span>

<span class="c0">Financial Liabilities Held for Trading</span>

<span class="c0">194</span>

<span class="c0">45</span>

<span class="c0">161</span>

<span class="c0">Financial Liabilities Designated at Fair Value Through
Profit or Loss</span>

<span class="c0">195</span>

<span class="c0">45</span>

<span class="c0">161</span>

<span class="c0">Derivative Liabilities</span>

<span class="c0">196</span>

<span class="c0">53</span>

<span class="c0">169</span>

<span class="c0">Accrued Utilities</span>

<span class="c0">197</span>

<span class="c0">53</span>

<span class="c0">169</span>

<span class="c0">Accrued Services</span>

<span class="c0">198</span>

<span class="c0">53</span>

<span class="c0">169</span>

<span class="c0">Accrual for Unused Compensated Absences</span>

<span class="c0">199</span>

<span class="c0">54</span>

<span class="c0">170</span>

<span class="c0">Deferred Income</span>

<span class="c0">200</span>

<span class="c0">54</span>

<span class="c0">170</span>

<span class="c0">Agency Retirement Plan</span>

<span class="c0">201</span>

<span class="c0">54</span>

<span class="c0">170</span>

<span class="c0">Agency Group Hospitalization Plan</span>

<span class="c0">202</span>

<span class="c0">54</span>

<span class="c0">170</span>

<span class="c0">Agency Group Term Plan</span>

<span class="c0">203</span>

<span class="c0">54</span>

<span class="c0">170</span>

<span class="c0">Agency Cash Bond Deposit</span>

<span class="c0">204</span>

<span class="c0">55</span>

<span class="c0">171</span>

<span class="c0">Fair Value Hedge</span>

<span class="c0">205</span>

<span class="c0">55</span>

<span class="c0">171</span>

<span class="c0">Cash Flow Hedge</span>

<span class="c0">206</span>

<span class="c0">55</span>

<span class="c0">171</span>

<span class="c0">Hedges of a Net Investment in Foreign Operation</span>

<span class="c0">207</span>

<span class="c0">2</span>

<span class="c0">-</span>

<span class="c0">Total Networth</span>

<span class="c0">208</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Capital Stock</span>

<span class="c0">209</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Statutory Deposit</span>

<span class="c0">210</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Capital Stock Subscribed</span>

<span class="c0">211</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Contributed Surplus</span>

<span class="c0">212</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Contingency Surplus / Home Office Inward
Remittances</span>

<span class="c0">213</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Capital Paid In Excess of Par</span>

<span class="c0">214</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Retained Earnings / Home Office Account</span>

<span class="c0">215</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Cost of Share-Based Payment</span>

<span class="c0">216</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Reserve Accounts</span>

<span class="c0">217</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Reserve for Appraisal Increment - Property and
Equipment</span>

<span class="c0">218</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Remeasurement Gains (Losses) on Retirement Pension
Asset (Obligation)</span>

<span class="c0">219</span>

<span class="c0">2</span>

<span class="c0">207</span>

<span class="c0">Treasury Stock</span>

<span class="c0">220</span>

<span class="c0">56</span>

<span class="c0">208</span>

<span class="c0">Preferred Stock</span>

<span class="c0">221</span>

<span class="c0">56</span>

<span class="c0">208</span>

<span class="c0">Common Stock</span>

<span class="c0">222</span>

<span class="c0">62</span>

<span class="c0">214</span>

<span class="c0">Retained Earnings - Appropriated for Negative
Reserve</span>

<span class="c0">223</span>

<span class="c0">62</span>

<span class="c0">214</span>

<span class="c0">Retained Earnings - Appropriated Others</span>

<span class="c0">224</span>

<span class="c0">62</span>

<span class="c0">214</span>

<span class="c0">Retained Earnings - Unappropriated</span>

<span class="c0">225</span>

<span class="c0">62</span>

<span class="c0">214</span>

<span class="c0">Retained Earnings - Transition Adjustments</span>

<span class="c0">226</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Reserve for AFS Securities</span>

<span class="c0">227</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Reserve for Cash Flow Hedge</span>

<span class="c0">228</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Reserve for Hedge of a Net Investment in Foreign
Operation</span>

<span class="c0">229</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Cumulative Foreign Currency Translation</span>

<span class="c0">230</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Remeasurement on Life Insurance Reserves</span>

<span class="c0">231</span>

<span class="c0">64</span>

<span class="c0">216</span>

<span class="c0">Reserve for Investment in Associates</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21">                Certificate Request</span>

<span class="c21"></span>

<span>                </span><span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 411.00px; height: 191.00px;">![](images/image29.png)</span>

1.  <span class="c75">Non-Life Division</span>
    ------------------------------------------

<!-- -->

1.  ### <span class="c26">Annual Submission and Examination of Financial Standing of Non-Life Companies</span>

### <span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 169.28px; height: 206.50px;">![](images/image10.png)</span>

1.  ### <span class="c26">Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 380.00px;">![](images/image39.png)</span>

1.  ### <span class="c26">Issuance of Certification on Financial Condition of Insurance Companies</span>

<span class="c21"></span>

<span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 391.00px; height: 171.00px;">![](images/image21.png)</span>
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<span class="c21"></span>

1.  <span class="c150">Pre-Need Division</span>
    -------------------------------------------

<!-- -->

1.  ### <span class="c26">Annual Submission and Examination of Pre-Need Companies</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 437.33px;">![](images/image8.png)</span>

<span class="c21"></span>

<span id="t.427f464538367e7e2c0c93bd903970984ea424c6"></span><span
id="t.10"></span>

<span class="c56">Requirement Checklist</span>

<span class="c0">id</span>

<span class="c0">requirement</span>

<span class="c0">1</span>

<span class="c0">Audited Financial Statements (AFS) duly stamped by the
Bureau of Internal Revenue (BIR)</span>

<span class="c0">2</span>

<span class="c0">Reconciliation Statement of The AFS versus the ATB
figures;</span>

<span class="c0">3</span>

<span class="c0">Detailed Reconciliation Statement of Trust Fund
Balances per Trustee Bank(s) versus AFS/AS;</span>

<span class="c0">4</span>

<span class="c0">Annual Pre-need Reserve Valuation Report</span>

<span class="c0">5</span>

<span class="c0">List of current members of board of directors and
officers, their respective addresses, position and committee
membership</span>

<span class="c0">6</span>

<span class="c0">Sworn Statement from the responsible officers of the
company stating that: "Any deficiency in Trust Funds has been duly
addressed, attaching the necessary documents as proofs thereof"</span>

<span class="c0">7</span>

<span class="c0">Sworn Statement of the company's insurer certifying the
coverage on the life insurance policies or guarantees on premium
payments assumed by the company, indicating the extent, term and
duration of such coverage/guarantees</span>

<span class="c0">8</span>

<span class="c0">Documents supporting the accounts held as corporate
assets</span>

<span class="c0">9</span>

<span class="c0">Annual Statements Showing the financial condition of
the pre-need company and all its exhibits and schedules</span>

<span class="c0">10</span>

<span class="c0">Exhibit 5 - Summary of Monthly Withdrawals from the
Trust Fund</span>

<span class="c0">11</span>

<span class="c0">Annual Statements of Trust Funds per type of plan and
all its exhibits and schedules</span>

<span class="c0">12</span>

<span class="c0">Complete details of Exhibits 6, Exhibit 7 and Exhibit
8</span>

<span class="c0">13</span>

<span class="c0">Adjusted Trial Balance (ATB)</span>

<span class="c0">14</span>

<span class="c0">Reconciliation Statement of the AFS versus the ATB
figures</span>

<span class="c0">15</span>

<span class="c0">Detailed Reconciliation Statement of Trust Fund
Balances per Trustee Bank(s) versus AFS/AS</span>

<span class="c0">16</span>

<span class="c0">PDF copy Audited Financial Statements (AFS) duly
Stamped by the bureau of Internal Revenue (BIR)</span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.47ddf14a7b6b8ad9e46dfafaffecfa5e772e4ca5"></span><span
id="t.11"></span>

<span class="c0">Additional Requirements</span>

<span class="c0">requirement\_id</span>

<span class="c0">addl\_requirement</span>

<span class="c0">4</span>

<span class="c0">Certification by the actuary and/or any accountable
officer of the company on the actuary and completeness of the in-force
files used in valuation of reserves</span>

<span class="c0">4</span>

<span class="c0">Certification on the prudent adequacy of the Pre-need
reserve that it shall provide at least the guaranteed contractual
benefits under each pre-need contract of the company</span>

<span class="c0">8</span>

<span class="c0">Certification of Custodian for Petty Cash Fund,
Revolving Fund</span>

<span class="c0">8</span>

<span class="c0">Bank reconciliation statements as of 31 December 20\_
and 31 January 20\_ using adjusted balance method, together with the
bank statements, passbooks and certificates of all current, saving and
time deposit account respectively</span>

<span class="c0">8</span>

<span class="c0">Official receipts, bank validated deposit slips and
bank statements to support year-end deposit in transit</span>

<span class="c0">8</span>

<span class="c0">Confirmation of Sales of investment in Bonds and
Treasury Bills (Government Securities) together with statement of
Securities Account of Btr-ROSS as 31 December 20\_</span>

<span class="c0">8</span>

<span class="c0">Statement of Holdings as of year-and from the company's
custodian bank for dollar denominated bonds</span>

<span class="c0">8</span>

<span class="c0">Statement of Accounts as of year-end from Philippine
Depository and Trust Corporation (PDTC) for scriptless stock
investment</span>

<span class="c0">8</span>

<span class="c0">Statement of Net Asset Value (NAV/pu) of Unit
Investment Trust Fund (UITF), Mutual Funds and related
investments</span>

<span class="c0">8</span>

<span class="c0">Report from a licensed real estate appraiser accredited
by the Philippine Association of Real Estate Appraisers to support any
increase or decrease in fair value or real estate properties</span>

<span class="c0">8</span>

<span class="c0">Document supporting investments in commercial
papers</span>

<span class="c0">8</span>

<span class="c0">Document supporting mortgage loans and other loans
accounts</span>

<span class="c0">8</span>

<span class="c0">BIR tax returns/payments for the year on Income Tax and
Value added Tax (VAT)</span>

<span class="c0">8</span>

<span class="c0">Summary of taxes paid including Documentary Stamps Tax
(DST), withholding taxes and all other Taxes</span>

<span class="c0">8</span>

<span class="c0">Other documents to support all other corporate assets
and liability accounts</span>

<span class="c0">9</span>

<span class="c0">Table of Contents showing the summary of Investment for
the year ended December 31, 20\_ and Certification</span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission and Monitoring of Interim Financial Statements</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 316.00px;">![](images/image28.png)</span>

<span class="c21"></span>

<span id="t.28a9d656a19a7f755d25f4396069fabe02e6d1fb"></span><span
id="t.12"></span>

<span class="c56">Report Types</span>

<span class="c0">id</span>

<span class="c0">report\_type</span>

<span class="c0">1</span>

<span class="c0">Interim Financial Statements</span>

<span class="c0">2</span>

<span class="c0">Sales Collections and Deposit to Trust Fund</span>

<span class="c21"></span>

<span id="t.43b58fb1aa9ef6df9c18a99da48fe679eb43396c"></span><span
id="t.13"></span>

<span class="c56">Trust Fund Types</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">1</span>

<span class="c0">Life</span>

<span class="c0">2</span>

<span class="c0">Pension</span>

<span class="c0">3</span>

<span class="c0">Education</span>

<span class="c0">4</span>

<span class="c0">Main</span>

<span class="c21"></span>

<span id="t.a25fda5813f8c574369ade1cf01a304d1f45d554"></span><span
id="t.14"></span>

<span class="c56">Account Types</span>

<span class="c0">id</span>

<span class="c0">account\_type</span>

<span class="c0">1</span>

<span class="c0">Assets</span>

<span class="c0">2</span>

<span class="c0">Liabilities</span>

<span class="c0">3</span>

<span class="c0">Fund Equity</span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.fae5b997913e9e5510aa99d204c5b60a2f8f464e"></span><span
id="t.15"></span>

<span class="c56">Accounts</span>

<span class="c0">id</span>

<span class="c0">account\_type</span>

<span class="c0">account\_name</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">Government Securities</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">Cash in Savings</span>

<span class="c0">3</span>

<span class="c0">1</span>

<span class="c0">Mutual Funds/UITF</span>

<span class="c0">4</span>

<span class="c0">1</span>

<span class="c0">Short Term Investments</span>

<span class="c0">5</span>

<span class="c0">1</span>

<span class="c0">Corporate Bonds</span>

<span class="c0">6</span>

<span class="c0">1</span>

<span class="c0">Mortgage Loans</span>

<span class="c0">7</span>

<span class="c0">1</span>

<span class="c0">Planholders' Loans</span>

<span class="c0">8</span>

<span class="c0">1</span>

<span class="c0">Stocks</span>

<span class="c0">9</span>

<span class="c0">1</span>

<span class="c0">Real Estate</span>

<span class="c0">10</span>

<span class="c0">1</span>

<span class="c0">Other Investments</span>

<span class="c0">11</span>

<span class="c0">1</span>

<span class="c0">Other Assets</span>

<span class="c0">12</span>

<span class="c0">2</span>

<span class="c0">Accrued Trust Fees</span>

<span class="c0">13</span>

<span class="c0">2</span>

<span class="c0">Accrued Taxes</span>

<span class="c0">14</span>

<span class="c0">2</span>

<span class="c0">Other Liabilities</span>

<span class="c0">15</span>

<span class="c0">3</span>

<span class="c0">Fund Balance</span>

<span class="c0">16</span>

<span class="c0">3</span>

<span class="c0">Additional Contribution</span>

<span class="c0">17</span>

<span class="c0">3</span>

<span class="c0">Withdrawals</span>

<span class="c0">18</span>

<span class="c0">3</span>

<span class="c0">Adjustments</span>

<span class="c0">19</span>

<span class="c0">3</span>

<span class="c0">Fund Balance</span>

<span class="c0">20</span>

<span class="c0">3</span>

<span class="c0">Retained Earnings</span>

<span class="c0">21</span>

<span class="c0">3</span>

<span class="c0">Net Income</span>

<span class="c0">22</span>

<span class="c0">3</span>

<span class="c0">Net Unrealized Income</span>

<span class="c21"></span>

<span id="t.5838c101a467e3785701299ca43fa21427b60d02"></span><span
id="t.16"></span>

<span class="c56">Income Statement Attributes</span>

<span class="c0">id</span>

<span class="c0">attribute</span>

<span class="c0">1</span>

<span class="c0">Premiums</span>

<span class="c0">2</span>

<span class="c0">Increase/Decrease in Pre-Need Reserves</span>

<span class="c0">3</span>

<span class="c0">Increase/Decrease in Premium Reserves</span>

<span class="c0">4</span>

<span class="c0">Increase/Decrease in Other Pre-Need Reserves</span>

<span class="c0">5</span>

<span class="c0">Other Direct Income</span>

<span class="c0">6</span>

<span class="c0">Total Direct Income</span>

<span class="c0">7</span>

<span class="c0">Benefit Payments</span>

<span class="c0">8</span>

<span class="c0">Commissions Expenses</span>

<span class="c0">9</span>

<span class="c0">Other Direct Expenses</span>

<span class="c0">10</span>

<span class="c0">Total Direct Expenses</span>

<span class="c0">11</span>

<span class="c0">Gain/Loss</span>

<span class="c0">12</span>

<span class="c0">Bonds</span>

<span class="c0">13</span>

<span class="c0">Stocks</span>

<span class="c0">14</span>

<span class="c0">Real Estate</span>

<span class="c0">15</span>

<span class="c0">Purchase Money Mortgage</span>

<span class="c0">16</span>

<span class="c0">Mortgage Loans</span>

<span class="c0">17</span>

<span class="c0">Collateral Loans</span>

<span class="c0">18</span>

<span class="c0">Guaranteed Loans</span>

<span class="c0">19</span>

<span class="c0">Other Loans</span>

<span class="c0">20</span>

<span class="c0">Short-term Investments</span>

<span class="c0">21</span>

<span class="c0">Other Investments</span>

<span class="c0">22</span>

<span class="c0">Bank Deposits</span>

<span class="c0">23</span>

<span class="c0">Total Gain/Loss and Interest Earned</span>

<span class="c0">24</span>

<span class="c0">Depreciation</span>

<span class="c0">25</span>

<span class="c0">Investment Expenses</span>

<span class="c0">26</span>

<span class="c0">Taxes on Real Estate</span>

<span class="c0">27</span>

<span class="c0">Documentary Stamp Tax</span>

<span class="c0">28</span>

<span class="c0">Corporate Residence Certificate</span>

<span class="c0">29</span>

<span class="c0">Assessment, License and Fees</span>

<span class="c0">30</span>

<span class="c0">VAT & Fringe Benefit Tax</span>

<span class="c0">31</span>

<span class="c0">Final Taxes</span>

<span class="c0">32</span>

<span class="c0">Salaries and Wages</span>

<span class="c0">33</span>

<span class="c0">Allowance to Officers</span>

<span class="c0">34</span>

<span class="c0">Allowance to Employees</span>

<span class="c0">35</span>

<span class="c0">Pension, Retirement and Other similar Benefits</span>

<span class="c0">36</span>

<span class="c0">Rent, Light and Water</span>

<span class="c0">37</span>

<span class="c0">Other General Expenses</span>

<span class="c0">38</span>

<span class="c0">Net Income/Loss before Income Tax</span>

<span class="c0">39</span>

<span class="c0">Income Tax</span>

<span class="c0">40</span>

<span class="c0">Net Income/Loss for the Quarter</span>

<span class="c21"></span>

<span class="c21"></span>

1.  ### <span class="c26">Monthly Submission and Monitoring of Sales Reports, Collection Reports and Deposit to Trust Fund Reports</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 574.67px;">![](images/image24.png)</span>

<span class="c82"></span>

<span id="t.1cc11d9fabf723182f8078a18af5a292f122f847"></span><span
id="t.17"></span>

<span class="c56">Plan Types</span>

<span class="c0">id</span>

<span class="c0">plan\_type</span>

<span class="c0">1</span>

<span class="c0">Life/Memorial Plan</span>

<span class="c0">2</span>

<span class="c0">Pension Plan</span>

<span class="c0">3</span>

<span class="c0">Education Plan</span>

<span class="c82"></span>

<span id="t.6d33b17d8d559dd4f51e534977a2e588fb521514"></span><span
id="t.18"></span>

<span class="c56">Categories</span>

<span class="c0">id</span>

<span class="c0">category</span>

<span class="c0">1</span>

<span class="c0">Fully Paid</span>

<span class="c0">2</span>

<span class="c0">Installment</span>

<span class="c82"></span>

<span class="c82"></span>

<span class="c82"></span>

<span class="c82"></span>

<span class="c82"></span>

<span class="c157 c150">Technical Services Group</span>
=======================================================

1.  <span class="c75">Statistics and Research Division</span>
    ---------------------------------------------------------

<!-- -->

1.  ### <span class="c26">Preparation of Annual Data - Annual Report (selected sample tables)</span>

<span>                        </span><span class="c93">Refer to the ERD
of all the Divisions of Financial Examination Group</span>

1.  ### <span class="c26">Preparation of Annual Data - Key Data</span>

<span class="c93">Refer to the ERD of all the Divisions of Financial
Examination Group</span>

1.  ### <span class="c26">Preparation of Annual Data - ASEAN Report</span>

<span>        </span><span class="c93">Refer to the ERD of all the
Divisions of Financial Examination Group</span>

1.  ### <span class="c26">Preparation of Annual Data - Ranking</span>

<span class="c93">Refer to the ERD of all the Divisions of Financial
Examination Group</span>

1.  ### <span class="c26">Preparation of Annual Data - Assets Information Management Report (AIMP)</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 252.00px;">![](images/image17.png)</span>

<span class="c21"></span>

<span id="t.ee7c50152c29ff5c2256418556c53fc52f6299c6"></span><span
id="t.19"></span>

<span class="c0">requirement\_list (look up table)</span>

<span class="c0">id</span>

<span class="c0">information\_name</span>

<span class="c0">information\_specific</span>

<span class="c0">information\_subtype</span>

<span class="c0">0</span>

<span class="c0">Basic Information</span>

<span class="c0">Certificate of Authenticity</span>

<span class="c0">none</span>

<span class="c0">1</span>

<span class="c0">Basic Information</span>

<span class="c0">Date of original Issue</span>

<span class="c0">none</span>

<span class="c0">2</span>

<span class="c0">Basic Information</span>

<span class="c0">Date of latest renewal</span>

<span class="c0">none</span>

<span class="c0">3</span>

<span class="c0">Basic Information</span>

<span class="c0">Taxpayer Name</span>

<span class="c0">none</span>

<span class="c0">4</span>

<span class="c0">Basic Information</span>

<span class="c0">Company/Trade Name</span>

<span class="c0">none</span>

<span class="c0">5</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">Name of affiliated company</span>

<span class="c0">none</span>

<span class="c0">6</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">TIN</span>

<span class="c0">none</span>

<span class="c0">7</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">Address</span>

<span class="c0">none</span>

<span class="c0">8</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Assets</span>

<span class="c0">none</span>

<span class="c0">9</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Liabilities</span>

<span class="c0">none</span>

<span class="c0">10</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Networth</span>

<span class="c0">none</span>

<span class="c0">11</span>

<span class="c0">Amount of Net Increase/ Decrease in Reserves on Direct
Business</span>

<span class="c0">none</span>

<span class="c0">none</span>

<span class="c0">12</span>

<span class="c0">Other Information</span>

<span class="c0">Total number of policies issued</span>

<span class="c0">none</span>

<span class="c0">13</span>

<span class="c0">Other Information</span>

<span class="c0">Total amount of policy coverage</span>

<span class="c0">none</span>

<span class="c0">14</span>

<span class="c0">Other Information</span>

<span class="c0">Total premiums from direct business</span>

<span class="c0">none</span>

<span class="c0">15</span>

<span class="c0">Other Information</span>

<span class="c0">Net Investment income</span>

<span class="c0">none</span>

<span class="c0">16</span>

<span class="c0">Other Information</span>

<span class="c0">Total Interest income earned</span>

<span class="c0">none</span>

<span class="c0">17</span>

<span class="c0">Other Information</span>

<span class="c0">Total Dividends Earned</span>

<span class="c0">none</span>

<span class="c0">18</span>

<span class="c0">Other Information</span>

<span class="c0">Total Real Estate Income Earned</span>

<span class="c0">none</span>

<span class="c0">19</span>

<span class="c0">Other Information</span>

<span class="c0">Gains and Losses on Investments</span>

<span class="c0">none</span>

<span class="c0">20</span>

<span class="c0">Other Information</span>

<span class="c0">Total amount of Policy and Contract Claims</span>

<span class="c0">none</span>

<span class="c0">21</span>

<span class="c0">Basic Information</span>

<span class="c0">Certificate of Authenticity</span>

<span class="c0">none</span>

<span class="c0">22</span>

<span class="c0">Basic Information</span>

<span class="c0">Date of original Issue</span>

<span class="c0">none</span>

<span class="c0">23</span>

<span class="c0">Basic Information</span>

<span class="c0">Date of latest renewal</span>

<span class="c0">none</span>

<span class="c0">24</span>

<span class="c0">Basic Information</span>

<span class="c0">Taxpayer Name</span>

<span class="c0">none</span>

<span class="c0">25</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">Name of affiliated company</span>

<span class="c0">none</span>

<span class="c0">26</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">TIN</span>

<span class="c0">none</span>

<span class="c0">27</span>

<span class="c0">List of affiliated companies</span>

<span class="c0">Address</span>

<span class="c0">none</span>

<span class="c0">28</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Assets</span>

<span class="c0">none</span>

<span class="c0">29</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Liabilities</span>

<span class="c0">none</span>

<span class="c0">30</span>

<span class="c0">Annual Statement for the Year</span>

<span class="c0">Total Networth</span>

<span class="c0">none</span>

<span class="c0">31</span>

<span class="c0">Amount of Net Increase/ Decrease in Reserves on Direct
Business</span>

<span class="c0">none</span>

<span class="c0">none</span>

<span class="c0">32</span>

<span class="c0">Other Information</span>

<span class="c0">Total number of policies issued</span>

<span class="c0">Health and Accident</span>

<span class="c0">33</span>

<span class="c0">Other Information</span>

<span class="c0">Total number of policies issued</span>

<span class="c0">TPL/CLB on automobiles</span>

<span class="c0">34</span>

<span class="c0">Other Information</span>

<span class="c0">Total number of policies issued</span>

<span class="c0">Marine</span>

<span class="c0">35</span>

<span class="c0">Other Information</span>

<span class="c0">Total number of policies issued</span>

<span class="c0">Others</span>

<span class="c0">36</span>

<span class="c0">Other Information</span>

<span class="c0">Total premiums from direct business</span>

<span class="c0">Health and Accident</span>

<span class="c0">37</span>

<span class="c0">Other Information</span>

<span class="c0">Total premiums from direct business</span>

<span class="c0">TPL/CLB on automobiles</span>

<span class="c0">38</span>

<span class="c0">Other Information</span>

<span class="c0">Total premiums from direct business</span>

<span class="c0">Marine</span>

<span class="c0">39</span>

<span class="c0">Other Information</span>

<span class="c0">Total premiums from direct business</span>

<span class="c0">Others</span>

<span class="c0">40</span>

<span class="c0">Other Information</span>

<span class="c0">Net Investment income</span>

<span class="c0">none</span>

<span class="c0">41</span>

<span class="c0">Other Information</span>

<span class="c0">Total Interest income earned</span>

<span class="c0">none</span>

<span class="c0">42</span>

<span class="c0">Other Information</span>

<span class="c0">Total Dividends Earned</span>

<span class="c0">none</span>

<span class="c0">43</span>

<span class="c0">Other Information</span>

<span class="c0">Total Real Estate Income Earned</span>

<span class="c0">none</span>

<span class="c0">44</span>

<span class="c0">Other Information</span>

<span class="c0">Gains and Losses on Investments</span>

<span class="c0">none</span>

<span class="c0">45</span>

<span class="c0">Other Information</span>

<span class="c0">Total amount of Policy and Contract Claims</span>

<span class="c0">none</span>

<span class="c0">46</span>

<span class="c0">Other information that may be needed to satisfy the
objectives of AIMP</span>

<span class="c0">none</span>

<span class="c0">none</span>

<span class="c21"></span>

<span id="t.525255e3db22578e99293be105c68be7503724c0"></span><span
id="t.20"></span>

<span class="c0">insurance\_sub\_sub\_type\_table (look up table)</span>

<span class="c0">id</span>

<span class="c0">insurance\_sub\_type\_id</span>

<span class="c0">sub\_sub\_type</span>

<span class="c0">0</span>

<span class="c0">Ordinary</span>

<span class="c0">Whole Life</span>

<span class="c0">1</span>

<span class="c0">Ordinary</span>

<span class="c0">Endowment</span>

<span class="c0">2</span>

<span class="c0">Ordinary</span>

<span class="c0">Term</span>

<span class="c0">3</span>

<span class="c0">Group</span>

<span class="c0">Permanent</span>

<span class="c0">4</span>

<span class="c0">Group</span>

<span class="c0">Term</span>

<span class="c0">5</span>

<span class="c0">Industrial</span>

<span class="c0">Whole Life</span>

<span class="c0">6</span>

<span class="c0">Industrial</span>

<span class="c0">Endowment</span>

<span class="c0">7</span>

<span class="c0">Industrial</span>

<span class="c0">Term</span>

<span class="c0">8</span>

<span class="c0">Health & Accident</span>

<span class="c0">Whole Life</span>

<span class="c0">9</span>

<span class="c0">Health & Accident</span>

<span class="c0">Endowment</span>

<span class="c0">10</span>

<span class="c0">Health & Accident</span>

<span class="c0">Term</span>

<span class="c0">11</span>

<span class="c0">None</span>

<span class="c0">Health & Accident</span>

<span class="c0">12</span>

<span class="c0">None</span>

<span class="c0">Variable Life</span>

<span class="c0">13</span>

<span class="c0">None</span>

<span class="c0">Micro Insurance</span>

<span class="c0">14</span>

<span class="c0">None</span>

<span class="c0">Migrant Workers</span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission of Compliance to Various Reportorial Requirements - Life, Non-Life, MBA</span>

<!-- -->

1.  <span class="c21">Life</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 681.33px;">![](images/image19.png)</span>

<span class="c21"></span>

<span id="t.a33c8cc0887eded629e8bf4e87b3dd71412a8de8"></span><span
id="t.21"></span>

<span class="c0">Life Financial Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Total Assets</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Cash & Invested Assets</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Net Life Insurance Premiums & Annuity Considerations
Due & Uncollected</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Reinsurance Accounts Receivable</span>

<span class="c0">1</span>

<span class="c0">5</span>

<span class="c0">Variable Life - Separate Account Assets</span>

<span class="c0">1</span>

<span class="c0">6</span>

<span class="c0">Total Liabilities</span>

<span class="c0">-</span>

<span class="c0">7</span>

<span class="c0">Legal Policy Reserves</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Policy & Contract Claims</span>

<span class="c0">6</span>

<span class="c0">9</span>

<span class="c0">Premium Deposits Fund</span>

<span class="c0">6</span>

<span class="c0">10</span>

<span class="c0">Reinsurance Accounts Payable</span>

<span class="c0">6</span>

<span class="c0">11</span>

<span class="c0">Variable Life Liabilities</span>

<span class="c0">6</span>

<span class="c0">12</span>

<span class="c0">Taxes Payable</span>

<span class="c0">6</span>

<span class="c0">13</span>

<span class="c0">Other Liabilities</span>

<span class="c0">6</span>

<span class="c0">14</span>

<span class="c0">Total Networth</span>

<span class="c0">-</span>

<span class="c0">15</span>

<span class="c0">Paid-Up Capital/ Statutory Deposit</span>

<span class="c0">14</span>

<span class="c0">16</span>

<span class="c0">Capital Paid in Excess of Par Value</span>

<span class="c0">14</span>

<span class="c0">17</span>

<span class="c0">Seed Capital on Variable Life</span>

<span class="c0">14</span>

<span class="c0">18</span>

<span class="c0">Contributed Surplus/ Home Office/ Inward
Remittances</span>

<span class="c0">14</span>

<span class="c0">19</span>

<span class="c0">Deposit for Future Subscription</span>

<span class="c0">14</span>

<span class="c0">20</span>

<span class="c0">Contingency Surplus</span>

<span class="c0">14</span>

<span class="c0">21</span>

<span class="c0">Investment Fluctuation Reserves</span>

<span class="c0">14</span>

<span class="c0">22</span>

<span class="c0">Unassigned/ Retained Earnings/ Home Office
Account</span>

<span class="c0">14</span>

<span class="c21"></span>

<span id="t.13b665d92bd0752778dc527b4de46991a59c6f28"></span><span
id="t.22"></span>

<span class="c0">Life Investments Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Long Term Investments</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Government Bonds</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Corporate Bonds</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Short-Term Investments</span>

<span class="c0">-</span>

<span class="c0">5</span>

<span class="c0">Government (Treasury Bills)</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">Corporate Investments</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Stocks</span>

<span class="c0">-</span>

<span class="c0">8</span>

<span class="c0">Real Estate</span>

<span class="c0">-</span>

<span class="c0">9</span>

<span class="c0">Mortgage Loans</span>

<span class="c0">-</span>

<span class="c0">10</span>

<span class="c0">Guaranteed Loans</span>

<span class="c0">-</span>

<span class="c0">11</span>

<span class="c0">Other Loans</span>

<span class="c0">-</span>

<span class="c0">12</span>

<span class="c0">Mutual Funds</span>

<span class="c0">-</span>

<span class="c0">13</span>

<span class="c0">Unit Investment Trust Funds</span>

<span class="c0">-</span>

<span class="c0">14</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">-</span>

<span class="c0">15</span>

<span class="c0">Time Deposits/Fixed Deposits</span>

<span class="c0">-</span>

<span class="c0">16</span>

<span class="c0">Other Investments</span>

<span class="c0">-</span>

<span class="c0">17</span>

<span class="c0">Proprietary Shares</span>

<span class="c0">16</span>

<span class="c0">18</span>

<span class="c0">Money Market Placement</span>

<span class="c0">16</span>

<span class="c0">19</span>

<span class="c0">etc\*</span>

<span class="c0">16</span>

<span class="c0">20</span>

<span class="c0">Others</span>

<span class="c0">-</span>

<span class="c0">21</span>

<span class="c0">Exchange Traded Fund</span>

<span class="c0">20</span>

<span class="c0">22</span>

<span class="c0">Securities Borrowing & Lending</span>

<span class="c0">20</span>

<span class="c0">23</span>

<span class="c0">etc\*</span>

<span class="c0">20</span>

<span class="c21"></span>

<span id="t.73a13705aa72dedbb311a9d6a05a0270bfbee725"></span><span
id="t.23"></span>

<span class="c0">insurance\_type\_table</span>

<span class="c0">id</span>

<span class="c0">insurance type</span>

<span class="c0">insurance\_sub\_type</span>

<span class="c0">sub\_sub\_type</span>

<span class="c0">0</span>

<span class="c0">Life insurance</span>

<span class="c0">Ordinary Insurance</span>

<span class="c0">Whole Life</span>

<span class="c0">1</span>

<span class="c0">Life insurance</span>

<span class="c0">Ordinary Insurance</span>

<span class="c0">Endowment</span>

<span class="c0">2</span>

<span class="c0">Life insurance</span>

<span class="c0">Ordinary Insurance</span>

<span class="c0">Term</span>

<span class="c0">3</span>

<span class="c0">Life insurance</span>

<span class="c0">Group & Industrial</span>

<span class="c0">Permanent</span>

<span class="c0">4</span>

<span class="c0">Life insurance</span>

<span class="c0">Group & Industrial</span>

<span class="c0">Term</span>

<span class="c0">5</span>

<span class="c0">Life insurance</span>

<span class="c0">Accident</span>

<span class="c0">Individual</span>

<span class="c0">6</span>

<span class="c0">Life insurance</span>

<span class="c0">Accident</span>

<span class="c0">Term</span>

<span class="c0">7</span>

<span class="c0">Life insurance</span>

<span class="c0">Health</span>

<span class="c0">Individual</span>

<span class="c0">8</span>

<span class="c0">Life insurance</span>

<span class="c0">Health</span>

<span class="c0">Term</span>

<span class="c0">9</span>

<span class="c0">Microinsurance</span>

<span class="c0">none</span>

<span class="c0">none</span>

<span class="c0">10</span>

<span class="c0">Migrant Worker</span>

<span class="c0">none</span>

<span class="c0">none</span>

<span class="c21"></span>

<span id="t.befa8b6a70fe0a09e8f30040b394bda4d8beba17"></span><span
id="t.24"></span>

<span class="c0">business\_type\_table</span>

<span class="c0">id</span>

<span class="c0">business\_type</span>

<span class="c0">business\_sub\_type</span>

<span class="c0">0</span>

<span class="c0">beginning\_balance</span>

<span class="c0">none</span>

<span class="c0">1</span>

<span class="c0">new\_business</span>

<span class="c0">Issued</span>

<span class="c0">2</span>

<span class="c0">new\_business</span>

<span class="c0">Revived</span>

<span class="c0">3</span>

<span class="c0">new\_business</span>

<span class="c0">Increased</span>

<span class="c0">4</span>

<span class="c0">new\_business</span>

<span class="c0">Others</span>

<span class="c0">5</span>

<span class="c0">insurance\_terminated</span>

<span class="c0">none</span>

<span class="c0">6</span>

<span class="c0">in\_force\_at\_end\_quarter</span>

<span class="c0">none</span>

<span class="c21"></span>

<span id="t.9049b946b626f637c8f6a424501e3cbbcea5b849"></span><span
id="t.25"></span>

<span class="c0">business\_line\_table</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">subtype</span>

<span class="c0">line</span>

<span class="c0">0</span>

<span class="c0">New Business</span>

<span class="c0">First Year</span>

<span class="c0">Premiums and considerations, direct business</span>

<span class="c0">1</span>

<span class="c0">New Business</span>

<span class="c0">First Year</span>

<span class="c0">Reinsurance premiums assumed</span>

<span class="c0">2</span>

<span class="c0">New Business</span>

<span class="c0">First Year</span>

<span class="c0">Reinsurance premiums ceded</span>

<span class="c0">3</span>

<span class="c0">New Business</span>

<span class="c0">Single</span>

<span class="c0">Premiums and considerations, direct business</span>

<span class="c0">4</span>

<span class="c0">New Business</span>

<span class="c0">Single</span>

<span class="c0">Reinsurance premiums assumed</span>

<span class="c0">5</span>

<span class="c0">New Business</span>

<span class="c0">Single</span>

<span class="c0">Reinsurance premiums ceded</span>

<span class="c0">6</span>

<span class="c0">Renewal</span>

<span class="c0">none</span>

<span class="c0">Premiums and considerations, direct business</span>

<span class="c0">7</span>

<span class="c0">Renewal</span>

<span class="c0">none</span>

<span class="c0">Reinsurance premiums assumed</span>

<span class="c0">8</span>

<span class="c0">Renewal</span>

<span class="c0">none</span>

<span class="c0">Reinsurance premiums ceded</span>

<span class="c21"></span>

<span id="t.b00f5b5155f210f373a14e4f1209fb90f39ded1b"></span><span
id="t.26"></span>

<span class="c0">seperate accounts table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Total Assets</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">A. Assets at Market Value</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">1. Bonds</span>

<span class="c0">2</span>

<span class="c0">4</span>

<span class="c0">2. Stocks</span>

<span class="c0">2</span>

<span class="c0">5</span>

<span class="c0">3. Fixed Deposit</span>

<span class="c0">2</span>

<span class="c0">6</span>

<span class="c0">4. Others</span>

<span class="c0">2</span>

<span class="c0">7</span>

<span class="c0">B. Net Investment Income due and accrued</span>

<span class="c0">1</span>

<span class="c0">8</span>

<span class="c0">C. Others</span>

<span class="c0">1</span>

<span class="c0">9</span>

<span class="c0">D. Debtors/ Accounts Receivables\*</span>

<span class="c0">1</span>

<span class="c0">10</span>

<span class="c0">Total Liabilities and Networth</span>

<span class="c0">-</span>

<span class="c0">11</span>

<span class="c0">Variable Life Liabilities</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">General Expenses Due and Accrued</span>

<span class="c0">10</span>

<span class="c0">13</span>

<span class="c0">Other Liabilities</span>

<span class="c0">10</span>

<span class="c0">14</span>

<span class="c0">Seed Capital on Variable Life</span>

<span class="c0">10</span>

<span class="c21"></span>

<span id="t.a4c26f69a75e21ee3f29a63f70fb836a53d562d7"></span><span
id="t.27"></span>

<span class="c0">statement of change in assets accounts table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Net Assets, beginning</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Total Additions</span>

<span class="c0">-</span>

<span class="c0">3</span>

<span class="c0">Deposits, net of Withdrawals</span>

<span class="c0">2</span>

<span class="c0">4</span>

<span class="c0">Gross Investment Income</span>

<span class="c0">2</span>

<span class="c0">5</span>

<span class="c0">Interest on Bonds</span>

<span class="c0">2</span>

<span class="c0">6</span>

<span class="c0">Dividend Income</span>

<span class="c0">2</span>

<span class="c0">7</span>

<span class="c0">Interest on Deposits</span>

<span class="c0">2</span>

<span class="c0">8</span>

<span class="c0">Interest on Loans</span>

<span class="c0">2</span>

<span class="c0">9</span>

<span class="c0">Other Income</span>

<span class="c0">2</span>

<span class="c0">10</span>

<span class="c0">Total Deductions</span>

<span class="c0">-</span>

<span class="c0">11</span>

<span class="c0">Investment Expenses</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Investment Management fees</span>

<span class="c0">10</span>

<span class="c0">13</span>

<span class="c0">Taxes</span>

<span class="c0">10</span>

<span class="c0">14</span>

<span class="c0">Other Expenses</span>

<span class="c0">10</span>

<span class="c0">15</span>

<span class="c0">Net Assets, end of the quarter</span>

<span class="c0">-</span>

<span class="c21"></span>

1.  <span class="c21">Non-Life</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 460.00px;">![](images/image34.png)</span>

<span class="c21"></span>

<span id="t.218452a2701d0a3119a6288050440cc054f00091"></span><span
id="t.28"></span>

<span class="c0">Non Life Financial Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Total Assets</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Cash & Invested Assets</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Premiums Receivable</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Reinsurance Accounts Receivable</span>

<span class="c0">1</span>

<span class="c0">5</span>

<span class="c0">Other Assets</span>

<span class="c0">1</span>

<span class="c0">6</span>

<span class="c0">Total Liabilities</span>

<span class="c0">-</span>

<span class="c0">7</span>

<span class="c0">Reserve for Unearned Premiums</span>

<span class="c0">6</span>

<span class="c0">8</span>

<span class="c0">Losses & Claims Payable</span>

<span class="c0">6</span>

<span class="c0">9</span>

<span class="c0">Catastrophe Loss Reserve</span>

<span class="c0">6</span>

<span class="c0">10</span>

<span class="c0">Reinsurance Accounts Payable</span>

<span class="c0">6</span>

<span class="c0">11</span>

<span class="c0">Taxes Payable</span>

<span class="c0">6</span>

<span class="c0">12</span>

<span class="c0">Other Liabilities</span>

<span class="c0">6</span>

<span class="c0">13</span>

<span class="c0">Total Networth</span>

<span class="c0">-</span>

<span class="c0">14</span>

<span class="c0">Paid-Up Capital/ Statutory Deposit</span>

<span class="c0">13</span>

<span class="c0">15</span>

<span class="c0">Capital Paid in Excess of Par Value</span>

<span class="c0">13</span>

<span class="c0">16</span>

<span class="c0">Contributed Surplus/ Home Office/ Inward
Remittances</span>

<span class="c0">13</span>

<span class="c0">17</span>

<span class="c0">Deposit for Future Subscription</span>

<span class="c0">13</span>

<span class="c0">18</span>

<span class="c0">Contingency Surplus</span>

<span class="c0">13</span>

<span class="c0">19</span>

<span class="c0">Investment Fluctuation Reserves</span>

<span class="c0">13</span>

<span class="c0">20</span>

<span class="c0">Other Assigned</span>

<span class="c0">13</span>

<span class="c0">21</span>

<span class="c0">Unassigned/ Retained Earnings/ Home Office
Account</span>

<span class="c0">13</span>

<span class="c21"></span>

<span id="t.ca3fa62924937249cbdfbdb155cc7ee63606c72f"></span><span
id="t.29"></span>

<span class="c0">Non Life Investments Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Bonds</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Government Bonds</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Corporate Bonds</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Short-Term Investments</span>

<span class="c0">-</span>

<span class="c0">5</span>

<span class="c0">Government (Treasury Bills)</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">Corporate Investments</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Stocks</span>

<span class="c0">-</span>

<span class="c0">8</span>

<span class="c0">Real Estate</span>

<span class="c0">-</span>

<span class="c0">9</span>

<span class="c0">Purchase Money Mortgages</span>

<span class="c0">-</span>

<span class="c0">10</span>

<span class="c0">Mortgage Loans on Real Estate</span>

<span class="c0">-</span>

<span class="c0">11</span>

<span class="c0">Collateral Loans</span>

<span class="c0">-</span>

<span class="c0">12</span>

<span class="c0">Guaranteed Loans</span>

<span class="c0">-</span>

<span class="c0">13</span>

<span class="c0">Other Loans</span>

<span class="c0">-</span>

<span class="c0">14</span>

<span class="c0">Mutual Funds</span>

<span class="c0">-</span>

<span class="c0">15</span>

<span class="c0">Unit Investment Trust Funds</span>

<span class="c0">-</span>

<span class="c0">16</span>

<span class="c0">Real Estate Investment Trusts</span>

<span class="c0">-</span>

<span class="c0">17</span>

<span class="c0">Time Deposits/Fixed Deposits</span>

<span class="c0">-</span>

<span class="c0">18</span>

<span class="c0">Other Investments</span>

<span class="c0">-</span>

<span class="c0">19</span>

<span class="c0">Proprietary Shares</span>

<span class="c0">18</span>

<span class="c0">20</span>

<span class="c0">Money Market Placement</span>

<span class="c0">18</span>

<span class="c0">21</span>

<span class="c0">etc\*</span>

<span class="c0">18</span>

<span class="c0">22</span>

<span class="c0">Others</span>

<span class="c0">-</span>

<span class="c0">23</span>

<span class="c0">Exchange Traded Fund</span>

<span class="c0">22</span>

<span class="c0">24</span>

<span class="c0">Securities Borrowing & Lending</span>

<span class="c0">22</span>

<span class="c0">25</span>

<span class="c0">etc\*</span>

<span class="c0">22</span>

<span class="c21"></span>

<span id="t.f35ccf6a76194bf24d568bf2240fac4fe53fa574"></span><span
id="t.30"></span>

<span class="c0">business\_type\_table</span>

<span class="c0">id</span>

<span class="c0">business\_type</span>

<span class="c0">business\_sub\_type</span>

<span class="c0">0</span>

<span class="c0">Fire</span>

<span class="c0">Regular</span>

<span class="c0">1</span>

<span class="c0">Fire</span>

<span class="c0">Microinsurance</span>

<span class="c0">2</span>

<span class="c0">Marine</span>

<span class="c0">None</span>

<span class="c0">3</span>

<span class="c0">Aviation</span>

<span class="c0">None</span>

<span class="c0">4</span>

<span class="c0">Motorcar</span>

<span class="c0">CMVL</span>

<span class="c0">5</span>

<span class="c0">Motorcar</span>

<span class="c0">Non-CMVL</span>

<span class="c0">6</span>

<span class="c0">Health</span>

<span class="c0">Regular</span>

<span class="c0">7</span>

<span class="c0">Health</span>

<span class="c0">Microinsurance</span>

<span class="c0">8</span>

<span class="c0">Health</span>

<span class="c0">Migrant Workers</span>

<span class="c0">9</span>

<span class="c0">Accident</span>

<span class="c0">Regular</span>

<span class="c0">10</span>

<span class="c0">Accident</span>

<span class="c0">Microinsurance</span>

<span class="c0">11</span>

<span class="c0">Accident</span>

<span class="c0">Migrant Workers</span>

<span class="c0">12</span>

<span class="c0">Engineering</span>

<span class="c0">None</span>

<span class="c0">13</span>

<span class="c0">Other Casualty</span>

<span class="c0">Regular</span>

<span class="c0">14</span>

<span class="c0">Other Casualty</span>

<span class="c0">Microinsurance</span>

<span class="c0">15</span>

<span class="c0">Other Casualty</span>

<span class="c0">Migrant Workers</span>

<span class="c0">16</span>

<span class="c0">Suretyship</span>

<span class="c0">None</span>

<span class="c21"></span>

1.  <span class="c21">MBA</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 588.00px;">![](images/image27.png)</span>

<span class="c21"></span>

<span id="t.426c86822bb944feae241c86a2fd1698504b7768"></span><span
id="t.31"></span>

<span class="c0">MBA Financial Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Total Assets</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">Cash and invested assets</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">Members' Fees & Dues Receivable</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Members' Contributions Due and Uncollected</span>

<span class="c0">1</span>

<span class="c0">5</span>

<span class="c0">Net Premiums Due and Uncollected</span>

<span class="c0">1</span>

<span class="c0">6</span>

<span class="c0">Unremitted Members' Contributions, Dues and Fees</span>

<span class="c0">1</span>

<span class="c0">7</span>

<span class="c0">Unremitted Premiums</span>

<span class="c0">1</span>

<span class="c0">8</span>

<span class="c0">Amounts Recoverable from Reinsurers</span>

<span class="c0">1</span>

<span class="c0">9</span>

<span class="c0">Other Assets</span>

<span class="c0">1</span>

<span class="c0">10</span>

<span class="c0">Total Liabilities</span>

<span class="c0">-</span>

<span class="c0">11</span>

<span class="c0">Liability on Individual Equity Value</span>

<span class="c0">10</span>

<span class="c0">12</span>

<span class="c0">Basic Contingent Benefit Reserve</span>

<span class="c0">10</span>

<span class="c0">13</span>

<span class="c0">Optional Benefit Reserve</span>

<span class="c0">10</span>

<span class="c0">14</span>

<span class="c0">Claims Payable on Basic Contingent Benefit</span>

<span class="c0">10</span>

<span class="c0">15</span>

<span class="c0">Claims Payable on Optional Benefits</span>

<span class="c0">10</span>

<span class="c0">16</span>

<span class="c0">Amounts Due to Reinsurers</span>

<span class="c0">10</span>

<span class="c0">17</span>

<span class="c0">Other Liabilites</span>

<span class="c0">10</span>

<span class="c0">18</span>

<span class="c0">Total Fund Balance</span>

<span class="c0">-</span>

<span class="c0">19</span>

<span class="c0">Assigned Fund Balance</span>

<span class="c0">18</span>

<span class="c0">20</span>

<span class="c0">Funds Assigned for Guarranty Fund</span>

<span class="c0">19</span>

<span class="c0">21</span>

<span class="c0">Funds Assigned for Members' Benefits</span>

<span class="c0">19</span>

<span class="c0">22</span>

<span class="c0">Funds Assigned for Community Development</span>

<span class="c0">19</span>

<span class="c0">23</span>

<span class="c0">Others</span>

<span class="c0">19</span>

<span class="c0">24</span>

<span class="c0">Revaluation/ Fluctuation Reserve</span>

<span class="c0">18</span>

<span class="c0">25</span>

<span class="c0">Free and Unassigned Fund Balance</span>

<span class="c0">18</span>

<span class="c21"></span>

<span id="t.9a7fb1ef30e0d981bd265584f41281f10c5aeaea"></span><span
id="t.32"></span>

<span class="c0">MBA Investments Accounts Table</span>

<span class="c0">id</span>

<span class="c0">account name</span>

<span class="c0">parent id</span>

<span class="c0">1</span>

<span class="c0">Long Term Investments</span>

<span class="c0">-</span>

<span class="c0">2</span>

<span class="c0">a. Government Bonds</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">b. Corporate Bonds</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">Short Term Investments</span>

<span class="c0">-</span>

<span class="c0">5</span>

<span class="c0">a. Government (Treasury Bills)</span>

<span class="c0">4</span>

<span class="c0">6</span>

<span class="c0">b. Corporate Investments</span>

<span class="c0">4</span>

<span class="c0">7</span>

<span class="c0">Stocks</span>

<span class="c0">-</span>

<span class="c0">8</span>

<span class="c0">Investment in Properties</span>

<span class="c0">-</span>

<span class="c0">9</span>

<span class="c0">Loans</span>

<span class="c0">-</span>

<span class="c0">10</span>

<span class="c0">a. Membership Certificate Loans</span>

<span class="c0">9</span>

<span class="c0">11</span>

<span class="c0">b. Policy Loans</span>

<span class="c0">9</span>

<span class="c0">12</span>

<span class="c0">c. Other Loans Receivable</span>

<span class="c0">9</span>

<span class="c0">13</span>

<span class="c0">Time Deposits/ Fixed Deposits</span>

<span class="c0">-</span>

<span class="c0">14</span>

<span class="c0">Other Investments</span>

<span class="c0">-</span>

<span class="c0">15</span>

<span class="c0">a. Proprietary Shares</span>

<span class="c0">14</span>

<span class="c0">16</span>

<span class="c0">b. Money Market Placements</span>

<span class="c0">14</span>

<span class="c0">17</span>

<span class="c0">c. Others</span>

<span class="c0">14</span>

<span class="c21"></span>

<span id="t.996f13e5d27dd53a5a8669580c51dc4ba17f8758"></span><span
id="t.33"></span>

<span class="c0">business\_type\_table</span>

<span class="c0">id</span>

<span class="c0">business\_type</span>

<span class="c0">0</span>

<span class="c0">micro business</span>

<span class="c0">1</span>

<span class="c0">non micro business</span>

<span class="c21"></span>

<span id="t.de5cf9e21f27172acdd6533782f2ac6e09b09ef4"></span><span
id="t.34"></span>

<span class="c0">basic\_sub\_type\_table</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">sub\_type</span>

<span class="c0">0</span>

<span class="c0">Beginning Balance</span>

<span class="c0">None</span>

<span class="c0">1</span>

<span class="c0">New Business</span>

<span class="c0">New Issues</span>

<span class="c0">2</span>

<span class="c0">New Business</span>

<span class="c0">Reinstated</span>

<span class="c0">3</span>

<span class="c0">New Business</span>

<span class="c0">Others</span>

<span class="c0">4</span>

<span class="c0">Terminations</span>

<span class="c0">Deaths</span>

<span class="c0">5</span>

<span class="c0">Terminations</span>

<span class="c0">Surrenders</span>

<span class="c0">6</span>

<span class="c0">Terminations</span>

<span class="c0">Lapsed</span>

<span class="c0">7</span>

<span class="c0">Terminations</span>

<span class="c0">Matured</span>

<span class="c0">8</span>

<span class="c0">Terminations</span>

<span class="c0">Others</span>

<span class="c21"></span>

<span id="t.36b48b2d833da0a5c44c11b049c9149fd42781c6"></span><span
id="t.35"></span>

<span class="c0">product\_type</span>

<span class="c0">id</span>

<span class="c0">product\_type</span>

<span class="c0">0</span>

<span class="c0">micro product</span>

<span class="c0">1</span>

<span class="c0">non micro product</span>

<span class="c21"></span>

<span id="t.de04725e2dc87036bd13d064312b271b86a308b7"></span><span
id="t.36"></span>

<span class="c0">basic\_member\_benefit\_type\_table</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">0</span>

<span class="c0">Member</span>

<span class="c0">1</span>

<span class="c0">Dependents</span>

<span class="c21"></span>

1.  ### <span class="c26">Report on Compulsory Insurance Coverage for Agency-Hired Migrant Workers</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 597.33px;">![](images/image30.png)</span>

<span class="c21"></span>

<span id="t.adcae73204ab0ae9a7e40abe0da9d4f41e1d7fa9"></span><span
id="t.37"></span>

<span class="c0">type\_of\_insurance\_table</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">subtype</span>

<span class="c0">0</span>

<span class="c0">Life</span>

<span class="c0">Land Based</span>

<span class="c0">1</span>

<span class="c0">Life</span>

<span class="c0">Personal Accident</span>

<span class="c0">2</span>

<span class="c0">Life</span>

<span class="c0">Medical Reimbursement</span>

<span class="c0">3</span>

<span class="c0">Life</span>

<span class="c0">Others</span>

<span class="c0">4</span>

<span class="c0">Sea Based</span>

<span class="c0">Natural Death</span>

<span class="c0">5</span>

<span class="c0">Sea Based</span>

<span class="c0">Personal Accident</span>

<span class="c0">6</span>

<span class="c0">Sea Based</span>

<span class="c0">Medical Reimbursement</span>

<span class="c0">7</span>

<span class="c0">Sea Based</span>

<span class="c0">Others</span>

<span class="c21"></span>

1.  <span class="c75">Rating Division</span>
    ----------------------------------------

<!-- -->

1.  ### <span class="c26">Approval of Premium Rates on Fire/ Motor Car Insurance and Surety</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 110.67px;">![](images/image1.png)</span>

1.  ### <span class="c26">Submission and Monitoring of Data from Compulsory Third Party Liability (CTPL)</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 337.33px;">![](images/image36.png)</span>

<span class="c21"></span>

<span id="t.146827080e7302bef4e4d67d24331f0a5615636c"></span><span
id="t.38"></span>

<span class="c0">vehicle types ctpl</span>

<span class="c0">id</span>

<span class="c0">vehicle type</span>

<span class="c0">1</span>

<span class="c0">Private Cars</span>

<span class="c0">2</span>

<span class="c0">Light Medium Trucks</span>

<span class="c0">3</span>

<span class="c0">Heavy Trucks</span>

<span class="c0">4</span>

<span class="c0">AC Tourits Cars</span>

<span class="c0">5</span>

<span class="c0">Taxi, PUJ and Mini Bus</span>

<span class="c0">6</span>

<span class="c0">PUB and Tourist Bus</span>

<span class="c0">7</span>

<span class="c0">Motorcycles/Tricycles/Trailers</span>

<span class="c0">8</span>

<span class="c0">Private Cars\*</span>

<span class="c0">9</span>

<span class="c0">Light Medium Trucks\*</span>

<span class="c0">10</span>

<span class="c0">Heavy Trucks\*</span>

<span class="c0">11</span>

<span class="c0">AC Tourits Cars\*</span>

<span class="c0">12</span>

<span class="c0">Taxi, PUJ and Mini Bus\*</span>

<span class="c0">13</span>

<span class="c0">PUB and Tourist Bus\*</span>

<span class="c0">14</span>

<span class="c0">Motorcycles/Tricycles/Trailers\*</span>

<span class="c21"></span>

1.  ### <span class="c26">Submission and Monitoring of Data from Passenger Personal Accident Insurance (PPAI)</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 561.33px;">![](images/image38.png)</span>

<span class="c21"></span>

<span id="t.2a40e402a05babda921c23fb2bcd57bc499c3dce"></span><span
id="t.39"></span>

<span class="c0">vehicle types ppai</span>

<span class="c0">id</span>

<span class="c0">vehicle type</span>

<span class="c0">1</span>

<span class="c0">AUV</span>

<span class="c0">2</span>

<span class="c0">BUS</span>

<span class="c0">3</span>

<span class="c0">JEEP</span>

<span class="c0">4</span>

<span class="c0">SEDAN</span>

<span class="c0">5</span>

<span class="c0">TRUCK</span>

<span class="c21"></span>

1.  ### <span class="c26">Off-site Examination of Non-Life Insurance Companies</span>

<!-- -->

1.  #### <span class="c69">Motor Car Total Loss</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 633.33px;">![](images/image25.png)</span>

1.  #### <span class="c69">Bonds</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 754.67px;">![](images/image5.png)</span>

<span class="c21"></span>

<span id="t.a43ac1200994fa9d3c5c6ee2a6d846e82f3abac7"></span><span
id="t.40"></span>

<span class="c0">bond class</span>

<span class="c0">id</span>

<span class="c0">class</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">5</span>

<span class="c0">5</span>

<span class="c21"></span>

<span id="t.9d795aa2adfafe7b40978486dc994bbcd43574f7"></span><span
id="t.41"></span>

<span class="c0">rate of bonds</span>

<span class="c0">id</span>

<span class="c0">class</span>

<span class="c0">max</span>

<span class="c0">base\_rate</span>

<span class="c0">rate\_step</span>

<span class="c0">base\_over</span>

<span class="c0">penalty\_rate</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">15000</span>

<span class="c0">0.48</span>

<span class="c0">0.048</span>

<span class="c0">0</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">50000</span>

<span class="c0">0.96</span>

<span class="c0">0.048</span>

<span class="c0">312</span>

<span class="c0">0.288</span>

<span class="c0">3</span>

<span class="c0">3</span>

<span class="c0">100000</span>

<span class="c0">1.92</span>

<span class="c0">0.048</span>

<span class="c0">1104</span>

<span class="c0">0.288</span>

<span class="c0">4</span>

<span class="c0">4</span>

<span class="c0">150000</span>

<span class="c0">2.88</span>

<span class="c0">0.048</span>

<span class="c0">2376</span>

<span class="c0">0.288</span>

<span class="c0">5</span>

<span class="c0">5</span>

<span class="c0">200000</span>

<span class="c0">3.84</span>

<span class="c0">0.048</span>

<span class="c0">4128</span>

<span class="c0">0.288</span>

<span class="c21"></span>

<span id="t.d74857e82eb9fc36bdec597e86d6d21fc4e6be48"></span><span
id="t.42"></span>

<span class="c0">bond type table</span>

<span class="c0">id</span>

<span class="c0">class</span>

<span class="c0">bond type</span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c0">bidders bond</span>

<span class="c0">2</span>

<span class="c0">2</span>

<span class="c0">berthing bond to pay</span>

<span class="c0">3</span>

<span class="c0">2</span>

<span class="c0">bidders bond standing</span>

<span class="c0">4</span>

<span class="c0">2</span>

<span class="c0">cigarette stamps bond</span>

<span class="c0">5</span>

<span class="c0">2</span>

<span class="c0">ships side bond</span>

<span class="c0">6</span>

<span class="c0">2</span>

<span class="c0">tonnage bond to pay</span>

<span class="c0">7</span>

<span class="c0">2</span>

<span class="c0">wharfage bond to pay</span>

<span class="c0">8</span>

<span class="c0">3</span>

<span class="c0">administrator s bond</span>

<span class="c0">9</span>

<span class="c0">3</span>

<span class="c0">arrastre bond</span>

<span class="c0">10</span>

<span class="c0">3</span>

<span class="c0">attachment bond</span>

<span class="c0">11</span>

<span class="c0">3</span>

<span class="c0">bid, performance bond</span>

<span class="c0">12</span>

<span class="c0">3</span>

<span class="c0">broker s bond</span>

<span class="c0">13</span>

<span class="c0">3</span>

<span class="c0">countervailing bond</span>

<span class="c0">14</span>

<span class="c0">3</span>

<span class="c0">fidelity bond individual</span>

<span class="c0">15</span>

<span class="c0">3</span>

<span class="c0">firearm bond</span>

<span class="c0">16</span>

<span class="c0">3</span>

<span class="c0">fishpond bond</span>

<span class="c0">17</span>

<span class="c0">3</span>

<span class="c0">guardian s bond</span>

<span class="c0">18</span>

<span class="c0">3</span>

<span class="c0">importer s bond general performance</span>

<span class="c0">19</span>

<span class="c0">3</span>

<span class="c0">reconstituted title bond</span>

<span class="c0">20</span>

<span class="c0">3</span>

<span class="c0">replevin bond</span>

<span class="c0">21</span>

<span class="c0">4</span>

<span class="c0">advanced baggage bond</span>

<span class="c0">22</span>

<span class="c0">4</span>

<span class="c0">anti-dumping bond</span>

<span class="c0">23</span>

<span class="c0">4</span>

<span class="c0">common carrier bond</span>

<span class="c0">24</span>

<span class="c0">4</span>

<span class="c0">contract worker s bond</span>

<span class="c0">25</span>

<span class="c0">4</span>

<span class="c0">execution pending appeal bond</span>

<span class="c0">26</span>

<span class="c0">4</span>

<span class="c0">export bond, re \[other than motor vehicles\]</span>

<span class="c0">27</span>

<span class="c0">4</span>

<span class="c0">forestry bond</span>

<span class="c0">28</span>

<span class="c0">4</span>

<span class="c0">forestry bond \[BFD\]</span>

<span class="c0">29</span>

<span class="c0">4</span>

<span class="c0">guarantee bond \[CB/re-import\]</span>

<span class="c0">30</span>

<span class="c0">4</span>

<span class="c0">guarantee bond \[maintenance/warranty\]</span>

<span class="c0">31</span>

<span class="c0">4</span>

<span class="c0">heir s bond</span>

<span class="c0">32</span>

<span class="c0">4</span>

<span class="c0">injunction bond, plaintiff</span>

<span class="c0">33</span>

<span class="c0">4</span>

<span class="c0">land carrier bond for \[TH\]</span>

<span class="c0">34</span>

<span class="c0">4</span>

<span class="c0">manufacturers official bond</span>

<span class="c0">35</span>

<span class="c0">4</span>

<span class="c0">millers bond \[NDA\]</span>

<span class="c0">36</span>

<span class="c0">4</span>

<span class="c0">milling contract bond \[NFA\]</span>

<span class="c0">37</span>

<span class="c0">4</span>

<span class="c0">miner s official bond</span>

<span class="c0">38</span>

<span class="c0">4</span>

<span class="c0">payment bond \[for construction\]</span>

<span class="c0">39</span>

<span class="c0">4</span>

<span class="c0">penalty and forfeiture bond</span>

<span class="c0">40</span>

<span class="c0">4</span>

<span class="c0">receiver s bond</span>

<span class="c0">41</span>

<span class="c0">4</span>

<span class="c0">receiver s bond, to appoint</span>

<span class="c0">42</span>

<span class="c0">4</span>

<span class="c0">replacement bond for lost TW, checks, ets</span>

<span class="c0">43</span>

<span class="c0">4</span>

<span class="c0">stock broker s and dealer s bond</span>

<span class="c0">44</span>

<span class="c0">4</span>

<span class="c0">tax exemption bond</span>

<span class="c0">45</span>

<span class="c0">4</span>

<span class="c0">truck operator s \[T\] bond for</span>

<span class="c0">46</span>

<span class="c0">4</span>

<span class="c0">valuation differences bond</span>

<span class="c0">47</span>

<span class="c0">4</span>

<span class="c0">warehouse bond, general</span>

<span class="c0">48</span>

<span class="c0">4</span>

<span class="c0">warehouse bond, for raw materials</span>

<span class="c0">49</span>

<span class="c0">4</span>

<span class="c0">warehouse bond, rice bonded \[BODT\]</span>

<span class="c0">50</span>

<span class="c0">4</span>

<span class="c0">warehouse bond, rice bonded \[NFA\]</span>

<span class="c0">51</span>

<span class="c0">5</span>

<span class="c0">attachment bond, to lift</span>

<span class="c0">52</span>

<span class="c0">5</span>

<span class="c0">bail bond</span>

<span class="c0">53</span>

<span class="c0">5</span>

<span class="c0">BOI omnibus bond</span>

<span class="c0">54</span>

<span class="c0">5</span>

<span class="c0">contract grower s bond</span>

<span class="c0">55</span>

<span class="c0">5</span>

<span class="c0">dealership bond</span>

<span class="c0">56</span>

<span class="c0">5</span>

<span class="c0">energy development bond \[BOED\]</span>

<span class="c0">57</span>

<span class="c0">5</span>

<span class="c0">export bond, re \[motor vehicles only\]</span>

<span class="c0">58</span>

<span class="c0">5</span>

<span class="c0">hauler s bond</span>

<span class="c0">59</span>

<span class="c0">5</span>

<span class="c0">immigration bond</span>

<span class="c0">60</span>

<span class="c0">5</span>

<span class="c0">indemnity bond, 3rd party, sheriffs</span>

<span class="c0">61</span>

<span class="c0">5</span>

<span class="c0">injunction bond, to lift</span>

<span class="c0">62</span>

<span class="c0">5</span>

<span class="c0">payment of taxes/duties bond</span>

<span class="c0">63</span>

<span class="c0">5</span>

<span class="c0">production of certificate of landing bond</span>

<span class="c0">64</span>

<span class="c0">5</span>

<span class="c0">quedans financing bond \[NFA\]</span>

<span class="c0">65</span>

<span class="c0">5</span>

<span class="c0">receivership bond, to lift</span>

<span class="c0">66</span>

<span class="c0">5</span>

<span class="c0">replevin bond, counter</span>

<span class="c0">67</span>

<span class="c0">5</span>

<span class="c0">supersedeas bond</span>

<span class="c0">68</span>

<span class="c0">5</span>

<span class="c0">surety bond, general</span>

<span class="c0">69</span>

<span class="c0">5</span>

<span class="c0">sweepstakes bond \[one year\]</span>

<span class="c21"></span>

<span id="t.78ae00e1035ee2b6e48f09a8d7e652155d6e8cca"></span><span
id="t.43"></span>

<span class="c0">special type bond table</span>

<span class="c0">id</span>

<span class="c0">bond\_type\_id</span>

<span class="c0">callable</span>

<span class="c0">max\_amt</span>

<span class="c0">penalty</span>

<span class="c0">1</span>

<span class="c0">11</span>

<span class="c0">1</span>

<span class="c0">300,000</span>

<span class="c0">0.55</span>

<span class="c0">2</span>

<span class="c0">11</span>

<span class="c0">0</span>

<span class="c0">300,000</span>

<span class="c0">0.5</span>

<span class="c0">3</span>

<span class="c0">68</span>

<span class="c0">1</span>

<span class="c0">500,000</span>

<span class="c0">0.6</span>

<span class="c0">4</span>

<span class="c0">68</span>

<span class="c0">0</span>

<span class="c0">500,000</span>

<span class="c0">0.55</span>

<span class="c21"></span>

1.  #### <span class="c69">Adjuster’s Quarterly Report</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 601.00px; height: 499.00px;">![](images/image13.png)</span>

1.  #### <span class="c69">Fire Service Tax (FST) Remittances</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 334.67px;">![](images/image15.png)</span>

1.  <span class="c75">Investment Division</span>
    --------------------------------------------

<!-- -->

1.  ### <span class="c26">Approval of Investments of Insurance Companies and Others (e.g. banks and investment houses)</span>

<span class="c21"></span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 282.67px;">![](images/image23.png)</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21">                        </span>

<span id="t.ced896f84996e9600904616d14d37f6bb61ab1b4"></span><span
id="t.44"></span>

<span class="c0">Type of Investment</span>

<span class="c0">id</span>

<span class="c0">type</span>

<span class="c0">0</span>

<span class="c0">Mutual Funds</span>

<span class="c0">1</span>

<span class="c0">UITF</span>

<span class="c0">2</span>

<span class="c0">Corporate Bonds</span>

<span class="c0">3</span>

<span class="c0">Foreign Currency Denominated Corporate Bonds or
Stocks</span>

<span class="c0">4</span>

<span class="c0">Preferred Stocks</span>

<span class="c0">5</span>

<span class="c0">Common Stocks</span>

<span class="c0">6</span>

<span class="c0">Purchase of Real Estate Properties</span>

<span class="c0">7</span>

<span class="c0">Valuation of Real Estate Properties</span>

<span class="c0">8</span>

<span class="c0">Income Producing Properties</span>

<span class="c0">9</span>

<span class="c0">EDP</span>

<span class="c0">10</span>

<span class="c0">Derivatives</span>

<span class="c0">11</span>

<span class="c0">Funds (Underlying for VUL Products)</span>

<span class="c0">12</span>

<span class="c0">Mortgage</span>

<span class="c0">13</span>

<span class="c0">Approval of IMA</span>

<span class="c0">14</span>

<span class="c0">Loan Facilities(Term Loan)</span>

<span class="c0">15</span>

<span class="c0">LTNCD</span>

<span class="c0">16</span>

<span class="c0">Commercial Paper</span>

<span class="c0">17</span>

<span class="c0">Salary Loan</span>

<span class="c0">18</span>

<span class="c0">Financial Assistance</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.007427598f18c561c05429ec571868d1d08532f9"></span><span
id="t.45"></span>

<span class="c0">Investment Requirements</span>

<span class="c0">id</span>

<span class="c0">type\_of\_investment\_id</span>

<span class="c0">requirement</span>

<span class="c0">investment\_cl\_id</span>

<span class="c0">0</span>

<span class="c0">0</span>

<span class="c0">SEC Approval</span>

<span class="c0">0</span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">Prospectus/Fund Fact Sheet</span>

<span class="c0">0</span>

<span class="c0">2</span>

<span class="c0">1</span>

<span class="c0">BSP Approval</span>

<span class="c0">1</span>

<span class="c0">3</span>

<span class="c0">1</span>

<span class="c0">Prospectus/Fund Fact Sheet</span>

<span class="c0">1</span>

<span class="c0">4</span>

<span class="c0">2</span>

<span class="c0">Prospectus</span>

<span class="c0">2</span>

<span class="c0">5</span>

<span class="c0">2</span>

<span class="c0">Latest 3 Year Audited F/S</span>

<span class="c0">2</span>

<span class="c0">6</span>

<span class="c0">2</span>

<span class="c0">Credit Rating</span>

<span class="c0">3</span>

<span class="c0">7</span>

<span class="c0">2</span>

<span class="c0">SEC Approval</span>

<span class="c0">3</span>

<span class="c0">8</span>

<span class="c0">3</span>

<span class="c0">Prospectus</span>

<span class="c0">4</span>

<span class="c0">9</span>

<span class="c0">3</span>

<span class="c0">Latest 3 Year Audited F/S</span>

<span class="c0">4</span>

<span class="c0">10</span>

<span class="c0">3</span>

<span class="c0">Credit Rating</span>

<span class="c0">4</span>

<span class="c0">11</span>

<span class="c0">3</span>

<span class="c0">SEC Approval</span>

<span class="c0">4</span>

<span class="c0">12</span>

<span class="c0">4</span>

<span class="c0">Prospectus</span>

<span class="c0">5</span>

<span class="c0">13</span>

<span class="c0">4</span>

<span class="c0">Latest 3 Year Audited F/S</span>

<span class="c0">5</span>

<span class="c0">14</span>

<span class="c0">4</span>

<span class="c0">SEC Approval</span>

<span class="c0">5</span>

<span class="c0">15</span>

<span class="c0">4</span>

<span class="c0">PSE Listing Approval (IPO)</span>

<span class="c0">5</span>

<span class="c0">16</span>

<span class="c0">5</span>

<span class="c0">Prospectus</span>

<span class="c0">6</span>

<span class="c0">17</span>

<span class="c0">5</span>

<span class="c0">Latest 3 Year Audited F/S</span>

<span class="c0">6</span>

<span class="c0">18</span>

<span class="c0">6</span>

<span class="c0">TCT/CCT In Company's Name</span>

<span class="c0">7</span>

<span class="c0">19</span>

<span class="c0">6</span>

<span class="c0">Copy Of Absolute Deed Of Sale</span>

<span class="c0">7</span>

<span class="c0">20</span>

<span class="c0">6</span>

<span class="c0">All Other Terms And Conditions Of The Purchase</span>

<span class="c0">7</span>

<span class="c0">21</span>

<span class="c0">7</span>

<span class="c0">Appraisal Report By An Appraisal Company Duly
Accredited By SEC</span>

<span class="c0">8</span>

<span class="c0">22</span>

<span class="c0">7</span>

<span class="c0">Photocopy Of TCT/CCT</span>

<span class="c0">8</span>

<span class="c0">23</span>

<span class="c0">7</span>

<span class="c0">Latest Real Estate Tax Declaration</span>

<span class="c0">8</span>

<span class="c0">24</span>

<span class="c0">7</span>

<span class="c0">Latest Real Estate Tax Payment Official Receipt</span>

<span class="c0">8</span>

<span class="c0">25</span>

<span class="c0">8</span>

<span class="c0">TCT/CCT In Company's Name</span>

<span class="c0">10</span>

<span class="c0">26</span>

<span class="c0">8</span>

<span class="c0">Copy Of Absolute Deed Of Sale</span>

<span class="c0">10</span>

<span class="c0">27</span>

<span class="c0">8</span>

<span class="c0">All Other Terms And Conditions Of The Purchase</span>

<span class="c0">10</span>

<span class="c0">28</span>

<span class="c0">9</span>

<span class="c0">Copy Of Official Receipt/Proof Of Acquisition</span>

<span class="c0">11</span>

<span class="c0">29</span>

<span class="c0">9</span>

<span class="c0">Summary Of Purchased Equipment</span>

<span class="c0">11</span>

<span class="c0">30</span>

<span class="c0">10</span>

<span class="c0">Written Request For Approval Stating Objectives And
Proof That The Company Understands And Able To Manage Risks</span>

<span class="c0">12</span>

<span class="c0">31</span>

<span class="c0">10</span>

<span class="c0">Duly Accomplished Questionnaire From IC (Annex
A)</span>

<span class="c0">12</span>

<span class="c0">32</span>

<span class="c0">10</span>

<span class="c0">ISDA And CSA (If Applicable)</span>

<span class="c0">12</span>

<span class="c0">33</span>

<span class="c0">11</span>

<span class="c0">Prospectus/General Information/Features Of The
Funds</span>

<span class="c0">13</span>

<span class="c0">34</span>

<span class="c0">11</span>

<span class="c0">Pertinent Regulatory Approvals</span>

<span class="c0">13</span>

<span class="c0">35</span>

<span class="c0">11</span>

<span class="c0">List Of Products That Will Be Linked To Each
Fund</span>

<span class="c0">13</span>

<span class="c0">36</span>

<span class="c0">11</span>

<span class="c0">Surplus Shall In No Event Be Less Than Php 1.0
Million</span>

<span class="c0">13</span>

<span class="c0">37</span>

<span class="c0">12</span>

<span class="c0">Mortgage Loan Contract</span>

<span class="c0">14</span>

<span class="c0">38</span>

<span class="c0">12</span>

<span class="c0">Mortgage Loan Schedule</span>

<span class="c0">15</span>

<span class="c0">39</span>

<span class="c0">12</span>

<span class="c0">Appraisal Report By An Appraisal Company Duly
Accredited By SEC</span>

<span class="c0">15</span>

<span class="c0">40</span>

<span class="c0">13</span>

<span class="c0">Board Resolution Authorizing the Placements under
IMA</span>

<span class="c0">16</span>

<span class="c0">41</span>

<span class="c0">13</span>

<span class="c0">Pro-Forma Copy Of The IMA</span>

<span class="c0">16</span>

<span class="c0">42</span>

<span class="c0">13</span>

<span class="c0">Three (3) Year Audited Financial Statements Of The
Investment Or Fund Manager</span>

<span class="c0">16</span>

<span class="c0">43</span>

<span class="c0">13</span>

<span class="c0">Investment/Fund Manager Shall Be Duly Licensed And
Authorized Bank Of BSP</span>

<span class="c0">16</span>

<span class="c0">44</span>

<span class="c0">14</span>

<span class="c0">Issuer's Credit Rating</span>

<span class="c0">17</span>

<span class="c0">45</span>

<span class="c0">14</span>

<span class="c0">Certificate Of No Event Of Default</span>

<span class="c0">17</span>

<span class="c0">46</span>

<span class="c0">14</span>

<span class="c0">Three (3) Year Audited Financial Statements Of The
Borrower</span>

<span class="c0">17</span>

<span class="c0">47</span>

<span class="c0">15</span>

<span class="c0">Issuer's Credit Rating</span>

<span class="c0">18</span>

<span class="c0">48</span>

<span class="c0">15</span>

<span class="c0">BSP Approval</span>

<span class="c0">18</span>

<span class="c0">49</span>

<span class="c0">15</span>

<span class="c0">Three (3) Year Audited Financial Statements Of The
Borrower</span>

<span class="c0">19</span>

<span class="c0">50</span>

<span class="c0">16</span>

<span class="c0">Pre-Approval</span>

<span class="c0">20</span>

<span class="c0">51</span>

<span class="c0">16</span>

<span class="c0">Three (3) Year Audited Financial Statements Of The
Borrower</span>

<span class="c0">20</span>

<span class="c0">52</span>

<span class="c0">17</span>

<span class="c0">Board Resolution</span>

<span class="c0">20</span>

<span class="c0">53</span>

<span class="c0">17</span>

<span class="c0">Terms And Conditions</span>

<span class="c0">20</span>

<span class="c0">54</span>

<span class="c0">17</span>

<span class="c0">MOA</span>

<span class="c0">20</span>

<span class="c0">55</span>

<span class="c0">18</span>

<span class="c0">Board Resolution Approving The Financial
Assistance</span>

<span class="c0">21</span>

<span class="c0">56</span>

<span class="c0">18</span>

<span class="c0">Terms And Conditions</span>

<span class="c0">21</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.adc02884f3fe5d348b23f199637ddbcec831bb06"></span><span
id="t.46"></span>

<span class="c0">Circular Letters</span>

<span class="c0">id</span>

<span class="c0">circular\_num</span>

<span class="c0">letters</span>

<span class="c0">0</span>

<span class="c0">CL No. 2014-50</span>

<span class="c0"></span>

<span class="c0">1</span>

<span class="c0">CL No. 2014-50</span>

<span class="c0"></span>

<span class="c0">2</span>

<span class="c0">Section 206 (b)(4)</span>

<span class="c0"></span>

<span class="c0">3</span>

<span class="c0">Section 211</span>

<span class="c0"></span>

<span class="c0">4</span>

<span class="c0">CL No. 2014-19</span>

<span class="c0"></span>

<span class="c0">5</span>

<span class="c0">Section 206 (b)(5)</span>

<span class="c0"></span>

<span class="c0">6</span>

<span class="c0">Section 206 (b)(6)</span>

<span class="c0"></span>

<span class="c0">7</span>

<span class="c0">Section 206 (b)(1)</span>

<span class="c0"></span>

<span class="c0">8</span>

<span class="c0">CL No. 2016-16</span>

<span class="c0"></span>

<span class="c0">9</span>

<span class="c0">Section 208 (a)</span>

<span class="c0"></span>

<span class="c0">10</span>

<span class="c0">CL No. 2017-43</span>

<span class="c0"></span>

<span class="c0">11</span>

<span class="c0">CL No. 2014-18</span>

<span class="c0"></span>

<span class="c0">12</span>

<span class="c0">CL No. 2015-56</span>

<span class="c0"></span>

<span class="c0">13</span>

<span class="c0">Section 243</span>

<span class="c0"></span>

<span class="c0">14</span>

<span class="c0">Section 204 (j)(l)</span>

<span class="c0"></span>

<span class="c0">15</span>

<span class="c0">CL No. 2014-21</span>

<span class="c0"></span>

<span class="c0">16</span>

<span class="c0">CL No. 2015-41-A</span>

<span class="c0"></span>

<span class="c0">17</span>

<span class="c0">Section 206 (b)(4)</span>

<span class="c0"></span>

<span class="c0">18</span>

<span class="c0">Section 204 (g)</span>

<span class="c0"></span>

<span class="c0">19</span>

<span class="c0">CL No. 2014-21</span>

<span class="c0"></span>

<span class="c0">20</span>

<span class="c0">CL No. 2018-43</span>

<span class="c0"></span>

<span class="c0">21</span>

<span class="c0">CL No. 2014-20</span>

<span class="c0"></span>

<span class="c21"></span>

1.  ### <span class="c26">Quarterly Submission and Monitoring of Statement of Paid Up Capital and Reserve Investment (SPUCRI)</span>

<span class="c21"></span>

<span class="c93">No ERD because there are no data that is yet to be
submitted by the Investment Division</span>

1.  ### <span class="c26">Monthly Submission and Assessment of Investments Made, Sold, and Disposed</span>

<span class="c21"></span>

<span class="c21">Investment Made and Sold - Monthly</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 369.33px;">![](images/image7.png)</span>

<span class="c21"></span>

<span class="c21"></span>

<span class="c21">        </span>

<span class="c21">        Investment Made and Sold - Quarterly
(MBA)</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 330.67px;">![](images/image11.png)</span>

1.  <span class="c75">Actuarial Division</span>
    -------------------------------------------

<!-- -->

1.  ### <span class="c26">Approval of Insurance Products under Expeditious Approval Process</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 365.33px;">![](images/image42.png)</span>

1.  ### <span class="c26">Submission of Insurance Products under Non-Expeditious Approval Process</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 426.67px;">![](images/image31.png)</span>

1.  <span class="c75">Reinsurance Division</span>
    ---------------------------------------------

<!-- -->

1.  ### <span class="c26">Submission and Monitoring of Facultative and Non-Facultative Placements</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 551.00px; height: 414.00px;">![](images/image40.png)</span>

<span class="c21"></span>

<span id="t.dfdc7d9ff63b23e6b33c41ba8946409439ab0bd0"></span><span
id="t.47"></span>

<span class="c56">Requirements</span>

<span class="c0">name</span>

<span class="c0">description</span>

<span class="c0">Name of Insurer</span>

<span class="c0"></span>

<span class="c0">Line of Business</span>

<span class="c0"></span>

<span class="c0">Policy Number</span>

<span class="c0"></span>

<span class="c0">Amount Ceded</span>

<span class="c0"></span>

<span class="c0">Sum Insured</span>

<span class="c0"></span>

<span class="c0">...</span>

<span class="c0">...</span>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.4102caf2c8ca7d17b2d483c926836c9394142d7d"></span><span
id="t.48"></span>

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><span class="c56">Report Types</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">report_type</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Facultative and Non-Facultative Placements</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Particulars of Reinsurance Treaties</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Foreign Receipts and Remittances</span></p></td>
</tr>
</tbody>
</table>

<span class="c21"></span>

1.  ### <span class="c26">Submission and Monitoring of Particulars of Reinsurance Treaties</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 321.33px;">![](images/image26.png)</span>

<span class="c61"></span>

<span id="t.4102caf2c8ca7d17b2d483c926836c9394142d7d"></span><span
id="t.49"></span>

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><span class="c56">Report Types</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">report_type</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Facultative and Non-Facultative Placements</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Particulars of Reinsurance Treaties</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Foreign Receipts and Remittances</span></p></td>
</tr>
</tbody>
</table>

<span class="c61"></span>

<span class="c61"></span>

<span id="t.ae4297097cd9a1d82a7462a1208328ca17148844"></span><span
id="t.50"></span>

<span class="c0">Name of requirements</span>

<span class="c0">name</span>

<span class="c0">description</span>

<span class="c0">with\_particulars</span>

<span class="c0">with\_percentage\_to\_equity</span>

<span class="c0">Name of Company</span>

<span class="c0"></span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">Kind of Treaty</span>

<span class="c0"></span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">Resident Agent</span>

<span class="c0"></span>

<span class="c0">1</span>

<span class="c0">0</span>

<span class="c0">Premium Reserved</span>

<span class="c0"></span>

<span class="c0">1</span>

<span class="c0">1</span>

<span class="c61"></span>

1.  ### <span class="c26">Submission and Monitoring of Foreign Receipts and Remittances</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 472.00px;">![](images/image18.png)</span>

<span class="c21"></span>

<span id="t.4102caf2c8ca7d17b2d483c926836c9394142d7d"></span><span
id="t.51"></span>

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><span class="c56">Report Types</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">report_type</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Facultative and Non-Facultative Placements</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Particulars of Reinsurance Treaties</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Foreign Receipts and Remittances</span></p></td>
</tr>
</tbody>
</table>

<span class="c21"></span>

<span class="c21"></span>

<span id="t.42943f84a47c1209a088ec3871d20981b2da0faa"></span><span
id="t.52"></span>

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><span class="c56">Risk Classifications</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">risk</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Fire</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Marine</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Engineering</span></p></td>
</tr>
</tbody>
</table>

<span class="c21"></span>

------------------------------------------------------------------------

<span class="c82"></span>

<span id="t.b508bfc07f93422bbaf6e6af43077b82010d2c0d"></span><span
id="t.53"></span>

<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><span class="c56">Report Purposes</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">purpose</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Premium</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Loss</span></p></td>
</tr>
<tr class="odd">
<td><p><span class="c0">Recovery</span></p></td>
</tr>
<tr class="even">
<td><p><span class="c0">Others</span></p></td>
</tr>
</tbody>
</table>

<span class="c82"></span>

<span class="c150 c157">Legal Services Group</span>
===================================================

1.  <span class="c75">Regulation, Enforcement and Prosecution Division (REPD)</span>
    --------------------------------------------------------------------------------

<!-- -->

1.  ### <span class="c26">Registration, Monitoring, and Reporting on Administrative Complaints Filed Against Regulated Entities</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 471.00px; height: 355.00px;">![](images/image9.png)</span>

1.  ### <span class="c26">Data Storage, Retrieval and Reporting on Approved Non-life Insurance Policies, Riders, Clauses, Warranties or Endorsements</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 754.00px; height: 156.50px;">![](images/image14.png)</span>

1.  <span class="c75">Public Assistance and Mediation Division (PAMD)</span>
    ------------------------------------------------------------------------

<!-- -->

1.  ### <span class="c26">Registration, Monitoring, and Reporting on Complaints/Requests for Assistance Concerning Regulated Entities</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 602.00px; height: 238.67px;">![](images/image35.png)</span>

1.  <span class="c75">Licensing Division</span>
    -------------------------------------------

<!-- -->

1.  ### <span class="c146">Recording and Retrieval of Basic Information of all Regulated Entities</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 693.50px; height: 571.30px;">![](images/image33.png)</span>

<span class="c21"></span>

1.  <span class="c75">Conservatorship, Receivership, and Liquidation Division (CRL)</span>
    --------------------------------------------------------------------------------------

<!-- -->

1.  ### <span class="c26">Registration, Monitoring and Reporting of Complaints/ Requests for Assistance Concerning Companies Placed under CRL</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 489.00px; height: 190.00px;">![](images/image6.png)</span>

1.  ### <span class="c114">Data Storage, Retrieval and Reporting on Companies under CRL</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 624.00px; height: 332.00px;">![](images/image16.png)</span>

1.  <span class="c75">Claims Adjudication Division (CAD)</span>
    -----------------------------------------------------------

<!-- -->

1.  ### <span class="c146">Registration, Monitoring, and Reporting on Complaints Lodged with CAD</span>

<span class="c21"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 645.26px; height: 350.50px;">![](images/image32.png)</span>

1.  <span class="c75">Suretyship Section</span>
    -------------------------------------------

<!-- -->

1.  ### <span class="c26">Registration, Monitoring, and Reporting on Requests Received and Certifications Issued</span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 539.00px; height: 333.00px;">![](images/image3.png)</span>

<span class="c21"></span>

<span class="c82"></span>

<span class="c93 c168">Collated Entity Relationship
Diagrams                                                            
           </span>
