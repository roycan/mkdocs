
INPUT="CoopTV-android-report.md"

# gets the input md document and turns them into slides using different themes.
# each is opened in firefox
# you can save the themed slide html of your liking.
# use/ distribute/ host with the appropriate images folder


themes = "beige
black
blood
league
moon
night
serif
simple
sky
solarized
white
"

t = themes.split("\n")

p themes
p t

t.each do |e|
  com = "pandoc -t revealjs -s -o myslides.html #{INPUT} -V revealjs-url=https://revealjs.com -V transition=cube -V theme=#{e}"
  system(com)
  system("firefox myslides.html")
  system("echo #{e}")
  system("sleep 2")

end
