





















IC-FEDS MOBILE APP

SAMPLE SCREENS
------------------------------------------------------------------------



A. For Company



![](up-ic-mobile/image1.png)![](up-ic-mobile/image4.png)



These are some screenshots of the proposed mobile
application for companies. The proposed mobile application should have
the capability to view submissions by the company, see the status of
their submission and view additional data such as penalties if they
exist. The example shown above is for the Life Division, submission of
Annual Statements.























B. For Public 



![](up-ic-mobile/image2.png)![](up-ic-mobile/image3.png)

These are some screenshots of the proposed mobile
application open for the public. The proposed mobile application should
have the capability to view information about the companies and entities
regulated by the commission. As for the information to be shown, the
data provided by the Licensing Division was chosen.


