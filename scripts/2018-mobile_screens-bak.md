<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0">IC-FEDS MOBILE APP</span>

<span class="c0">SAMPLE SCREENS</span>
------------------------------------------------------------------------

<span class="c5"></span>

<span class="c5">A. For Company</span>

<span class="c5"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 295.50px; height: 525.18px;">![](images/image1.png)</span><span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 295.68px; height: 525.50px;">![](images/image4.png)</span>

<span class="c1"></span>

<span class="c1">These are some screenshots of the proposed mobile
application for companies. The proposed mobile application should have
the capability to view submissions by the company, see the status of
their submission and view additional data such as penalties if they
exist. The example shown above is for the Life Division, submission of
Annual Statements.</span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c1"></span>

<span class="c5">B. For Public </span>

<span class="c5"></span>

<span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 294.58px; height: 522.50px;">![](images/image2.png)</span><span
style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 296.50px; height: 527.48px;">![](images/image3.png)</span>

<span class="c1">These are some screenshots of the proposed mobile
application open for the public. The proposed mobile application should
have the capability to view information about the companies and entities
regulated by the commission. As for the information to be shown, the
data provided by the Licensing Division was chosen.</span>

<span class="c1"></span>
