













IC-FEDS WEBSITE SCREENS













































SCREENS SECTION
========================================

This section shows the screens that were designed
corresponding to the various Insurance Commission processes that were
selected for Proof-of-Concept development. Note that the website will
have screens for Insurance Commission examiners, and screens for the
users from regulated entities (companies). This document presents both
types of screens together, organized by IC Division, for greater ease of
understanding. 



The screens presented here are updated from the preliminary
designs that are included in the System Design Specification
document.The updates are based on the comments of IC staff who reviewed
the site during user-acceptance testing. A compilation of these comments
can be found in the Comments Section of this document.  

FINANCIAL EXAMINATIONS GROUP
=====================================================

1.  Brokers Division
    -----------------------------------------

![](images/image141.png)\[Company’s view\] This is the Submissions Dashboard for
companies under Brokers Division.

![](images/image92.png)\[Examiner’s view\] This is the dashboard for examiners of
 Brokers Division.





1.  ### Annual Submission and Examination of Financial Condition/Standing

![](images/image108.png)

\[Company’s view\] This is the screen where the company
must upload the checklist of files needed.

![](images/image138.png)

\[Examiner’s view\] This is the Submitted Annual
Reports page. The year, together with the number of companies that
submitted an Annual Report,  are listed on this screen. Upon clicking
the year, the user will see the list of companies that submitted that
year.





![](images/image76.png)

\[Examiner’s view\] This is the list of companies that
submitted Annual Report for the year 2017. The name of Company, Total
Assets, Total Liabilities, Total Equity, Net Worth and Remarks are
already flashed on the screen. When the user click the company name, the
user will see the Working Balance sheet of the company.



![](images/image35.png)\[Examiner’s view\] This is the Working balance sheet. When
the user click the accounts (summaries), the details will be
shown.



![](images/image135.png)\[Examiner’s view\] This is the details of the summary. For
this example, this is the Schedule of Cash and Cash Equivalents.





1.  ### Quarterly Submission and Monitoring of Client’s Money Compliance

![](images/image55.png)

\[Company’s view\] The first screen for the Statement
of Business Operation (SBO) Form is the Company Profile which will come
straight from the Licensing’s records. All forms for this process is
separated using tabs. Once the user click a tab, he/she will be
redirected to the form indicated at the top of the tab.



![](images/image160.png)

\[Company’s view\] This is the Receivable from insured
tab.

![](images/image180.png)

\[Company’s view\] This is the Clients’ Money Account
tab.

![](images/image149.png)

\[Company’s view\] This is the Payable from Insurance
Companies tab.



![](images/image74.png)\[Examiner’s view\] This screen lists all the companies that
submitted SBO. Once the examiner click the name of the company, the
screen will show the submission details of the company.



![](images/image70.png)\[Examiner’s view\] Like the company’s view, each section is
separated using tabs. This screen is the Company profile tab.





![](images/image189.png)\[Examiner’s view\] This is the receivable from insured
tab.



![](images/image5.png)\[Examiner’s view\] This is the Clients’ Money Account
tab.



![](images/image175.png)\[Examiner’s view\] This is the Payable to Insurance Company
tab.



![](images/image12.png)\[Examiner’s view\] Lastly, this is the Summary tab. The
examiner can already view the details here - total Receivable from
insured (A), total Clients’ Money (B) , the sum of Receivable and
Clients’ Money (A+B), the total Payable to insurance company (C), the
difference between the sum of Receivable and Clients’ Money, and Payable
to insurance company (A+B-C). Finally, the examiner can put any remarks,
or comments, he/she needs to write.



------------------------------------------------------------------------


=============================

1.  HMO Division
    -------------------------------------

\[Company View\] Below is the the company dashboard.
Shown are the processes accessible by companies.

![](images/image197.png)

\[Examiner View\] Below is the examiner dashboard.
Shown are the submissions made by companies.

![](images/image179.png)

1.  ### Annual Submission and Examination of Financial Statements

\[Company View\] Below is where companies can upload
their files for the annual submission.

![](images/image87.png)

\[Company View\] Below are the forms for companies to
fill out which are tabbed as SFP and SCI for annual submissions. 

![](images/image217.png)

![](images/image115.png)

\[Examiner View\] Below is the submissions by companies
for annual reports.

![](images/image53.png)

1.  ### Quarterly Submission and Examination of Interim Financial Statements

\[Company View\] Below is where companies can upload their files
for quarterly submission![](images/image167.png)

\[Company View\] Below are the forms for companies to
fill out which are tabbed as SFP and SCI for quarterly submissions.


![](images/image116.png)

![](images/image211.png)

\[Examiner View\] Below are the quarterly submissions
by companies summarized and individual company view,
respectively.

![](images/image125.png)

![](images/image96.png)

------------------------------------------------------------------------


=============================

1.  Life/ MBA/ Trust Division
    --------------------------------------------------



1.  ### Annual Submission and Examination of Financial Standing of Life Companies with Separate Presentation for Composite Insurance Companies



Company View

This page will appear upon logging in a company
account. The page consist of a sidebar on the left side, a navigation
bar above with the insurance commission logo and a specified label, if
any. On the middle, it has the dashboard which has the
announcements/updates regarding submissions of documents. There's also
the cards which are consisting of all the submissions needed by the
companies. Each card consist of the Document submission header, current
quarter of submission, its deadline and status of the company on
submission. Beside the document submission header is a question icon
which pops up a modal when clicked that consist of requirements,
guidelines and circular letters for that specific submission.

![](images/image111.png)



------------------------------------------------------------------------



Upon clicking the Annual Submission card or the Annual
Statement from the sidebar, it will redirect to this page. Here, the
company can upload the required documents based on the checklist
provided by the insurance commission. Each file submission has specific
file type allowed. Once the required documents are passed, It will be
checked. The submit button is blocked out until all required documents
are uploaded. The company can also save as draft to continue uploading
later.

![](images/image64.png)

------------------------------------------------------------------------



Upon clicking View History on the sidebar, the user is
given an option to which division submission it wants to view. In this
specific web screen, It can choose to view all its submissions to Life
division. The main page has tables which are divided based on the
deadlines of each processes. Each tables can be sorted and filtered to
be able to track if they have submitted the documents.

![](images/image99.png)



------------------------------------------------------------------------



Examiner View

This page will appear upon logging in an examiner
account of insurance commission. The page consist of a sidebar on the
left side, a navigation bar above with the insurance commission logo and
a specified label, if any. On the middle, it has the dashboard which has
the cards which are consisting of all the submissions needed to be done
by the companies. Each card consist of the Document submission header,
current quarter of submission and its deadline. Beside the document
submission header is a question icon which pops up a modal when clicked
that consist of requirements, guidelines and circular letters for that
specific submission. Below the page is a table of certificate requests.
The examiner can view incoming requests for certification of companies
through a table. The examiner can then take action on each request by
viewing it.

------------------------------------------------------------------------

![](images/image47.png)

Upon clicking the Annual Submission card or the Annual
Statement from the sidebar, the examiner will be redirected to the page
with company list. The table with company list shows important
informations needed immediately for viewing. An examiner can also take
action of viewing the company submission in a much detailed form.

##### ![](images/image95.png)

##### 

##### 

##### 

##### 

##### 

##### 

##### 

##### 

##### 



































Upon clicking the view button, the examiner will be
directed to the page where you can change the status of the submission,
download the submitted checklist, and company files.

##### ![](images/image129.png)





Note: Upon clicking the view icon the computation for
net worth will be shown



1.  ### Annual Submission and Examination of Mutual Benefit Association (MBA)



Company View

This page will appear upon logging in a company
account. The page consist of a sidebar on the left side, a navigation
bar above with the insurance commission logo and a specified label, if
any. On the middle, it has the dashboard which has the
announcements/updates regarding submissions of documents. There's also
the cards which are consisting of all the submissions needed by the
companies. Each card consist of the Document submission header, current
quarter of submission, its deadline and status of the company on
submission. Beside the document submission header is a question icon
which pops up a modal when clicked that consist of requirements,
guidelines and circular letters for that specific submission.

------------------------------------------------------------------------

![](images/image22.png)

Upon clicking the Annual Submission card or the Annual
Statement from the sidebar, it will redirect to this page. Here, the
company can upload the required documents based on the checklist
provided by the insurance commission. Each file submission has specific
file type allowed. Once the required documents are passed, It will be
checked. The submit button is blocked out until all required documents
are uploaded. The company can also save as draft to continue uploading
later. 

------------------------------------------------------------------------

![](images/image206.png)

Upon clicking View History on the sidebar, the user is
given an option to which division submission it wants to view. In this
specific web screen, It can choose to view all its submissions to MBA
section. The main page has tables which are divided based on the
deadlines of each processes. Each tables can be sorted and filtered to
be able to track if they have submitted the documents.

![](images/image10.png)

------------------------------------------------------------------------



Examiner View

This page will appear upon logging in an examiner account of
insurance commission. The page consist of a sidebar on the left side, a
navigation bar above with the insurance commission logo and a specified
label, if any. On the middle, it has the dashboard which has the cards
which are consisting of all the submissions needed to be done by the
companies. Each card consist of the Document submission header, current
quarter of submission and its deadline. Beside the document submission
header is a question icon which pops up a modal when clicked that
consist of requirements, guidelines and circular letters for that
specific submission. Below the page is a table of certificate requests.
The examiner can view incoming requests for certification of companies
through a table. The examiner can then take action on each request by
viewing it.![](images/image47.png)

------------------------------------------------------------------------



Upon clicking the MBA Annual Submission card or the MBA
Annual Statement from the sidebar, the examiner will be redirected to
the page with company list. The table with company list shows important
informations needed immediately for viewing. An examiner can also take
action of viewing the company submission in a much detailed form.

![](images/image120.png)



1.  ### Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report



Company View

This page will appear upon logging in a company
account. The page consist of a sidebar on the left side, a navigation
bar above with the insurance commission logo and a specified label, if
any. On the middle, it has the dashboard which has the
announcements/updates regarding submissions of documents. There's also
the cards which are consisting of all the submissions needed by the
companies. Each card consist of the Document submission header, current
quarter of submission, its deadline and status of the company on
submission. Beside the document submission header is a question icon
which pops up a modal when clicked that consist of requirements,
guidelines and circular letters for that specific submission.

![](images/image148.png)

------------------------------------------------------------------------



Upon clicking the Quarterly Submission on the sidebar
or the button on the Quarterly Submission card, it will redirect to this
page. This page lists down further processes on quarterly submission.
From each quarterly document submission, the user has the option to
upload file or input forms.

![](images/image36.png)

------------------------------------------------------------------------



Upon clicking inputs forms from the quarterly submissions page for
FRF, it will redirect to this page. The company can input their account
here just like what they would in an excel sheet.

![](images/image166.png)

------------------------------------------------------------------------



Upon clicking upload file from the quarterly
submissions page for FRF, it will redirect to this page. The user can
upload the specified template for the FRF submission. It shall follow
the naming convention as well as the file type specified (excel) to be
able to upload. 

![](images/image29.png)

Upon clicking upload file from the quarterly
submissions page for RBC2, it will redirect to this page. The user can
upload the specified template for the RBC2 submission. It shall follow
the naming convention as well as the file type specified (excel) to be
able to upload. 

![](images/image85.png)

Upon clicking upload file from the quarterly
submissions page for AVR, it will redirect to this page. The user can
upload the specified template for the AVR submission. It shall follow
the naming convention as well as the file type specified (excel) to be
able to upload. 

![](images/image126.png)

------------------------------------------------------------------------



Upon clicking View History on the sidebar, the user is
given an option to which division submission it wants to view. In this
specific web screen, It can choose to view all its submissions to Life
division. The main page has tables which are divided based on the
deadlines of each processes. Each tables can be sorted and filtered to
be able to track if they have submitted the documents.

![](images/image99.png)

------------------------------------------------------------------------



Examiner View

This page will appear upon logging in an examiner
account of insurance commission. The page consist of a sidebar on the
left side, a navigation bar above with the insurance commission logo and
a specified label, if any. On the middle, it has the dashboard which has
the cards which are consisting of all the submissions needed to be done
by the companies. Each card consist of the Document submission header,
current quarter of submission and its deadline. Beside the document
submission header is a question icon which pops up a modal when clicked
that consist of requirements, guidelines and circular letters for that
specific submission. Below the page is a table of certificate requests.
The examiner can view incoming requests for certification of companies
through a table. The examiner can then take action on each request by
viewing it.

![](images/image47.png)

------------------------------------------------------------------------



Upon clicking the Quarterly Submission button from the
card or the Quarterly Submission from the sidebar, the examiner will be
redirected to this page. This page lists down further processes on
quarterly submission. From each quarterly document submission, the
examiner has the option to view company submissions by clicking View
submissions.

![](images/image1.png)

------------------------------------------------------------------------



Upon clicking the View Submissions from the FRF
Submissions card on Quarterly Submissions page, the examiner will be
redirected to the page with company list. The table with company list
shows important informations needed immediately for viewing. An examiner
can also take action of viewing the company submission in a much
detailed form.

![](images/image97.png)





------------------------------------------------------------------------



Upon clicking the View Submissions from the RBC
Submissions card on Quarterly Submissions page, the examiner will be
redirected to the page with company list. The table with company list
shows important informations needed immediately for viewing. An examiner
can also take action of viewing the company submission in a much
detailed form.

![](images/image106.png)





















Upon clicking the view button, it will redirect to this
page where you can change the status, download submitted checklist, and
company files.

![](images/image198.png)

Note: Show the computation for RBC

















Upon clicking the View Submissions from the AVR
Submissions card on Quarterly Submissions page, the examiner will be
redirected to the page with company list. The table with company list
shows important informations needed immediately for viewing. An examiner
can also take action of viewing the company submission in a much
detailed form.

![](images/image177.png)



------------------------------------------------------------------------


=============================

1.  Non-Life Division
    ------------------------------------------



1.  ### Annual Submission and Examination of Financial Standing of Non-Life Companies

![](images/image89.png)

\[Company Side\]



Upon logging in, the designated staff will be seeing
this screen, the Dashboard. There are three cards on the screen, each
card represents 1 process under the Division. The first card is for the
annual statement submission. The second one is for the quarterly submission and the last
one is for the certification request
under the Division. On the first card, once the
‘submissions’ button is clicked, the user will be redirected to the
Input forms shown in the next picture.



![](images/image142.png)

\[Company Side\]



This is the checklist for the annual submission. Each
checklist should have an attachment to be considered or to have a check
mark in the allotted box. Unfinished uploads could be continued if ‘save
as draft’ button is clicked. In order to view all the records, the user
must click the ‘View Output’ link which can be seen on the left side of
the screen. Once all of the requirements is uploaded, press the ‘submit’
button.





Once done with all of the processes, the user can log
out by clicking the dropdown on the upper right corner of the page. Now,
the following screens are for the Non-Life staff (examiner).



![](images/image2.png)

\[Examiner Side\]



Upon logging in, the designated staff will be seeing
this screen, the Dashboard. There are two cards on the screen, each card
represents submissions. The first card is for the annual statement submission. The
second one is for the quarterly
submissions. The list of certification request
is already displayed and can be sorted by company, the name of the
request and the date it was submitted. On the first card, once the ‘view
submissions’ button is clicked, the user will be redirected to the Input
forms shown in the next picture.



![](images/image84.png)

\[Examiner Side\]



This screen displays the submitted annual reports per
year, the total number of companies under the division and the number of
companies who submitted. Once the year is clicked, it will redirect to
this page.



![](images/image163.png)

\[Examiner Side\]



This page shows information about the submissions of a
company. There’s a ‘view’ action that once clicked, it will redirect to
this page that lets the examiner download the submission checklist as
well as update the status.



![](images/image25.png)

\[Examiner Side\]



There a link below the company name that says ‘view
working balance sheet’. Once clicked, it will redirect to this
page.



![](images/image68.png)

\[Examiner Side\]



This page shows the entire balance sheet that is
plotted in the excel template. It can be exported to excel as well as
update it by uploading the updated wbs excel file. Once uploaded, it
will be parsed by the system and will automatically display the updated
version.





1.  ### Quarterly Submission and Examination of FRF, RBC2, and Actuarial Valuation Report



![](images/image112.png)

\[Company Side\]



On the second card, once the ‘submissions’ button is
clicked, the user will be redirected to this page. There are three cards
inside the quarterly submissions. The frf
submission, rbc2 submission, and avr submission. Once the ‘upload files’ in the first card is clicked, it
will redirect to a page shown in the next picture.



![](images/image200.png)

\[Company Side\]



This page is where the user will upload the frf
submission. It is important to follow the file format for the accurate
parsing of data. Once the ‘Input form’ button is clicked in the previous
page, the user will be redirected to the Input forms shown in the next
picture.



![](images/image150.png)

\[Company Side\]



This page shows the sample template for the statement
of financial position as well as the statement of comprehensive income
that has been plotted in the original excel templates.



![](images/image208.png)

\[Company Side\]



Going back to the cards inside the process number 2,
once the ‘upload files’ in the second card is clicked, it will redirect
to this page that lets the user upload the desired document with the
right file format. 



![](images/image75.png)

\[Company Side\]



Once the ‘upload files’ in the last card under the 2nd
process is clicked, it will redirect to this page which will also let
the user upload the desired document with the right file format as
well.





Once done with all of the processes, the user can log
out by clicking the dropdown on the upper right corner of the page. Now,
the following screens are for the Non-Life staff (examiner).



![](images/image186.png)

\[Examiner Side\]



On the second card, once the ‘submissions’ button is
clicked, the user will be redirected to this page. There are three cards
inside the quarterly submissions. The frf
submission, rbc2 submission, and avr submission. Once the ‘view submissions’ in the first card is clicked,
it will redirect to a page shown in the next picture.



![](images/image159.png)

\[Examiner Side\]



This page shows the submissions and some information
per company. Once the ‘view’ action is clicked, it redirects to the next
page shown below.



![](images/image171.png)

\[Examiner Side\]



This page lets the examiner download the submission
checklist as well as update the status.



1.  ### Issuance of Certification on Financial Condition of Insurance Companies



![](images/image213.png)

\[Company Side\]



Once the ‘submissions’ in the last card is clicked, it
will redirect to this page which is a set of checklist of certifications
that could be requested. If the desired certification is not included in
the checklist, the user could add the name if ‘add’ button is clicked.
For additional notes or information, it could be stated in the space
below titled ‘Reason for request’.



![](images/image7.png)

\[Company Side\]



Below the options in the left side of the page are the
history area. In this page, all of the submissions are recorded and
categorized by year, document (description/title), deadline, date
submitted, status and penalty. In the annual submission, once the
document title is clicked, it will show a page of the document
preview.

------------------------------------------------------------------------


=============================

1.  Pre-Need Division
    ------------------------------------------

![](images/image199.png)

\[Company’s view\] This is the Company’s dashboard;
each card representing each Division’s process.



![](images/image113.png)

\[Examiner’s view\] This is the examiner’s dashboard.
The ‘submissions’ button will lead the examiner to the submissions page
of each process.

1.  ### Annual Submission and Examination of Pre-Need Companies



![](images/image72.png)

\[Company’s view\] This is the uploading page for the
requirements checklist of Annual Statement as well as the Annual
Statement itself.



![](images/image195.png)

\[Examiner’s view\] In this screen, the examiner can
view the list of companies that submitted Annual Statement.

![](images/image122.png)

\[Examiner’s view\] This is the submission details of
the Company, which can be seen by the examiners.

1.  ### Quarterly Submission and Monitoring of Interim Financial Statements

![](images/image114.png)\[Company’s view\] This is the balance sheet form  for the
Interim Financial Statement Submission.



![](images/image127.png)

\[Company’s view\] This is the income statement form
for the Interim Financial Statement Submission.





1.  ### Monthly Submission and Monitoring of Sales Reports, Collection Reports and Deposit to Trust Fund Reports

![](images/image11.png)\[Company’s view\] This screen is for the forms for Sales
Report - Schedule of Plans/Contract Sold and Summary of Plans/Contracts
Sold - to be filled-out by the Company.

![](images/image48.png)\[Examiner’s view\] This is the Summary of Plans/Contracts
Sold.



![](images/image128.png)\[Company’s view\] This is the screen for all the forms of
Collection Report - Schedule of Collection, Planholder’s Outstanding
Balance, and Trust fund Requirement.

![](images/image201.png)

\[Examiner’s view\] This is the summary of Collection
Report for the Month.



![](images/image219.png)\[Company’s view\] This is the form for the Deposit to Trust
Fund Report.

![](images/image57.png)\[Examiner’s view\] This is the summary for the Deposit to
Trust Fund.



TECHNICAL SERVICES GROUP
=================================================

1.  Statistics and Research Division
    ---------------------------------------------------------

\[Company View\] Below is the screen for the dashboard
which includes Statistic’s processes. 

![](images/image190.png)

\[Examiner View\] Below is the screen for the dashboard
which includes Statistics’s companies’s submissions.

![](images/image88.png)

\[Examiner View\] For Processes A through E, Statistics
Division will use the Annual and Quarterly Financial Statements from the
Financial Examination Group

![](images/image80.png)![](images/image23.png)![](images/image104.png)![](images/image147.png)

![](images/image62.png)

![](images/image33.png)![](images/image58.png)![](images/image137.png)

1.  ### Preparation of Annual Data - Annual Report (selected sample tables)

2.  ### Preparation of Annual Data - Key Data

3.  ### Preparation of Annual Data - ASEAN Report

4.  ### Preparation of Annual Data - Ranking

5.  ### Preparation of Annual Data - Assets Information Management Report (AIMP)

6.  ### Quarterly Submission of Compliance to Various Reportorial Requirements - Life, Non-Life, MBA

\[Company View\] Shown below is the screen for
uploading submissions for companies.

![](images/image178.png)

\[Life\]

![](images/image118.png)![](images/image182.png)![](images/image26.png)![](images/image203.png)![](images/image78.png)![](images/image18.png)

\[Non Life\]

![](images/image39.png)![](images/image19.png)![](images/image40.png)![](images/image41.png)![](images/image20.png)

\[MBA\]

![](images/image132.png)![](images/image170.png)![](images/image146.png)![](images/image193.png)![](images/image215.png)

\[Examiner View\] Shown below is the screen for viewing
companies’ submissions.

![](images/image79.png)

1.  ### Report on Compulsory Insurance Coverage for Agency-Hired Migrant Workers

\[Company View\] Shown below is the screen for
uploading submissions for companies.

![](images/image100.png)![](images/image152.png)![](images/image145.png)![](images/image188.png)

\[Examiner View\] Shown below is the screen for viewing
companies’ submissions.

![](images/image187.png)![](images/image13.png)![](images/image63.png)![](images/image174.png)

------------------------------------------------------------------------


=============================

1.  Rating Division
    ----------------------------------------

\[Company View\] Below is the screen for the dashboard
which includes Rating’s processes. 

![](images/image101.png)

\[Examiner View\] Below is the screen for the dashboard
which includes Rating’s companies’s submissions.

![](images/image61.png)

1.  ### Approval of Premium Rates on Fire/ Motor Car Insurance and Surety

\[Company View\] Shown below is the screen for
uploading submissions for companies.

![](images/image164.png)



\[Examiner View\] Shown below is the screen for viewing
companies’ submissions.

![](images/image98.png)

1.  ### Submission and Monitoring of Data from Compulsory Third Party Liability (CTPL)

\[Company View\] Shown below is where companies can
upload their files.

![](images/image54.png)



\[Examiner View\] Shown below is where examiners can
view a list of submissions made by companies.

![](images/image91.png)

1.  ### Submission and Monitoring of Data from Passenger Personal Accident Insurance (PPAI)

\[Company View\] Shown below is the dashboard for
PPAI’s subprocesses.

![](images/image94.png)

\[Company View\] Shown below is where companies can
submit upload their files.

![](images/image164.png)

\[Company View\] Shown below is where companies can
fill out forms that are tabbed as “Total Benefits Paid” and “Total
Premium Paid.”

![](images/image207.png)

![](images/image30.png)

\[Company View\] Shown below is where companies can
upload supporting documents for PPAI.

![](images/image109.png)

\[Examiner View\] Shown below is where examiners can
view submissions of companies for PPAI.

![](images/image183.png)

1.  ### Off-site Examination of Non-Life Insurance Companies

\[Company View\] Shown below is the dashboard for
Off-site’s subprocesses.

![](images/image52.png)

\[Examiner View\] Shown below is the dashboard for
offsite’s subprocesses’ submissions.

![](images/image45.png)

1.  #### Motor Car Total Loss

\[Company View\] Shown below is where companies can
upload their submissions.

![](images/image8.png)

\[Company View\] Shown below are forms for inputting
details for vehicles that are unrestorable, restorable and subject to
total loss subrogation.

![](images/image156.png)



![](images/image77.png)

![](images/image210.png)

\[Examiner View\] Shown below is where examiners see
the companies’ submissions.

![](images/image14.png)

1.  #### Bonds

\[Company View\] Shown below are the screens for
creating and uploading new bonds for both Government Bonds and Judiciary
Bonds.



![](images/image136.png)

![](images/image123.png)

![](images/image218.png)

![](images/image38.png)

\[Examiner View\] Shown below are the list of Judiciary
and Government bonds submitted by companies.

![](images/image110.png)

![](images/image46.png)

1.  #### Adjuster’s Quarterly Report

\[Company View\] Shown below is where companies can
upload their submissions.

![](images/image51.png)

\[Examiner View\] Shown below are the lists of
submissions made by companies.

![](images/image4.png)

1.  #### Fire Service Tax (FST) Remittances

\[Company View\] Shown below is where companies can
upload their files.

![](images/image16.png)

\[Examiner View\] Shown below is the list of
submissions made by companies.

![](images/image56.png)

------------------------------------------------------------------------


=============================

1.  Investment Division
    --------------------------------------------



1.  ### Approval of Investments of Insurance Companies and Others (e.g. banks and investment houses)



\[Company View\]

Here’s the dashboard for Investment Division that
allows you to access each process.

![](images/image82.png)









































\[Company View\]

After clicking the “Submissions” for Approval of
Investment, it will direct you this page where you choose the type of
investment that you want.

![](images/image60.png)



\[Company View\]

Next is you have to upload the requirements that
are needed under the type of investment you chose then click
“Submit”.

![](images/image90.png)





\[Examiner View\]

This is the examiner’s dashboard where you can see the
processes just like the company’s side. Click the action button on the
table to edit submission status or to edit the file.

![](images/image44.png)



\[Examiner View\]

After clicking the action button, here’s the page for
downloading files and editing statuses.

![](images/image3.png)

1.  ### Quarterly Submission and Monitoring of Statement of Paid Up Capital and Reserve Investment (SPUCRI)

\*\*Note: Screens are “to follow” because there are no
data that is yet to be submitted by the Investment Division

1.  ### Monthly Submission and Assessment of Investments Made, Sold, and Disposed

\[Company View\]

After clicking the “Submissions” for Monthly Submissions and
Assessment of Investments Made, Sold and Disposed, it will direct you
this page where you choose whether it is monthly or quarterly
submission. 

![](images/image86.png)





































\[Company View\]

If you click “Upload files” under Monthly
Submission, it will direct you to this page where you can upload the
template and then submit it or you can save as draft.![](images/image216.png)



\[Company View\]

If you click “Input forms” under Monthly
Submission, it will direct you to this page where you can input data for
the investments made and once you’re done, click “Next”.





\[Company View\]

![](images/image119.png)





\[Company View\]

After clicking the “Next” button, it will direct
you to this page where you can input data for the investments sold and
then click “Submit”.

![](images/image31.png)





\[Company View\]

If you click “Upload files” under Quarterly
Submission, it will direct you to this page where you can upload the
template and then submit it.

![](images/image134.png)





\[Company View\]

If you click “Input forms” under Quarterly
Submission, it will direct you to this page where you can input data for
the investments made and once you’re done, click “Next”.

![](images/image37.png)





\[Company View\]

After clicking the “Next” button, it will direct
you to this page where you can input data for the investments sold and
then click “Submit”.

![](images/image181.png)

















\[Examiner View\]

Here’s the dashboard for Investment Division that
allows you to access each process. Click the “View Submissions” for
submission and assessment of investment made, sold, and
disposed. 

![](images/image44.png)



\[Examiner View\]

After clicking the “View Submissions”, shown below is
the cards for monthly submissions and quarterly submissions. 

![](images/image220.png)



\[Examiner View\]

For monthly submissions, the list of companies will be
shown after clicking the view submissions. Click the action button to
view the details for the chosen company.

![](images/image139.png)

\[Examiner View\]

Upon clicking the action button, shown below is the
viewing for the files that have been submitted by the companies, also
downloading of the file, and changing the status of the
submission.

![](images/image124.png)

\[Examiner View\]

For quarterly submissions, the list of companies will
be shown after clicking the view submissions. Click the action button to
view the details for the chosen company.

![](images/image102.png)

\[Examiner View\]

Upon clicking the action button, shown below is the
viewing for the files that have been submitted by the companies, also
downloading of the file, and changing the status of the
submission.

![](images/image185.png)




=============================

1.  Actuarial Division
    -------------------------------------------

\[Company View\] Below is the screen for the dashboard
which includes Actuarial’s processes. 

![](images/image151.png)

\[Examiner View\] Below is the screen for the dashboard
which includes Actuarial’s companies’s submissions.

![](images/image73.png)

1.  ### Approval of Insurance Products under Expeditious Approval Process

\[Company View\] Shown below is the screen for
uploading submissions for companies.

![](images/image168.png)

\[Examiner View\] Shown below is the screen for viewing
companies’ submissions.

![](images/image6.png)

1.  ### Submission of Insurance Products under Non-Expeditious Approval Process

\[Company View\] Shown below is the screen for
uploading submissions for companies.

![](images/image69.png)

![](images/image173.png)

![](images/image214.png)

\[Examiner View\] Shown below is the screen for viewing
companies’ submissions.

![](images/image24.png)

![](images/image32.png)

![](images/image43.png)



------------------------------------------------------------------------


=============================

1.  Reinsurance Division
    ---------------------------------------------

![](images/image131.png)\[Company’s view\] This is the company’s dashboard. Each card
represents one process. For each process, a company may choose to either
upload the excel file or manually input/encode the details to a
form.



![](images/image191.png)\[Examiner’s view\] This is the examiner’s dashboard. Each
card represents one process, like the one on the company’s view. The
‘submissions’ button will lead the examiner to the submissions page of
each process.



1.  ### .Submission and Monitoring of Facultative and Non-Facultative Placements

![](images/image65.png)

\[Company’s view\] This is the input form for the
approval of Facultative Placements Abroad.



![](images/image130.png)

\[Examiner’s view\] This is the list of Approved
Facultative Placements Abroad for the Month.

1.  ### Submission and Monitoring of Particulars of Reinsurance Treaties

![](images/image59.png)

\[Company’s view\] This is the input forms for the
approval of Reinsurance treaties.



![](images/image154.png)

\[Examiner’s view\] This is the screen for viewing the
list of approved Reinsurance treaties.

1.  ### Submission and Monitoring of Foreign Receipts and Remittances

![](images/image133.png)

\[Company’s view\] This is the input forms for the
Statement of Foreign Exchange Receipts and Remittances.



![](images/image93.png)\[Examiner’s view\] This the screen for listing the summary
of all the submitted Foreign Exchange Receipts and Remittances. 











































LEGAL SERVICES GROUP
=============================================

1.  Regulation, Enforcement and Prosecution Division (REPD)
    --------------------------------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Administrative Complaints Filed Against Regulated Entities

![](images/image81.png)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Upon logging in, the REPD staff will be seeing this
screen, the Dashboard. There are two cards on the screen, each card
represents 1 process under the Division. The first card is for the
administrative cases while the one on the right is for the product approval. On the left card,
once the ‘Input form’ button is clicked, the user will be redirected to
the Input forms shown in the next picture.

![](images/image205.png)

This is the input form for all the administrative
cases under the division. REPD staff should encode all the details in
here then click submit for the record to be saved. In order to view all
the records, the user must click the ‘View Output’ link which can be
seen on the left side of the screen.

![](images/image121.png)

All the encoded accounts shall be seen on this screen.
The user can easily look for records using the search function found on
the upper right, or sort these based on your preference.



1.  ### Data Storage, Retrieval and Reporting on Approved Non-life Insurance Policies, Riders, Clauses, Warranties or Endorsements



![](images/image161.png)

On the other hand, this is the input form for the
product approval under the division. REPD staff should encode all the
details in here then click submit for the record to be saved. In order
to view all the records, the user must click the ‘View Output’ link
which can be seen on the left side of the screen.



![](images/image28.png)

Same as the output in the process one, all the encoded
accounts shall be seen on this screen. The user can easily look for
records using the search function found on the upper right, or sort
these based on your preference.

------------------------------------------------------------------------


=============================

1.  Public Assistance and Mediation Division (PAMD)
    ------------------------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Complaints/Requests for Assistance Concerning Regulated Entities



Input



This page shows the input form as well as the output
list of the complaints lodged with PAMD.

 ![](images/image196.png)



Once “Input Form” button is clicked, it will redirect to this page which the
staff could put the information regarding the complaints.

![](images/image158.png)



If the fields aren’t completed yet, the staff can save
it as a draft and continue whenever. Cancel will redirect to the
previous page. 



### Output

If the view output button is clicked, it will show the
list of complaints that have been added previously categorized by
industry and number of complaints. 





![](images/image103.png)



At the rightmost part of the card, once the “eye” icon
under the action category is clicked it will redirect to a page where
you can choose which company you want to view.



![](images/image157.png)



After choosing the company, this page shows a table of
summary of complaints.

![](images/image9.png)



------------------------------------------------------------------------

Licensing Division
-------------------------------------------

1.  ### Recording and Retrieval of Basic Information of all Regulated Entities

![](images/image144.png)

Upon logging in, the LICENSING staff will be seeing
this screen, the Dashboard. There’s a card on the screen that represent
the process under the Division. The card is for the regulated entities information.
Once the ‘Input form’ button is clicked, the user will be redirected to
the Input forms shown in the next picture.



![](images/image27.png)
=============================================================================================================================================================================================================================================================================

This is the input form for all the regulated entities.
LICENSING staff should encode all the details in here then click submit
for the record to be saved. In order to view all the records, the user
must click the ‘View Output’ link which can be seen on the left side of
the screen.

![](images/image184.png)
==============================================================================================================================================================================================================================================================================

All the encoded accounts shall be seen on this screen.
The user can easily look for records using the search function found on
the upper right, or sort these based on your preference. When the
information that is encoded is too long, it could be minimized by
clicking the ‘+’ button and maximize by pressing it as well. Also,
company information could be previewed when the company name is
clicked.

------------------------------------------------------------------------


=============================

1.  Conservatorship, Receivership, and Liquidation Division (CRL)
    --------------------------------------------------------------------------------------



1.  ### Registration, Monitoring and Reporting of Complaints/ Requests for Assistance Concerning Companies placed under CRL

![](images/image21.png)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Upon logging in, the CRL staff will be seeing this
screen, the Dashboard. There are two cards on the screen, each card
represents 1 process under the Division. The first card is for the
record keeping of claims records under CRL while the one on the right is for the
monitoring of companies under the Division. On the left card, once the ‘Input form’
button is clicked, the user will be redirected to the Input forms shown
in the next picture.

![](images/image153.png)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This is the input form for all the claims filed under
the division. CRL staff should encode all the details in here then click
submit for the record to be saved. In order to view all the records, the
user must click the ‘View Output’ link which can be seen on the left
side of the screen.![](images/image202.png)

All the encoded accounts shall be seen on this screen.
Each row of information is editable and can be deleted. The user can
easily look for records using the search function found on the upper
right; or sort these based on the Name of Claimant, Action Requested,
Name of Company Concerned, and Date Received.

1.  ### Data Storage, Retrieval and Reporting on Companies under CRL

![](images/image17.png)



![](images/image169.png)

------------------------------------------------------------------------


=============================

1.  Claims Adjudication Division (CAD)
    -----------------------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Complaints Lodged with CAD

![](images/image209.png)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Upon logging in, the CAD staff will be seeing this
screen, the Dashboard. There’s a card on the screen that represent the
process under the Division. The card is for the complaints lodged with CAD. Once
the ‘Input form’ button is clicked, the user will be redirected to the
Input forms shown in the next picture.



![](images/image176.png)

This is the input form for all the registration,
monitoring, and reporting on complaints lodged with CAD. LICENSING staff
should encode all the details in here then click submit for the record
to be saved. In order to view all the records, the user must click the
‘View Output’ link which can be seen on the left side of the
screen.

![](images/image140.png)
==============================================================================================================================================================================================================================================================================

All the encoded accounts shall be seen on this screen.
The user can easily look for records using the search function found on
the upper right, or sort these based on your preference. Also, there are
three tabs under the output which are summary, calendar of hearing, and
date of due.

------------------------------------------------------------------------


=============================

1.  Suretyship Section
    -------------------------------------------



1.  ### Registration, Monitoring, and Reporting on Requests Received and Certifications Issued

![](images/image34.png)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This is the Dashboard screen for the Suretyship
Section. Once the ‘Add New Record’ is clicked, the user will be
redirected to the input screen; while the ‘View History’ will bring the
user to the list of records encoded in the system.

![](images/image172.png)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This will be the input page. The user shall encode all
the details in this form then click the submit button to save.

![](images/image50.png)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This is the History page. All records shall be seen on
this screen. The user, a Suretyship Section Staff, has the power to edit
or delete a record when necessary.





MICROINSURANCE
---------------------------------------

### The Dashboard

![](images/image194.png)

Upon logging in, the Microinsurance staff will be
seeing this screen, the Dashboard. There are two cards on the screen,
each card represents 1 process under the Division. The first card is for
the Annual Impact Regulatory Report while the one on the right is for the Quarterly Statistical Report.

1.  ### Generation of Microinsurance Industry Quarterly Statistical Report

![](images/image204.png)

This is the Microinsurance data as of the quarter
ending screen.

![](images/image49.png)

Data comparison from previous year of the
Contribution/Premium Production.



![](images/image67.png)

Data comparison from previous year of the number of
insured lives.

![](images/image143.png)

Number of persons covered by Microinsurance under
MBA.



![](images/image42.png)

Number of persons covered by Microinsurance under Life
Sector.



![](images/image66.png)

Number of persons covered by Microinsurance under
Non-Life Sector.



![](images/image212.png)

Premium production/contribution under MBA.

![](images/image155.png)

Premium production/contribution under Life
Sector.



![](images/image83.png)

Premium production/contribution under Non-Life
Sector.



1.  ### Generation of Annual Regulatory Impact Report

![](images/image105.png)

Annual Regulatory Impact - Insurance coverage



![](images/image165.png)

Annual Regulatory Impact - Microinsurance
coverage

![](images/image162.png)

Annual Regulatory Impact - Number of insured
lives



![](images/image117.png)

Annual Regulatory Impact - Net Premium
(Industry)

![](images/image71.png)

Annual Regulatory Impact - Net Premium
(Microinsurance)



![](images/image107.png)

Annual Regulatory Impact - Total assets

![](images/image15.png)

Annual Regulatory Impact - Loss or claims ratio



![](images/image192.png)

Annual Regulatory Impact - Number of licensed
intermediaries from different sectors




























