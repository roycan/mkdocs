## Taxi

|Plate No. | Name | Date (2018) | Trip | Cost (Php)
-----------|------|--------------|-----|--------
ABF2628 | Ricafort Cuaresma | July 16 | UP to IC | 372
UVA723 | Albert Buencamino | July 16 | IC to UP | 419
TXN324 | Vergil Anastacio | July 23 | IC to UP  | 266
WA3197 | Tony Nicanor | July 23 | UP to IC | 368
UWM750 | Wilbert Austria | July 27 | UP to IC  |  252
AAZ2934 | Ulyssis Madamba | July 27 | IC to UP | 530
WOG223 | Anton Abueva | July 31 | UP to IC | 312
ATA9755 | Ogie Galman | July 31 | IC to UP | 386
UWH184 | Anthony Talawe | August 2 | UP to IC | 267
UVV120 | Wayne Hilario | August 2 | IC to UP | 508
UVX878 | Xeno Vivacio | August 7 | UP to IC | 274
UKO457 | Boy Sanchez | August 7 | IC to UP | 545
AAM2424 | Caloy Oligario | August 9 | UP to IC | 313
UWF318 | Amando Marquez | August 9 | IC to UP | 415
TYX651 | Fred Yulo | August 10 | UP to IC | 308
UVY153 | Tim Yambot | August 10 | IC to UP | 427
UWA908 | Vic Yasay | August 14 | UP to IC | 388
ASA6004 | Adrian Oblepias | August 14 | IC to UP | 574
UVP397 | Aldrin Salonga | August 24 | UP to IC | 313
TXP375 | Paulo Ubando | August 24 | IC to UP | 352
AGA9411 | Philip Tampalasan | August 28 | UP to IC | 388
UVJ965 | Allan Agarin | August 28 | IC to UP | 549
VWH765 | Jerome Vieno | September 4 | UP to IC | 291
UVP479 | Herbert Villarin |September 4 | IC to UP | 554
ABF8388 | Paz Urban | September 6 | UP to IC | 348
UVE184 | Bill Festejo | September 6 | IC to UP | 493
ABX5544 | Elmo Unciano | September 13 | UP to IC | 259
TYW386 | Basti Ambrosio | September 13 | IC to UP | 582
WQL150 | Waldo Tulfo | September 18 | UP to IC | 258
AQA7318 | Lambert Quijano | September 18 | IC to UP | 523
AEA4339 | Albert Quintos | September 20 | UP to IC | 274
UOU754 | Eldrich Alunan | September 20 | IC to UP | 409
UVX770 | Sid Balderama | September 25 | UP to IC | 364
UMJ173 | Oni Onco | September 25 | IC to UP | 397
UWF703 | James Magno | September 27 | UP to IC | 364
AAI4204 | Fred Guzman | September 27 | IC to UP | 596
UVH312 | Arman Ilagan | October 2 | UP to IC | 298
UWC894 | Honesto Garcia | October 2 | IC to UP | 414
UWJ505 | Wawi Carlos | October 18 | UP to IC | 257
ACD1194 | June Oba | October 18 | IC to UP | 570
ABF2628 | Corwyn Datu | October 25 | UP to IC | 262
ABT4515 | Francisco Bustos | October 25 | IC to UP | 582
TXX129 | Ted Abiola | October 30 | IC to UP | 393
UWL851 | Zack Tantuling | October 30 | UP to IC | 492
UVV567 | Ligaya Ubando | November 7 | UP to IC | 315
UVN625 | Van Nazareno | November 7 | IC to UP | 353
UWK169 | Nick Vivecencio | November 22 | IC to UP | 332
UWN826 | Willy Kwapeng | November 22 | UP to IC | 353
BI6491 | Noel Manio | November 23 | UP to IC | 364
UNB569 | Basil Nuevas | November 23 | IC to UP | 523

Total Taxi Reimbursement Request :  **Php19,746.00**

