Q2 - Programming Project - TicTacToe
========================

## Useful Links

Documentation Template link :

 https://docs.google.com/document/d/1kWEDRdOH8ELIubh_kq80bTKxaY60WedXFf2b6G0BCGw/edit?usp=sharing

Emergency Submission Form Link:  

https://docs.google.com/forms/d/1s9gHJLTs7hGDHVS_c5TSpxiy55FUBRCSJmbvxp3SLBg/edit?usp=drive_web


## Instructions

1. (optional) Find a partner and help each other. One can be in-charge of the game play and the other can create code to check if a player has already won.
2. Use the template code provided to create a 2-player Tic Tac Toe game.
3. Add one of the following features to the game (more than one becomes a bonus)

Features (1 required): 
* After the game, the players are asked if they want to play another round. If they say "yes", the game restarts
* There's a tournament option at the start where the first to score 3 points wins (best of 5)
* The players are asked their names and their names are shown whenever it's their turn and if they win. 

Answer the gdoc documentation individually and submit. Note that you may need to make a copy of the gdoc docu template if it's view only. 

 ## Template Code
 
 ```
 /**************************************************************************************
 * 
 * This is the Q2 Programming Project Template. Use it. 
 * 
 * by: Roy Canseco 
 * 
 * ***********************************************************************************/

 #include <iostream>
using namespace std;

int main() {

    // initialize variables to 'X', 'O' or blank
    char a1 = 'X'; char a2 = 'O'; char a3 = 'X';
    char b1 = 'O'; char b2 = 'X'; char b3 = 'O';
    char c1 = 'X'; char c2 = 'O'; char c3 = 'X';
    

    // printout the board 
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;
    cout << "\t" << a1 << "\t" << "|" << "\t" << a2 << "\t" << "|" << "\t" << a3 << endl;
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;
    cout << "------------------------------------------------"<< endl;
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;
    cout << "\t" << b1 << "\t" << "|" << "\t" << b2 << "\t" << "|" << "\t" << b3 << endl;
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;
    cout << "------------------------------------------------"<< endl;
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;
    cout << "\t" << c1 << "\t" << "|" << "\t" << c2 << "\t" << "|" << "\t" << c3 << endl;
    cout << "\t \t" << "|" << "\t \t" <<  "|" << endl;



}
 ```