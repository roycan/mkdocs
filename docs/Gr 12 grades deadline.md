Gr 12 grades deadline 
========================

Efren Paz
Tue, Mar 23, 3:53 PM
to PSHS, Academic, Leonel, Rosalie, Caryljae, Eileen, Adrian, MC-Guidance, MC-Registrar's

Dear Grade 12 Teachers,

Please take note of our fourth-quarter schedule. I am sending you this in advance to anticipate that we need to come up with the grades immediately after our 4th Quarter Perio. Our schedule is as follows:

Grade 12
May 5 - 6 - Perio 4th Quarter
May 10 - AM - Encoding of Grades
May 10 - PM - Scholarship Committee Meeting

We cannot entertain further adjustments on our schedule due to the following plan set by the OED.

May 13 - Execom Meeting to Recommend
Candidates for Graduation
May 19 - BOT Meeting to approve Candidates for Graduation

Best Regards,
Efren Paz

--
Mr. Efren P. Paz
Special Science Teacher V
Chemistry Unit
Philippine Science High School - Main Campus
Agham Road, Diliman, Quezon City
Philippines 1104

