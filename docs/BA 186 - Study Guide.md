BA 186 - Study Guide
========================
- by Roy Vincent L. Canseco 
- SY 2020- 2021

## Week 1 - Information Systems Development Environment


### Let's Begin

In week 1, we get introduced to what information systems are and how they come about.

----

Please accomplish the following before our first meeting for the week. 

### Read

* pp 9-13. What Are Information Systems?. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

Stop before *What Does an IS Career Look Like?*

### Watch 

https://edpuzzle.com/assignments/5f154e802cc63e3f2812f318/watch

### Think 
* What are the similarities of building a house and developing an information system?
* Can you imagine a house, where the builder forgot to install a toilet?
* Can you as easily see if you missed to develop some part of the information system?
* What are the differences in building a house and developing an information system?

---

Please accomplish the following before our second meeting for the week.

### Watch 

https://edpuzzle.com/assignments/5f1681ccbcac813f230610a5/watch

### Read
pp 17-19. Systems Development Life Cycle (SDLC). https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that


### Think 
* SDLC is sometimes defined as: Plan – Analyze – Design – Implement – Maintain and sometimes it is defined as: Analysis – Requirements – Design – Development – Implementation . With which do you agree more? Why?
* From what you’ve studied, if you were to have your own personal Software Development Life Cycle, what would the steps be?


### Dig Deeper
* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 1 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 2. This is useful if you find yourself having trouble with quizzes.  

---

## Week 2 - The Origins of Software

### Let's Begin

Here we look at where software comes from. We will see that software are either bought or build. We'll see that either way, most software today originate from teams with  a purpose. Then we'll go through the experience by forming our teams, coming up with our software idea, analyzing then designing the software.

---

Do these before the first meeting for the week

### Watch 
* https://edpuzzle.com/assignments/5f16d5f40d40a83f8cfc3e75/watch

### Read 

First download the *Entrepreneur Wireframe Kit – PowerPoint – FREE Version* from 
https://knockoutprezo.com/freethings/free-powerpoint-templates/entrepreneurs-wireframe-kit-powerpoint-free-version/

Then read 
* pp. 70-76. Plan and Design Your App. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that 

### Think

A design team benefits from having the following:

1. a person with a creative eye and can communicate ideas
1. a person who can research and validate/ support ideas 
1. a person who can keep track of the work 

* Which kind(s) of person describes you best?  
* Do you know of a classmate with compatible skills to what you have? 
* Just by looking at a person, what he/ she wears, how he/ she speaks, the kind of questions that he or she asks, can you predict what value he/ she can contribute to your team? 

---

Do this before the second meeting for the week.

### Read

* pp. 93-94. Build vs. Buy. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

### Think 

* What kind of software source was used for the office software I'm using, bought or built?
* What kind of software source(s) do you think was used for UP CRS, bought or built?
* What kind of software source(s) do you think was used for UVLE? 

### Dig Deeper 

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 2 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 2. This is useful if you find yourself having trouble with quizzes.  


---

## Week 3 - Identifying and Selecting Systems Development Projects

### Let's Begin

In this week, we shall look at company goals and the processes that bring value to it. We then look at how  Information Systems can enhance that value.

---

Please do these before the first meeting for the week.

### Read 

* pp 24-29. What Is a Business Process?. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

### Watch

* Measurable Organizational Value. https://edpuzzle.com/assignments/5f168d4e967c073f4d9c8339/watch
* Developing the MOV. https://edpuzzle.com/assignments/5f1699f626dbe93f38346b4d/watch 

### Think

* What's the MOV for our project? 
* How can the value be measured? In what way can it be estimated?
* What's the baseline value of the parameter our app seeks to improve?
* At what percent improvement would be our app a success?

---

Please do the following before the second meeting for the week.

### Watch 

* Business Case. https://edpuzzle.com/assignments/5f16a443928d7f3f1d04bd07/watch

### Think

* What's our Business Case for our app? 


### Dig Deeper
* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 4 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 4. This is useful if you find yourself having trouble with quizzes.  

---

## Week 4 - Managing the Information Systems Project

### Let's Begin

In this week, we shall learn about project management, with a focus on the usually large and complex Information System projects. The lessons will help you in creating your group project as well as other large projects you'll do in the future. 


---

Do these before the first meeting for the week 

### Watch

1. https://edpuzzle.com/assignments/5f16dad87ea8933f1205b68c/watch
2. https://edpuzzle.com/assignments/5f17f907725b3c3f44aaf80e/watch

### Think

* What are the major work activities that we'll need for your project?
* Which activities are dependent on each other?
* Are there activities that be done simultaneously?

---

Please do the following before the second zoom meeting for the week

### Watch

* https://edpuzzle.com/assignments/5f17fa6d725b3c3f44aafb51/watch

### Read 
*  pp 116-136. Knowledge with Information Systems: Forecast Revenues and Expenses for the App. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that 

### Think

* What apps are similar to our group app? 
* Around how much do those apps cost?
* Compared with the app estimated in the text, will our app cost more or less than that? Much more? Much less?
* Around how much might the development of our app cost?

### Dig Deeper
* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 3 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 3. This is useful if you find yourself having trouble with quizzes.  

---


## Week 5 - Initiating and Planning Systems Development Projects

### Let's Begin

In this week, we look at the details and calculations that go into initiating and planning for information systems development projects.

---

Please do the following before the first meeting this week.

### Watch 

* Breakeven point. https://edpuzzle.com/assignments/5f16b003bcac813f2306744f/watch
* Project Planning. https://edpuzzle.com/assignments/5f16b915967c073f4d9ce299/watch

### Think 

* What steps would you need to do to predict when breakeven would happen for our app project?
* What other calculations can we do to get people to have confidence that our app is a good project to undertake?
* What other details can we add to convince possible partners that we've thought of our app project thoroughly?

---

Please do the following before the second meeting for this week.

### Watch

* Improving Plans. https://edpuzzle.com/assignments/5f178a7471b5b93f64c4cdc5/watch


### Read

* pp 137-147. Decision Support: Determine Feasibility of a BusinessLoan for the App. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

### Think

* How would our current app work schedule change when we apply the learning from this recent video?
* What are the calculations necessary to check whether our app development can be loaned from a bank or similar institution?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 5 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 5. This is useful if you find yourself having trouble with quizzes.  

---

## Week 6 - Determining System Requirements 


### Let's Begin

In this week, we shall talk about validating our ideas of the information system requirements and estimates by comparing it to reality, or at least to as much reality that our senses can reasonably gather.


### Watch
* Estimation. https://edpuzzle.com/assignments/5f16df5c16f7663f04f3ebb7/watch

### Read
* System Service Request. https://docs.google.com/document/d/1Uu5kRo7oG46h4C1ICSVe1wd7Ss66LubeSloeOeFPReo/edit?usp=sharing

Read this template so as to be able to create a version for our group project. 

### Think 
Our goal is to create Information Systems that bring value to users and organizations.

* Do you think interviewing our perspective app users would be important? How so?
* Do you think observing our perspective app users would be important? How so?
* Do you think studying the forms they use for data input would be important? How so?
* Do you think that studying the reports they create would be important? How so?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 6 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 6. 

---

## Week 7 - Structuring System Process Requirements 

### Let's Begin

In this week, we look at how we can efficiently record business processes so that we can effectively communicate the written form to our teammates and promote a single understanding of the work we do.


### Read
* pp. 30-35. Diagramming a Business Process. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

### Watch

* System Scope.  https://edpuzzle.com/assignments/5f16e7d9a4b24a3f2e2bd4bc/watch

### Think

* Using what you've read, how would you make the process flow chart for our app?
* Looking at what you've watched, what other types of process diagrams might be useful to have to our app?
* What are the advantages do each of the diagramming methods you've learned?

 ### Dig Deeper
* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 7 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 7. 

---


## Week 8 - Designing Forms and Reports


### Let's Begin

In this week, we learn to group together inputs into concise forms. We also learn to group together outputs into meaningful reports.

### Read 

* pp 37-53. Professionalism in Deliverables: Principles of Graphic Design. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

* chek out:   https://color.adobe.com/create/color-wheel

The color wheel will help you forms and reports that are cool to look at. Having a good color scheme will also help in the future.

### Think 

* Isn't it annoying when you have to re-type information that you've already submitted previously? What advantages can forms in information systems have over paper forms?
* Besides in forms and reports, where else can good color schemes help in the design of our app? 
* What are the forms necessary for our app?
* What reports are necessary for our app?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 10 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 10. 

---

## Week 9 - Graphical User Interfaces and Dialogues

### Let's Begin

In this week, we look at how to put together the user interfaces and dialogues of our mobile app for a good user experience.

### Read

* pp 57-69. User Centered Design: Design an iPhone App. https://open.umn.edu/opentextbooks/textbooks/business-information-systems-design-an-app-for-that

### Think 

* What screens would be necessary for our app to have? 
* When a user submits something, what kind of dialogue boxes should appear? What would the boxes say?

### Dig Deeper
* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 11 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 11. 
----

## Week 10 - Structuring System Data Requirements

### Let's Begin

In this week, we look at how data can be represented in diagrams so that it can be communicated easily and efficiently to the team. 

---

Please do the following before the first meeting for this week.

### Read

* pp 21-30. Entity Relationships. https://bookboon.com/en/database-design-and-implementation-ebook

### Think

* Give an example of an entity type that can be found in our group app?
* Give an example of an entity occurrence that can be found in our group app?

--- 

Please do the following before the second meeting for this week.

### Read 

* pp 31-43. Complex relationships.  https://bookboon.com/en/database-design-and-implementation-ebook

### Think

* What many-to-many entity relationship can be found in our group app? What link/ associative entity would link the two entities?
* What would be the steps in creating an Entity Relationship Diagram (ERD) for our group app?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 8 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 8. 

---

## Week 11 - Designing Databases 

### Let's Begin

In this week, we look at converting ERD's into tables that computers can understand and use. We also look into optimizing the resulting tables for faster searches and maximized storage.

---

Please read the following before the first meeting for the week. 

### Read 

* pp 44-61. Logical Database Design. https://bookboon.com/en/database-design-and-implementation-ebook

### Think

* What are the steps to convert an ERD into a set of related tables?

---

Please read the following before the second meeting for the week.

### Read 

* pp 62-84. CH5 Normalisation. https://bookboon.com/en/database-design-and-implementation-ebook

### Think 

* Why do we do normalization, in your own words? 
* What is something unique about the first normal form?
* What is something unique about the second  normal form?
* What is something unique about the third normal form?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 9 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 9. 

---


## Week 12 - Distributed and Internet Systems Security

### Let's Begin

In this week, we acknowledge the pervasiveness of the cloud-based applications and look at the security that modern information systems have to consider.

### Read

* pp.64-73. CH 6. Information Systems Security.  https://open.umn.edu/opentextbooks/textbooks/information-systems-for-business-and-beyond

### Think

* Describe one method of multi-factor authentication that you have experienced and discuss the
pros and cons of using multi-factor authentication.
* When was the last time you backed up your data? What method did you use?

### Dig Deeper

* Make sure you join the zoom class meeting scheduled for your section.
* You can also read chapter 12 of our textbook:
Modern Systems Analysis and Design, 8th edition. Jeffrey A. Hoffer, Joey F. George, and Joseph S. Valacich. (2017)
* You can also check out the extended slides for Chapter 12. 

---

## Week 13 - Consultations for Presentation Content

Explore canva for slides creation. 

* Canva. https://www.canva.com/


## Week 14 - Video Presentation of Mobile App Design

Explore Screencastify for video creation.

* Screencastify. https://www.screencastify.com/



