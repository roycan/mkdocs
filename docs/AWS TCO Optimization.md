AWS TCO Optimization 
========================

> 2020-10-29T12.54.05  
> Roy Canseco

## Context

By attending this year's AWS International Public Sector Summit, I was able to determine the best practices in cost-optimizing Web Applications running on AWS infrastructure.

This might be important for PSHS because we are experiencing Moodle/ KHub downtime every 2 weeks due to under-capacity of our underlying infrastructure. Our provider, OED/KID, said that this is due to the ballooning of monthly bills from AWS. 

In an attempt to find alternatives to alleviate the matter, the PSHS- MC leadership, under Dr. Lawrence Madriaga, the KHub Coordinator, and the MIS wish to present the latest best practice roadmap for minimizing Total Cost of Ownership for software applications hosted on AWS.


## Big Idea


![qownnotes-media-dngdff](media/qownnotes-media-dngdff-500324109.png)



1. [perfect] We adaptively Right-size
2. [fail] We can implement auto-scaling for various parts of the system (e.g. storage, CPU, cache) 
3. [?] This means going prepaid, much like we do on cellphones. Using reserved nodes/ instances normally gives 30-50% savings. Best is to do pre-paid for expected minimum use, then postpaid (i.e. monthly billing) for anything above the minimum expected use. This is most effective in conjunction with #2. 
4. [fail] According to the 2018 suggested architecture for moodle released by AWS, the current practice of keeping moodledata inside the webserver is *not* best practice. I add that such makes doing #2 more difficult.
5. [perfect] We are measuring and monitoring

## Benefits

![qownnotes-media-YLLyVy](media/qownnotes-media-YLLyVy-2550839853.png)


By utilizing the innate flexibility of the AWS platform, there is a large room for improvement open to us. We are just on the first step of the stairs going in getting our costs down, which is Instance Right-Sizing. It was said in the talk that the Lift and Shift stage has minimal to negligible cost benefits when compared to On-Premise installations. 

## Details 

![qownnotes-media-SBYmMz](media/qownnotes-media-SBYmMz-105362698.png)

The talk was given in a Breakout Session for TCO Optimization in the 2020 AWS Public Sector Summit Online. The speaker was Mr. David Lurie, who works in AWS- Canada.

There are different services in AWS that will be used for optimizing depending on the specific needs, but the following services will be crucial to everyone serious in making the most (and paying the least) for the AWS platform. 

![qownnotes-media-GVUXZE](media/qownnotes-media-GVUXZE-466380556.png)


## Next Steps

The speaker acknowledges that setting up AWS well can be technical and tricky. Thus, David urges reaching out to AWS partners, consultants, and soluton architects. 

David also encourages organizations with AWS workloads to send their people to train.

![qownnotes-media-zBpvpK](media/qownnotes-media-zBpvpK-2938731343.png)



