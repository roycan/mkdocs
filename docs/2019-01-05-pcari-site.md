## Key Design Decisions

1. Backend Language : PHP
2. Framework : Wordpress CMS
3. HTML & CSS
4. Database : MySQL
5. Version Control
6. Testing
7. Deployment : Wordpress.com


## Main Software Sources

* Third party developer
* Cloud services : Wordpress PAAS
