# Email marketing

![Email Marketing](2019-01/email-marketing.png) [^1]




## Advantages and Disadvantages

 | Pros | Cons
 -------|------
 reach both mobile and pc      |    spam filter blocks
 effective for info dissemination  |
 cheap |


## Quick start

  Have a sign-up form in your website, collect emails and pilot test an email marketing campaign.

Gmail and Yahoo gives ~500 free emails per day per account

You can pay monthly :

![Monthly Subscription](2019-01/monthly-provider.jpg)[^3]


Or you can pay per use:

![Pay Per Use](2019-01/per-use-provider.jpg)[^3]


If you pay, you get to easily track:

![Email Gang Tracking](2019-01/email-gang-track.png)[^6]




## Collecting Emails

1. Have multiple subscribe CTA's  (i.e. Calls to Action)
2. Exchange something for their emails
    1. coupons
    2. convenience appeals
    3. rewards
3. Disclose your Social Strength (e.g. number of people served, accounts, years up)

## Sustaining interest

1. Have great content
2. Encourage signing up and forwarding to friends
3. Limit ads to around 10%
4. Great headings (eg "KATTSI is happy to announce a Destination..." , "This is perfect for..." , "Book now and you get ..." , "For a limited time only, save ....")
5. Invite past clients first and offer them a discount
6. Offer early bird and regular pricing
7. Send greeting cards

## Organizing the Mailing List

1. Put everything on an Excel sheet
2. Remove bad/ bouncing emails (~22.5% emails are deactivated yearly)
3. Have Email Service Validation  (so you're not marked as a spammer)
4. Protect your brand by having an Opt-in Policy (6-month last touch expiry)
5. Have Opt-out/ Unsubscribe links

![Double Opt-In](2019-01/double-opt-in.png)[^4]


## Email content

1. Have subjects that are 6-10 words -[source](https://www.tutorialspoint.com/email_marketing/email_marketing_content.htm)

2. Connect with the client
    1. Segment by location (L V S)
    2. Segment by age (single, family, seniors)
    3. Interests (weekend warriors , corporate leave)
3. Add photo/ logo to the email signature
4. Have social media icons in the signature

![Email Signature](2019-01/email-sig.png)[^2]



##  be CAN-SPAM compliant


>>  `CAN-SPAM stands for Controlling the Assault of Non-Solicited Pornography and Marketing Act of 2003)`

Steps:

1. Be Who You Say You Are
2. Don't Lie (in the Subject Line)
3. Tell recipients where you are (mailing address)
4. *Tell people how to Opt-Out (universal unsubscribe rule)*
5. Honor Opt-Out requests promptly (< 10 days)
6. Monitor what others are doing on your behalf


![CAN-SPAM COMPLIANT](2019-01/can-spam.png)[^5]

## Do Spam testing
* [Mailing Check](http://mailingcheck.com)
* [Is Not Spam](http://isnotspam.com)
* [Mail-Tester](http://mail-tester.com)


## Popular Email Blacklisting websites

* Barracuda Reputation Block List
* MXToolBox
* Invaluement
* MultiRBL
* Spamcop
* Spamhaus
* SURBL

If you are blacklisted, follow the instructions to fix the problems and remove your IP address from the list

## Email Validators

Prevention is better than cure, so here are services that check if the emails in your list are still active, so that you don't get accused of spamming.

![Email Validators](2019-01/email-validation.png)[^3]


## Tools we can practice on

1. MS Excel
2. Google Website Optimizer (free)
3. [Open Refine](http://openrefine.org) (formerly Google Refine)


----
[^1]: https://www.tutorialspoint.com/email_marketing/email_marketing_overview.htm
[^2]:https://www.tutorialspoint.com/email_marketing/email_marketing_content.htm
[^3]:https://www.tutorialspoint.com/email_marketing/email_marketing_service_providers.htm
[^4]:https://www.tutorialspoint.com/email_marketing/email_marketing_avoid_being_blacklisted.htm
[^5]:https://www.tutorialspoint.com/email_marketing/email_marketing_spam_compliance.htm
[^6]:https://www.tutorialspoint.com/email_marketing/email_marketing_automation.htm
