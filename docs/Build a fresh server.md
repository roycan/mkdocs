Build a fresh server
========================

## ubuntu 20.04 LTS

`adduser sir_roy`

`usermod -aG sudo sir_roy`

* -a :: append
* -G :: group

`su - sir_roy`

setup firewall:
```
ufw app list
ufw allow OpenSSH
ufw enable
ufw status numbered
 sudo ufw allow ssh
 sudo ufw allow 22
 sudo ufw allow http
 sudo ufw allow 80
 sudo ufw allow https
 sudo ufw allow 443
 sudo ufw allow proto tcp from any to any port 80,443
 sudo ufw allow 5432
```

`dpkg-reconfigure tzdata`