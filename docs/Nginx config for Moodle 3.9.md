Nginx config for Moodle 3.9
========================




Nginx configuration
-------------------

### PHP-FPM

Nginx is usually configured to interface with PHP via [php-fpm](http://php.net/manual/en/install.fpm.php). This is both fast and robust.

PHP-FPM's default behaviour for pools is usually to restrict the execution of scripts to a specific extension, i.e. .php. You should ensure that this behaviour is configured within your particular package/distribution, e.g. for debian,

**/etc/php5/fpm/pool.d/www.conf**

security.limit\_extensions = .php

### Nginx

Add the following 'slash arguments' compatible 'location' block to your vhosts 'server' in your nginx configuration (further explanation at '[Using slash arguments](/39/en/Using_slash_arguments "Using slash arguments")').

**nginx.conf location:**

```bash
location ~ \[^/\]\\.php(/|$) {
    fastcgi\_split\_path\_info  ^(.+\\.php)(/.+)$;
    fastcgi\_index            index.php;
    fastcgi\_pass             127.0.0.1:9000 (or your php-fpm socket);
    include                  fastcgi\_params;
    fastcgi\_param   PATH\_INFO       $fastcgi\_path\_info;
    fastcgi\_param   SCRIPT\_FILENAME $document\_root$fastcgi\_script\_name;
}
```


If the above does not work try the following: Note: This was for CentOS 7.6 (1804), MariaDB 10.3, Nginx 1.15 and PHP 7.3.5

```
location ~ ^(.+\\.php)(.\*)$ {
    root /usr/share/nginx/html/moodle/;
    fastcgi\_split\_path\_info  ^(.+\\.php)(.\*)$;
    fastcgi\_index            index.php;
    fastcgi\_pass             127.0.0.1:9000;
    include /etc/nginx/mime.types;
    include                  fastcgi\_params;
    fastcgi\_param   PATH\_INFO       $fastcgi\_path\_info;
    fastcgi\_param   SCRIPT\_FILENAME $document\_root$fastcgi\_script\_name;
}
```

  

If you find that this does not work (scripts, styles, and images not loading) **and** that there are

`open() "..." failed (20: Not a directory)`

lines appearing in your logs: Check whether there are any directives related to static content **before** this block and try moving them **after** this block.

  

Another potential pitfall is the use of **try\_files**. Many guides recommend configurations like


`try\_files $fastcgi\_script\_name =404;`


. But if try\_files is used, nginx deletes the content of the **$fastcgi\_path\_info** variable (this is [intended behavior](https://trac.nginx.org/nginx/ticket/321)). This causes **PATH\_INFO** to also be empty, so slash arguments don't work. An alternative is the use of


`if(!-f $document\_root$fastcgi\_script\_name) {return 404;}``


[as recommended](https://www.nginx.com/resources/wiki/start/topics/examples/phpfcgi/#connecting-nginx-to-php-fpm) by the nginx documentation, although the nginx documentation also recommends [not using if](https://www.nginx.com/resources/wiki/start/topics/depth/ifisevil/).

#### Error pages

```
# This passes 404 pages to Moodle so they can be themed
error\_page 404 /error/index.php;    error\_page 403 =404 /error/index.php;
```


#### Hiding internal files

```
# Hide all dot files but allow "Well-Known URIs" as per RFC 5785
location ~ /\\.(?!well-known).\* {
    return 404;
}
 
#This should be after the php fpm rule and very close to the last nginx ruleset.
#Don't allow direct access to various internal files. See MDL-69333
location ~ (/vendor/|/node\_modules/|composer\\.json|/readme|/README|readme\\.txt|/upgrade\\.txt|db/install\\.xml|/fixtures/|/behat/|phpunit\\.xml|\\.lock|environment\\.xml) {
    deny all;
    return 404;
}
```


#### XSendfile aka X-Accel-Redirect

Setting Moodle and Nginx to use XSendfile functionality is a big win as it frees PHP from delivering files allowing Nginx to do what it does best, i.e. deliver files.

Enable xsendfile for Nginx in Moodles config.php, this is documented in the config-dist.php, a minimal configuration look like this,

```
$CFG->xsendfile = 'X-Accel-Redirect';
$CFG->xsendfilealiases = array(
    '/dataroot/' => $CFG->dataroot
);
```


Accompany this with a matching 'location' block in your nginx server configuration.

```
location /dataroot/ {
    internal;
    alias <full\_moodledata\_path>; # ensure the path ends with /
}
```


The definition of 'internal' here is **critical** as it prevents client access to your dataroot.

