# Reversing cli commands in RoR

~~~~
$ rails generate controller StaticPages home help
$ rails destrol controller StaticPages home help
~~~~

~~~~
$ rails generate model User name:string email:string
$ rails destroy model User
~~~~


~~~~
$ rails db:migrate
$ rails db:rollback
~~~~

You can also "migrate back" to a certain version. The first version is 0 and moves sequentially along the migrations after that.  

`$ rails db:migrate VERSION=0`
