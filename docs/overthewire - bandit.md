overthewire - bandit
========================

prior: https://www.redhat.com/sysadmin/managing-files-linux-terminal

https://overthewire.org/wargames/bandit/bandit1.html

Host: bandit.labs.overthewire.org
Port: 2220

ssh -p 2220 bandit4@bandit.labs.overthewire.org


1. boJ9jbbUNNfktd78OOpsqOltutMc3MY1
2. CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
3. UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
4. pIwrPrtPN36QITSp3EQaw936yaFoFgAB
5. koReBOKuIDDepwhWk7jZC0RTdopnAYKh
6. DXjZPULLxYr17uwoI01bNLQbtFemEgo7
