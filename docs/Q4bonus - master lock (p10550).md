Q4bonus - master lock (p10550)
========================

Now that you’re back to school for another term, you need to
remember how to work the combination lock on your locker.
A common design is that of the Master Brand, shown at right.
The lock has a dial with 40 calibration marks numbered 0 to
39. A combination consists of 3 of these numbers; for example:
15-25-8. To open the lock, the following steps are taken:

* • turn the dial clockwise 2 full turns
* • stop at the first number of the combination
* • turn the dial counter-clockwise 1 full turn
* • continue turning counter-clockwise until the 2nd number is reached
* • turn the dial clockwise again until the 3rd number is
reached
* • pull the shank and the lock will open.

Given the initial position of the dial and the combination for the lock, how many degrees is the dial
rotated in total (clockwise plus counter-clockwise) in opening the lock?

![qownnotes-media-xbKBsW](media/qownnotes-media-xbKBsW-3630771876.png)



## Input

For each case there is a line of input containing 4 numbers between
0 and 39. The first number is the position of the dial. The next three numbers are the combination.
Consecutive numbers in the combination will be distinct. 

## Output

For each case, print a line with a single integer: the number of degrees that the dial must be turned to
open the lock.

## Sample Input/ Output



|Sample Input  | Sample Output  |
|---|---|
| 0 30 0 30 | 1350 degrees |
| 5 35 5 35 | 1350 degrees |
| 0 20 0 20 | 1620 degrees |
| 7 27 7 27 | 1620 degrees |
| 0 10 0 10 | 1890 degrees |
| 9 19 9 19 | 1890 degrees |


## Codepost Guide

Create the solution and rename the file to *lock.cpp*. 
Upload the file to *codepost.io* 

sample compile command:
```
    g++ -o lock lock.cpp
```
sample run command:
```
    ./lock 0 30 0 30
```
corresponding output:
```
    1350 degrees
```

## Working Template Code
```
/*******************************************************
 * 
 * Q4bonus - master lock (p10550)
 * 
 * Given the starting position in a Master (tm) Lock,
 *   and the combination, calculate the total degrees
 *   of clockwise and counter-clockwise rotation needed
 *   to open the lock
 * 
sample compile command:
    g++ -o lock lock.cpp
sample run command:
    ./lock 0 30 0 30
corresponding output:
    1350 degrees
* 
 * ****************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{
    int arr[4] = {
        atoi(input_array[1]) ,
        atoi(input_array[2]) ,
        atoi(input_array[3]) ,
        atoi(input_array[4]) 
    };


    // change the following to solve the problem

    cout << arr[0] << " " << arr[1] << " " << arr[2] << " " << arr[3] << " degrees" << endl; 

}
```
