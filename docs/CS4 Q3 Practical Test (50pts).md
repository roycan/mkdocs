CS4 Q3 Practical Test (50pts)
========================

1. Create the following Scenes with pictures
2. Allow scene changing
3. Make the Random Crush button change the image and name
4. Change the background-color and increase font-size using CSS
5. Put your name on the first scene and decorate it with a unique font and color
6. [bonus] ** you must have the previous parts checked first

***make sure to create a gdoc with screenshots. no gdoc, no grade***

**upload the exported netbeans zip and the gdoc to google classroom**

![qownnotes-media-cq9252](media/1421954525.png)


![qownnotes-media-pQ9252](media/108877760.png)


![qownnotes-media-zp9252](media/246410739.png)


## Points Distribution

1. ### Create the following Scenes with pictures
    * 10 points
    * This is a check point, you may lock a minimum grade here
2. ### Allow scene changing
    * 10 points
    * This is a check point, you may lock a minimum grade here
3. ### Make the Random Crush button change the image and name
    * 10 points    
    * This is a check point, you may lock a minimum grade here
4. ### Change the background-color and increase font-size using CSS
    * 10 points
    * This is a check point, you may lock a minimum grade here
4. ### Put your name on the first scene and decorate it with a unique font and color
    * 10 points
    * This is a check point, you may lock your perfect grade here

### Bonus:

Create another scene, this time, with your CS4 groupmates as characters and some pictures of persons you admire. Create the appropriate buttons to link this scene to your main program.

* 10 points
* This is a check point, you may lock a minimum grade here
* You may make full use of internet at this point.