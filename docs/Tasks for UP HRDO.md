Tasks for UP HRDO
========================

## Backup System

Options: 
1. manual psql dump
2. backup gem 
3. octopus gem  or  rails 6 replication    

* https://github.com/thiagopradi/octopus
* https://github.com/thiagopradi/octopus_replication_example
* https://freeletics.engineering/2017/03/06/database-replicas.html
* https://medium.com/@zek/automated-backups-with-the-ruby-backup-gem-and-amazon-s3-f0f2f986876e
* http://backup.github.io/backup/v4/getting-started/
* https://gorails.com/guides/hourly-production-server-database-and-file-backups
* https://hostadvice.com/how-to/how-to-backup-postgresql-database-on-ubuntu-18/

## Simpler Authentication System