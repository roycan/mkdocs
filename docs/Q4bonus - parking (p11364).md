Q4bonus - parking (p11364)
========================

When shopping on Long Street, Michael usually parks his car at some
random location, and then walks to the stores he needs. Can you help
Michael choose a place to park which minimises the distance he needs to
walk on his shopping round?

![qownnotes-media-julsrK](media/qownnotes-media-julsrK-3273383172.png)


Long Street is a straight line, where all positions are integer. You pay
for parking in a specific slot, which is an integer position on Long Street.
Michael does not want to pay for more than one parking though. He is
very strong, and does not mind carrying all the bags around.


## Input

 The first number gives the number of stores Michael wants to visit, 1 ≤ n ≤ 20, and the following numbers give
their n integer positions on Long Street, 0 ≤ x<sub>i</sub> ≤ 99.

## Output
Output for each test case a line with the minimal distance Michael must walk given optimal parking.

## Sample Input / Output



| Input              | Output |
| ------------------ | ------:|
| 4 24 13 89 37      | 152    |
| 6 7 30 41 14 39 42 | 70     |



## Codepost guide 

Solve the problem and rename the file into *parking.cpp*.
Upload the file to *codepost.io*

sample compile command:
```
    g++ -o parking parking.cpp
```
sample run command:
```
    ./parking 4 24 13 89 37
```
corresponding output:
```
    152
```

## Working code template
```cpp
/*******************************************************
 * 
 * Q4bonus - parking (p11364)
 * 
 * Given the optimal parking in a Long Street
 *   what is the minimum walk distance needed to visit
 *   a given number of shops
 * 
sample compile command:
    g++ -o parking parking.cpp
sample run command:
    ./parking 4 24 13 89 37
corresponding output:
    152
 * 
 * ****************************************************/

#include <bits/stdc++.h>

using namespace std;


int main (int input_count, char* input_array[])
{
    int shop_count = atoi( input_array[1] );


// change the code below to solve the problem

    vector<int> v ; 

    for (int i=0; i<shop_count; i++){

        v.push_back( atoi(input_array[i+2] ));
    }

    for( auto &x : v ){

        cout << x << " ";
    }
    cout << endl;


}
```


