PHP 7.4
========================

We want Moodle 3.9 in an Ubuntu 20.04 server with PHP 7.4.


`sudo apt install graphviz aspell ghostscript clamav php7.4-pspell php7.4-curl php7.4-gd php7.4-intl php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-ldap php7.4-zip php7.4-soap php7.4-mbstring php7.4-pgsql php7.4-cli php7.4-fpm php7.4-common php7.4-iconv php7.4-curl    php7.4-json libpcre3 libpcre3-dev `

Check:

```
$ sudo systemctl status php7.4-fpm
$ sudo systemctl is-enabled php7.4-fpm
```
