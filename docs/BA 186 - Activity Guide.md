BA 186 - Activity Guide
========================


## Week 1 - Information Systems Development Environment

We will have a discussion activity for this topic.

### Instructions

We will do a **1-3-2** for this. 
Each student shall post 1 reflection on the questions we raised. 
Each student will react/ respond to at least 3 other students’ reflections. Let’s try to make sure each student gets at least 2 replies.
Each student will respond/ reply to at least 2 responses to their reflections.

### Questions Raised

1. SDLC is sometimes defined as: Plan – Analyze – Design – Implement – Maintain and sometimes it is defined as: Analysis – Requirements – Design – Development – Implementation . With which do you agree more? Why?

2.  From what you’ve studied, if you were to have your own personal Software Development Life Cycle, what would the steps be?

### Netiquette

* **Participate**: This is a shared learning environment. No lurking in the cyberspace background. It is not enough to log in and read the discussion thread of others. For the maximum benefit to all, everyone must contribute.

* **Avoid Repetition**: Read everything in the discussion thread before replying. This will help you avoid repeating something someone else has already contributed. Acknowledge the points made with which you agree and suggest alternatives for those with which you don’t.

* **Use Proper Writing Style**: The academic environment expects a higher-order language. Write as if you were writing a term paper. Correct spelling, grammatical construction, and sentence structure are expected in every other writing activity associated with scholarship and academic engagement. Online discussions are no different. Avoid profanity. 

* **Cite Your Sources**: Another big must! If your contribution to the conversation includes the intellectual property (authored material) of others, e.g., books, newspaper, magazine, or journal articles—online or in print—they must be given proper attribution.

* **Respect Diversity**: It’s an ethnically rich and diverse, multi-cultural world in which we live. Use no language that is—or that could be construed to be—offensive toward others. Racists, sexist, and heterosexist comments and jokes are unacceptable, as are derogatory and/or sarcastic comments and jokes directed at religious beliefs, disabilities, and age. We all come with different perspectives, so please be respectful and resist the urge to tell anyone they are wrong. Understand they have had different life experiences and all of our world views are simply different.

* **No YELLING!** Using bold upper-case letters is bad form, like stomping around and yelling at somebody (NOT TO MENTION BEING HARD ON THE EYE).

* **Remember, You Can't Un-Ring the Bell**: Language is your only tool in an online environment. Be mindful. How others perceive you will be large—as always—up to you. Once you've hit the send button, you've rung the bell. Review your written posts and responses to ensure that you’ve conveyed exactly what you intended. This is an excellent opportunity to practice your proofreading, revision, and rewriting skills—valuable assets in the professional world for which you are now preparing. Read your post out loud before hitting the send button. This will tell you a lot about whether your grammar and sentence structure are correct, your tone is appropriate, and your contribution clear or not.


Adapted from [Colorado State University](http://teaching.colostate.edu/tips/tip.cfm?tipid=128)

---

## Week 2 - The Origins of Software

We will have a zoom webinar for this.

### Instructions

1. Login using the zoom link that will be provided.
1. Rename using surname first.
1. Discuss with the class about the sources of software.
1. All sources of software originate from ideas of how someone's life can be made better with technology.
1. The more clearly imagined the idea, the more possible it is to make real, like the ipod.
1. The ideas that are tied to how feel, are more real to you. 
1. Think of 3 pain points, problems that you feel deeply about. List them and send them to me via chat.
1. Now think of the opposites of these problems. What would that look like? Describe them in 1 sentence each. Send them to me via chat. 
1. Now think of a mobile app that, if existed, would ba part of the solution that would bring about the situation you desire. What would the app do? How will it help? Answer these in 2 sentences for each app.  Send me your app descriptions via chat.
1. Go into prof-assigned breakout rooms of 3-5 students each. 
1. Get to know each other. Turn on your video when you introduce yourself.
2. Assign a group secretary
2. Take 2-minute turns introducing your App Ideas while the secretary lists
3. The secretary will then share screen and the group discusses and eliminates ideas that are vague or unpalatable for the group to pursue until 3 app ideas remain. Send me your group's top 3 via chat.
4. Think about personas for each of the 3 app ideas. Tell me about the persona, the problem and the app when I visit your breakup room. 



### Netiquette

Remember:
* Participate
* Respect differing opinions
* Each individual is important
* NO YELLING!
* Think before you speak

---

## Week 3 - Identifying and Selecting Systems Development Projects

We will have an inter-group discussion activity for this topic.

### Instructions

We will do a **1-4-3** for this. 
Each group shall post 1 article based on the questions for this week. 
Each group will react/ respond to at least 4 other groups' articles. Let’s try to make sure each group gets at least 3 substantial comments.
Each group will respond/ reply to at least 3 comments to their articles.

Use the zoom breakout room to jointly create your article, while the secretary shares her screen and types in UVLE.

### Questions 

1. What is the problem/ pain point / need that your app is trying to address?
2.  What's the big idea / value behind your app?
3.  Put into words the Measurable Organizational Value (MOV) of your app for the company/ group who will use it.


### Netiquette

Remember:
* Participate
* Respect differing opinions
* Each individual is important
* NO YELLING!
* Think before you speak/ type


---

## Week 4 - Managing the Information Systems Project

We will have a reflection paper for this topic.

### Instructions

* Make a 1-2 page reflection paper on the most unforgettable experience you had as a Project Manager. You may attach pictures, links, Gantt charts and the like for bonus points. 
* In case, you haven't been a Project Manager before, reflect on an experience where you were a part of the team that took care of an event or created a project.
* Relate your experience to the things we discussed this week in class.

---


## Weeks 5 - 11

We'll have Class Discussions with updates on group deliverables with respect to their project assignments

## Week 12 

We'll have a mini lecture 

## Weeks 13 - 14

Agile-style stand-up meetings for group presentations and class production