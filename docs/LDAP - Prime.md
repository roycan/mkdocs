LDAP - Prime 
========================
* 2020-11-17T13.11.31


[original source](https://gitlab.prime.edu.ph/sysad/tid_remote_access/-/blob/master/part1_ldap.md)

### Install necessary packages.
- `yum update -y && yum install -y openldap openldap-clients openldap-servers`

Initialize service.
- `cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG`
- `chown ldap. /var/lib/ldap/DB_CONFIG`
- `systemctl start slapd`
- `systemctl enable slapd`

Create ROOT account.
- `ldapadd -Y EXTERNAL -H ldapi:/// -f root.ldif`
> SASL/EXTERNAL authentication started <br />
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth <br />
SASL SSF: 0 <br />
modifying entry "olcDatabase={0}config,cn=config" <br />

Build the directory.

- `ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif`
- `ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif`
- `ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif`

Replace the content of the templates.

- `sed -i 's/myorg/prime/g' *`<br/>
- `sed -i 's/desired_manager_password/strong_manager_password/g' *`<br/>
- `sed -i 's/MYORG/PRIME/g' *` <br/>

OR

- `vim access.ldif` <br />
*lines 4, 9, 16, 26, 28: replace with own domain* <br />
*line 21: replace with directory manager password* <br />

```
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=manager,dc=myorg,dc=edu,dc=ph" read by * none

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=myorg,dc=edu,dc=ph

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=manager,dc=myorg,dc=edu,dc=ph

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcRootPW
olcRootPW: desired_manager_password

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange by dn="cn=ext.services,dc=myorg,dc=edu,dc=ph" write by anonymous auth by self write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by self read by dn="cn=ext.services,dc=myorg,dc=edu,dc=ph" read by * none
```

Continue building the directory while setting access controls.

- `ldapmodify -Y EXTERNAL -H ldapi:/// -f access.ldif`
> SASL/EXTERNAL authentication started<br/>
SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth<br/>
SASL SSF: 0<br/>
modifying entry "olcDatabase={1}monitor,cn=config"<br/>
modifying entry "olcDatabase={2}hdb,cn=config"<br/>
modifying entry "olcDatabase={2}hdb,cn=config"<br/>
modifying entry "olcDatabase={2}hdb,cn=config"<br/>
modifying entry "olcDatabase={2}hdb,cn=config"<br/>

Create the domain, configure domain MANAGER account, and create the OUs.

- `ldapadd -x -D cn=manager,dc=prime,dc=edu,dc=ph -W -f domain.ldif`
> Enter LDAP Password: strong_manager_password <br/>
> adding new entry "dc=prime,dc=edu,dc=ph" <br/>
adding new entry "cn=manager,dc=prime,dc=edu,dc=ph" <br/>
adding new entry "ou=people,dc=prime,dc=edu,dc=ph" <br/>
adding new entry "ou=group,dc=prime,dc=edu,dc=ph" <br/>

At this point, the directory is complete and we can now add objects such as users and groups.

#

Replace the test user password.

- `sed -i 's/your_desired_password/test.user_password/g' *`

Upload the test user directory entries.

- `ldapadd -xD cn=manager,dc=prime,dc=edu,dc=ph -W -f test-user.ldif`
> Enter LDAP Password: strong_manager_password<br/>
adding new entry "uid=test.user,ou=people,dc=prime,dc=edu,dc=ph"<br/>

Set up bind account credentials.

- `sed -i 's/desired_bind_password/strong_bind_password/g' *`
- `ldapadd -xD cn=manager,dc=prime,dc=edu,dc=ph -W -f bindaccount.ldif`
> Enter LDAP Password: strong_manager_password<br/>
adding new entry "cn=ext.services,dc=prime,dc=edu,dc=ph"<br/>

#

**Verify records**
You can check the directory for a record with `ldapsearch` as a user with read access, or with `ldapwhoami` as the account holder.

Perform a `search` using the external service bind account to verify read-access:
- `ldapsearch -xb dc=prime,dc=edu,dc=ph -D cn=ext.services,dc=prime,dc=edu,dc=ph -W`
> Enter LDAP Password: strong_bind_password<br/>
> < directory content >

Or perform a `whoami` operation:
- `ldapwhoami -xD uid=test.user,ou=people,dc=prime,dc=edu,dc=ph -W`
> Enter LDAP Password: test.user_password<br/>
dn:uid=test.user,ou=people,dc=prime,dc=edu,dc=ph


**[Proceed to the VPN exercises](VPN Prime.md)**

