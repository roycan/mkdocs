Q4bonus - hola (p12250)
========================

English, Spanish, German, French, Italian and
Russian are the 6 most prominent languages
in the countries of European Union. Figure
on the left shows intensity of English speaking
people in different European countries. All of
these languages have different words to repre-
sent the English word “HELLO”. For example
in Spanish the word equivalent to “HELLO” is
“HOLA”. In German, French, Italian and Rus-
sian language the word that means (or simi-
lar to) “HELLO” is “HALLO”, “BONJOUR”,
“CIAO” and “ZDRAVSTVUJTE” respectively.
In this problem your task is pretty simple. You
will be given one of the six words mentioned
above or any other word and you will have to
try and detect the language it is from.

![qownnotes-media-ENVIIX](media/qownnotes-media-ENVIIX-1928701336.png)


## Input

Each input contains a string S. You can assume that all the letters of the string are uppercase English
letters and the maximum length of the string is 14. 

## Output

This line contains the serial of
output followed by a language name. If the input string is ‘HELLO’ or ‘HOLA’ or ‘HALLO’ or ‘BONJOUR’
or ‘CIAO’ or ‘ZDRAVSTVUJTE’ then you should report the language it belongs to. If the input string is
something other than these 6 strings print the string ‘UNKNOWN’ (without the quotes) instead. All characters in the output strings are uppercase as well. Look at the output for sample input for formatting
details.

## Sample Input/ Output



| Sample Input | Sample Output  |
|---|---|
| HELLO | ENGLISH |
| HOLA | SPANISH |
| HALLO | GERMAN |
| BONJOUR | FRENCH |
| CIAO | ITALIAN |
| ZDRAVSTUJTE | RUSSIAN |


## Codepost guide

Create a program that solves the problem and rename the file *hola.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command:
```
    g++ -o hola hola.cpp
```
sample run command:
```
    ./hola HELLO
```
corresponding output:
```
    ENGLISH
```

## Working template code
```
/*******************************************************
 * 
 * Q4bonus - hola (p12250)
 * 
 *  If the input string is ‘HELLO’ or ‘HOLA’ or ‘HALLO’ or ‘BONJOUR’ or ‘CIAO’ or ‘ZDRAVSTVUJTE’ 
 *      then you should report the language it belongs to. 
 *      If the input string is something other than these 6 strings print the string ‘UNKNOWN’
 * 
 sample compile command:
    g++ -o hola hola.cpp
sample run command:
    ./hola HELLO
corresponding output:
    ENGLISH
 * 
 * ****************************************************/

#include <bits/stdc++.h>

using namespace std;


int main (int input_count, char* input_array[])
{
    string S = input_array[1];

    // change the code below to solve the problem

    cout << S << endl;
}
```


