---
title: Definitions of Done - UPPFI-WW web app
author: Roy Vincent Luna Canseco
date: February 2019
---

## Abstract

This document puts together the issues that UPPFI found and the tests cases created that we believe will more or else ensure that the root causes of the issues have been addressed.[^1] 

Addressing the issues listed in this document is expected to allow UPPFI to finish testing the functionalities of the Web App, leading to the soonest possible date for going Live as well as project source code verification and turn-over. 



## Issue Prioritization 

Priority one:  The Template/ Sample CSV for Loans uploading catches errors even without us doing anything to it. Kindly fix the bad "loans_template.csv"

Priority two:  The "TRANSACTIONS" field in both the  "loans_template.csv"  and the "contributions_template.csv"   should take in variable length strings.  This field's purpose is to inform members of the nature of each loan or contribution record/ transaction and though for a lot of accounts, this includes putting in the OR#, it more often than not, needs to includes some text and dates as well. Please check the View for this also. Remove the automatic "OR#" in the transactions column in the views. The UPPFI staff will manually put the "OR#" whenever they see appropriate. 

Priority three:  In the "loans_template_csv", the following fields should accept "0"  : 
AMOUNT	AMORTIZATION	INTEREST
and the following fields should be allowed to be blank:
AMORTIZATION START DATE	AMORTIZATION END DATE

Priority three also:  In the "contributions_template.csv"  the  following fields should accept "0"  : 
UP CONTRIBUTION	MEMBER CONTRIBUTION	EARNINGS ON UP CONTRIBUTION	EARNINGS ON MEMBER CONTRIBUTION

Priority four: The allowed values for the "TYPE" field in the "loans_template.csv"  should be editable/ configurable to allow the PFI to create new loan products are retire old ones. 


Please note that when Excel exports csv files, default non-string datatypes (e.g. dates and numbers) are not wrapped in quotes. The Web App should anticipate and validate this.  


## Membership Data Import

The template workflow is as follows : 

1.  UPPFI does uses the templates and does their work in Excel. 
2. At the end of their work in filling the spreadsheet, they will do an extra step to save in csv format.
3. They will use the web app to upload the resulting csv to the database.
4. The database will be accessed by the members through the corresponding views. 

Below is a sample xlsx file that includes testcases mapped to the listed priority issues. Note that we are not changing any of the rules, but rather generalizing them to include cases that occur in practice and are expected from the Web App by the members/ end-users of UPPFI.  

[test file: members data - excel](2019-02/members_template-test.xlsx)  

The above text file was converted into a *csv* below. It is the resulting *csv* that is expected to be imported correctly and reliably to the database. 

[test file: members data - csv](2019-02/members_template-test.csv)


## Contribution Data Import

The above text file was converted into a *csv* below. It is the resulting *csv* that is expected to be imported correctly and reliably to the database. 

[test file: contributions data - excel](2019-02/contributions_template-test.xlsx)  

The above text file was converted into a *csv* below. It is the resulting *csv* that is expected to be imported correctly and reliably to the database. 

[test file: contributions data - csv](2019-02/contributions_template-test.csv)

Below is a 2nd excel sheet that should be acceptable. Please try to save as *csv* and check that it will be importable to the database. 

[2nd test file: contributions data - excel](2019-02/contributions_template-test-2.xlsx)  


## Loans Data Import

Below is a sample xlsx file that includes testcases mapped to the listed priority issues. Note that we are not changing any of the rules, but rather generalizing them to include cases that occur in practice and are expected from the Web App by the members/ end-users of UPPFI.  

[test file: loans data - excel](2019-02/loans_template-test.xlsx)  

The above text file was converted into a *csv* below. It is the resulting *csv* that is expected to be imported correctly and reliably to the database. 

[test file: loans data - csv](2019-02/loans_template-test.csv)

Below is a 2nd excel sheet that should be acceptable. Please try to save as *csv* and check that it will be importable to the database. 

[2nd test file: loans data - excel](2019-02/loans_template-test-2.xlsx) 
 


----
[^1]: Any personally identifiable information have been scrubbed out.