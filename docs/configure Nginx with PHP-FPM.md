configure Nginx with PHP-FPM
========================

 Now you need to configure NGINX to proxy client requests to PHP-FPM, which by default is configured to listen on a UNIX socket as defined by the listen parameter in the /etc/php/7.4/fpm/pool.d/www.conf default pool configuration file.
 
`$ sudo vim /etc/php/7.4/fpm/pool.d/www.conf `

![media-oFAejk](media/media-oFAejk.png)

 In the default server block configuration file (/etc/nginx/sites-available/default), uncomment the location directive for processing PHP requests to look like the one shown in the following screenshot.


`$ sudo vim /etc/nginx/sites-available/default`

![media-hxTKTG](media/media-hxTKTG.png)

Then test the NGINX configuration syntax for correctness. If it is OK, restart the Nginx service to apply the new changes.

`$ sudo nginx -t`

`$ sudo systemctl restart nginx`

Now test if NGINX can work in conjunction with PHP-FPM to process PHP requests. Create a simple info.php page under the document root directory.

`$ echo "<?php phpinfo(); ?>" | sudo tee /var/www/html/info.php`

In your browser, navigate using the following address. The PHP configuration page should load showing as shown in the following screenshot.

`http://SERVER_IP/info.php`

![media-FnIjfE](media/media-FnIjfE.png)