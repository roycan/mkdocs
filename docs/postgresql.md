postgresql 
========================


## postgresql 11

### installing postgresql 11

Step 1: Update system and install dependencies


Step 2: Add PostgreSQL 11 APT repository
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

RELEASE=$(lsb_release -cs)
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list

```

```
$ cat /etc/apt/sources.list.d/pgdg.list
deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main
```

Step 3:  Install PostgreSQL 11 on Ubuntu 20.04/18.04/16.04
```
sudo apt update
sudo apt-cache search postgresql-11
sudo apt -y install postgresql-11
sudo apt install postgresql-client-11
```

```
sudo systemctl start postgresql.service
sudo systemctl enable postgresql.service
sudo systemctl status postgresql.service
```

![media-FMAjaq](media/media-FMAjaq.png)


Step 4: Allow access to PostgreSQL from remote hosts

By default, access to PostgreSQL database server is only from localhost.
```
$ sudo ss -tunelp | grep 5432
tcp   LISTEN  0  128  127.0.0.1:5432         0.0.0.0:*      users:(("postgres",pid=15785,fd=3)) uid:111 ino:42331 sk:6 <->
```

```
sudo vim /etc/postgresql/11/main/postgresql.conf

```
Add below line under CONNECTIONS AND AUTHENTICATION section.
You can also specify server IP Address.
Or you can skip this. It might be more secure to skip it anyway.  :)

```
listen_addresses = '*'
```
or
```
listen_addresses = '192.168.17.12'
```



### setup postgresql

[reference](https://gist.github.com/burningTyger/9a8be114731a3cc041d7689247bbdd52)

[reference](https://www.linode.com/docs/guides/how-to-install-use-postgresql-ubuntu-20-04/)

`sudo -i -u postgres psql`

* -i :: interactive (simulate login)
* -u :: user

```sql
CREATE ROLE sir_roy WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'sir_roy_password';
CREATE DATABSE sir_roy WITH OWNER sir_roy;
\du
SHOW hba_file;
\q
```

choose a suitablystrongpassword.


do some tcp socket process testing:
```
ss -plnt
ss -plt
```
* -l :: listen
*  -p :: process
*  -t :: tcp
*  -n :: number ports

```sql
postgres=# CREATE USER moodle_user WITH PASSWORD 'yourpassword';
postgres=# CREATE DATABASE moodle_db WITH OWNER moodle_user;
postgres=# \l
```

```
$ cd /etc/postgresql/11/main/
$ sudo cp pg_hba.conf.bak
$ sudo vim pg_hba.conf
```

In the client authentication file, you'll find the following line (starting with a '#' sign), which tells you the order to add each piece of data. To grant permission for Moodle to connect to a PostgreSQL server on the same machine, just add the following line, changing the DATABASE and USER columns to your actual database name and the username you set up above. The METHOD field should say "password" - don't put your actual password here.

```
   # TYPE      DATABASE    USER            CIDR-ADDRESS        METHOD
   host        moodle_db      moodle_user      127.0.0.1/32        password
```

If your database lives on a separate server from your Moodle installation, you can change the IP address above to match. In this case, it is recommended to not use 'password' in the method field unless the connection between the two servers is secure, as 'password' sends the password in plain text. In these circumstances, you should consider changing METHOD to 'md5' instead.

`sudo systemctl restart postgresql.service`


### another setup for postgresql

```
su -
su - postgres
createdb test
psql test
=# create user username password ' password ';
-- To change a password:
=# alter role username password ' password ';
=# create database databasename with encoding 'utf8';
=# grant all privileges on database databasename to username;
=# \l
=# \c databasename
=# \q
```
$ sudo vim /etc/postgresql/11/main/pg_hba.conf

`host    all         all         0.0.0.0           0.0.0.0            md5`

be SURE to cover this security issue with **iptables**!

`/etc/init.d/postgresql reload or /usr/lib/postgresql/bin/pg_ctl reload`


`psql -h server -d databasename -U username`




## pgloader
pgloader is a data loading tool for PostgreSQL, using the COPY command.

`$ sudo apt-get install pgloader`

For example, for a full migration from SQLite:

```
$ createdb newdb
$ pgloader ./test/sqlite/sqlite.db postgresql:///newdb
```

Or for a full migration from MySQL,

```
$ createdb pagila
$ pgloader mysql://user:password@localhost/sakila postgresql:///pagila
```


Another option:

`sudo -u www-data /usr/bin/php /path/to/moodle/admin/tool/dbtransfer/cli/migrate.php --help`

`sudo -u www-data /usr/bin/php /path/to/moodle/admin/tool/dbtransfer/cli/migrate.php --dbtype='pgsql' --dbhost='x.x.x.x' --dbname='moodleDbName' --dbuser='moodleUser' --dbpass='***' --dbport=5432 --prefix='mdl_' --dbsocket='/var/run/postgresql'`


[reference](https://en.wikibooks.org/wiki/Converting_MySQL_to_PostgreSQL)