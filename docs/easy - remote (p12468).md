easy - remote (p12468)
========================

I’m a big fan of watching TV. However, I don’t like to watch a single channel; I’m constantly zapping
between different channels.

My dog tried to eat my remote controller and unfortunately he partially destroyed it. The numeric
buttons I used to press to quickly change channels are not working anymore. Now, I only have available
two buttons to change channels: one to go up to the next channel (the △ button) and one to go down
to the previous channel (the ▽ button). This is very annoying because if I’m watching channel 3 and
want to change to channel 9 I have to press the △ button 6 times!

![qownnotes-media-RgMShJ](media/qownnotes-media-RgMShJ.png)




My TV has 100 channels conveniently numbered 0 through 99. They are cyclic, in the sense that
if I’m on channel 99 and press △ I’ll go to channel 0. Similarly, if I’m on channel 0 and press ▽ I’ll
change to channel 99.

I would like a program that, given the channel I’m currently watching and the channel I would like
to change to, tells me the minimum number of button presses I need to reach that channel.

## Input

Each test case is described by two integers a and b in a single line. a is the channel I’m currently
watching and b is the channel I would like to go to (0 ≤ a, b ≤ 99).

## Output

For each test case, output a single integer on a single line — the minimum number of button presses
needed to reach the new channel (Remember, the only two buttons I have available are △ and ▽).

## Sample Input / Output



| Input | Output |
|---|---|
| 3 9 | 6 |
| 0 99 | 1 |
| 12 27 | 15 |

## Codepost Guide

Create a program to solve the problem and rename it to *remote.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o remote remote.cpp
```
sample run command:
```
    ./remote 3 9
```
corresponding output:
```
    6
```
## Working Template Code
```
/***********************************************************************
 * 
 * easy - remote (p12468)
 * 
 sample compile command:
    g++ -o remote remote.cpp
sample run command:
    ./remote 3 9
corresponding output:
    6
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int ch1 = atoi( input_array[1]);
    int ch2 = atoi( input_array[2]);

    cout << min(diff1, diff2) << endl;
}
```

## Sir Roy's Solution

```
/***********************************************************************
 * 
 * easy - remote (p12468)
 * 
 sample compile command:
    g++ -o remote remote.cpp
sample run command:
    ./remote 3 9
corresponding output:
    6
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int ch1 = atoi( input_array[1]);
    int ch2 = atoi( input_array[2]);

    // cout << ch1 << " " << ch2 << endl; 

    //make ch2 be the larger channel
    int temp;
    if (ch1 > ch2){
        temp = ch1;
        ch1 = ch2;
        ch2 = temp;
    }

    int diff1 = ch2 - ch1;

    int diff2 = 99-ch2 + ch1-0 + 1;

    cout << min(diff1, diff2) << endl;
}
```