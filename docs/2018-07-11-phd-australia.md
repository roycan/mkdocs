---
layout: post
title: "Australian Uni's w/ good CS PhD Programs "	
remarks: "listing"
categories: Research
tags: 
- PhD
- personal
---

## (day 5)

The list of good CS Universities in Australia are as follows:
1. University of New South Wales- Sydney  (UNSW)
2. University of Newcastle
3. University of Sydney
4. RMIT University
5. ~~Edith Cowan University~~
6. The University of Queensland
7. University of Western Australia
8. *University of Technology Sydney*
9. Curtin University
10. **Queensland University of Technolgy**
11. Deakin University
12. University of South Australia
13. University of Wollongong
14. Australian National University
15. Western Sydney University
16. University of Canberra
17. Murdoch University
18. Central Queensland University
19. The University of Melbourne
20. Monash University
21. University of Tasmania
22. Swinburne University of Technology
23. La Trobe University
24. Victoria University
25. University of New England Australia
26. Carnegie Mellon University Australia
27. Australian Catholic University
28. Federation University, Australia

