PLDT Digital Transformation 2020-06-16T10.46.01
========================

1. CRM migrates to SalesForce
2. Industry-specific applications migrate to Vlocity
3. Operations migrate to Capgemini

## Salesforce

Salesforce.com, inc. is an American cloud-based software company headquartered in San Francisco, California. It provides customer relationship management (CRM) service and also sells a complementary suite of enterprise applications focused on customer service, marketing automation, analytics, and application development.

In 2020, Fortune magazine ranked Salesforce at number six on their Fortune List of the Top 100 Companies to Work For in 2020 based on an employee survey of satisfaction.[1]


![](https://upload.wikimedia.org/wikipedia/en/thumb/8/83/Salesforce_logo.svg/220px-Salesforce_logo.svg.png)


![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Salesforce_Tower_SF_2017.jpg/220px-Salesforce_Tower_SF_2017.jpg)


[1]: [Jessica Snouwaert.  The 25 best companies to work for, based on employee satisfaction - Business Insider](https://www.businessinsider.com/best-companies-to-work-for-based-on-employee-satisfaction-fortune-2020-2) 


---


## Vlocity

Built natively on the Salesforce platform, Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations.

---

## Capgemini


Capgemini SE is a French multinational corporation that provides consulting, technology, professional, and outsourcing services. It is headquartered in Paris, France. Capgemini has over 270,000 employees in over 50 countries, of whom nearly 120,000 are in India.



![](https://upload.wikimedia.org/wikipedia/en/thumb/d/d8/Capgemini_logo.svg/250px-Capgemini_logo.svg.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/11_rue_de_Tilsitt.jpg/250px-11_rue_de_Tilsitt.jpg)


---


```
There's an article about this in the Philippine Star, Business Section, June 16, 2020
```

## Toastmasters TME script 

DT/ DX 

Developed countries have 10-20% of their potential, pre-COVID. 

1. DT of Information 
2. DT of Industries/ Organizations
3. DT of Societies
4. DT of Education / Training 


### Introduction 

A terrific Tuesday evening to our Club President, fellow toastmasters and friends. Welcome to tonight's meeting dubbed *Digital Transformation 101*

My name is Roy Canseco and I am honored to be your Taostmaster of the Evening. 

...


> I am seeing ____ new faces.  Can I have a show of hands from anyone new to toastmasters here? 
> For the benefit of the guests here, I shall explain a bit. 
> Sultan Toastmasters club is a unique, fun and encouraging environment where we learn public speaking and communication through practice. 
> A typical club meeting consists of 3 main parts
> The first part is where we get comfortable and give short impromptu speeches. 
> The second part is where chosen members deliver speech projects.
> The third segment is where feedback is given to the speakers, allowing them to reflect and have learning actually happen. 


Next I want to introduce some people, who are set to make this meeting more exciting and educational. 
_TM Ceasar Jacinto_ _______will track the time. So if you see him eyeing you weirdly, maybe it's time start building your speech to a close. 
_TM Alice Kalaw____  will be our Ah-Counter. He/ she will track your err, ahh, umm, ok, alright, actually... " and other word crutches that we commonly fall into during our speech. 

_TM Michael Jacinto, DTM, TC_ will be our  Grammarian for tonight. He will be our grammar police but this is a good thing. 

---

To set us off into learning frame of mind, let me call _TM Micheal Jacinto_____ to share with us the Word of the Day. 


----

Up next is the prepared speeches segment. As tonight's TME, I want to start and keep a tradition of giving all our prepared speeches speakers a 10-second round of applause after the end of each speech.  
We give them extended applause because we understand that it takes time and effort to prepare a speech, and it certainly takes courage to deliver it in front of so many people here tonight.
So please unmute your mics and give your thunderous applause later for each speaker after they have delivered their speeches. 

---

 

### Intro Speech

Most TME uses the “POETTS” structure when introducing the speaker:

P – Project Title of the speech

O – Objectives of the speech

E – Evaluator: who is the evaluator of this speech

T – Time allocated for the speech

T – Title of the speech to be given

S – Speaker’s introduction

Prepare remarks to be used.

Consult the VPE for last-minute changes to the program. 
Check with the speakers for any last-minute changes. 


Our ______ speaker this evening is ___Linda Navarro, ACB, ALB______ ________ ______.
He/ She will be delivering his/ her speech entitled _Crash Landing on H'yu______________.
He/ She will then be evaluated by _Lolita Bravo, DL2________.

Let me call first the evaluator to briefly tell us the Project Title of the speech,
 The Objectives by which he/ she will be evaluating,
and the time alloted for the speech. 


Thank you evaluator TM __Lolita Bravo, DL2____________
Let's welcome TM __Linda Navarro, ACB, ALB_________ to deliver to us _Crash Landing on H'yu________________. 






Our ______ speaker this evening is ___Teddy Tardecilla, ACB,AD______ ________ ______.
He/ She will be delivering his/ her speech entitled _ULYSSES___________.
He/ She will then be evaluated by _Sonia M. Roco, ACG, ALB,VC2_______.

Let me call first the evaluator to briefly tell us the Project Title of the speech,
 The Objectives by which he/ she will be evaluating,
and the time alloted for the speech. 


Thank you evaluator TM __Sonia M. Roco, ACG, ALB,VC2____________
Let's welcome TM __Teddy Tardecilla, ACB,AD________ to deliver to us _ULYSSES_________________. 

    