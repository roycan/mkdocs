
# Beginning an Information System Project

## Key Design Decisions

1. Backend Language
2. Framework
3. HTML & CSS
4. Database
5. Version Control
6. Testing
7. Deployment

## Sources of Software

There are 6 main sources of software. Which of these would be utilized in this project and in what ways?

1. In-house development
2. Third-party developer
3. COTS producer
4. ERP provider
5. Open Source software
6. Cloud services
