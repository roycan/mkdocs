Q2/ Q3 Grades Deadline
========================


Efren Paz
Mar 23, 2021, 1:21 PM
to Jericca, Christopher, Lieza, Yna, Donna, Mark, Edge, Ralph, Benigno, Efren, Charles, Charles, Robert, Floriedes, Dawn, Gladys, Judah, Leonel, Eileen, Lawrence, PSHS

Dear Teachers,

Below is our schedule of Encoding of Grades and Third Quarter Scholarship Meeting

-March 19 Last day of completion of Q2 NGs (can be extended until March 23)
-March 23 submission of APR for Q2 Grades 3.0 and below
-April 13 Deadline of encoding of Q2 grades for students with NGs
-April 14 CLOSING Q2 EBASED; START OF Q3 ENCODING OF GRADES
-April 16 Deadline of encoding of Q3 grades. For students with Final Grades 4.0 and 5.0 below -temporary NG will be given

-April 19 - 20 Scholarship Committee Meeting

FYP April 19
G8 9:00AM-12:00 NN
G7 1:30PM-4:30PM


AYP April 20
G9 9:00AM-12:00 NN
G10 1:30PM-4:30PM

SYP April 20
G11 9 AM-12NN
G12 1:30 PM-5:30 PM

May 21 Deadline of submission of Q3 pending requirements for students with NGs to get a grade of 3.0 (except for SWANs or with approved requests)

Best Regards,
Efren Paz

--
Mr. Efren P. Paz
Special Science Teacher V
Chemistry Unit
Philippine Science High School - Main Campus
Agham Road, Diliman, Quezon City
Philippines 1104


----

Efren Paz
Mar 23, 2021, 1:59 PM
to Jericca, Christopher, Lieza, Yna, Donna, Mark, Edge, Ralph, Benigno, Efren, Charles, Charles, Robert, Floriedes, Dawn, Gladys, Judah, Leonel, Eileen, Lawrence, PSHS

Sorry for the confusion, the

-April 16 Deadline of encoding of Q3 grades. For students with Final Grades 4.0 and 5.0 below -temporary NG will be given

THIS SHOULD WITH

-April 16 Deadline of encoding of Q3 grades. For students with TENTATIVE GRADES OF 4.0 and 5.0 below -temporary NG will be given

Regards,
Sir Efren