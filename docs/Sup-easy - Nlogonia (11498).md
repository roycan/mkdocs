Sup-easy - Nlogonia (11498) 
========================

After centuries of hostilities and skirmishes between the four nations living in the land generally known
as Nlogonia, and years of negotiations involving diplomats, politicians and the armed forces of all
interested parties, with mediation by UN, NATO, G7 and SBC, it was at last agreed by all the way to
end the dispute, dividing the land into four independent territories.

It was agreed that one point, called division point, with coordinates established in the negotiations,
would define the country division, in the following way. Two lines, both containing the division point,
one in the North-South direction and one in the East-West direction, would be drawn on the map,
dividing the land into four new countries. Starting from the Western-most, Northern-most quadrant,
in clockwise direction, the new countries will be called Northwestern Nlogonia, Northeastern Nlogonia,
Southeastern Nlogonia and Southwestern Nlogonia.


![qownnotes-media-ueufWb](media/qownnotes-media-ueufWb-3368193250.png)


The UN determined that a page in the Internet should exist so that the inhabitants could check in
which of the countries their homes are. You have been hired to help implementing the system.


## Input

The first line of a test case contains two
integers N and M representing the coordinates of the division point (−10^4 < N, M < 10^4 ).
This is followed by two integers X and Y representing the coordinates of a residence
(−10^4 ≤ X, Y ≤ 10^4 ).


## Output

For each test case in the input your program must print one line containing:
*  the word ‘divisa’ (means border in Portuguese) if the residence is on one of the border lines
(North-South or East-West);
*  ‘NO’ (means NW in Portuguese) if the residence is in Northwestern Nlogonia;
*  ‘NE’ if the residence is in Northeastern Nlogonia;
*  ‘SE’ if the residence is in Southeastern Nlogonia;
*  ‘SO’ (means SW in Portuguese) if the residence is in Southwestern Nlogonia.

## Sample Input/ Output


| sample input | corresponding output  |
|---|---|
| 2 1 10 10 | NE |
| 2 1 -10 1 | divisa |
| 2 1 0 33 | NO |
| -1000 -1000 -1000 -1000 | divisa |
| -1000 -1000 0 0 | NE  |
| -1000 -1000 -2000 -10000 | SO  |
| -1000 -1000 -999 -1001 | SE |


## Codepoint Guide

Create the solution to the problem and rename the file nlogonia.cpp and submit to codepost.io

sample compile command:
```
    g++ -o nlogonia nlogonia.cpp
```

sample run command:
```
    ./nlogonia 2 1 10 10
```
corresponding output:
```
    NE
```

## Working Template Code

```
/*******************************************************
 * 
 * Q4bonus - Nlogonia (11498)

 * 
 sample compile command:
    g++ -o nlogonia nlogonia.cpp
sample run command:
    ./nlogonia 2 1 10 10
corresponding output:
    NE
 * 
 * ****************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{

    int divisax = atoi( input_array[1] );
    int divisay = atoi( input_array[2] );
    int x = atoi( input_array[3] );
    int y = atoi( input_array[4] );


    // change the code after the line to solve the problem

    cout << "division point coordinates: " << divisax << " " << divisay << endl;
    cout << "country coordinates: " << x << " " << y << endl; 
}
```
## Sir Roy's Solution

```
/*******************************************************
 * 
 * Q4bonus - Nlogonia (11498)

 * 
 sample compile command:
    g++ -o nlogonia nlogonia.cpp
sample run command:
    ./nlogonia 2 1 10 10
corresponding output:
    NE
 * 
 * ****************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{

    int divisax = atoi( input_array[1] );
    int divisay = atoi( input_array[2] );
    int x = atoi( input_array[3] );
    int y = atoi( input_array[4] );


    // change the code after the line to solve the problem

    // cout << "division point coordinates: " << divisax << " " << divisay << endl;
    // cout << "country coordinates: " << x << " " << y << endl; 

    if (divisax == x  ||  divisay == y) {
        cout << "divisa" << endl;
    } 
    else if (x > divisax && y > divisay ){
        cout << "NE" <<endl;
    }
    else if ( x > divisax && y < divisay){
        cout << "SE" << endl;
    }
    else if ( x < divisax && y > divisay) {
        cout << "NO" << endl;
    }
    else {
        cout << "SO" << endl;
    }
}
```