---
title: Teacher Career Path Evaluation Sheet
author: Roy Vincent Luna Canseco
date: February 2019
---

Dear Venerable Appointment Committee,

I wish to apply for a Special Science Teacher post in Philippine Science High School - Main Campus in the Computer Science Unit.

Here are some points of interest for consideration. I am putting self evaluations points on the work that I was able to find the certificates for.

Thank you very much for considering.


Sincerely,

Roy Canseco


---

## Teaching Performance
As my teaching experience is outside PSHS and is in educating college and post-college students, I have no points for this criterion.

I do would wish to highlight that some of my early teaching (1st and 2nd year University Computer Science subjects) do apply to PSHS now because of the K12 changes and my effectiveness as a senior high school teacher may be inferred from these.

I have the SET's (the evaluation tool of UP Diliman) and the Chairman of Computer Science (during the time I taught freshmen and sophomores) will corroborate this. His contact number is among my references.

Kindly take the above into consideration. You won't be disappointed.


## Relevant Education (19 points)

Bachelor of Science in Electrical Engineering  -  I got the honor of graduating top of the B.S.E.E. class of 2008.

Master of Science in Electrical Engineering maj. Power Systems  - I got the honor of finishing program in 1 year, instead of 2. My adviser and I believes that I was the first to do so. This can easily be checked by looking at my Bachelor's diploma (year 2008) and comparing it to my Master of Science diploma (year 2009).

Both degrees are from UP Diliman.

I would like to request 19 points.


## Relevant Teaching Experience (5.0 points)

I include my Service Record in UP Diliman, both for teaching in Electrical and Electronics Engineering and in Computer Science. If you add the years/months/days, it would show a few days more than 5 years worth teaching to present.


## Relevant Training
This is for the usual summer schools and winter schools that are done in more developed countries. It also provides acknowledgment to conferences and seminars. I have some of those. Aside from those I continually undergo online trainings to keep abreast of the very dynamic field of computer science. So my credits would be taken on 0.2 points / day of the relevant training.

I ask that the following be credited:

1. Beginning PHP and MySQL Tutorial online course - 0.2   
    `7-10-2018	7-10-2018	10.5`
2. Research Methods and Statistics: An Introduction online course - 0.2  
3. Nuclear Energy Forum: Considerations and Essential Information - 0.2
4. 2017 Mathematical Society of the Philippines Annual Convention - 0.6
5. Workshop on Empowering Uncommon Knowledge - 0.2
6. Introduction to Business Analytics Lecture - 0.2
7. Amian 2017: A Winter School on Modeling, Separation and Purification - 1.6
8. The Street Strategists' Financial Literacy Program - 0.4
9. Software Testing - 0.8
10. UP BEST Technology Tournament - 0.2
11. 12th ANGCA NGV School (CNG Cylinder Safety Workshop) - 0.2
12. Inquies Summer Course - 0.8
13. Chemical Engineering and Forensic Science: A closer look at DNA Analysis - 0.2
14. Open Sourcing UP seminar - 0.2
15. Technical Support Fundamentals - a Google-authorized Coursera Course - 0.5
16. The Bits and Bytes of Computer Networking - a Google-authorized Coursera Course - 0.6
17. Elsevier's Research Writing and Publication Workshop - 0.2
18. Teaching Effectiveness Course - 1.0
19. WCTP 2018 - Workshop on Computation: Theory and Practice - 0.4
6. 19th Regional Symposium on Chemical Engineering




## Professional Output / Activities

### *Teacher* Training Involvement

1.  2018 Faculty Conference on Engineering Education Innovation - 0.5
2. PSHS System’s Science Immersion Program - 2
3. Blended (Face to Face & Online) Training on the Essentials of Microsoft Excel - 0.5
4. EPDP Training on Programming Languages: MATLAB - 2
5. Exploring Non-Linear Temporal Dynamics of Photocatalytic Hydrogen Production using Agent-based Simulations - 2
7.  First Green Tech Boot Camp - 2
8. Seminar-Workshop on Power System Modeling - 0.25
9. OpenOffice Training and Open Source Migration Lecture - 2
10. W.C.C. CISCO Networking I.T. Teachers Training Workshop Series - 1.5
11. W.C.C. Q.C. I.T. Teachers Training Series - 1.0


### *Student* Training Involvement (4 points)

1. BOOTCAMP 4.0 - 0.5
2. 11.12.13 Ateneo Junior High Math Day Celebration - 0.5
3. CarEEEr Talk: E-nnovations - 0.5
4. ES Engineering Symposium 2011 - 0.5
5. Hotwired 4: Pinoy Tech! - 0.5
6. WCC Intercampus I.T. Conference - 1.5



### Leading a Professional Organization

I have none here at the moment. I am a member of the IEEE Philippine Section though.

### Committee Membership (4.0)

* University Project Competition committee (2 years) - 1.0
* College Information Officer for Computer Science
* PDA for Training, Education and Development (3 semestral appointments) - 3.0

### Administrative Assignments
OIC Laboratory Head of the Service Science and Software Engineering Lab (1 year)

### Books Published

none yet (still in the bucket list)

### Research / Professional Articles Published (4.0 points)

R. V. L. Canseco and V. P. Bongolan, "Markov Chain Monte Carlo optimization of visible light-driven hydrogen production," TENCON 2012 IEEE Region 10 Conference, Cebu, 2012, pp. 1-5.
doi: 10.1109/TENCON.2012.6412278

* SCOPUS indexed - 4.0


### Lab Manual/ Workbook published

none yet (planning to for Web/ Mobile Information System Development)

### Involvement in Research

* Was the Primary Technical Consultant for the STR project of Ryan Misola and Kenji Paralejo (PSHS batch 2012)
* Was the Primary Technical Consultant for the STR project of Raphael Canseco (PSHS batch 2008)

### Professional Awards and Recognition (1.0 points)

* Jose P. Segovia Professorial Chair Award (2016)  -  1.0
* Nokia Teaching Innovation Grant (2017)

### Coaching Co-curricular Activities

none yet (no plans)

### Involvement in Outreach Activities (1.2 points)

* Development of the Scholarship and Fellowship Data System - 1.0  
* UP OVCA Offices Information Systems Development - 0.2

----

All in all, I am asking for myself some 62.85 raw points. When compensated with a factor of (119/89), it becomes 84.04 points.

Thank you for your kind consideration!
