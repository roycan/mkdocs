Laravel on XAMPP in Ubuntu 18.04
========================

## setup XAMPP  

Note that you may need to uninstall apache first 

`$ sudo apt remove apache*`

https://www.5balloons.info/how-to-install-laravel-5-5-with-xampp-on-linux/

~~~~bash
$ sudo apt install php7.4 php7.4-cli php7.4-common php7.4-json php7.4-opcache php7.4-mysql php7.4-mbstring php7.4-zip php7.4-fpm php7.4-xml -y

~~~~


* open xampp from /opt/lampp/xampp
* start the apache server

* go to    authproject.localhost/myProject


## Connect to a database 

https://www.5balloons.info/connecting-your-project-to-database-in-laravel/



