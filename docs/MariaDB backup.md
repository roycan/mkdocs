MariaDB backup
========================

* `mysqldump -u -p`
* restore DB:   `moodle < moodle.sql`

```
cd /var/www
mkdir backup
cp -pr moodle backup/
cp -rp moodledata backup/
cd 
sudo mysqldump -u root -p moodle_db > moodle_db.sql
sudo mysql -u root -p 
```
```
show databases
drop database moodle_db
create database moodle_db
```

```
cd
sudo mysql -u root -p moodle_db < moodle_db.sql
```

Backup the Moodle database on the old server:
`mysqldump -h example.com -u myusername -p'mypassword' -C -Q -e --create-options mydatabasename > moodle-database.sql`

* -h :: hostname
* -C :: compress
* -Q :: quote identifiers
* -e :: extended-insert (speed up)
* --create-options :: include all mysql-specific table options


`mysqldump --allow-keywords --opt -uMySQL_USERNAME -pPASSWORD DATABASE | ssh USER@DOMAIN "mysql -uMySQL_USERNAME -pPASSWORD DATABASE"`

* --allow-keywords :: allow creation of column names that are keywords
* --opt :: 	Shorthand for --add-drop-table --add-locks --create-options --disable-keys --extended-insert --lock-tables --quick --set-charset


Restore the database backup to the new server:
`mysql -p new_database < moodle-database.sql`
