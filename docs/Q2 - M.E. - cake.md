Q2 - M.E. - cake
========================

##  4.2.2.2 - Selection Structure (if-else)

### Instruction:
Create a program based on the problem details below and save filename to *cake.cpp* 

sample compile command: `g++ -o cake cake.cpp`
 
sample run command: `./cake 72 20`

output: `Excess: 12 slice(s)`

### Problem 

![qownnotes-media-zbTMeh](media/qownnotes-media-zbTMeh-2674162978.png)


### Sample Test Cases and Format

command: `./cake 36 12`

output: `Exact for all`

command: `./cake 72 20`

output: `Excess: 12 slice(s)`

command: `./cake 51 25`

output: `Excess: 1 slice(s)`



### Sample code template
```
#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {

    int slices = atoi(argv[1]);
    int people = atoi(argv[2]);

    //insert solution code below


}
```