VPN Prime 
========================
* 2020-11-24T13.49.43

Install necessary packages
- `yum update -y && yum install -y epel-release && yum install -y openssl openvpn openvpn-auth-ldap`

Generate CA certificate

- `cd /etc/openvpn/server`

- `openssl genrsa -out ca.key 2048`

> Generating RSA private key, 4096 bit long modulus <br />
..........................................++ <br />
e is 65537 (0x10001) <br />

- `openssl req -new -x509 -days 3650 -key ca.key -out ca.crt`
> You are about to be asked to enter information that will be incorporated into your certificate request. <br />
What you are about to enter is what is called a Distinguished Name or a DN. <br />
There are quite a few fields but you can leave some blank <br />
For some fields there will be a default value, <br />
If you enter '.', the field will be left blank. <br />
----- <br />
Country Name (2 letter code) [XX]: **PH** <br />
State or Province Name (full name) []: **NCR** <br />
Locality Name (eg, city) [Default City]: **Quezon City** <br />
Organization Name (eg, company) [Default Company Ltd]: **PRIME** <br />
Organizational Unit Name (eg, section) []: **CA** <br />
Common Name (eg, your name or your server's hostname) []: **PRIME CA** <br />
Email Address []: **support@prime.edu.ph** <br />

**Server certificate**

- `openssl genrsa -out server.key 2048`
> Generating RSA private key, 4096 bit long modulus <br />
..........................................++ <br />
e is 65537 (0x10001) <br />

- `openssl req -new -key server.key -out server.csr`
> You are about to be asked to enter information that will be incorporated into your certificate request. <br />
What you are about to enter is what is called a Distinguished Name or a DN. <br />
There are quite a few fields but you can leave some blank <br />
For some fields there will be a default value, <br />
If you enter '.', the field will be left blank. <br />
----- <br />
Country Name (2 letter code) [XX]: **PH** <br />
State or Province Name (full name) []: **NCR** <br />
Locality Name (eg, city) [Default City]: **Quezon City** <br />
Organization Name (eg, company) [Default Company Ltd]: **PRIME** <br />
Organizational Unit Name (eg, section) []: **Service** <br />
Common Name (eg, your name or your server's hostname) []: **vpn.prime.edu.ph** <br />
Email Address []: **support@prime.edu.ph** <br />

- `openssl x509 -req -in server.csr -out server.crt -CA ca.crt -CAkey ca.key -CAcreateserial -days 365`
> Signature ok<br />
subject=/C=PH/ST=NCR/L=Quezon City/O=PRIME/OU=Service/CN=vpn.prime.edu.ph/emailAddress=support.eee.upd.edu.ph<br />
Getting CA Private Key<br />

- `openssl dhparam -out dh2048.pem 2048`
>Generating DH parameters, 2048 bit long safe prime, generator 2<br />
This is going to take a long time<br />
..........+.............................++*++<br />

- `openvpn --genkey --secret ta.key`

**Modify permissions**

- `chmod 600 /etc/openvpn/server/*.key`

Verify the content of the VPN server configuration.

- `vim /etc/openvpn/server/server.conf`

```
port 1194    
proto udp    
dev tun    
ca ca.crt
cert server.crt
key server.key  
dh dh2048.pem  
topology subnet
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 1.1.1.1"
push "dhcp-option DNS 8.8.8.8"
duplicate-cn    
keepalive 10 120
tls-auth ta.key 0
cipher AES-256-CBC
;max-clients 100
persist-key    
persist-tun
status openvpn-status.log
explicit-exit-notify 1
verb 3
plugin /usr/lib64/openvpn/plugin/lib/openvpn-auth-ldap.so /etc/openvpn/auth/ldap.conf

;reneg-sec 0    
;tun-mtu 1468    
;mssfix 1400      
```

#

Enable IP forwarding.

- `echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/10-ipv4_forward.conf`
- `sysctl -p /etc/sysctl.d/10-ipv4_forward.conf`

Enable IP masquerading for the VPN subnet. 
- `iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE`
- `iptables-save`

#

(Optional) Debugging mode. Make sure you are inside the directory where the configuration file is located. Otherwise, substitute the location of the certificate files with an absolute path in your `server.conf`.

- `cd /etc/openvpn/server`
- `openvpn --config server.conf`
>OpenVPN 2.4.9 x86_64-redhat-linux-gnu [Fedora EPEL patched] [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Apr 24 2020<br />
Thu Oct  8 14:55:42 2020 library versions: OpenSSL 1.0.2k-fips  26 Jan 2017, LZO 2.06<br />
Thu Oct  8 14:55:42 2020 Diffie-Hellman initialized with 2048 bit key<br />
Thu Oct  8 14:55:42 2020 Outgoing Control Channel Authentication: Using 160 bit message hash 'SHA1' for HMAC authentication<br />
Thu Oct  8 14:55:42 2020 Incoming Control Channel Authentication: Using 160 bit message hash 'SHA1' for HMAC authentication<br />
Thu Oct  8 14:55:42 2020 TUN/TAP device tun0 opened<br />
Thu Oct  8 14:55:42 2020 TUN/TAP TX queue length set to 100<br />
Thu Oct  8 14:55:42 2020 /sbin/ip link set dev tun0 up mtu 1500<br />
Thu Oct  8 14:55:42 2020 /sbin/ip addr add dev tun0 10.8.0.1/24 broadcast 10.8.0.255<br />
Thu Oct  8 14:55:42 2020 Could not determine IPv4/IPv6 protocol. Using AF_INET<br />
Thu Oct  8 14:55:42 2020 Socket Buffers: R=[212992->212992] S=[212992->212992]<br />
Thu Oct  8 14:55:42 2020 UDPv4 link local (bound): [AF_INET][undef]:1194<br />
Thu Oct  8 14:55:42 2020 UDPv4 link remote: [AF_UNSPEC]<br />
Thu Oct  8 14:55:42 2020 MULTI: multi_init called, r=256 v=256<br />
Thu Oct  8 14:55:42 2020 IFCONFIG POOL: base=10.8.0.2 size=252, ipv6=0<br />
Thu Oct  8 14:55:42 2020 IFCONFIG POOL LIST<br />
Thu Oct  8 14:55:42 2020 Initialization Sequence Completed<br />

**LDAP INTEGRATION**

- `cd /etc/openvpn/auth`
- `sed -i 's/myorg/prime/g' *`
- `sed -i 's/desired_bind_password/strong_bind_password/g' *`

Start and enable the service on boot.

- `systemctl start openvpn-server@server`
- `systemctl enable openvpn-server@server`

#

**CLIENT CONFIGURATION**

Generate and sign client certificates.

- `cd /etc/openvpn/client`

- `openssl genrsa -out test.user.key 2048`
> Generating RSA private key, 2048 bit long modulus <br />
..........................................++ <br />
e is 65537 (0x10001) <br />

- `openssl req -new -key test.user.key -out test.user.csr`
> You are about to be asked to enter information that will be incorporated into your certificate request. <br />
What you are about to enter is what is called a Distinguished Name or a DN. <br />
There are quite a few fields but you can leave some blank <br />
For some fields there will be a default value, <br />
If you enter '.', the field will be left blank. <br />
----- <br />
Country Name (2 letter code) [XX]: **PH** <br />
State or Province Name (full name) []: **NCR** <br />
Locality Name (eg, city) [Default City]: **Quezon City** <br />
Organization Name (eg, company) [Default Company Ltd]: **PRIME** <br />
Organizational Unit Name (eg, section) []: **Support** <br />
Common Name (eg, your name or your server's hostname) []: **test.user** <br />
Email Address []: **test.user@prime.edu.ph** <br />

Please enter the following 'extra' attributes<br/>
to be sent with your certificate request<br/>
A challenge password []:<br/>
An optional company name []:<br/>

- `openssl x509 -req -in test.user.csr -out test.user.crt -CA ../server/ca.crt -CAkey ../server/ca.key -CAcreateserial -days 365`
>Signature ok<br/>
subject=/C=PH/ST=NCR/L=Quezon City/O=PRIME/OU=Support/CN=test.user/emailAddress=test.user@prime.edu.ph<br/>
Getting CA Private Key<br/>

#

Create client VPN profile

- `bash generate.sh`

OR

- `vim test.user.ovpn`
*line 2: replace with your public IP address*<br/>
*insert appropriate certificate content*<br />

```
client
dev tun
proto udp
remote 103.137.220.X 1194
cipher AES-256-GCM
resolv-retry infinite
nobind
persist-key
persist-tun
tls-client
key-direction 1
redirect-gateway def1
auth-user-pass

<ca>
*insert content of ca cert*
</ca>

<tls-auth>
*insert content of ta.key
</tls-auth>

<cert>
*insert content of client cert*
</cert>

<key>
*insert content of client key*
</key>
```

#

Upload the VPN profile to the web server for easy transfer.

- `scp test.user.ovpn tid:~/test-user.ovpn`

Download them to your devices for use with the OpenVPN Connect app.

- [TID Web Repo](http://tid.prime.edu.ph)

