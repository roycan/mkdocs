---
layout: post
title: "My PhD Hunt  (day 4)"	
remarks: "diary-ish"
categories: Research
tags: 
- PhD
- personal
---


There was a delay in me writing this, but that's fine. I had a good drive with Sir Gil and we were able to imagine my work as some way of getting the information from the user in almost natural language and having the machine convert it to some useful code. 

Later that evening, I was talking with Monette about it and I saw the possibility of having machine learning in rails to inform the system of the priority parts for automation improvement. 

In the next morning I talked with Dad and we put things together to see that the system will be working on three things:
* data entry
* DB data viewing
* calculators with values coming from and being saved in the database

All these would be forming small and useful programs for the End User. 