easy - rujia (p12554)
========================

There are n people (excluding myself) in my 30th birthday party. They sing the traditional “happy
birthday” song:

> *Happy birthday to you! Happy birthday to you! Happy birthday to Rujia! Happy birthday to you!!!*

Since I love music, I want to hear something more interesting, not that everyone sings together. Ah
yes, I want one person to sing one word!

![qownnotes-media-fRFSMD](media/qownnotes-media-fRFSMD.png)


For example, there are three people: Mom, Dad, Girlfriend, I’d like them to sing like this:

```
Mom: Happy
Dad: birthday
Girlfriend: to
Mom: you
Dad: Happy
Girlfriend: birthday
Mom: to
Dad: you
Girlfriend: Happy
Mom: birthday
Dad: to
Girlfriend: Rujia
Mom: Happy
Dad: birthday
Girlfriend: to
Mom: you
```

Very nice, right? What if there are more than 16 people? That’s easy: repeat the song until
everyone has sung at least once :)

Please, don’t stop in the middle of the song.

## Input

The input contains a single integer n (1 ≤ n ≤ 100). Then each of the
next n strings contains a capitalized name (i.e. one upper-case letter followed by zero or more lowercase
letters). Each name contains at most 100 characters and do not have whitespace characters inside.

## Output

Output the song, formatted as above.

## Codepost Guide

Create a program to solve the problem and rename it to *rujia.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o rujia rujia.cpp
```
sample run command:
```
    ./rujia 3 Mom Dad Girlfriend
```
corresponding output:
```
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
    Dad: Happy
    Girlfriend: birthday
    Mom: to
    Dad: you
    Girlfriend: Happy
    Mom: birthday
    Dad: to
    Girlfriend: Rujia
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
```
## Working Template Code
```
/***********************************************************************
 * 
 * easy - rujia (p12554)
 * 
 * Sing happy birthday for Rujia the way he likes it
 * 
sample compile command:
    g++ -o rujia rujia.cpp
sample run command:
    ./rujia 3 Mom Dad Girlfriend
corresponding output:
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
    Dad: Happy
    Girlfriend: birthday
    Mom: to
    Dad: you
    Girlfriend: Happy
    Mom: birthday
    Dad: to
    Girlfriend: Rujia
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int count = atoi( input_array[1]);

    string arr[count];

    for (int i=0; i< count; i++){

        arr[i] = input_array[i+2];
        cout << arr[i] << " " ;
    }
    cout << endl;


}
```

## Sir Roy's Solution
```
/***********************************************************************
 * 
 * easy - rujia (p12554)
 * 
 * Sing happy birthday for Rujia the way he likes it
 * 
sample compile command:
    g++ -o rujia rujia.cpp
sample run command:
    ./rujia 3 Mom Dad Girlfriend
corresponding output:
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
    Dad: Happy
    Girlfriend: birthday
    Mom: to
    Dad: you
    Girlfriend: Happy
    Mom: birthday
    Dad: to
    Girlfriend: Rujia
    Mom: Happy
    Dad: birthday
    Girlfriend: to
    Mom: you
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int count = atoi( input_array[1]);

    string arr[count];

    for (int i=0; i< count; i++){

        arr[i] = input_array[i+2];
        // cout << arr[i] << " " ;
    }
    // cout << endl;

    int songs;
    if (count % 16 != 0 ){
        songs = (count / 16) + 1;
    }
    else{
        songs = (count / 16) ;
    }
    // cout << songs << endl;

    string lyrics[16] = {"Happy", "birthday", "to", "you", "Happy", "birthday", "to", "you", 
            "Happy", "birthday", "to", "Rujia", "Happy", "birthday", "to", "you"};
    

    int j,k = 0;

    for (int i=0; i < songs*16 ; i++ ){

        cout << arr[j]<< ": " << lyrics[k] << endl; 

        j++;
        k++;

        if (j == count ){
            j = 0;
        }
        if (k == 16 ) {
            k=0;
        }

    }

}
```