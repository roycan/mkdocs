---
layout: post
title: "Setting up an AWS account for Team Development"	
remarks: quick guide
categories: Teaching
tags: 
- soft_engg
---

These are my 6 steps for using an AWS account for a development team.

This is best after a weekend of AWS workshops/ training.

1. Remove access rights from outsiders (non-team members)
2. Set an agreement on naming conventions (e.g. prefix new instances with 3-letter acronyms or first 3 letters of the name)
3. Self-name/ prefix cloud resources so we know who will terminate 
4. Terminate all un-named/ unclaimed billable cloud resources
5. Assign someone to monitor billing on a regular basis
6. When there are live projects, assign someone to monitor instances on a regular basis  