Robbin's Edge
=============



| + emotions | - emotions  |
|---|---|
| grateful | overwhelmed  |
| wealthy | weak |
| hopeful | sickly |
| blessed | unreliable |
