Q4 - Quiz - functions
========================

## Instructions

Modify the program in the Template Code.
 Your task is to display also the difference, product and quotient
of the two input numbers. You have to create one function per operation. See sample output below.

### Example Run:
```
Enter first number: 9
Enter second number: 3
Sum: 12
Difference: 6
Product: 27
Quotient: 3

```



## Codepost Guide

Create a program to solve the problem and rename it to *functions.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o fucntions fucntions.cpp
```
sample run command:
```
    ./functions
```
input the 2 numbers via cin

check the corresponding output for correctness and adherence to format




## Working Template Code


```
/*********************************************************************************
 * 
 * Modify the following program to display also the difference, product and quotient
of the two input numbers. 
You have to create one function per operation.
 *
 * Example Run:
        Enter first number: 9
        Enter second number: 3
        Sum: 12
        Difference: 6
        Product: 27
        Quotient: 3
 * 
sample compile command:
    g++ -o fucntions fucntions.cpp
sample run command:
    ./functions
input the 2 numbers via cin
 * 
 * Make sure the answers (i.e. quotients) make sense. 
 * Check the format of the output is correct.
 * 
 * ******************************************************************************/


#include <iostream>
#include <cmath>
using namespace std;

//function declaration
float sum(float,float);

int main(){
    float x, y;
    cout<<"Enter first number: ";
    cin>> x;
    cout<<"Enter second number: ";
    cin>>y;
    
    
// CHANGE THE CODE BELOW TO SOLVE THE PROBLEM    
    
    cout<<"Sum: "<<sum(x,y) << endl; //function call
    return 0;
}

//function definition
float sum(float a, float b) {
    float c = a+b;
    return c;
}

```


## Sir Roy's Solution

```
/*********************************************************************************
 * 
 * Modify the following program to display also the difference, product and quotient
of the two input numbers. 
You have to create one function per operation.
 *
 * Example Run:
        Enter first number: 9
        Enter second number: 3
        Sum: 12
        Difference: 6
        Product: 27
        Quotient: 3
 * 
sample compile command:
    g++ -o fucntions fucntions.cpp
sample run command:
    ./functions
input the 2 numbers via cin
 * 
 * Make sure the answers (i.e. quotients) make sense. 
 * Check the format of the output is correct.
 * 
 * ******************************************************************************/

#include <iostream>
using namespace std;

//function declaration
float sum(float,float);
float diff(float,float);
float prod(float,float);
float quo(float,float);


int main(){
    float x, y;
    cout<<"Enter first number: ";
    cin>> x;
    cout << endl;
    cout<<"Enter second number: ";
    cin>>y;
    cout << endl;
    cout<<"Sum: "<<sum(x,y) << endl; //function call
    cout<<"Difference: "<<diff(x,y) << endl; //function call
    cout<<"Product: "<<prod(x,y) << endl; //function call
    cout<<"Quotient: "<<quo(x,y) << endl; //function call
    return 0;
}


//function definition
float sum(float a, float b) {
    float c = a+b;
    return c;
}
float diff(float a, float b) {
    float c = a-b;
    return c;
}
float prod(float a, float b) {
    float c = a*b;
    return c;
}
float quo(float a, float b) {
    float c = a/b;
    return c;
}
``` 