Q3 - LT2 - bubble
========================

Sorting algorithms are important in Computer Science.

One of the simpler (but more processor-intensive) ways of sorting a group of items in an array is bubble sort, where each element is compared to the one next to it and they are swapped if the one on the left is larger than the one on the right. This continues until the array is eventually sorted.

Check out [this video from Harvard’s CS50x on Bubble Sort](https://www.youtube.com/watch?v=8Kp-8OGwphY).

There’s also [an entry on Bubble Sort on Wikipedia](http://en.wikipedia.org/wiki/Bubble_sort) that’s worth taking a look at.


![media-AmqzxU](media/media-AmqzxU-3848870752.gif)


## Codepost instructions

Your task is to create a program that takes a set of numbers, puts them in an array, and outputs the numbers as they get sorted.

 * Output the result of each iteration.
 * The first number of the input is the count of numbers to be arranged
 * The second to last numbers are to be sorted
* Rename the file to bubble.cpp and upload it to codepost.io

sample compile command: 
```
    g++ -o bubble bubble.cpp
```

sample run command:
```
    ./bubble 3 3 2 1
    
```
corresponding output:
```
    2 3 1 
    2 1 3 
    1 2 3 
```


sample run command:
```
    ./bubble 3 1 2 3
    
```
corresponding output:
```
    1 2 3 
    1 2 3 
    1 2 3 
```

sample run command:
```
    ./bubble 5 4 3 78 2 0 
    
```
corresponding output:
```
	4 3 78 2 0 
	3 4 78 2 0 
	3 4 78 2 0 
	3 4 2 78 0 
	3 4 2 0 78 
	3 4 2 0 78 
	3 2 4 0 78 
	3 2 0 4 78 
	2 3 0 4 78 
	2 0 3 4 78 
	0 2 3 4 78 
```

## Required template code

```
/***********************************************************************
 * This is the starter code for the bubble sort.
 * Those taking the Q3 LT2 are required to use this code.
 * You may only change the parts indicated for changing
 * 
 * Output the result of each iteration.
 * The first number of the input is the count of numbers to be arranged
 * The second to last numbers are to be sorted
 * 
 * sample compile command:    
       g++ -o bubble bubble.cpp
 * sample run command: 
       ./bubble 3 3 2 1
 * corresponding output:
        2 3 1 
        2 1 3 
        1 2 3 
 * 		
 * 
 * *********************************************************************/


#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{

	int count = atoi(argv[1]);

	int arr[count]; 
	
	for (int i=0; i<count; i++){

		arr[i] = atoi(argv[i+2]);
	} 



	// change the parts below to solve the problem

	for (int i=0; i<count; i++){

		cout << arr[i] << " " ;
	}
	cout << endl;


}
```