Easter Bonus points
========================


## 2 points for the after party

b2025lbencarnacion@pshs.edu.ph
b2025bgbsison@pshs.edu.ph
b2025hyramos@pshs.edu.ph
b2025rvbjanapin@pshs.edu.ph
b2025akgbermudo@pshs.edu.ph
b2025bcjcatli@pshs.edu.ph
b2025mmcbaniqued@pshs.edu.ph

Encarnacion, Lior

Sison, Bree Genzoe

Ramos, Hannah

Janapin, Raphael Vitto

Bermudo, Aryenne Kiersten

CATLI, Bob Christian

Baniqued, Marky Miguel


## Contest winner

Hannah Ramos - 3

## First to achieve a correct answer 

Vitto Janapin

## First to complete each problem 

Hannah Ramos - 4
 Vitto Janapin - 4
 
 
 ## Joining bonus
 
 Hannah Ramos
 Vitto Janapin