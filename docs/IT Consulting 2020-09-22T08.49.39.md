IT Consulting 2020-09-22T08.49.39
========================

I.T. Consulting Engagement Proposal 

Hi, I'm Roy Canseco, I am the Management Information Systems Head of Philippine Science High School in Diliman. I do active consulting for the UP Provident Fund Office for which I supervise the development of the Online Loans Application Module for UP employees nationwide. I also teach I.T. System Analysis and Computer Science in U.P. Diliman and Philippine Science. 

Previously, I managed the UP HRDO SQL migration project from a 25-year old legacy dos-based system into the modern web-based Ruby-on-Rails application they are using right now. Previously, I also co-founded an I.T. company that concentrated on Open Sourcing consulting, which I exited to join the academe. 

Currently, I am working on the development of an open source Online Learning Backup System for Philippine Science.  


I am applying to be your I.T. Consultant for the Telemedicine Project. 

As I.T. Consultant, I can provide the following services.

* Primarily author the project Terms of Reference
* Offer clarifications on technical issues and concerns
* Define fair expectations on I.T. deliverables
* Protect the client's technical interests
* Verify free and open-source software license effectiveness



I engage in either hourly or monthly consulting. Monthly consulting is based on the assumption of around an hour of I.T. work daily. Hourly work is rounded to the nearest hour.



Here are my standard rates as of 2020.

* For UP: 1k/ hr or 25k/ mo
* For Government: 1.5k/ hr or 35k/ mo
* For Cooperatives: 2k/ hr or 45k/ mo
* For Corporate: 3k/ hr or 60k/ mo


Thank you for your kind consideration.


Sincerely,

Roy Canseco




