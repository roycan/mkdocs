easy - google (p12015)
========================

Google is one of the most famous Internet search engines which hosts and develops a number of Internet-
based services and products. On its search engine website, an interesting button ‘I’m feeling lucky’
attracts our eyes. This feature could allow the user skip the search result page and goes directly to the
first ranked page. Amazing! It saves a lot of time.

![qownnotes-media-MGXvHw](media/qownnotes-media-MGXvHw.png)



The question is, when one types some keywords and presses ‘I’m feeling lucky’ button, which web
page will appear? Google does a lot and comes up with excellent approaches to deal with it. In this
simplified problem, let us just consider that Google assigns every web page an integer-valued relevance.
The most related page will be chosen. If there is a tie, all the pages with the highest relevance are
possible to be chosen.

Your task is simple, given 10 web pages and their relevance. Just pick out all the possible candidates
which will be served to the user when ‘I’m feeling lucky’.

## Input

The input contains multiple test cases. The number of test cases T is in the first line of the input file.
For each test case, there are 10 lines, describing the web page and the relevance. Each line contains
a character string without any blank characters denoting the URL of this web page and an integer
V <sub>i</sub> denoting the relevance of this web page. The length of the URL is between 1 and 100 inclusively.
(1 ≤ V <sub>i</sub> ≤ 100)

## Output 

For each test case, output several lines which are the URLs of the web pages which are possible to be
chosen. The order of the URLs is the same as the input. Please look at the sample output for further
information of output format.

## Sample Input
Put the following in an input textfile and rename it *google_in.txt* .

```
2
www.youtube.com 1
www.google.com 2
www.google.com.hk 3
www.alibaba.com 10
www.taobao.com 5
www.bad.com 10
www.good.com 7
www.fudan.edu.cn 8
www.university.edu.cn 9
acm.university.edu.cn 10
www.youtube.com 1
www.google.com 2
www.google.com.hk 3
www.alibaba.com 11
www.taobao.com 5
www.bad.com 10
www.good.com 7
www.fudan.edu.cn 8
acm.university.edu.cn 9
acm.university.edu.cn 10

```

## Sample Output
Output the following in the terminal
```
Case #1:
www.alibaba.com
www.bad.com
acm.university.edu.cn
Case #2:
www.alibaba.com
```

## Codepost Guide


Create a program to solve the problem and rename it to *google.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*
An input textfile named *google_in.txt* shall be used by codepost to auto check your submission.


sample compile command: 
```
    g++ -o google google.cpp
```
sample run command:
```
    ./google google_in.txt
```
corresponding output:
```
    Case #1:
    www.alibaba.com
    www.bad.com
    acm.university.edu.cn
    Case #2:
    www.alibaba.com
```

## Working Code Template

```
/***********************************************************************
 * 
 * easy - google (p12015)
 * 
 * find the most relevant websites
 * 
 sample compile command:
    g++ -o google google.cpp
sample run command:
    ./google google_in.txt
corresponding output:
    Case #1:
    www.alibaba.com
    www.bad.com
    acm.university.edu.cn
    Case #2:
    www.alibaba.com
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    ifstream file (input_array[1]);
    
    int cases;
    file >> cases;

    vector <pair <int, string> > v;
    string url;
    int relevance;

    // loop per case
    for (int i=0; i< cases; i++){

        v.clear();

        for (int j=0; j<10; j++){
            file >> url;
            file >> relevance;

            v.push_back( make_pair( relevance, url));
        }


        for( auto &x : v){
            cout << x.first << " ";
        }
        cout << endl;
        
    }
}
```

## Sir Roy's Solution

```
/***********************************************************************
 * 
 * easy - google (p12015)
 * 
 * find the most relevant websites
 * 
 sample compile command:
    g++ -o google google.cpp
sample run command:
    ./google google_in.txt
corresponding output:
    Case #1:
    www.alibaba.com
    www.bad.com
    acm.university.edu.cn
    Case #2:
    www.alibaba.com
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    ifstream file (input_array[1]);
    
    int cases;
    file >> cases;

    vector <pair <int, string> > v;
    string url;
    int relevance;

    // loop per case
    for (int i=0; i< cases; i++){

        v.clear();

        for (int j=0; j<10; j++){
            file >> url;
            file >> relevance;

            v.push_back( make_pair( relevance, url));
        }


        // for( auto &x : v){
        //     cout << x.first << " ";
        // }
        // cout << endl;


        sort(v.rbegin(), v.rend());

        int highest = v[0].first;

        cout<< "Case #" << i+1 << ":" << endl;

        for (auto &x : v){

            if (x.first == highest){

                cout << x.second << endl;
            }
        }

        
    }


}
```