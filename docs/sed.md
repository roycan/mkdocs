sed
========================

`#sed -e 's/oldserver.com/newserver.com/g' oldmysqldump.sql > newmysqldump.sql`

* -e :: script

`#sed -i.bak -e 's/oldserver.com/newserver.com/g' mysqldump.sql `

* -i.bak :: save the file changes in-place, but backup file first