## People


| Name | email
------|------
| Roy Canseco | rlcanseco@up.edu.ph |
Paulo Santiago | paulo64santaigo@gmail.com
Anthony Cornell Dacoco | amdacoco@up.edu.ph
Blaine Corwyn Aboy | bdaboy@up.edu.ph
Daisyree D. Viscaya | daisyreeviscaya@gmail.com
Dianne Pauline Urban | diannepauline.urban@gmail.com
Loren Anne Lacanilao | lorenannelacanilao@gmail.com




## Key Design Decisions

1. Backend language: PHP
2. Framework: laravel
3. HTML / CSS :  HTML 5 / Boostrap 4
4. Database : MySQL
5. Version Control : Gitlab
6. Testing :  (to follow)
7. Deployment : AWS


## Approved Design Documents

[Collated Screens](2018-collated_screens.md)

[Collated ERDs](up-ic-erd/collated_erd.html)

[Mobile Screens](2018-mobile_screens.md)
