---
layout: post
title: "End-User Software Engineering"	
remarks: "seminar notes"
categories: Research
tags: 
- PhD
- EUD
---


February 18 – 23 , 2007, Dagstuhl Seminar 07081


#### Organizers: 
* Margaret M. Burnett (Oregon State University, US)
* Gregor Engels (Universität Paderborn, DE)
* Brad A. Myers (Carnegie Mellon University, US)
* Gregg Rothermel (University of Nebraska – Lincoln, US)

<hr>

#### Quoted Content:
~~~~
The number of end users creating software is far larger than the number of professional programmers. These end users are using various languages and programming systems to create software in forms such as:
~~~~ 
* spreadsheets, 
* dynamic web applications, and 
* scientific simulations. 


This software needs to be sufficiently dependable, but _substantial evidence suggests that it is not_.

~~~~
Solving these problems involves not just software engineering issues, but also several challenges related to the users that the end user software engineering intends to benefit. End users have very different training and background, and face different motivations and work constraints, than professional programmers. They are not likely to know about such things as:
~~~~

 1. formal development processes, 
 1. system models, 
 2. language design characteristics, 
 1. quality control mechanisms, 
 5. or test adequacy criteria.

~~~~
These challenges require collaborations by teams of researchers from various computer science subfields, including specialists in:
~~~~
* end-user-programming (EUP) and end-user development (EUD), 
* researchers expert in software engineering methodologies and programming language design, 
* human-computer interaction experts focusing on end-user programming, and 
* empiricists who can evaluate emerging results and help understand fundamental issues in supporting end-user problem solving.


## Summary
In summary, here are my take-aways from the seminar

### Classification
* Sw-engineering
* Programming Languages
* Human-computer Interaction
* Empirical Studies

### Keywords
* End-user software engineering
* End-user programming



