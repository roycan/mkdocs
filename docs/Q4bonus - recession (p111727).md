Q4bonus - recession (p111727)
========================

Company XYZ have been badly hit by recession and is taking
a lot of cost cutting measures. Some of these measures include
giving up office space, going open source, reducing incentives,
cutting on luxuries and issuing pink slips.

They have got three (3) employees working in the accounts
department and are going to lay-off two (2) of them. After a
series of meetings, they have decided to dislodge the person
who gets the most salary and the one who gets the least. This
is usually the general trend during crisis like this.

You will be given the salaries of these 3 employees working
in the accounts department. You have to find out the salary
of the person who survives.

![qownnotes-media-Yraxxk](media/qownnotes-media-Yraxxk-2294516829.png)


## Input

Each case
consists of a line with 3 distinct positive integers. These 3 integers represent the daily salaries of the three
employees in Philippine pesos. All these integers will be in the range [1000, 10000].

## Output

For each case, output the salary of the person who survives.


| Sample input | Sample output  |
|---|---|
| 1000 2000 3000 | Php2000 |
| 3000 2500 1500 | Php2500 |
| 1500 1200 1800 | Php1500 |


## Codepost Guide

Create a program that solves the problem and name it *recession.cpp*. 
Upload the file to *codepost.io* 

Sample compile command: 
```
    g++ -o recession recession.cpp 
```
Sample run command:
```
    ./recession 1500 1200 1800
```
Corresponding output:
```
    Php1500
```

## Working Template Code
```
/*******************************************************
 * 
 * Q4bonus - recession (p111727)
 * 
 * Output the middle salary
 * 
Sample compile command:
    g++ -o recession recession.cpp 
Sample run command:
    ./recession 1500 1200 1800
Corresponding output:
    Php1500

 * 
 * ****************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{

    int a = atoi( input_array[1]);
    int b = atoi( input_array[2]);
    int c = atoi( input_array[3]);


    // change the following code to solve the problem

    cout << a << " " << b << " " << c << endl; 

}
```