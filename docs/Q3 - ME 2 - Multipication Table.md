Q3 - ME 2 - Multipication Table
========================
## Instructions

![Screenshot from 2021-02-11 10-28-34](media/Screenshot-from-2021-02-11-10-28-3806309786.png)

## Codepost guide

* Create a program to solve the problem and rename it *mult_table2.cpp*
* Upload the file to codepost
* Make sure you use commandline parameters like in the *Template Code*
* Do **not** use *cin* 

### Sample commands and output

Sample compile command: `g++ -o mult_table mult_table.cpp`

Sample run command: `./mult_table2 2`

Corresponding output: 

```
1    2
2    4
```

Sample run command: `./mult_table2 3`

Corresponding output: 

```
1    2    3
2    4    6
3    6    9
```

### Template Code

```
/*********************************************************************************
 * 
 * Q3 - ME 2 - Multipication Table
 * 
 * 
Sample compile command: `g++ -o mult_table mult_table.cpp`

Sample run command: `./mult_table2 2`

Corresponding output: 

1    2
2    4
 * 
 * ******************************************************************************/


#include <iostream>
using namespace std;

int main (int argc, char* argv[])
{
    int n = atoi(argv[1]);

    // change the code below

    cout << "1" << "\t" <<  n << "\t" << n * n << endl;

}
```

