# Installing Phalcon PHP in Ubuntu 18.04

## requirements

* database connectivity will require the `php_pdo` extension which can be found in the `php7.2-common` package

* MySQL/ MariaDB or Aurora databases will need `php_mysqlnd` extension , while PostgreSQL will need `php_pgsql` extension. Both extensions are in the `php7.2-pdo*` packages


### Hardware requirements

An AWS VM with 512 RAM and 1vCPU will be enough for simple sites such as blogs.

### software prerequisites

We assume that we want to use PHP 7+ (e.g. 7.2)

Step 1 will be to install the following prerequisite programs:

* curl
* gettext
* gd2 (php-gd)
* libpcre3-dev
* json (php-json)
* mbstring (php-mbstring)
* pdo_\* (php7.2-pdo*)
* fileinfo (php7.2-common)
* openssl

Note that depending on what you want to do, `pdo_*` , `openssl` and `mbstring` may be optional.


## Installation proper

1. Get a stable release script:  
```bash
 curl - https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash
```
2. Install Phalcon
```bash
sudo apt-get update &&
sudo apt-get install php7.2-phalcon
```
3. Install Phalcon Developer Tools for Linux
```bash
sudo apt install -y git
cd
git clone git://github.com/phalcon/phalcon-devtools.git
```
4. Enter the cloned directory and run
```bash
cd phalcon-devtools/
. ./phalcon.sh
```
5. Create a symlink to the phalcon script
```bash
sudo ln -s ~/phalcon-devtools/phalcon.php /usr/bin/phalcon
sudo chmod ugo+x /usr/bin/phalcon
```
6. Check for installation success using `phalcon commands`

```bash
$ phalcon commands

Phalcon DevTools (3.0.0)

Available commands:
  commands         (alias of: list, enumerate)
  controller       (alias of: create-controller)
  module           (alias of: create-module)
  model            (alias of: create-model)
  all-models       (alias of: create-all-models)
  project          (alias of: create-project)
  scaffold         (alias of: create-scaffold)
  migration        (alias of: create-migration)
  webtools         (alias of: create-webtools)
```

## Generating a Project Skeleton

```bash
$ cd
$ mkdir projects
$ cd projects
$ phalcon create-project store
```

## Using the Built-in Webserver (for Development)

```bash
$(which php) -S localhost:8000 -t public .htrouter.php
```

* `$(which php)` - will insert the absolute path to your PHP binary
* `-S localhost:8000` - invokes server mode with the provided host:port
* `-t public` - defines the servers root directory, necessary for php to route requests to assets like JS, CSS, and images in your public directory
* `.htrouter.php` - the entry point that will be evaluated for each request

** Go to *http://localhost:8000/* to check if everything is working.**
