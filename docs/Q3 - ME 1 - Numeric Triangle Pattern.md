Q3 - ME 1 - Numeric Triangle Pattern
========================

*Machine Exercise* 


## Instructions 
Based on the example given on the previous module, create a program that replicated
the triangle pattern shown. Try to look at the examples closely and think of an algorithm
that replicates the outputs. Let’s see if you still look at Triangle problems the same way
after this problem.

Input:
Integer value n.

Output:
A triangle based on the inputted value of n.

## Sample Input and Outputs:

sample input:  
3 

corresponding output:
``````
*
**
***
``````
---
sample input:
4 

corresponding output:
``````
*
**
***
****
``````
---
sample input:
7 

corresponding output:
``````
*
**
***
****
*****
******
*******
``````

## Codepost submission

Create a C++ program to solve the problem and rename it *triangle.cpp*  .
Upload this to Codepost.

sample compile command: *g++ -o triangle triangle.cpp*   
sample run command: *./triangle 3*  
corresponding output: 
``````
*
**
***
``````

## Template Code

```
/************************************************************
 * 
 * Q3 - ME 1 - Numeric Triangle Pattern
 * 
sample compile command: *g++ -o triangle triangle.cpp*   
sample run command: *./triangle 3*
corresponding output: 

*
**
***

 * 
 * *********************************************************/


#include <iostream>
using namespace std;


int main (int argc, char* argv[]) 
{

    int n = atoi(argv[1]);

    //edit the code below

    cout << "*" << endl;
    cout << "**" << endl;
 
}
```
