easy - results (p621)
========================

At a certain laboratory results of *secret research* are thoroughly encrypted. A result of a single experiment is stored as an information of its completion:
> ‘positive result’, ‘negative result’, ‘experiment failed’ or ‘experiment not completed’

The encrypted result constitutes a string of digits S, which may take one of the following forms:


|  result |encryption  |
|---|---|
| positive result | S= 1 or S = 4 or S = 78 |
| negative result | S= S35 |
| experiment failed |S= 9S4  |
| experiment not completed | S= 190S  |

(A sample result S35 means that if we add digits 35 from the right hand side to a digit sequence
then we shall get the digit sequence corresponding to a failed experiment)

![qownnotes-media-bkADtM](media/qownnotes-media-bkADtM-3593070431.png)






You are to write a program which decrypts given sequences of digits.

## Input 

A integer n stating the number of encrypted results and then consecutive n lines, each containing a
sequence of digits given as ASCII strings.


## Output

For each analysed sequence of digits the following lines should be sent to output (in separate lines):

| result | output |
|---|---|
| positive result | + |
| negative result | - |
| failed experiment | * |
| not completed experiment | ?  |


In case the analysed string does not determine the experiment result, a first match from the above
list should be outputted.

## Sample Input/ Output


| input  | output  |
|---|---|
| 78 | + |
| 7835 | - |
| 19078 | ?  |
| 944 | * |

## Codepost Guide

Create a program to solve the problem and rename it to *results.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o results results.cpp
```
sample run command:
```
    ./results 78
```
corresponding output:
```
    +
```
## Working Template Code
```
/*******************************************************
 * 
 * easy - results (p621)
 * 
 * 
 * INPUTS:
positive result
    S= 1 or S = 4 or S = 78
negative result
    S= S35
experiment failed
    S= 9S4
experiment not completed
    S= 190S
 * 
 * 
 * OUTPUTS:
positive result
    +
negative result
    -
failed experiment
    *
not completed experiment
    ?
 * 
 * 
sample compile command:
    g++ -o results results.cpp
sample run command:
    ./results 78
corresponding output:
    +
 * 
 * ****************************************************/

#include <bits/stdc++.h>
using namespace std;


int main (int input_count, char* input_array[])
{
    string S = input_array[1];


// change the following code to solve the problem    

    int n = S.length();

    if (S == "1" || S== "4" || S=="78"){
        
        cout << "+" << endl;
    }

}

```

## Sir Roy's Solution Code
```
/*******************************************************
 * 
 * easy - results (p621)
 * 
 * 
 * INPUTS:
positive result
    S= 1 or S = 4 or S = 78
negative result
    S= S35
experiment failed
    S= 9S4
experiment not completed
    S= 190S
 * 
 * 
 * OUTPUTS:
positive result
    +
negative result
    -
failed experiment
    *
not completed experiment
    ?
 * 
 * 
sample compile command:
    g++ -o results results.cpp
sample run command:
    ./results 78
corresponding output:
    +
 * 
 * ****************************************************/

#include <bits/stdc++.h>
using namespace std;


int main (int input_count, char* input_array[])
{
    string S = input_array[1];


// change the following code to solve the problem    

    int n = S.length();

    if (S == "1" || S== "4" || S=="78"){
        
        cout << "+" << endl;
    }
    
    else if ( S[n-2] == '3' && S[n-1]== '5'){
        
        cout << "-" << endl;
    }
    else if ( S[0]=='1' && S[1]=='9' && S[2]=='0'){

        cout << "?" << endl;
    }
    else if ( S[0] == '9' && S[n-1] == '4'){
       
        cout << "*" << endl;
    }


}


```