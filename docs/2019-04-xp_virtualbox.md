# Setting up Networking for WinXP on virtualbox

Windows XP may be necessary in getting some old system to work for the purposes of Migration.

One of the best ways is to use virtualbox. The following steps will allow a virtualized winxp installation to have internet.

## Setup a shared folder between host os and guest xp

![steps 1-6](./2019-04/xp-vb-1.png)

![steps 7-9](./2019-04/xp-vb-2.png)



## Download and install updated drivers

1. Download the following using the host os
    1. Intel Pro/1000 MT Desktop network drivers
    2. winxp service pack 3
    3. firefox 52.3.0esr.exe
2. Copy the installers into the winxp guest using the shared folder
3. Install the 3 installers
4. Reboot winxp
5. Check if everything worked using the installed Firefox
