easy - mario (p11764)
========================

Mario is in the final castle. He now needs to
jump over few walls and then enter the Koopa’s
Chamber where he has to defeat the monster in
order to save the princess. 

![qownnotes-media-srVErN](media/qownnotes-media-srVErN.png)


For this problem,
we are only concerned with the “jumping over
the wall” part. You will be given the heights
of N walls from left to right. Mario is currently
standing on the first wall. He has to jump to the
adjacent walls one after another until he reaches
the last one. That means, he will make (N − 1)
jumps. A *high jump* is one where Mario has to
jump to a taller wall, and similarly, a *low jump* is one where Mario has to jump to a shorter wall. Can
you find out the total number of *high jumps* and *low jumps* Mario has to make?


## Input

 Each case starts
with an integer N (0 < N < 50) that determines the number of walls. The next numbers give the height
of the N walls from left to right. Each height is a positive integer not exceeding 10.

## Output

Print out 2 integers, total high jumps and total low jumps,
respectively. Look at the sample for exact format.

## Sample Input / Output



|  Sample Input| Sample Output  |
|---|---|
| 8 1 4 2 2 3 5 3 4 | high jumps: 4, low jumps: 2 |
| 1 9 | high jumps: 0, low jumps: 0 |
| 5 1 2 3 4 5 | high jumps: 4, low jumps: 0 |

## Codepost Guide

Create a program to solve the problem and rename it to *mario.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o mario mario.cpp
```
sample run command:
```
    ./mario 5 1 2 3 4 5
```
corresponding output:
```
    high jumps: 4, low jumps: 0
```


## Working Template Code
```
/***********************************************************************
 * 
 * easy - mario (p11764)
 * 
 * count the number of mario's high jumps and low jumps given the terrain data
 * 
sample compile command:
    g++ -o mario mario.cpp
sample run command:
    ./mario 5 1 2 3 4 5
corresponding output:
    high jumps: 4, low jumps: 0
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
    int count = input_count -2;

    int high_jumps = 0;
    int low_jumps = 0;

 // change the following code to solve the problem
    
    cout << "high jumps: " << high_jumps <<", low jumps: " << low_jumps << endl;

}
```

## Sir Roy's Solution
```
/***********************************************************************
 * 
 * easy - mario (p11764)
 * 
 * count the number of mario's high jumps and low jumps given the terrain data
 * 
sample compile command:
    g++ -o mario mario.cpp
sample run command:
    ./mario 5 1 2 3 4 5
corresponding output:
    high jumps: 4, low jumps: 0
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
    int count = input_count -2;

    int high_jumps = 0;
    int low_jumps = 0;

 // change the following code to solve the problem
    
    for (int i=0; i<count-1; i++){

        int h1 = atoi( input_array[i+2]);
        int h2 = atoi( input_array[i+3]);
        if (h1 > h2) {
            
            low_jumps++;
        }
        else if (h1 < h2){

            high_jumps++;
        }
    }

    cout << "high jumps: " << high_jumps <<", low jumps: " << low_jumps << endl;


}
```