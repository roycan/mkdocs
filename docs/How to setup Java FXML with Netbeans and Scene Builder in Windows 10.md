How to setup Java FXML with Netbeans and Scene Builder in Windows 10
========================
*A guide for my students in Computer Science*


<hr>

## Uninstall old SceneBuilder, NetBeans and Java
* there might be conflicts with old installations, so do this step
* It's good to uninstall in reverse order
    * remove SceneBuilder first
    * then remove netbeans
    * then remove Java (all versions and updates)
* for SceneBuilder, go to "Start-> Apps and Features"
    * uninstall SceneBuilder
* for Apache Netbeans, you can just delete the Installation Folder (usually in Downloads )  
* for Java, go to "Start-> Apps and Features"
    * uninstall Java

![VirtualBox_MSEdge - Win10_20_03_2020_23_25_16](media/1373920327.png)

<hr>

## Download Oracle JDK 1.8 
* this has javafx automatically included to it.
* it's free
* you'll need to sign up, but other alternatives are way harder to do 
* JDK 1.8 is also know as JDK 8
* We can also use higher versions of java, but we'll need more complicated build tools like maven or gradle to work with JavaFX. We want Java 8 so we can stick to the default ANT build tool.

To install NetBeans, you have to download it first from the Oracle official website:

https://www.oracle.com/java/technologies/javase-jdk8-downloads.html

![qownnotes-media-W12311](media/179859311.png)

1. Scroll down until you find the right download for your machine (probably the 64-bit version for windows)
2. Download and install 

<hr>

## Install NetBeans
To install NetBeans, you have to download it first from the NetBeans official website:

https://netbeans.apache.org/download/index.html

1. Click on the green “Download” button under the ***Apache NetBeans LTS*** line
2. You can also learn more about NetBeans by clicking the blue “Features” button next to the “Download” button.

    ![qownnotes-media-P12311](media/1512443135.png)
    
3. You can also download other previous versions as well but I recommend the LTS version.
4. This will take you to a page titled “Downloading Apache NetBeans (incubating)”.
5. From this page, click on the ***“Binaries”*** link. You can click the “Source” link and download that if you want to compile it yourself but downloading the “Binaries” is faster and easier.
 
     ![qownnotes-media-T12311](media/724898476.png)

6. Once clicked, this will redirect you to the apache page where a link will be generated for you.
7. Click the link and it will start downloading.
8. Unzip 
9. After extraction, go in the extracted directory and look for the “bin” folder.
10. Go in the “bin” folder and you will see two “Application” files; “netbeans.exe” and “netbeans64.exe”. You probably should run the 64-bit version
9. If you are greeted with this view once it opens, then NetBeans has been successfully installed. You are ready to start writing java programs.


![qownnotes-media-x12311](media/1972724940.png)


<hr>

## Install the JavaFX Scene Builder


Now that NetBeans has been successfully installed and is working properly, we can move to the next step; install JavaFX Scene Builder. 

First, we have to download Scene Builder from its official website:

https://gluonhq.com/products/scene-builder/#download

![qownnotes-media-t12311](media/1902213677.png)


1. On this page, scroll down until you see the download options for your version of Java (Java 8).
2. Look for the right Scene Builder version for your operating system and click the green “Download” button.
3. This will redirect you to a “Thanks” page and start downloading.
4. Once downloaded, open the installer.
5. In the installer, accept the terms and conditions and click install.
6. Once installed, click finish.
To check whether the installation was successful or not you can open “SceneBuilder” and will be greeted with the following view if the installation was successful:

![qownnotes-media-G12311](media/2053071218.png)


<hr>

## Integrate JavaFX Scene Builder and NetBeans

Once Scene Builder and NetBeans have been installed, it is time to move to the final step; integrate both the programs.

1.  Open NetBeans, 
2.  click on “File” and then “New project.” in the dropdown menu. A new window will open.
3. Click on “JavaFX” from the “Categories” tab and “Java FXML Application” from  “Projects” tab.
        ![qownnotes-media-F12311](media/594897691.png)
4. Click “Next”.
5. Install any modules, if it prompts to download and activate any modules. In the next window give the project a name and click “Finish”.
     ![qownnotes-media-h12311](media/1483339083.png)
6.  Once opened, go to the “Projects” tab and click on your project.
7. Then click on “Source Packages”, 
8. click on your project’s package, 
9.  double-click on the .fxml file 
10.  This will open the file in Scene Builder for you to work on it.
    ![qownnotes-media-z12311](media/1508710837.png)
11. Run your new fxml project and see the default "Hello World" program. 

<hr>

## Next Steps
You have successfully integrated Scene Builder and NetBeans to work together. Once you are done with making the GUI,  add functionality and logic to it by editing the “Controller.Java” file, which is directly below the .fxml file. Enjoy making GUI for your programs in an intuitive and fun way using JavaFX Scene Builder.