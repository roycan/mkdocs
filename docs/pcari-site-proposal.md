PCARI Project Website

Roy Canseco  with  Romeo Ramos Jr. and James Karl Quiño

December 2018

---

# PCARI Website Project

## Purpose

We propose to create a blog-styled technical project website for the purpose of sharing research results and data with the general public according to the guidelines of PCARI. 

## Plan

Use Wordpress as the content management system (CMS). Host the site in Wordpress.com using a subdomain of the client's choosing.  
 
## Duties

1. To setup the project website locally 
2. To seed the website with initial project content
2. To host the project website on the cloud
3. To train users in using, updating and managing the website

## Deliverables

1. source code for the project website 
2. SQL export of the website database
3. Web access to the host server account 
4. Website administration account
5. List of plugins and software versions


## Sample pages

![Sample Landing Page](img/pcari01.png)

![Sample Inner Page](img/pcari02.png)