---
title:   CoopTV Assessment 
author:  Roy Canseco
date: 		14 Jan 2019
---

## CoopTV Android Beta-Testing Report

### General Findings
* The Developer is delivering within specifications

---

* The ACDI-content team has been timely in providing sample video material 

---

* The Video on Demand service came out within expectations

---

* *The performance of the Linear Video or **TV** function is poor and generally unacceptable*

### Good Points
* The `CoopTV Team` is powerfully setup and is poised to execute well on management direction
* The `Developer` has shown adequate skill for the delivery of in-contract requirements 

### Points for Improvement/ Compensation
* The average present Internet speed is inadequate for prolonged **TV** use. The ordinary 1-2 second delay every so often messes the timing of the **TV**.
	* Proposed Solution :  **Video Pre-Caching**

---

* The initial content (~25 videos) would need some (~25 videos) more for a good *Video Service Launch*
	* Team Proposed Solution : Ask `CDA` and *Coop Federations* for seed content.
	* Have `ACDI` as the official *CoopTV Content Storage Partner*, so that Coops can go straight and send videos their videos to be aired in CoopTV.  

## Projections 

### Learnings 
* The current project is on-track to provide an acceptable Video on Demand experience but a poor *TV* experience at the moment.

---

* `CoopTV` , as an organization has talented and passionate individuals, but will find it very hard to manage video collection, monitoring and storage in the foreseeable time. 

## Recommendations 

### Alternatives for ACDI


4. ACDI simply continues => ACDI Video-on-Demand App 


---

2. ACDI solves the LinearTV problem only => ACDI-TV

---

3. ACDI solves the Content Management problem only => Coop Video-on-Demand App

---

1. ACDI solves the LinearTV problem and the Content Management problem => CoopTV lives as envisioned


## Thank you for listening