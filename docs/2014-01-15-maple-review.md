---
layout: post
title: " Quickly relearning Maple "
date: 2014-01-01	
remarks: "steps"
categories: Skills
tags: 
- Maple
- "symbolic math"
---

## maple review
quick reminders for relearning maple commands.

1. do simple arithmetic (pemdas)
2. visit   help    for a quick run through
3.  ctrl space  to  produce special char  (e.g. pi, integral sign...)
4. right button to get out of  ^ 
5. a video that helps :   http://www.maplesoft.com/support/training/videos/quickstart/MapleQuickStart.aspx
6. use ctrl-L  to refer back to previous results
7. print and go over this:    http://www.phys.unsw.edu.au/2nd_and_3rd_syllabi/mapleprimer.pdf

