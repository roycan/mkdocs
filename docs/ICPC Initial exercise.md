ICPC Initial exercise 
========================

Produce working code that is *as concise as possible* for the following tasks:

1. Using C++, read in a double
(e.g. 1.4732, 15.324547327, etc.)
echo it, but with a minimum field width of 7 and 3 digits after the decimal point
(e.g. ss1.473 (where ‘s’ denotes a space), s15.325, etc.)

2. Given an integer *n (n ≤ 15)*, print π to *n* digits after the decimal point (rounded).
(e.g. for *n = 2*, print 3.14; for *n = 4*, print 3.1416; for n = 5, prints 3.14159.)

3. Given a date, determine the day of the week (Monday, . . . , Sunday) on that day.
(e.g. 9 August 2010—the launch date of the first edition of this book—is a Monday.)

4. Given n random integers, print the distinct (unique) integers in sorted order.

5. Given the distinct and valid birthdates of n people as triples (DD, MM, YYYY), order
them first by ascending birth months (MM), then by ascending birth dates (DD), and
finally by ascending age.

6. Given a list of *sorted* integers L of size up to 1M items, determine whether a value v
exists in L with no more than 20 comparisons.

7. Generate all possible permutations of {‘A’, ‘B’, ‘C’, . . . , ‘J’}, the first N = 10 letters
in the alphabet.

8. Generate all possible subsets of {0, 1, 2, . . . , N-1}, for N = 20.

9. Given a string that represents a base X number, convert it to an equivalent string in
base Y, 2 ≤ X, Y ≤ 36. For example: “FF” in base X = 16 (hexadecimal) is “255” in
base Y 1 = 10 (decimal) and “11111111” in base Y 2 = 2 (binary). 

10. Let’s define a ‘special word’ as a lowercase alphabet followed by two consecutive digits.
Given a string, replace all ‘special words’ of length 3 with 3 stars “***”, e.g.
S = “line: a70 and z72 will be replaced, aa24 and a872 will not”
should be transformed into:
S = “line: *** and *** will be replaced, aa24 and a872 will not”.

11. Given a *valid* mathematical expression involving ‘+’, ‘-’, ‘*’, ‘/’, ‘(’, and ‘)’ in a single
line, evaluate that expression. (e.g. a rather complicated but valid expression 3 + (8 -
7.5) * 10 / 5 - (2 + 5 * 7) should produce -33.0 when evaluated with standard
operator precedence.)



