Saving ArrayLists to a file
========================

 write an ArrayList<String> into a text file
 
 ````java
 import java.io.FileWriter;
...
FileWriter writer = new FileWriter("output.txt"); 
for(String str: arr) {
  writer.write(str + System.lineSeparator());
}
writer.close(); 
````

*  define your array as:     `arrayList<String> arr = new ArrayList<String>();`
*  `arr` is the name of the variable that holds reference to array
* `output.txt`  will be in current working directory (i.e. directory from which you started your java program) 



