Project Proposal Template
========================

Since we are in the last submission for the project proposal, we're going to standardize the content via this template. Feel free to choose your fonts, colors and text alignment.


## Title Page

\<Project Name\> Proposal

by \<Put your names here>

Submitted to 

Roy Vincent L. Canseco

In partial fulfillment of the requirements for CS 4

this \<enter date here>


## Table of contents (TOC)

* no need to do this if you have 6 or less pages
* optional for 7-10 pages
* mandatory for 11 or more pages

## First page

1. Put a picture/ screenshot from your project
1. Introduction OR Context OR Background  
2. Big Idea that addresses the concern in the Intro
3. List of proposed features and key advantages
4. List of expected constraints and limitations

## Details pages

1. Show CRC cards
2. Narrate how the program would work while mentioning the different classes, responsibilities and collaborations in the CRD cards
3. Show the UML Diagrams
4. Explain the relationships found in the UML Diagrams
5. Give sample scenarios as examples of  how your proposed software will work
6. Show sample User Interfaces or pictures relating to your scenarios

 
## User Interface pages

Include various user interfaces (UI) from your project. Explain each UI with at least 2 sentences.

## Milestones and Next steps page

1. List the activities done for the project at this point. (Attach the results of the activities as Appendix)
2. List at least the next 5 activities that are to be done for the project.

## Bonus

Since you are already doing research, if you can include Gantt charts, network diagrams, task matrices and any other project management tools that you were taught, you'll get good bonuses for each of those. 

## Appendix 

Place any useful information or material here. I will check this for possible bonuses as well.  
