easy - robot (p12503)
========================


You have a robot standing on the origin of x axis. The robot will be given some instructions. Your
task is to predict its position after executing all the instructions.

![qownnotes-media-nLumSo](media/qownnotes-media-nLumSo.png)



*  LEFT: move one unit left (decrease p by 1, where p is the position of the robot before moving)
*  RIGHT: move one unit right (increase p by 1)
*  SAME AS i: perform the same action as in the i-th instruction. It is guaranteed that i is a positive
integer not greater than the number of instructions before this.

## Input 

The first line contains the number of test cases T (T ≤ 100). Each test case begins with an integer n
(1 ≤ n ≤ 100), the number of instructions. Each of the following n lines contains an instruction.

## Output

For each test case, print the final position of the robot. Note that after processing each test case, the
robot should be reset to the origin.

## Sample Input
Put the following in an input textfile and rename it *robot_in.txt* .

```
2
3
LEFT
RIGHT
SAME AS 2
5
LEFT
SAME AS 1
SAME AS 2
SAME AS 1
SAME AS 4
```

## Sample Output
Output the following in the terminal
```
1
-5
```

## Codepost Guide


Create a program to solve the problem and rename it to *robot.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*
An input textfile named *robot_in.txt* shall be used by codepost to auto check your submission.


sample compile command: 
```
    g++ -o robot robot.cpp
```
sample run command:
```
    ./robot robot_in.txt
```
corresponding output:
```
    1
    -5
```

## Working Code Template

```
/***********************************************************************
 * 
 * easy - robot (p12503)
 * 
 * predict robot's position after executing all the instructions
 * 
 sample compile command:
    g++ -o robot robot.cpp
sample run command:
    ./robot robot_in.txt
corresponding output:
    1
    -5
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    ifstream file (input_array[1]);

    int cases;
    file >> cases;

    cout << cases << endl;

    file.close();
}
```

## Sir Roy's Solution

```
/***********************************************************************
 * 
 * easy - robot (p12503)
 * 
 * predict robot's position after executing all the instructions
 * 
 sample compile command:
    g++ -o robot robot.cpp
sample run command:
    ./robot robot_in.txt
corresponding output:
    1
    -5
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    ifstream file (input_array[1]);

    int cases;
    file >> cases;

    for (int i=0; i< cases; i++){

        int instructions;
        file >> instructions;Output the song, formatted as above.
        vector <int> v; 

        int position=0;

        for (int j=0; j< instructions; j++){

            string instruction;
            file >> instruction;
            // cout << instruction << " " ;

            if (instruction == "LEFT"){
                position -= 1; 
                v.push_back(-1);
                // cout << position << endl;
            }
            else if (instruction == "RIGHT"){
                position += 1;
                v.push_back(1);
                // cout << position << endl;
            }
            else if (instruction == "SAME"){
                string temp; 
                file >> temp;  // get AS 
                int repeat;
                file >> repeat;

                position += v[repeat -1]; 
                v.push_back( v[repeat -1] ) ;

                // cout << position << " " << repeat << endl;
            }
        }
        cout << position << endl;
        
        // for (auto &x : v) {
        //     cout << x << " " ;
        // }
        // cout << endl;

    }
    file.close();
}
```