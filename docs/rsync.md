rsync
========================


 `rsync -av -e ssh SOURCE/ USERNAME@NEW_SERVER.COM:/PATH/TO/DESTINATION/`
 
 * -a :: archive mode (preserve file permissions)
 * -v :: verbose
 * -e ssh :: use ssh 
 * -z :: compress

 * --progress :: show progress 
 * --del :: deletes extra files so both folders are the same


    
## Reasons to Consider rsync Over cp or SCP

1. Creates incremental data backups.
2. Copies from source to destination only the data which is different between the two locations.
3.  Each file is checksummed on transfer using MD5.
4.  rsync’s --del option deletes files located at the destination which are no longer at the source.
5. rsync can resume failed transfers (as long as they were started with rsync).
6. rsync can be run as a daemon.
7. rsync can compress data with the -z option, so no need to pipe to an archiving utility.



## Special uses

### Excluding/ ignoring files

 
 `rsync -avz --exclude={'*.txt','dir3','dir4'} sourcedir/ destinationdir/`
 
 
 ### Pull multiple files with rsync
 
 `rsync -avz calomel@somemachine:/remote_machine/{file1,file2,file3} /local/disk/`
 
 ### Rsync when remote files or directories contain spaces
 
 `rsync -av calomel@somemachine:'/I\ Hate\ Spaces/some\ File.avi' /current_dir/`
