Q3 - bonus - ceasar_cipher
========================

From Wikipedia:

In cryptography, a Caesar cipher, also known as Caesar’s cipher, the shift cipher, Caesar’s code or Caesar shift, is one of the simplest and most widely known encryption techniques. It is a type of substitution cipher in which each letter in the plaintext is replaced by a letter some fixed number of positions down the alphabet. For example, with a left shift of 3, D would be replaced by A, E would become B, and so on. The method is named after Julius Caesar, who used it in his private correspondence.

There’s a video about it 

https://youtu.be/36xNpbosfTY



## Instructions

Implement a caesar cipher that takes in a string and the shift factor and then outputs the modified string:

Name the file  ceasar_cipher.cpp 


## Codepost guidance

sample compile command:   g++ -o ceasar_cipher ceasar_cipher.cpp
sample run command:   ./caesar_cipher "What a string!" 5
sample output:    Bmfy f xywnsl!

## Quick Tips

Don't forget to maintain the punctuation
Don’t forget to wrap from z to a.
Don’t forget to keep the same case.


## Template code

```
/***************************************************************

Implement a caesar cipher that takes in a string and the shift factor and then outputs the modified string

sample run command:   ./caesar_cipher "What a string!" 5
sample output:    Bmfy f xywnsl!

Don't forget to maintain the punctuation
Don’t forget to wrap from z to a.
Don’t forget to keep the same case.

I. Explore:
   1. get the string
   2. get n
   3. shift the string n characters down 

II. Improve:
	1. wrap from z to a.
	2. maintain the punctuation
	3. keep the same case

III. Test with other strings


****************************************************************/

#include <iostream>
using namespace std;

int main (int argc, char* argv[])
{
	string s = argv[1];		// s is the input string
	int n = atoi(argv[2]);	// n is the number of characters to shift


	// change the code below to solve the problem

	cout << s << endl;  
	cout << n << endl;  


}
```
