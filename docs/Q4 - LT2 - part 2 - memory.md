Q4 - LT2 - part 2 - memory
========================

A game called *Memory* is played using 50 cards. Each
card has one of the letters from A to Y (ASCII 65 to 89)
printed on the face, so that each letter appears on
exactly two cards. The cards are shuffled into some
random order and dealt face down on the table.

![qownnotes-media-rtnOxu](media/qownnotes-media-rtnOxu.png)


Jack plays the game by turning two cards face up so the letters are visible. For each of
the 25 letters Jack gets a candy from his mother the first time he sees both copies of
the letter on the two face up cards. For example, the first time Jack turns over both
cards that contain the letter M, he gets a candy. Regardless of whether the letters were
equal or not, Jack then turns both cards face down again. The process is repeated until
Jack receives 25 candies – one for each letter.

You are to implement a function **play** that plays the game. Your implementation
should call the function **faceup(C)** which is implemented by the grader. **C** is a
number between 1 and 50 denoting a particular card you wish to be turned face up.
The card must not currently be face up. **faceup(C)** returns the character that is printed
on the card C.

After every second call to faceup, the grader automatically turns both cards face down
again.

Your function **play** may only terminate once Jack has received all 25 candies. It is
allowed to make calls to **faceup(C)** even after the moment when Jack gets the last
candy.

## Instructions

1. Randomly assign letters A to Y in an array or vector.
2. Implement function **play** and function **faceup(C)** 
3. Function **play** continuously calls function **faceup(C)** until Jack receives all 25 candies
3. Produce output according to the following example.

## Example

The following is one possible sequence of calls your function play could make, with
explanations.



| Call | cout Output  |
|---|---|
| faceup(1) |1- Card 1 contains B. |
| faceup(7) |2- Card 7 contains X. The letters are not equal. |
|  | The grader automatically turns cards 1 and 7 face down.  |
| faceup(7) |3- Card 7 contains X. |
| faceup(15) |4- Card 15 contains O. The letters are not equal. |
|  | The grader automatically turns cards 7 and 15 face down. |
| faceup(50) |5- Card 50 contains X.  |
| faceup(7) | 6- Card 7 contains X. Jack gets candy #1. |
|  | The grader automatically turns cards 50 and 7 face down. |
| faceup(7) |7- Card 7 contains X.  |
| faceup(50) |8- Card 50 contains X. Equal letters, but Jack gets no candy. |
|  | The grader automatically turns cards 7 and 50 face down. |
| faceup(2) |9- Card 2 contains B. |
|  | *...* |
|  | *(some function calls were omitted)* |
|  | *...*  |
|faceup(1)  |99- Card 1 contains B. |
| faceup(2)|100- Card 2 contains B. Jack gets candy #25. |


## Points - total of 50

[40 points] - Implement any strategy that obeys the rules of the game


[10 points] - Implement a strategy that finishes any possible game with at most 100 calls
to faceup(C).

