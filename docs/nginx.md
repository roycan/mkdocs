nginx
========================

`sudo apt install nginx`

Confirm that it's running:

```
$ sudo systemctl status nginx 
$ sudo systemctl is-enabled nginx
```

Check your IP Address:

`$ ip addr show`

go to http://<ip_addess>

![qownnotes-media-RCjeeG](media/qownnotes-media-RCjeeG.png)