# This Project Planning from SDLC

![project planning](./2019-03/se_proj_planning.png)


## Cost Considerations

* COTS for general problems is cheaper
* Custom dev for specific problems is cheaper
* In-house for small dev projects is cheaper
* Out-source for large dev projects is cheaper
* Clear is Cheap.
* Ambiguity is Costly


## Manage by

1. Scope (in and out)
2. Definitions of Done
3. Milestones
4. Prioritization


## Prioritize by

1. Value
2. Cost
3. Time
4. Scope
