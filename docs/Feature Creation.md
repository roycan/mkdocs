Feature Creation
========================

This document talks about how we add features to the UPPFI Laravel Portal.


## Considerations

* Business Case
* Feasiblity
* Priority
* How will we test
* Github

## Example

For the case of the Online Loan Application Feature:


 * The business case is that an Online Loans Application is critical for the New Normal Scenario
 *  The IT of Provident has determined it's feasibility beforehand
 *  The recent ECQ made this feature a priority
 *  We shall test it via subject matter experts (SMEs)
     *  IT (Roy and Regi)
     *  Operations (May)
     *  Management (James)

## Current Feature Workflow

1. Regi is assigned a laptop and he creates the feature using laravel code using the laptop. 
1. He then uploads this code into the Staging Server. 
1. The staging server uses an old copy of the database. 
1. I.T. gives the SME testers credentials they can use to test the new software.
1. The SME testers use the staging server to try out things.
1. The SME's and the IT group iterate until they're satisfied.
1. The code is placed into the Production Server.
1. A github branch is made for the feature.



## Target Workflow

1. I.T. is assigned a laptop and he creates the feature using laravel code using the laptop. 
1. He then uploads this code into the Staging Server. 
2. A feature branch is created in Github with the staging.
1. The staging server uses an old copy of the database. 
1. I.T. gives the SME testers credentials they can use to test the new software.
1. The SME testers use the staging server to try out things.
1. I.T. conducts a code review on the diffs of the files used for the new feature to check that the minimum amount of impact to other code is achieved.
1. The SME's and the IT group iterate until they're satisfied.
1. The code is placed into the Production Server.
2. The Github feature branch is updated.
3. The feature branch is pulled into the master
4. The master is tagged with a version number/ text
