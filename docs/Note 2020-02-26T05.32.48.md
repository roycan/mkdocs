Note 2020-02-26T05.32.48
========================

## Gratitude

1. I am so happy and grateful for the wonderful trip to Dentist Teng with my wife. 
2. I am so happy and grateful that I get to go to Mass more and am more attentive to it. 
3. I am so happy and grateful for the cleaning of my ears in a manner that's fun and relaxing and loving.
4. I am so happy and grateful I have wonderful healthy meals.
5. I am so happy and grateful that I can try out setups in windows 10 now.
6. I am so happy and grateful now that there's a reactos project that I see be helping me in porting legacy software later on.   
7. I am so happy and grateful now that I'm happily transitioning to a plant-based diet. 

8. I am so happy and grateful now that I live in a wonderful, healthy and strong body.
9. I am so happy and grateful now that I have set programs for Pisay CS and BA ISAD. 
10. I am so happy and grateful that I have a bouncing jolly baby. 


## PSHS

There's going to be practical tests today!  hehehehe. Let's make them.
