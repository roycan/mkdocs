Punlaan Tablet  2020-06-16T21.57.24
========================

These are questions from sponsors

1. Is an external keyboard compatible with a tablet? Because we were thinking of asking also for additional funds for an external keyboard...do you think it would be useful or not really needed?
2. The sponsor is requiring us for safety guidelines on child protection for the use of the tablets (ex blocking of unwanted sites)... .what could these guideliines be kaya? We have no idea what could you suggest we reply to the sponsor?


## External keyboards for tablets

This can be useful. It makes a lot of sense in my head, but I haven't seen it in practice.

 If you want to try it out, I suggest getting one that comes with a case for the tablet and connects by wire. This is normally cheaper. It physically protects the tablet from most falls and is less likely to be lost than bluetooth wireless keyboards.

![](https://my-live-01.slatic.net/p/982e142d241a7c5f4e29c2b19dfa3bb6.jpg)

![](https://my-live-01.slatic.net/p/49030fec3471c78687ca4cb410f77b61.jpg)

## Safety guidelines to protect children


Some tablets, for example those sold by Vibal or Felta, already come with child-filters. 
I don't think those are always appropriate for Senior High School students though. 

What would be more appropriate would be to setup restrictions to what apps they can download, watch on you-tube and search in their web browser.  It would also be good to install a Phone / Tablet Finder App so that you know where each tablet is being used as long at it has internet.

Setting this up will require some manpower, because it's done on a per tablet basis. 

You can install software that does most of it automatically with artificial intelligence. For example MMGuardian app. However this costs around 35 dollars per year per tablet. 

![](https://wp_media.storage.googleapis.com/cat1.png)