grading plan 2021-04-05T11.43.55
========================

1. check all manual checkables in KHub (learning logs, google assignments, group projects)
2. put in the quarter participation bonus
3. put in the quarter project bonus
4. check all the codepost submissions 
5. download and import the codepost grades into KHub
6. check the *emergency submission bin* and adjust KHub grades
7. export the KHub grades 
8. announce that students check their khub grade books
9. mail merge grades to the students
10. 