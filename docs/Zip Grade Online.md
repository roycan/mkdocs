Zip Grade Online
========================


"Verified" submission is only available for ZipGrade account that has defined Student records and have assigned students to Classes.  

How to use 'Verified' online submission of quizzes by students

* A student portal is enabled as an account setting on the 'My Account' screen
* Students are assigned Student Access Codes, which are visible, exportable, and resettable on the 'Students' tab
* Teachers distribute the individual Student Access Code to each student along with their existing Student ID number
* The student logs into the Student Portal (https://ZipGrade.com/student/) with their Student ID number and * Student Access Code to view quizzes assigned to them
* The teacher, on the individual quiz screen on the website, assigns each quiz to each class with options including:
    * Time/Date Window when student may begin quiz
    * Time Limit: How long is the student allowed for this quiz once they begin
* Multiple Answer Key PDFs: The students are randomly assigned a question PDF from those uploaded by the teacher
* Show Score and Questions On Submit: On submission, show the student the graded paper 


CAVEATS:

* Student and class records are required
* Because there is a submission limit, if a student accidentally submits an answer sheet they will not be able to re-take a test unless you remove that graded paper.