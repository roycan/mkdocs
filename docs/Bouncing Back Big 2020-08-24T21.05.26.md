Bouncing Back Big 2020-08-24T21.05.26
========================

    3 Step Formula

Great things are waiting for you around the corner...
In fact, they have arrived.

You took action, you're here.

How to feel good right now. Whatever is happening right now. 

Focus on what you want. Focus on the positive, the wonderful things arriving to your life. 
They are on the way because you are open to it. 

Imagine your greatest life because it's possible. 

You can achieve all you want in life and go through stuff.

Bad stuff just bounce of you as you love life. 

Just be open to receive.



## Intention 

What I want.

## Declaration

All of this will glorify the God who loves me. 


## 3 Step Formula 

If you keep doing what you're doing, you'll get what you're getting

### Think Different

Manage your perception

* your *thinking* is what is causing your suffering
* every time you dwell on the negative is like staring at a close door. step away from the closed door. look at the open doors.
* the words *I am* are the most powerful words you'll ever speak

### Reprogram your Beliefs

### Project Positive Points



* Choose your friends carefully
* Let go of the past
* Forgive yourself and others
* 