Q4 removals - Notes
========================

First off, I am deeply sorry Ma'am Gladys for the anxiety and sadness my late submission of grades caused you. If it's some small comfort, know that the students get their scores via KHub Grad Book for the most part automatically and for a lot of it, in real-time. 

I provide here some details on how I took care of the grades this school year as well as up-to-date notes on the students of interest you've mentioned. I haven't figured out how to resolve past quarter NG's, that process is something I'll be seeking and asking about.

## Grading details 

I employ auto-grading MCQ quizzes, auto-testing for programming submissions, and manual checking for coding projects (alternative assessments). I've also set KHub to compute all their tentative and final grades per quarter so that the student can see every submission he/ she did in past quarters and see how that factored in the computation of his/ her grade. If the student drills down, they see that each Project has wholistic and analytic rubrics, and each coding exercise has comments for every deduction in score. This led to >20% of students getting 1.0 in their final grade.

But with the catch-up plans, the ebase grades uploading, and the seeking of unit head approvals for grades, I wasn't able to put up the grades in a timely manner. For that, again, I'm sorry.


## Students of interest

### 8- Camia
#### LIM, JAMAICA CEZ L. (CLC)  -Q2-Q4

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
5|	2|	3|	2.5|	2.75	|5	|4


* she has emailed me today confirming that she is doing the CS 2 Removals Plan
* her adviser has emailed that she is reminding Jamaica via messages and calls
* as of today, her final grade is 4.0
* all other grades have been submitted to the google sheet

#### RAMOS, KELVIN ANGEL F. (IRC) - Q2-Q4
* All OK. Grade submitted to the google sheet

#### SALUDES, MATTHEW C. (CLC)Q3- Q4
* All OK. Grade submitted to the google sheet

#### ALETA, IVAN KRISTOFF A. (Main Campus)-Q2-Q4

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
2	| 2.25	| 2.25	| 4	| 3|	5| 	4

* has not replied to my email on the CS 2 Removals Plan
* no new submissions

#### TAMBALQUE, QUENSO A. (Main Campus)-Q2-Q4

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
4	| 2.5| 	3	| 4	| 4	| 5	| 5

* has not replied to my email on the CS 2 Removals Plan
* no new submissions

### 8- Ilang-ilang
#### DANGUILAN, ALBERTA KRISTEA C.  (CARC) -Q2-Q4
* All OK. Grade submitted to the google sheet

#### DOMINGO, NED RAMESES (CARC)- Q2-Q4
* All OK. Grade submitted to the google sheet

#### TANG, ALBERT KENNETH B. (CARC)-Q2-Q4
* All OK. Grade submitted to the google sheet

#### AGCAOILI, CARL FRANCIS D.(Main Campus)

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
4	| 5	| 5	| 5	| 5|	5| 	5

* has an approved extension
* has not replied to my email on the CS 2 Removals Plan

#### CHI-YOUNG, DANTE JR. B.(Main Campus) Q2-Q4

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
2.75	|2.75|	2.75|	5	|4	|5	|5

* has an approved extension
* has not replied to my email on the CS 2 Removals Plan

#### ESCOBIDO, MICHAEL EARL J. (Main Campus) Q2-Q4

Q1 - Final Grade |	Q2 - tentative |	Q2 - Final Grade | 	Q3 - tentative |	Q3 - Final Grade |	Q4 - Tentative | 	CS 2 - FINAL GRADE |
:-----:|:----:|:----:|:----:|:----:|:-----:|:----:
5	| 2.5|	3	|2.75|	2.75|	4.0| 3.0(after removals)

* Had just PASSED removals
* I don't know how to remove/ update his previous NG's 
* I plan to ask Sir Paolo Santos for help