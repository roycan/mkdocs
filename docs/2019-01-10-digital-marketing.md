# Digitial marketing


Right now, on worldwide average, **Facebook** is your number 1 priority in terms of conversion. Next is **Mobile** (Android or iOS) ads. This is followed by **organic marketing**, focusing on the *human experience and relationship*. These three mix in and with themselves.

Traditional Digital Marketing in based on **SEO** (Search Engine Optimization) **PPC** (Pay per Click) techniques. These are normally what the management will be expecting in your Digital Marketing Campaigns. This is normally done in conjunction with **Google Analytics**.

Other marketing platforms of growing import are:
* youtube
* Twitter
* Instagram
* Pinterest
* Linkedin

Once you have emails and people interested and going to the site, it would be time to build the presence in the respective social media platforms to take care of repeat business, referrals and organic growth.

A generic way to reach-out and take care of clients and followers is through email. I have a blog post on that specifics of doing that [here](2019-01-09-email-marketing.md).


## Popular Platforms

* [Facebook](https://www.facebook.com/business?ref=sem_smb&utm_source=GOOGLE&utm_medium=fbsmbsem&utm_campaign=G_S_Beta_BusinessAds_Brand_PH_EN&kenid=_k_Cj0KCQiA1NbhBRCBARIsAKOTmUtyccFhpDqIIkjnnoQWoSfNbdEff1_9Oum-mzhVXf1C0NYPdIzYqvMaAgMdEALw_wcB_k_&gclid=Cj0KCQiA1NbhBRCBARIsAKOTmUtyccFhpDqIIkjnnoQWoSfNbdEff1_9Oum-mzhVXf1C0NYPdIzYqvMaAgMdEALw_wcB)

* [Android](https://smallbusiness.chron.com/advertise-android-12816.html)


## Classic platforms

* [Google Ads](https://ads.google.com/intl/en_ph/start/?subid=ph-en-ha-g-aw-c-skmv_1!o2~Cj0KCQiA1NbhBRCBARIsAKOTmUtHJShp0Xiq2JqA0KJsdtkE3W0_Npk_1PmUYmiEidfyuQK-xDvax58aAmLLEALw_wcB~61952089110~kwd-20062491978~1502507178~286902264611&gclid=Cj0KCQiA1NbhBRCBARIsAKOTmUtHJShp0Xiq2JqA0KJsdtkE3W0_Npk_1PmUYmiEidfyuQK-xDvax58aAmLLEALw_wcB)

* [Usage Guide](https://support.google.com/google-ads/answer/6146252?hl=en)

* [Expectations Guide](https://neilpatel.com/what-is-google-adwords/ )

* [Step by Step blog](https://www.oberlo.com/blog/google-adwords-tutorial)

* [*Google Analytics*](https://analytics.google.com/analytics/web/#/report-home/a42819599w72689038p75047817)

## Other Important platforms

* [Twitter](https://ads.twitter.com/login)
* [Instagram](https://business.instagram.com/advertising/)
* [youtube](https://www.youtube.com/intl/en-GB/yt/advertise/)
* [Pinterest](https://business.pinterest.com/en/why-pinterest-ads-work)
* [Linkedin](https://business.linkedin.com/marketing-solutions/cx/17/06/advertise-on-linkedin?src=go-pa&trk=sem_lms_gaw&veh=Google_Search_APAC_PH_Brand-LMS_Beta_DR_English_260515272179__%2Blinkedin%20%2Badvertising_c__kwd-298089255836_1338272046&gclid=Cj0KCQiA1NbhBRCBARIsAKOTmUsPijVL54fSXFCDwj2ahJBkstuBDo6mwe1Ov2SrKCcOvSU6icXHMrEaArTTEALw_wcB)
