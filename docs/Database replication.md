Database replication
========================

```
Careful note: I had set this up and the triggers kept putting 
my single-core VPS to 100% utilization
May this solution is best for multi-core or dedicated VPS's
```



So, you have postgre installed locallly and a in a VPS, both running Ubuntu 18.04. Here's what to do and what not to do in order to have a database replication between those two. 

````
        > this should work with mysql databases too. haven't tried though.
        > VPS like linode or digital ocean or AWS.  
````

We'll go roughly through the following steps
1. create a database and create tables locally
2. create a database of the same name and tables in the VPS
3. install rubyrep locally 
4. create a rubyrep.conf file 
5. scan the databases for differences
6. sync the databases manually 
7. start the replication 
8. test by inserting records in the local and viewing them in the VPS
9. test by inserting records in the VPS and viewing them locally


````
        > it's important that the tables are already set up in both databases
        > it's possible to map local tables to vps tables manually if they have different names
        > The rubyrep.conf file can be named anything, e.g. myrubyrep.conf
````

## Create a database locally 

I'm using Ubuntu 18.04 and am practicing working with psql on the commandline. You can use whatever method is applicable with you. 

```bash
$ sudo -u postgres psql 

=# CREATE DATABASE ls_burgers
=# \c ls_burgers
=# CREATE TABLE orders (
    id integer,
    customer_name text,
    burger text,
    side text,
    drink text
);

=# INSERT INTO orders VALUES (1, 'Todd Perez', 'LS Burger', 'Fries', 'Lemonade');
INSERT INTO orders VALUES (2, 'Florence Jordan', 'LS Cheeseburger', 'Fries', 'Chocolate Shake');
INSERT INTO orders VALUES (3, 'Robin Barnes', 'LS Burger', 'Onion Rings', 'Vanilla Shake');
INSERT INTO orders VALUES (4, 'Joyce Silva', 'LS Double Deluxe Burger', 'Fries', 'Chocolate Shake');
INSERT INTO orders VALUES (5, 'Joyce Silva', 'LS Chicken Burger', 'Onion Rings', 'Cola');

=# SELECT * FROM orders;
\q
```

That should create your sample database with your sample table including some sample rows. It should also show (with the SELECT statment) that everything works. 

## Create a database on the VPS

This assumes that postgres is happily installed in your VPS as well.   So we connect to the VPS using ssh and we'll just repeat what we did locally. 

```bash
$ sudo -u postgres psql 

=# CREATE DATABASE ls_burgers
=# \c ls_burgers
=# CREATE TABLE orders (
    id integer,
    customer_name text,
    burger text,
    side text,
    drink text
);

=# INSERT INTO orders VALUES (1, 'Todd Perez', 'LS Burger', 'Fries', 'Lemonade');
INSERT INTO orders VALUES (2, 'Florence Jordan', 'LS Cheeseburger', 'Fries', 'Chocolate Shake');
INSERT INTO orders VALUES (3, 'Robin Barnes', 'LS Burger', 'Onion Rings', 'Vanilla Shake');
INSERT INTO orders VALUES (4, 'Joyce Silva', 'LS Double Deluxe Burger', 'Fries', 'Chocolate Shake');
INSERT INTO orders VALUES (5, 'Joyce Silva', 'LS Chicken Burger', 'Onion Rings', 'Cola');

=# SELECT * FROM orders;
\q
```
Then we setup the server so that our local computer can access server postgres db.
Here are the steps:

0.  enable the postgresql port 
            
         `sudo ufw enable 5432/tcp`
1.  put a `password` on the default postgres account  

        ```psql
        =# ALTER USER postgres PASSWORD 'myPassword';
        ALTER ROLE
        ```

2.  go to /etc/postgresql/10/main
3.  open postgresql.conf for editing
4.  set to  `listen_addresses '*'`   to  allow postgresql to listen to any ip address
5.  open pg_hba.conf
6.  set to   `host     allow    allow    0.0.0.0/0    md5`


```
        > i think just enabling 5432 would work as well
        > we can leave the password empty, but if we do, we can't use *md5* in pg_hba.conf
        > instead *md5*, we'll have to use *trust*
        > this makes things even less secure
        > the folder in etc will depend on your postgres version. mine is 10
        > for editing, i just use:  sudo nano postgresql.conf
```

## Install rubyrep gem locally and run

Go to [rubyrep.org](http://www.rubyrep.org/) 

There are 2 options: standard ruby or jruby.
I will of course go with *standard ruby*  since ruby is installed in my local ubuntu and my ubuntu vps. 
It doesn't say so, you should create a Gemfile.

1. create a new directory `$ mkdir pg_replication`
2. `cd pg_replication`
3. `nano Gemfile` 

```ruby
gem 'pg', '~> 0.20.0'
gem 'rubyrep'
```
 
 4. `bundle install`
 5.  `nano myrubyrep.conf`

```ruby
RR::Initializer::run do |config|
  config.left = {
    :adapter  => 'postgresql', # or 'mysql'
    :database => 'SCOTT',
    :username => 'scott',
    :password => 'tiger',
    :host     => '172.16.1.1'
  }

  config.right = {
    :adapter  => 'postgresql',
    :database => 'SCOTT',
    :username => 'scott',
    :password => 'tiger',
    :host     => '172.16.1.2'
  }

  config.include_tables /./

end
```
6. `bundle exec rubyrep scan -c myrubyrep.conf`
7. `bundle exec rubyrep sync -c myrubyrep.conf`
8. `bundle exec rubyrep replicate -c myrubyrep.conf`

```
        > you can name your directory anything you want
        > the Gemfile keeps you the the verions of pg gem that is compatible with rubyrep
        > without the Gemfile, you might get depreciated command errors
        > afaik, version 0.20.x is the latest that would work
        > just `bundle` will probably work too
        > you don't have to use *nano*, vim or any other text editor should work
        > to make sure the Gemfile is enforced we add *bundle exec* to the rubyrep commands
        > after running *scan*, you should see the number of unsynced rows per matched table in the two databases
        > after running *sync*, you should see the number or rows synced
        > you can run *scan* again, this should give you 0 rows unsynced
        > once you run replicate, it doesn't stop until the process is manually killed or the local computer is shut. 
```


## References
* http://www.rubyrep.org/
* https://github.com/launchschool/sql_course_data/blob/master/sql-and-relational-databases/introduction-to-the-course/read-the-launch-school-sql-book/ls_burger.sql
* https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
* https://www.digitalocean.com/community/questions/remote-access-to-postgresql-with-pgadmin
* https://digitaloceancode.com/remote-access-to-a-postgresql-database-digitalocean/