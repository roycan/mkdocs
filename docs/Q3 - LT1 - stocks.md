Q3 - LT1 - stocks
========================


Let's practice working with arrays.
You’ll need to rely on some of your basic knowledge of conditionals and flow control too.
You shouldn’t need to do anything you haven’t seen before.

![qownnotes-media-OuExjs](media/qownnotes-media-OuExjs-606596194.png)



## Instructions

Implement a stock picker that takes in an array of stock prices, one for each hypothetical day. With stocks, you first buy low and then sell high. The program should return a pair of days representing the best day to buy and the best day to sell. Days start at 0.


## Sample input/ output

The first number is the input count

input:`9 17,3,6,9,15,8,6,1,10`

output: `1,4 with a profit of Php15 - Php3 = Php12`

## Codepost guidance

Solve the problem and change the filename into *stocks.cpp*
The first number gives the count of the inputs.
All the succeeding numbers are input stock prices on their corresponding dates starting with Day 0.

Sample compile commands :
`g++ -o stocks stocks.cpp`

Sample run command: 
`.\stocks 9 17 3 6 9 15 8 6 1 10`

Corresponding output: 
`Day 1 buy, Day 4 sell, with a profit of Php15 - Php3 = Php12`

### Note 

* make sure to follow the input and output format strictly for codepost.
* do not use **cin** in codepost, use commandline parameters

## Tips 
* You need to buy before you can sell
* Pay attention to edge cases like when the lowest day is the last day or the highest day is the first day.


## Template code

```
/****************************************************************
 * 
 * Implement a stock picker that takes in an array of stock prices, one for each hypothetical day. 
 * With stocks, you first buy low and then sell high. 
 * The program should return a pair of days representing the best day to buy and the best day to sell. 
 * Days start at 0.
 * 
 * Sample compile commands: g++ -o stocks stocks.cpp

Sample run command: .\stocks 9 17 3 6 9 15 8 6 1 10

Corresponding output: Day 1 buy, Day 4 sell, with a profit of Php15 - Php3 = Php12


 * You need to buy before you can sell
Pay attention to edge cases like when the lowest day is the last day or the highest day is the first day.
 * 
 * 1. Get the input count
 * 2. Get the inputs into an array
 * 3. 
 * 4.
 * 
 ****************************************************************/


#include <iostream>
using namespace std;


int main (int argc, char* argv[])
{

	// get the count from the commandline arguments
	int count = atoi(argv[1]);
	

	// get the inputs and put them in the array
	float arr[count];

	for (int i=0; i< count; i++){

		arr[i] = atof(argv[i+2]);
	}


	// change the following code to solve the problem

	cout << count << endl;

	for (int i=0; i<count; i++){
		cout<< arr[i] << " ";

	}
	cout << endl;
}
```


## Credits

* Idea adapted The Odin Project
* Picture from Katrina.Tuliao - https://www.tradergroup.org
CC BY 2.0
File:Philippine-stock-market-board.jpg
Created: 30 July 2008
 