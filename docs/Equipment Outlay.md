Equipment Outlay
========================




Azlek Owen Tan <aobtan@pshs.edu.ph>
Wed, Mar 11, 2020, 2:50 PM
to Carlo, Edge, Edge, Edge, me

Good afternoon po,

Eto po yung tentative list of ICT equipments for 2020. Pakicheck nalang po yung mga priority this year kasi nagexceed na po tayo ng budget amounting to Php7,697,000.00. Kailangan po natin magbawas para isama nalang sa list of equipment for savings sa last quarter or next year budget. Pa-advise nalng po ng changes.
Thank you.


![qownnotes-media-FaWMMj](media/qownnotes-media-FaWMMj.png)


--

AZLEK OWEN B. TAN
Administrative Officer V
Procurement Unit
Philippine Science High School - Main Campus
Telefax: 7500-1448
