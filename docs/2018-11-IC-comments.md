<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c57 c62">COMMENTS SECTION</span>
=============================================

<span class="c0"></span>

<span class="c0">This section contains the collated comments from the IC
personnel who reviewed the website screens. These were gathered during a
series of walkthroughs and user acceptance tests conducted at the
Commission. Note that major comments were immediately implemented, and
are reflected in the screens shown in the previous section. Other
comments will be revisited as the system is developed. </span>

<span class="c0"> </span>

<span class="c54">FINANCIAL EXAMINATIONS GROUP</span>
-----------------------------------------------------

1.  ### <span class="c8">Brokers</span>

<span class="c5">Company side:</span>

-   <span class="c5">Change the input format for the schedules and
    summary; align the design with the format of the output</span>
-   <span class="c5">Create an attachment column on companies' side for
    supporting documents/uploading of excel</span>

<span class="c5"></span>

<span class="c5">                Examiner’s side:</span>

-   <span class="c5">Create an organizational structure for quarterly
    submissions. See Annual</span>
-   <span class="c5">Remarks column on summary</span>

<span class="c5"></span>

1.  ### <span class="c8"> HMO</span>

### <span class="c18">Company Side:</span>

-   <span class="c5">Question: What if the company wasn’t able to submit
    last quarter?</span>

<span class="c5">Answer: It will be noted in the status how many years
or quarters the company is late.</span>

### <span class="c18">        Examiner Side:</span>

-   <span class="c5">See compliance with Net worth, RBC, and ATR</span>
-   <span class="c5">Instantly show all good companies</span>
-   <span class="c5">Fix data tables</span>
-   <span class="c5">Add total assets, total liabilities, net worth,
    share capital, adjustments, accounted and not accounted in the view
    action</span>

1.  ### <span class="c8">Life/MBA/Trust</span>

<span class="c1">Company Side:</span>

<span class="c1">Issues on composite companies</span>

-   <span class="c1">2 Annual Statement submissions</span>
-   <span class="c1"> 2 Quarterly Submissions</span>
-   <span class="c1">Add another non-life database for composite
    companies</span>
-   <span class="c1">Checklist for MBA</span>

<span class="c1">Status Terms:</span>

-   <span class="c1">In process</span>
-   <span class="c1"> For payment and Pick - up</span>

<span class="c1">Quarterly Submission Columns (Summary)</span>

-   <span class="c1">Net worth, RBC2, etc</span>
-   <span class="c1">Real-time generation of report for
    Examinations</span>

1.  ### <span class="c8"> Non-Life</span>

<span class="c5">Company Side</span>
------------------------------------

-   <span class="c5">Annual Statement Checklist (to be discussed
    further)</span>
-   <span class="c5">In the view history:  
             The company should be able to see the file they have
    submitted (add a link for a preview (only) of submission)</span>
-   <span class="c5">Status for certificate request as well as the
    payment amount</span>
-   <span class="c5">Status Terms:  
            In process  
            For Payment and Pick - Up</span>

<span class="c5">Examiner Side</span>
-------------------------------------

-   <span class="c5">Certification Request  
            Display number of companies who submitted as of the deadline
    (annual and quarterly)  
            Pick up for Certificate Request (to be discussed further)
    </span>
-   <span class="c5">Status Terms  
            In process  
            For Payment and Pick - Up</span>

<!-- -->

-   <span class="c5">Pick up details / Remarks / Claim Stub / Link to
    Cashier for Order of Payment (for discussion)</span>

<!-- -->

-   <span class="c5">Annual Statement Submission  
            Columns:</span>

<!-- -->

-   <span class="c5">Company Name, Assets, Liabilities, Net Worth</span>
-   <span class="c5">Real-time generation of the report (changes in net
    worth, etc)</span>

<span class="c5"></span>

<span class="c5"></span>

1.  ### <span class="c8">Pre-Need Division</span>

<span class="c0"></span>

<span class="c5">Company side:</span>

-   <span class="c5">Quarterly: Trust fund has maximum of three separate
    Balance Sheet and Income Statement for the plans: Life/Memorial,
    Pension, Education</span>
-   <span class="c5">Per line item, there will be a working balance
    sheet</span>
-   <span class="c5">Collections submission: Remove the Planholders’
    Outstanding Balance Table</span>
-   <span class="c5">Add feature for viewing of outstanding balance per
    unique number of plan</span>

<span class="c5"></span>

<span class="c5">                Examiner side:</span>

-   <span class="c5">Create an organizational structure for the
    submissions.</span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c21">TECHNICAL SERVICES GROUP</span>
-------------------------------------------------

1.  ### <span class="c8">Statistics and Research Division</span>

### <span class="c18">Company Side:</span>

-   <span class="c5">No Comment</span>

### <span class="c18">        Examiner Side:</span>

<span class="c5">General Comments:</span>

-   <span class="c5">Annual Statements and Quarterly Statements from FEG
    are not yet ok, Annual and Quarterly Statements of FEG side should
    be first completed before they comment on it.</span>
-   <span class="c5">Industry Summary should also be shown (convert the
    Card List into tables with columns)</span>

<span class="c5">Specific Comments:</span>

<span class="c5">Selected Financial Statement Process</span>

<span class="c5">Similar for all:</span>

-   <span class="c5">Total Assets</span>
-   <span class="c5">Total Liabilities</span>
-   <span class="c5">Total Networth/ Fund Balance</span>
-   <span class="c5">Total Investments</span>
-   <span class="c5">Net Income</span>
-   <span class="c5">Premiums Earned</span>

<span class="c5">Specific to Life</span>

<span class="c5">Business Done</span>

-   <span class="c5">Number of Policies</span>
-   <span class="c5">Number of Certificates</span>
-   <span class="c5">Number of Insured Lives</span>
-   <span class="c5">Number of Sum Assured</span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5">Premiums By Type</span>

-   <span class="c5">Total Premiums and Considerations New Business:
    First Year</span>
-   <span class="c5">Total Premiums and Considerations New Business:
    Single</span>
-   <span class="c5">Total Premiums and Considerations Renewal</span>
-   <span class="c5">Total Premiums and Considerations Direct
    Business</span>
-   <span class="c5">Total Reinsurance Premiums Assumed</span>
-   <span class="c5">Total Reinsurance Premiums Ceded</span>
-   <span class="c5">Total Premiums and Considerations</span>

<span class="c5">Specific to Non-Life</span>

<span class="c5">Business Done</span>

-   <span class="c5">Number of Policies per Business Line</span>
-   <span class="c5">Number of Insured Lives per Business Line</span>
-   <span class="c5">Premiums Earned per Business Line</span>
-   <span class="c5">Losses Incurred per Business Line</span>

<span class="c5">Specific to MBA</span>

<span class="c5">Business Done</span>

<span class="c5">Basic Fund:</span>

-   <span class="c5">Number of Certificates, Members and
    Dependents</span>
-   <span class="c5">Total Amount of Insurance: Members and
    Dependents</span>
-   <span class="c5">Optional Fund (Micro Products and Other Than Micro
    Products are separated):</span>
-   <span class="c5">Number of Policies (Individual, Insurance Policies
    and Certificates)</span>
-   <span class="c5">Number of Amount of Insurance</span>

<span class="c5">Basic Member Benefits:</span>

-   <span class="c5">Total Number of Claims</span>
-   <span class="c5">Total Amounts Paid</span>

<span class="c5">Optional Insurance:</span>

-   <span class="c5">Total Number of Claims</span>
-   <span class="c5">Total Amounts Paid</span>

<span class="c5">Agency Hired Migrant Workers:</span>

-   <span class="c5">To be emailed</span>

<span class="c5"></span>

<span class="c5"></span>

1.  ### <span class="c8">Rating Division</span>

### <span class="c18">Company Side:</span>

<span class="c5">CTPL</span>

-   <span class="c5">Monthly Submissions Dapat</span>
-   <span class="c5">COCAF and OICP are the only groups who submit to
    this</span>
-   <span class="c5">It depends in DOTR if the companies will submit
    individually or the COCAF and OICP only</span>
-   <span class="c5">For discussion with the Deputy Commissioner but for
    now, retain current design</span>

<span class="c5">Quarterly - Total Loss:</span>

-   <span class="c5">A company can't submit a vehicle in subrogation if
    it is declared in nonrestorable. can't also submit if the vehicle is
    not in restorable</span>

<span class="c5">Quarterly - FST:</span>

-   <span class="c5">Row input per OR and place of payment</span>
-   <span class="c5">Include schedule (computation for the all the
    OR)</span>

### <span class="c18">        Examiner Side:</span>

<span class="c5">General Comments</span>

-   <span class="c5">Fix Column names and data shown</span>

<span class="c5">Specific Comments</span>

<span class="c5">PPAI</span>

-   <span class="c5">Grand Total Vehicle Insured</span>
-   <span class="c5">Grand Total Basic Premium</span>
-   <span class="c5">Grand Total Incidents</span>
-   <span class="c5">Grand Total Count for Death, Medical and
    Disability</span>

<span class="c5">Offsite - Total Loss</span>

-   <span class="c5">Current Submission \# of Restorable/ Unrestorable/
    Subrogation</span>
-   <span class="c5">Running Total of \# of Restorable/ Unrestorable/
    Subrogation</span>
-   <span class="c5">Want to have: screen for all submissions of
    company</span>

<span class="c5">Offsite - Adjuster</span>

-   <span class="c5">Current Submission \# of Reported Pending/
    Finished</span>
-   <span class="c5">Running Total of \# of Reported Pending/
    Finished</span>
-   <span class="c5">Want to have: screen for all submissions of
    company</span>

<span class="c5">Offsite - FST</span>

-   <span class="c5">Agency place where OR is paid</span>

### <span class="c8"></span>

1.  ### <span class="c8">Investment Division</span>

### <span class="c18">Company Side:</span>

-   <span class="c5">Include Covering Letter</span>
-   <span class="c5">Letter Request to cover how much he/she wants to
    invest, others</span>
-   <span class="c5">Some entities such as &lt;Financial Institutions,
    Banks&gt; also request to investment</span>
-   <span class="c5">Paulo’s Suggestion: Edit submission requirement and
    attach additional instructions for companies who will submit a
    request to investment</span>
-   <span class="c5">SPUCRI screens, data flow diagram, and database
    design</span>
-   <span class="c5">Link of submitted files in the history for
    downloading</span>
-   <span class="c5">View days delayed</span>

<span class="c5">Status Terms </span>

-   <span class="c5">Processing</span>
-   <span class="c5">Ready for Payment</span>
-   <span class="c5">For Releasing</span>
-   <span class="c5">Completed</span>

### <span class="c18">        Examiner Side:</span>

-   <span class="c5">Filter submission by the examiner</span>
-   <span class="c5">Assignment Roles</span>
-   <span class="c5">Letter Reply to be print and sign by the
    commissioner the send back to the company</span>
-   <span class="c5">Generate Tracking Number</span>
-   <span class="c5">Approval of Investment: Additional Column and
    remove the deadline</span>

<span class="c5">Status Terms</span>

-   <span class="c5">Processing</span>
-   <span class="c5">Ready for Payment</span>
-   <span class="c5">For Releasing</span>
-   <span class="c5">Specific bond, company, etc then total</span>

<span class="c5"></span>

<span class="c5"></span>

1.  ### <span class="c8">Actuarial Division</span>

### <span class="c18">Company Side:</span>

-   <span class="c5">Non-expeditious upload files include supporting
    submission per product instead of separate supporting submissions
    card</span>
-   <span class="c5">Request: For examiners to be able to edit/change
    uploadable files</span>
-   <span class="c5">Unify Status and FIO Status: New | Pending | FIO |
    Approval</span>
-   <span class="c5">Request: Add Status Column to Company Submissions
    where examiner can add/comment whatever supporting documents the
    companies need to submit</span>
-   <span class="c5">Request: Upon error, display contact
    person/email/number</span>

### <span class="c18">        Examiner Side:</span>

-   <span class="c5">Add Column "company type"</span>
-   <span class="c5">Add: Status of Submission</span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

<span class="c5"></span>

1.  ### <span class="c8">Reinsurance Division</span>

<span class="c5">Company Side:        </span>

<span class="c1">For Approval of Facultative Placements Abroad form,
create functionality in which company can add rows for the:  
</span>

-   <span class="c1">name of reinsurer/broker</span>
-   <span class="c1"> resident agent</span>
-   <span class="c1">list of securities of Broker</span>

<span class="c1"></span>

<span class="c1">                Examiner Side:</span>

-   <span class="c1">Create organizational structure for the list of
    outputs. (i.e. can be sorted by date submitted or by name of
    company)</span>

<span class="c0"></span>

<span class="c0"></span>

<span class="c21">LEGAL SERVICES GROUP</span>
---------------------------------------------

1.  ### <span class="c8">Regulation, Enforcement, and Prosecution Division (REPD)</span>

### <span class="c18">Staff Side:</span>

-   <span class="c5">Change the type of form and type of product to a
    drop-down</span>

<span class="c5"></span>

1.  ### <span class="c8">Public Assistance and Mediation Division (PAMD)</span>

### <span class="c18">Staff Side:</span>

-   <span class="c5">Change drop-down to a table with searching function
    (for company list)</span>
-   <span class="c5">Edit output form columns</span>
-   <span class="c5">Change Industry type to Company Type</span>
-   <span class="c5">Remarks</span>

<span class="c5">Status type: Terminated and Settled</span>

<span class="c5">1. If settled</span>

-   <span class="c5">Status, Date of Settlement, and amount</span>

<span class="c5">2. If terminated</span>

-   <span class="c5">Status and Date Terminated</span>

<!-- -->

-   <span class="c5">Policy number / Contract Number</span>

<span class="c5"></span>

1.  ### <span class="c8">Licensing Division</span>

<span class="c5">The company profile could be edited, updated and can
add additional details (when permitted to do so)</span>

<span class="c5">If a company will edit their profile, they will put the
edited data but should be checked by the staff of licensing for
approval</span>

<span class="c5">Historical data (for clarification) but not included in
this phase</span>

<span class="c5">Company type</span>

<span class="c5">Add license status  
- active  
- inactive  
- with pending application  
- under conservatorship  
- under receivership  
- under liquidation  
- others (add input type)  
- add remarks in status  
- add history when and who edited the status</span>

<span class="c5">when company link is clicked in the table, it will
redirect to a page that shows the company profile</span>

<span class="c0"></span>

1.  ### <span class="c8">Conservatorship, Receivership, and Liquidation Division (CRL)</span>

<span class="c59">No comments</span>

1.  ### <span class="c8">Claims Adjudication Division (CAD)</span>

<span class="c5">Input</span>
-----------------------------

-   <span class="c5">If the type is chosen, add input type:  
     - Date Due (D) - date  
     - Date of Hearing (H) - date and time</span>
-   <span class="c5">If the type drop-down is clicked, the status will
    be shown.</span>
-   <span class="c5">Initial Status  
     - Summons Issued</span>

<span class="c5">Output</span>
------------------------------

<span class="c5">New Tab  
- Summary  
         Add column action (Update Case)  
- Calendar of Hearings (Date of Hearing only)  
          Date and Time  
          Case Number  
          Type  
          Complainant  
          Respondent  
          Hearing officer  
          Stenographer  
          Status  
          Remarks  
- Calendar of Due (Date Due only)  
          Case Number  
           Type  
           Complainant  
           Respondent  
           Stenographer  
           Hearing Officer  
           Date  
           Days Pending</span>

<span class="c5">Dismissed and Decided are not included in the D or
H</span>

<span class="c0"></span>

1.  ### <span class="c8">Suretyship Section</span>

<span class="c51">No comments</span>

<span class="c0"></span>

1.  <span>Microinsurance</span>
    ---------------------------

<span class="c51">        No comments</span>

<span class="c0"></span>
