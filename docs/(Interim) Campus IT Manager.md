(Interim) Campus IT Manager
========================


* requesting name to be Interim Campus IT Manager
    * reason is because it's ideally a full-time position for a campus as big as main
    * reason is because it's ideally done by someone with more training/ certifications


## Software for the lab
* NetBeans
* IntelliJ Community Edition
* Code Blocks

## Scope
* requesting scope to include presence to meetings directly impacting campus IT 
* to include liason duties to convey management needs and MIS constraints for working out solutions
* to include documentation of single page processes for important or repeated activities
* to do consultative management in light of the interim nature of the requested appointment 


## Support for MIS/ IT Unit
* requesting support for basic IT certification for all IT/ MIS personnel (internationally recognized, online training and certification)
* requesting a PD be organized for faculty/and admin staff regarding Basic IT Troubleshooting once a year
    * slated for the 1st sem
    * reason is to allow interested faculty to help out in maintaining our IT infrastructure 
* requesting a PD be organized for faculty regarding Maximizing IT Services once a year 
    * slated for the 2nd sem
    * reason is to boost awareness regarding IT services available to the faculty
* setup an official group chat for IT friends where people can post IT concerns and updates
* support for advanced training as recommended by IQA OFI's or as projected to proactively answer IT-related risks

## Support for the (Interim) IT Manager
* requesting 9 units
* requesting to be released from IQA auditing duties
* requesting to be maintained as BAC-TWG for IT-related matters
* requesting support for conducting an IT audit
* requesting support for determining and maintaining an inventory 
* may I request to be able to consult people in UP Diliman every so often to pull in best practices?

## Clarifications
* where in the org chart does a Campus IT Manager sit?
    * who shall I answer to?
    * who answers to me?
    * what shall I be accountable/ responsible for?
    * are there any specific deliverables at this time? i'll deliver standard IT documentary deliverables eitherway. :) 
    * are there specific people to consult or inform?
* can i request a table and chair in the MIS room?
* can i request for a small petty cash fund for urgent IT consumables
* is it possible to have another person for the MIS for the first purpose of IT audit and inventory. secretarial skills would work.  