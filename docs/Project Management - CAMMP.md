Project Management - CAMMP
=====================

## Customizable Adaptive Methodology for Management of Projects


* I Project Overview
    1. Intro
    2. Basis
    3. General Process
    4. Structure
    
* II Project Pre-Launch Stage
    1. Introduction
    2. Idea Statement
        * Project description
        * Project justification
        * Strategic alignment        
    3. Stage Gate 1
        * Decision
        * Explanation of decision
    4. Feasibility study
        * What is the Project
        * Feasibility study outline
            * Man
            * Machine
            * Materials
            * Method
            * Money
            * Market
            * Mess
            * Considerations 
            * Risks
            * Evaluations and acceptance
            * Conclusion and recommendation
    5. Stage Gate 2
    6. Project Authorization Document (PAD)
        * Project description and justification
        * Location and duration
        * Cost and management
        * Assumptions and risks
        * Constraints and stakeholders
        * Success
    7. Stage Lessons Learned
    8. Closure

* III Project Launch Stage
    1. Intro
    2. Basic Requirements Document (BRD)
        * description and characteristics
        * expectations and requirements
        * alternatives evaluations 
        * stakeholders' input
        * deliverables and initial WBS
        * project boundaries
        * success factors
        * acceptance criteria
        * sustainability requirements
        * best practices
        * assumptions and constraints 
        * project manager comments
    3. Stage Gate 3
    4. Project Management Plan
    5. Stage Gate 4
    6. Stage Lessons Learned
    7. Closure

* IV Project Definition Stage
    1. Intro
    2. Project Detailed Plan
        * Scope of Work and WBS
        * Quality, Safety, Health and Environment
        * Time estimates and Schedule
        * Cost Estimate
        * Human Resources
        * Stakeholders
        * Communications 
        * Risk Assessment and Management
            * 3-point scale matrix for impact x probability
        * Procurement
        * Sustainability Requirements
        * Other Considerations
        * Project Manager Comments
    3. Stage Gate 5
    4. Stage Lessons Learned
    5. Closure

* Project Implementation Stage
    1. Intro
    2. Implementation Summary
    3. Stage Gate 6
    4. Stage Lessons Learned

* Project Operation Readiness Stage
* Project Close Stage
    1. Intro
    2. Lessons Learned
    3. Performance Reconciliation
    4. Project Closure Comments
    5. Project Success
    6. Future Follow-up
    7. Organizational Records pdate
    8. Project Manager Comments
    9. Stage Gate 8

