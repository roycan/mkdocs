easy - excursion (p11559)
========================


As you didn’t show up to the yearly general meeting of the Nordic
Club of Pin Collectors, you were unanimously elected to organize
this years excursion to Pin City. You are free to choose from a
number of weekends this autumn, and have to find a suitable hotel
to stay at, preferably as cheap as possible.

![qownnotes-media-OyQyWc](media/qownnotes-media-OyQyWc-3675787435.png)


You have some constraints: The total cost of the trip must be
within budget, of course. All participants must stay at the same
hotel, to avoid last years catastrophe, where some members got lost
in the city, never being seen again.


## Input

The input file contains several test cases, each of them as described
below.

The first line of input consists of four integers: 1 ≤ N ≤ 200, the number of participants, 1 ≤ B ≤
500000, the budget, 1 ≤ H ≤ 18, the number of hotels to consider, and 1 ≤ W ≤ 13, the number
of weeks you can choose between. Then follow two lines for each of the H hotels. The first gives
1 ≤ p ≤ 10000, the price for one person staying the weekend at the hotel. The second contains W
integers, 0 ≤ a ≤ 1000, giving the number of available beds for each weekend at the hotel.

## Output

For each test case, write to the output the minimum cost of the stay for your group, or “stay home”
if nothing can be found within the budget, on a line by itself.

## Sample Input
Put the following in an input textfile and rename it *excursion_in.txt* .
```
3 1000 2 3
200
0 2 2
300
27 3 20
5 2000 2 4
300
4 3 0 4
450
7 8 0 13
``` 

## Sample Output
Output the following in the terminal
```
900
stay home
```

## Codepost Guide


Create a program to solve the problem and rename it to *excursion.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*
An input textfile named *excursion_in.txt* shall be used by codepost to auto check your submission.


sample compile command: 
```
    g++ -o excursion excursion.cpp
```
sample run command:
```
    ./car excursion_in.txt
```
corresponding output:
```
    900
    stay home
```

## Working Code Template

```
/*******************************************************
 * 
 * easy - excursion (p11559)
 * 
 * determine the best price for the hotel stay
 *      given the input textfile 
 * 
sample compile command:
    g++ -o excursion excursion.cpp
sample run command:
    ./excursion excursion_in.txt
corresponding output:
    900
    stay home
 * 
 * ****************************************************/

#include <bits/stdc++.h>
using namespace std;


int participants=0;
int budget=0;
int hotels=0;
int bed_count=0;
int min_cost = 0; 
int cost=0;


int main (int input_count, char* input_array[])
{
    
    ifstream file( input_array[1] );

    // loop through the test cases
    while(!file.eof()){

        file >> participants;
        file >> budget;
        file >> hotels;
        file >> bed_count;
        min_cost = 0;


// change the code below to solve the problem.


        cout << "Case: " << participants << " " << budget << " " << hotels << " " << bed_count << endl;

        // loop through the hotels
        for (int i=0; i< hotels; i++){
            file >> cost;

            cout << "hotel: " << cost << endl;     

            cout << "beds: " ;
            // loop through the bed count
            for(int j=0; j<bed_count; j++){

                int beds;
                file >> beds;

                cout << beds << " " ;
            }
            cout << endl;
        }
    }

    file.close();
}

```

## Sir Roy's Solution
```
/*******************************************************
 * 
 * easy - excursion (p11559)
 * 
 * determine the best price for the hotel stay
 *      given the input textfile 
 * 
sample compile command:
    g++ -o excursion excursion.cpp
sample run command:
    ./excursion excursion_in.txt
corresponding output:
    900
    stay home
 * 
 * ****************************************************/

#include <bits/stdc++.h>
using namespace std;


int participants=0;
int budget=0;
int hotels=0;
int bed_count=0;
int min_cost = 0; 
int cost=0;

bool over_budget(){
    return cost*participants > budget ?  true:  false;
}


int main (int input_count, char* input_array[])
{
    ifstream file("excursion_in.txt");

    // loop through the test cases
    while(!file.eof()){

        file >> participants;
        file >> budget;
        file >> hotels;
        file >> bed_count;
        min_cost = 0;

        // cout << participants << " " << budget << " " << hotels << " " << bed_count << endl;

        // loop through the hotels
        for (int i=0; i< hotels; i++){
            file >> cost;

            // cout << "hotel: " << cost << endl;
            

            // cout << "beds: " ;
            // loop through the bed count
            for(int j=0; j<bed_count; j++){

                int beds;
                file >> beds;

                // cout << beds << " " ;

                if (beds < participants){
                    // continue;
                }
                else{
                    if (!over_budget()){
                        if (min_cost == 0 || (participants*cost) < min_cost ) {
                            min_cost = participants * cost;
                            //break;
                        }
                    }
                    
                }
            }
            // cout << endl;
                
            

        }
        if (min_cost != 0) {
            cout << min_cost << endl  ;
        }
        else{
            cout << "stay home" << endl;
        }
        //return 1;
    }

    file.close();
}


```