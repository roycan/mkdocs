# Professional Rails Project Steps

-Roy Canseco


## New Rails Project

use

`$ rails _5.1.6_ new sample_project --database=postgresql`

instead of:

`rails new sample_project`

* Specify the rails version you want to use for the project. It's good to indicate this in the Gemfile too.
* Immediate use the same database as what you plan to use in production. This is good practice.



## Connect to a database

* make sure you have the database software available
* open `config/database.yml`
* edit the `default` section and add the following with your database details
	* host:
	* username:
	* password:
	* port:
* you can leave the default `pool=5`
* run `$ rails db:create` to see if the connection is successful (ie no errors)


## Create the README

include the following sections in the README

* what is this Project
* License
* Getting Started
* More info / Contact Info

We can use this also as the configuration document. Any sensitive information should be left as configuration variables (eg `$PASSWORD`) with a mapping in a private spreadsheet somewhere.


## Create the privete password spreadsheet

* Google spreadsheets might be a good fit for this.
* Sample accounts can be stored here.


## Private Source Code Repository

* Let's start with gitlab
* Have the team as collaborators
* Assign someone for branch merging to master

## Make "Hello World"

* have the root route to a controller method that says "Hello World"
* this allows us to deploy as early as possible without arousing suspicion

## Heroku deploy

* sign-up for Heroku
* see if you have heroku cli properly installed

`$ heroku --version`

* login and save keys
* the create a project and deploy
* open to see your successful "hello world" deployment

~~~~
$ heroku login
$ heroku keys:add
$ heroku create
$ heroku push master
$ heroku open
~~~~


## Create Static Pages

* This is you About page etc.
* practicing branching would be good now too
		* create a new branch
		* develop the static Pages
		* checkout master and merge branch
		* delete branch

`$ git checkout -b static_pages`

`$ rails generate controller StaticPages home help`



~~~~
$ git add .
$ git commit -m "Add static pages controller"
$ git push -u origin static-pages
~~~~

subsequent pushing is ok with  `$ git push`


## Setup Automatic Testing

* Test the titles on the static Pages

## Freeze the Gemfile

Freezing the versions in the Gemfile can be a good idea now. Since you can now have tests that will check if upgrades will break anything important.

## Setup Continous Integration

* use Gitlab CI
