moosh - Moodle Shell
========================


```
 sudo apt-add-repository 'deb http://ppa.launchpad.net/zabuch/ppa/ubuntu trusty main'
 sudo apt update --allow-unauthenticated
 sudo apt-get update --allow-insecure-repositories
 sudo apt-get install moosh
 sudo apt update --no-allow-unauthenticated
 sudo apt-get update --no-allow-insecure-repositories
```

download backup files
```
$ curl -o FILENAME URL
```
`sudo -u www-data watch moosh top`

Let's try running a bulk course restore script from ~
* get the list of courses 
* get the course IDs of the 5 test courses in *miscellaneous*
* get a full backup of the test courses into /var/www/moodle_tmp
* get the category ID of *miscellaneous*
* try a moosh restore on 1 test course
* try a moosh create for 1 mbz backup
* try to restore the backup to the created course
* make a create-restore script 



 List only empty courses from category 1
`moosh course-list -c 1 -e yes`


Backup course id=3 and save it as /tmp/mybackup.mbz:
`moosh course-backup -f /tmp/mybackup.mbz 3`

Backup course id=3, including logs and grade histories:
`moosh course-backup --fullbackup 3`

Backup course id=3 without any user data (excludes users, logs, grade historyies, role assignments, comments, and filters):
`moosh course-backup --template 3`


 List all categories:
 `moosh category-list`
 
 List all categories with name "test" OR "foobar":
 `moosh category-list test foobar`
 
 Create 10 new courses using bash/zim expansion:
 `moosh course-create newcourse{1..10}`

delete courses id 2,3 and 4:
`moosh course-delete 2 3 4`




