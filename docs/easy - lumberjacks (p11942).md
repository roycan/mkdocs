easy - lumberjacks (p11942)
========================

Another tale of lumberjacks?. Let see . . .

The lumberjacks are rude, bearded workers, while foremen tend to be bossy and simpleminded.
The foremen like to harass the lumberjacks by making them line up in groups of ten, ordered by the
length of their beards. The lumberjacks, being of different physical heights, vary their arrangements to
confuse the foremen. Therefore, the foremen must actually measure the beards in centimeters to see if
everyone is lined up in order.

![qownnotes-media-NEPHgo](media/qownnotes-media-NEPHgo.png)


Your task is to write a program to assist the foremen in determining whether or not the lumberjacks
are lined up properly, either from shortest to longest beard or from longest to shortest.

## Input

Each input contains ten distinct positive integers less than 100.

## Output

There is a title line, then a description of the beard lengths. See the sample output for capitalization
and punctuation.

## Sample Input / Output



| Sample Input | Sample Output  |
|---|---|
| 13 25 39 40 55 62 68 77 88 95 | Lumberjacks: Ordered |
| 88 62 77 20 40 10 99 56 45 36 | Lumberjacks: Unordered |
| 91 78 61 59 54 49 43 33 26 18 | Lumberjacks: Ordered |

## Codepost Guide

Create a program to solve the problem and rename it to *lumberjacks.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o lumberjacks lumberjacks.cpp
```
sample run command:
```
    ./lumberjacks 13 25 39 40 55 62 68 77 88 95 
```
corresponding output:
```
    Lumberjacks: Ordered
```
## Working Template Code

```
/***********************************************************************
 * 
 * easy - lumberjacks (p11942)
 * 
 * check if the lumberjacks are ordered (asc or desc) or unordered
 * 
sample compile command:
    g++ -o lumberjacks lumberjacks.cpp
sample run command:
    ./lumberjacks 13 25 39 40 55 62 68 77 88 95 
corresponding output:
    Lumberjacks: Ordered
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem.

    vector <int> v;

    for (int i=0; i< 10 ; i++){
        
        v.push_back( atoi(input_array[i+1]));
    }

    for (auto &x : v){
        cout << x << " " ;
    }
    cout << endl;

}
```

## Sir Roy's Solution

```
/***********************************************************************
 * 
 * easy - lumberjacks (p11942)
 * 
 * check if the lumberjacks are ordered (asc or desc) or unordered
 * 
sample compile command:
    g++ -o lumberjacks lumberjacks.cpp
sample run command:
    ./lumberjacks 13 25 39 40 55 62 68 77 88 95 
corresponding output:
    Lumberjacks: Ordered
 * 
***********************************************************************/

#include <bits/stdc++.h>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem.

    vector <int> v;

    for (int i=0; i< 10 ; i++){
        
        v.push_back( atoi(input_array[i+1]));
    }

    // for (auto &x : v){
    //     cout << x << " " ;
    // }
    // cout << endl;


    vector <int> v2;

    v2 = v;
    sort(v.begin(), v.end());

    if (v2 == v) {
        
        cout << "Lumberjacks: Ordered" << endl;
        return 0;
    } 

    sort(v.rbegin(), v.rend());

    if (v2 == v){
        
        cout << "Lumberjacks: Ordered" << endl;
        return 0;
    }
    else {
       cout << "Lumberjacks: Unordered" << endl; 
       return 0;
    }

}
```