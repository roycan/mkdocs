goodls - gdrive file download
========================

* Install using https://github.com/tanaikech/goodls
* consider: *Retrieve API key*



## File with several URLs

If you have a file including URLs, you can input the URL data using standard input and pipe as follows. If wrong URL is included, the URL is skipped.

$ cat sample.txt | goodls

or

$ goodls < sample.txt

sample.txt

https://docs.google.com/spreadsheets/d/#####/edit?usp=sharing
https://docs.google.com/document/d/#####/edit?usp=sharing
https://docs.google.com/presentation/d/#####/edit?usp=sharing



## For file

must be a publicly shared file

$ goodls -u https://docs.google.com/spreadsheets/d/#####/edit?usp=sharing -key [APIkey] -i

* -i :: information only

## For folder

must be a publicly shared folder

$ goodls -u https://drive.google.com/drive/folders/#####?usp=sharing -key [APIkey] -i

* -i :: information only
