Q4 - bonus - comparisons (p11172)
========================

Some operators checks about the relationship between two values and these operators are called relational operators. Given two numerical values your job is just to find out the relationship between them
that is 

* (i) First one is greater than the second 
* (ii) First one is less than the second or 
* (iii) First and
second one is equal.

## Input

Each input contains two integers a and b (|a|, |b| < 1000000001).


## Output

This line contains any one of the relational operators
‘>’, ‘<’ or ‘=’, which indicates the relation that is appropriate for the given two numbers.


## Sample Input / Output

sample input: 10 20

corresponding output: < 
 
sample input: 20 10

corresponding output: >

sample input: 10 10

corresponding output: =
 
 
 ## Codepost Guide
 
 sample compile command:
 ```
     g++ -o comparisons comparisons.cpp
 ```
 sample run command:
 ```
     ./comparisons 10 20
 ```
 corresponding output:
```
    <
```

## Template code

```
/*****************************************************************************
 * 
 * check if two integers are > , < , or =
 * 
 sample compile command:
    g++ -o comparisons comparisons.cpp
sample run command:
    ./comparisons 10 20
corresponding output:
    < 
 * 
 * **************************************************************************/

#include <iostream>

using namespace std;

int main (int input_count, char* input_array[])
{
    int n1 = atoi(input_array[1]);
    int n2 = atoi(input_array[2]);


// change the following code to solve the problem

    cout << n1 << " " << n2 << endl;
}
```


