Q3 - Project - Mastermind
========================

## Introduction
Mastermind is a game where you have to guess your opponent’s secret code within a certain number of turns (like hangman with colored pegs), check it out on [Wikipedia](http://en.wikipedia.org/wiki/Mastermind_(board_game)). Each turn you get some feedback about how good your guess was – whether it was exactly correct or just the correct color but in the wrong space.


## Project

Build a simplified Mastermind game from the command line where you have ***7 turns*** to guess the secret ***3-color code***.

* The options for colors are Red, Orange, Yellow, Green, Blue, and Violet represented by R, O, Y, G, B, V, respectively.
* The feedback is represented by numbers. 
    * 0 - a guess is both in the *wrong* location and has a *wrong* color
    * 1 - a guess is in the *wrong* location but has the right color
    * 2 - a guess is in the right location and has the right color

* The secret must *not* repeat colors.
* The feedback numbers should be sorted (highest to lowest).

## Sample Game

```
roycanseco@pop-os:~/projects/cpp_prac$ ./mastermind
         -------------------
         |  1 _ _ _ 0 0 0  |
         -------------------
         |  2 _ _ _ 0 0 0  |
         -------------------
         |  3 _ _ _ 0 0 0  |
         -------------------
         |  4 _ _ _ 0 0 0  |
         -------------------
         |  5 _ _ _ 0 0 0  |
         -------------------
         |  6 _ _ _ 0 0 0  |
         -------------------
         |  7 _ _ _ 0 0 0  |

enter your 3 guesses (R, O, Y, G, B, V):  ROY
         -------------------
         |  1 R O Y 0 0 0  |
         -------------------
         |  2 _ _ _ 0 0 0  |
         -------------------
         |  3 _ _ _ 0 0 0  |
         -------------------
         |  4 _ _ _ 0 0 0  |
         -------------------
         |  5 _ _ _ 0 0 0  |
         -------------------
         |  6 _ _ _ 0 0 0  |
         -------------------
         |  7 _ _ _ 0 0 0  |

enter your 3 guesses (R, O, Y, G, B, V):  GBV
         -------------------
         |  1 R O Y 0 0 0  |
         -------------------
         |  2 G B V 2 1 0  |
         -------------------
         |  3 _ _ _ 0 0 0  |
         -------------------
         |  4 _ _ _ 0 0 0  |
         -------------------
         |  5 _ _ _ 0 0 0  |
         -------------------
         |  6 _ _ _ 0 0 0  |
         -------------------
         |  7 _ _ _ 0 0 0  |

enter your 3 guesses (R, O, Y, G, B, V):  GBB
         -------------------
         |  1 R O Y 0 0 0  |
         -------------------
         |  2 G B V 2 1 0  |
         -------------------
         |  3 G B B 2 1 0  |
         -------------------
         |  4 _ _ _ 0 0 0  |
         -------------------
         |  5 _ _ _ 0 0 0  |
         -------------------
         |  6 _ _ _ 0 0 0  |
         -------------------
         |  7 _ _ _ 0 0 0  |

enter your 3 guesses (R, O, Y, G, B, V):  BBG
         -------------------
         |  1 R O Y 0 0 0  |
         -------------------
         |  2 G B V 2 1 0  |
         -------------------
         |  3 G B B 2 1 0  |
         -------------------
         |  4 B B G 2 2 2  |
         -------------------
         |  5 _ _ _ 0 0 0  |
         -------------------
         |  6 _ _ _ 0 0 0  |
         -------------------
         |  7 _ _ _ 0 0 0  |
```

When you get a "222", you've won. :)


## Template code

```
/*************************************************************************
 * 
 * This is the CS2 Q3 Programming Project
 * You can have solo, pair, trio or quad groupings.
 * You will make a simplified MasterMind Game. 
 * 
 * The game will give you 7 chances to guess a secret series of 3 letters corresponding to colors:
 *  R- red, O- orange, Y- yellow, G- green, B- blue, V- violet
 * For every guess, the game will give you a clue in the form of 3 numbers in a sorted fashion:
 *  0- there is a guessed color that is *not* among the secret colors
 *  1- there is a guessed color that is among the secret colors, but in the *wrong* position
 *  2- there is a guessed color that is among the secret colors and is in the right position
 * 
 * Colors in the secret color set may *not* be repeated
 * 
 * 
 * ************************************************************************/


#include <iostream>

using namespace std;


char secret[3] ={'B', 'B', 'G'} ;


char board [7][7]  = {
							{'1' , '_', '_', '_', '0', '0', '0' } ,
							{'2' , '_', '_', '_', '0', '0', '0' } ,
							{'3' , '_', '_', '_', '0', '0', '0' } ,
							{'4' , '_', '_', '_', '0', '0', '0' } ,
							{'5' , '_', '_', '_', '0', '0', '0' } ,
							{'6' , '_', '_', '_', '0', '0', '0' } ,
							{'7' , '_', '_', '_', '0', '0', '0' } ,
						};




void print_board ()
{
	

	for (int i=0; i<7; i++){

		cout << "\t -------------------"  << endl;
		cout << "\t |  " ;

		for (int j=0; j<7; j++){

			
			cout  << board[i][j] << " ";
		}

		cout << " |";
		cout << endl;
	}
}





int main () 
{
    print_board();
    
    for (int turn = 0; turn < 7; turn++){

		    cout << endl << "enter your 3 guesses (R, O, Y, G, B, V):  ";
		    cin >> board[turn][1] >> board[turn][2] >> board[turn][3] ;
		    
		    print_board();
		    
		}

}
```
