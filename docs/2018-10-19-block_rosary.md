---
layout: post
title: "Block Rosary"	
remarks: prayers
categories: Relationships
tags: 
- Mother Mary
---




# ON THE ARRIVAL OF THE IMAGE

O Lady of Fatima, sovereign Queen of Heaven and Empress of all creatures, we joyfully welcome you on your visit to the home of your humble servants and devoted children. 

Take, O glorious Lady, possession of not only our humble home, but also of ourselves. So most pure Virgin, the only owner and mistress of this house and inmates, who promised to be your loyal and devoted children.

Shower on us your holy blessings and with it the treasure of goodness, health and mercy, which escapes from your most pure hands. 

We promise to render you devoted homage of love while you live in our midst, AMEN.

(3x) Hail Mary, full of grace, the Lord is with you, blessed are you among women and blessed is the fruit of your womb, Jesus. Holy Mary, Mother of God, pray for us sinners, now and at the hour of our death, AMEN.

Our Lady of Fatima, pray for us.

# INTENTIONS FOR EACH NIGHT
**(To be read before reciting the rosary)**

### **FIRST NIGHT:**
In honor of God, the Holy Spirit, the third person of the Blessed Trinity, in honor of the Most Sacred Heart of Jesus and in honor of the Immaculate Heart of Mary.

### **SECOND NIGHT:**
For the protection of the Holy Catholic Church, for the Holy Father, for the Catholic religious and faithful, particularly those who are prosecuted.

### **THIRD NIGHT:** 
For peace in every Christian family and for the members of the family, both living and dead, where the Image of Our Lady is venerated.

### **FOURTH NIGHT:**
For the spiritual and temporal needs of the Rosarians, for the spiritual welfare of children of all nations, for hte increase of priests and religious in the Philippines, for the propagation of Catholic faith in the world.

### **FIFTH NIGHT:**
For the Armed Forces fighting for peace, for a clean and honest government in the Philippines.

### **SIXTH NIGHT:**
For the conversion of sinners and Non-Catholics.

### **SEVENTH NIGHT:**
For the conversion and preservation of faith in the Philippines, in honor of Our Lady of Fatima and most of all for universal peace. 

# THE ROSARY

In the name of the Father and of the Son and of the Holy Spirit, AMEN.

## *THE APOSTLES CREED*

I believe in God, the Father Almighty, creator of heaven and earth. I believe in Jesus Christ, His only Son, Our Lord, who was conceived by the power of the Holy Spirit, born of the Virgin Mary, suffered under Pontius Pilate, was crucified, died and was buried; He decended into the dead; on the third day, He rose again, He ascended into heaven and seated at the right hand of the Father Almighty. He will come again to judge the living and the dead. I believe in the Holy Spirit, the Holy Catholic Church, the Communion of Saints, the forgiveness of sins, the resurrection of the body and life everlasting, AMEN.

**LORD, GIVE US MORE FAITH, HOPE AND CHARITY**


    (1x) Our Father in heaven, holy by your name, your kingdom, your will be done, on earth as in heaven. Give us this day our daily bread and forgive us our sins, as we forgive those who sinned against us; do not bring us to the test, but deliver us from evil, AMEN.

    (3x) Hail Mary, full of grace, the Lord is with you, blessed are you among women and blessed is the fruit of your womb, Jesus. Holy Mary, Mother of God, pray for us sinners, now and at the hour of our death, AMEN.

    (1x) Glory be to the Father, to the Son and to the Holy Spirit. As it was in the beginning, is now ever shall be world without end, AMEN.


## *DECADES OF THE HOLY ROSARY*

### **THE JOYFUL MYSTERIES (MONDAYS AND SATURDAYS)**

    1.  The Annunciation
    2.  The Visitation
    3.  The Birth of our Lord
    4.  The Presentation of the Child Jesus in the Temple
    5.  The Finding of the Child Jesus in the Temple

### **THE SORROWFUL MYSTERIES (TUESDAYS AND FRIDAYS)**

    1.  The Agony in the Garden
    2.  The Scourging at the Pillar
    3.  The Crowning with Thorns
    4.  The Carrying of the Cross
    5.  The Cruxifiction and Death of our Lord Jesus Christ

### **THE GLORIOUS MYSTERIES (WEDNESDAYS AND SUNDAYS)**

    1.  The Resurrection of Our Lord
    2.  The Ascension of Our Lord into Heaven
    3.  The Descent of the Holy Spirit upon the Apostles and the Blessed Virgin Mary
    4.  The Assumption of the Blessed Virgin 
    5.  The Coronation of the Blessed Virgin Mary as Queen of Heaven and Earth

### **THE LUMINOUS MYSTERIES (THURSDAYS)**

    1.  The Baptism of Our Lord
    2.  The Wedding at Cana
    3.  The Proclamation of the Good News and Call to Conversion
    4.  The Transfiguration
    5.  The Institution of the Holy Eucharist


***After reciting Glory be of each decade, say...**

O my Jesus, forgive us our sins, save us from the fires of hell and bring all souls into heaven especially those who most need of your mercy.


## *HAIL HOLY QUEEN*

Hail Holy Queen, Mother of Mercy, hail our life, our sweetness and our hope. To you do we cry, poor banished children of Eve, to you do we send up our sighs, mourning and weeping in this valley of tears. Turn then, most gracious advocate, your eyes of mercy towards us; and after this our exile, show unto us the blessed fruit of your womb, Jesus. O clement, o loving, o sweet Virgin Mary.

V. Pray for us, O Holy Mother of God  
R. That we may be made worthy of the promises of Christ.

**LET US PRAY:**

O God, whose only begotten Son, by His life, death and resurrection, has purchased for us the rewards of eternal life. Grant, we beseech you, that meditating upon these mysteries of the most Holy Rosary of the Blessed Virgin Mary, we may imitate what they contain and obtain what they promised, through Christ, our Lord, AMEN.

## *ANTIPHON*

We fly to your patronage, o Holy Mother of God, despise not our petitions in our necessities, but deliver us from all dangers, o glorious and blessed Virgin Mary.


# THE LITANY OF THE IMMACULATE HEART OF MARY
### *(to be recited on Saturday)*

V. Lord, have mercy on us.  
R. Lord, have mercy on us.  
V. Christ, have mercy on us.  
R. Christ, have mercy on us.  
V. Lord, have mercy on us.  
R. Lord, have mercy on us.  
V. Christ, hear us.
R. Christ, hear us.  
V. Christ, graciously hear us.  
R. Christ, graciously hear us.  
V. God the Father in heaven  
R. Have mercy on us.  
V. God the Holy Spirit  
R. Have mercy on us.  

Litany | response
-----|-------
Heart of Mary, | Pray for us
Heart of Mary, united to the heart of Jesus, | Pray for us
Heart of Mary, Orgen of the Holy Spirit, | Pray for us
Heart of Mary, Sanctuary of Divine Trinity, | Pray for us
Heart of Mary, Tabernacle of God Incarnate, | Pray for us
Heart of Mary, imaculate from your creation, | Pray for us
Heart of Mary, full of grace, | Pray for us
Heart of Mary, blessed among all hearts, | Pray for us
Heart of Mary, Throne of Glory, | Pray for us
Heart of Mary, holy cause of Divine Love, | Pray for us
Heart of Mary, fastened to the cross with Jesus Crucified, | Pray for us
Heart of Mary, comforter of the afflicted, | Pray for us
Heart of Mary, refuge of sinners, | Pray for us
Heart of Mary, hope of the agonizing, | Pray for us
Heart of Mary, seat of mercy, | Pray for us

V. Lamb of God, who takes away the sins of the world,   
R. Spare us O Lord  
V. Lamb of God, who takes away the sins of the world,   
R. Graciously hear us O Lord  
V. Lamb of God, who takes away the sins of the world,   
R. Have mercy on us  
V. Christ hear us  
R. Christ, graciously hear us  
V. Immaculate Mary, meek and humble of heart,  
R. Make our hearts according to the heart of Jesus  
V. Pray for us, O Holy Mother of God  
R. That we may be made worthy of the promises of Christ  

# IN MEMORARE
### *(to be recited everyday)*

Remember, O most gracious Virgin Mary, that never was it known that anyone, who fled to your protection, implored your help and sought your intercession, was left unaided. Inspired with this confidence, I fly unto you, O Virgin of Virgins, my Mother, to you I come, before you I stand, sinful and sorrowful. O Mother of the Word Incarnate, despise not my petitions, but in your mercy, hear and answer me, AMEN. 

# ACT OF CONSECRATION TO THE IMMACULATE HEART OF MARY
### *(to be recited on Saturday)*

O Mary, powerful Virgin and Mother of Mercy, Queen of Heaven and Refuge of Sinners, we consecrate ourselves to your Immaculate Heart. To you, we consecrate all the we have and all that we are. To you, we entrust our home, our family and our country. We wish that everything be yours and may share in the benefits of your motherly protection. Therefore we solemnly renew our baptismal vows. We renounce the devil, his works and his attractions. We pledge loyalty to the guidance and leadership of the Holy Father, the Pope and the Bishops. We promise to obey the commandments of God and our Holy Mother, the Church, and in particular, to sanctify the Lord's day, Sunday. We promise to say our daily prayers, to recite the rosary in our family and to make frequent use of the sacraments of Confession and Holy Communion. Finally, O glorious Mother of God and merciful Mother of Mankind, we humbly pray that this wholehearted consecration to your Immaculate Heart, may obtain the conversion of pagans and of sinners, that it may hasten the triumph of the Sacred Heart of Jesus, your Divine Son, all over the world, AMEN.

# PEACE PRAYER TO OUR LADY OF FATIMA
### *(to be recited on Saturday)*

Queen of the Most Holy Rosary, refuge of the human race, we are confident in obtaining peace, mercy, grace and help in the  present calamity, through the goodness of your maternal heart.

**FOR PEACE IN THE PHILIPPINES**  

    Three (3) Hail Marys...
    Our Lady of Fatima, Pray for us

**FOR THE INTENTIONS OF THE SPONSORS**

    One (1) Our Father
    One (1) Hail Mary
    One (1) Glory be
    Our Lady of Fatima, Pray for us

**FOR THE POOR SOULS IN PURGATORY**

    One (1) Our Father
    One (1) Hail Mary
    One (1) Glory be
    Our Lady of Fatima, Pray for us

V. Eternal rest give unto them, O Lord  
R. And let your perpetual light shine upon them  
V. May the rest in peace  
R. AMEN.  
V. Most Sacred Heart of Jesus (3x)  
R. Have mercy on us (3x)  
V. Immaculate Heart of Mary  
R. Pray for us  

**KEEP SILENT AND ASK WHATEVER FAVOR YOU WISH FROM OUR BLESSED MOTHER.**



# FAREWELL TO OUR LADY
### *(to be recited on the last day of visit)*

Our Lady of Fatima and Immaculate Mother of God, before leaving our humble dwelling, we desire to express the pain and sorrow that fill our souls at seeing you depart from our midst.   
Thanks O Mother, for having remained among us, your humble and devoted children.   
Thank you for the benefits, with which no merit on our partyou have granted on us.   
Do not forget that in this house, there are hearts that love you and are eager for the moment of receiving you anew in our midst.  
Forgive and excusee us, Most Merciful Mother, if we did not know how to honor you as due and for not having profited enough from the visit through our frailty and ignorance.  
Mother, never forget us in time or in eternity and we hope from your unlimited bounty  that in the same way as we willingly have opened our doors to welcome you in our home, you may also one day open for us the gates of heaven, AMEN.

**OUR LADY OF FATIMA, PRAY FOR US**


```
Legion of Mary
Christ the King Parish
050803
```
