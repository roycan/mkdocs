Fiber Project
========================

CASE NUMBER

CAS-07733-L3X6B4

CUSTOMER INFORMATION

Company Name

:

PSHS – QC

Address

:

Agham road, Quezon City


AGENDA/ACTIVITY DESCRIPTION

Date of Activity

:

05/05/2021

Support Type

:

Pre-sales



Details of meeting

1. Supply and installation of 6core outdoor FOC from MIS Server room to Admin Server room (2nd floor)
2. Existing underground conduit to be used from MIS to Admin building
2. Existing indoor conduit (pvc pipes) to be used from admin entrance to Server room (2nd floor)
3. Supply and installation of 6core outdoor FOC from MIS server room to Gymnasium ground floor
4. To layout a new underground conduit
5. To submit an Underground fiber layout 
6. Supply and installation of fiber patchpanels, fiber patchcords, connectors
Supply and installation of 42U cabinet at Admin building and 3ft data cabinet at gymnasium 
7. Termination of all cat6 cables in the cubicles of the admin building 


RECOMMENDATION

PENDING

List of files to be provided by the client:

Termination of cat6 scope
Complete Floorplan of admin building
REMARKS

-

ASSIGNED ENGINEER/S

Christian Guevarra
Ponciano Malubay


## Pictures


![qownnotes-media-cZhMbR](media/qownnotes-media-cZhMbR.png)



![qownnotes-media-LtcWes](media/qownnotes-media-LtcWes.png)
