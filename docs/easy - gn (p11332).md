easy - gn (p11332)
========================


For a positive integer n, let f (n) denote the
sum of the digits of n when represented in base
10. It is easy to see that the sequence of numbers n, f (n), f (f (n)), f (f (f (n))), . . . eventually
becomes a single digit number that repeats for-
ever. Let this single digit be denoted g(n).

![qownnotes-media-WUwWAk](media/qownnotes-media-WUwWAk.png)



For example, consider n = 1234567892.

Then:

> f (n) = 1+2+3+4+5+6+7+8+9+2 = 47

> f (f (n)) = 4 + 7 = 11

> f (f (f (n))) = 1 + 1 = 2

Therefore, g(1234567892) = 2.

## Input 

Each line of input contains a single positive integer n at most 2,000,000,000. 

## Output

For each such integer, you are to output a single
line containing g(n).

## Sample Input/ Output


| Input | Output  |
|---|---|
| 2 | 2 |
| 11 | 2 |
| 47 | 2 |
| 1234567892 | 2 |

## Codepost Guide

Create a program to solve the problem and rename it to *gn.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o gn gn.cpp
```
sample run command:
```
    ./gn 47
```
corresponding output:
```
    2
```
## Working Template Code
```
/******************************************************
 * 
 * easy - gn (p11332)
 * 
 * For a positive integer n, let f (n) denote the sum of the digits of n when represented in base 10. It is easy to see that the sequence of numbers n, f (n), f (f (n)), f (f (f (n))), . . . eventually becomes a single digit number that repeats for- ever. Let this single digit be denoted g(n).
 * 
sample compile command:
    g++ -o gn gn.cpp
sample run command:
    ./gn 47
corresponding output:
    2
 * 
 * ***************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{
    string n = input_array[1] ;

// change the code below to solve the problem

    cout << n << endl;
}

```
## Sir Roy's Solution
```
/******************************************************
 * 
 * easy - gn (p11332)
 * 
 * For a positive integer n, let f (n) denote the sum of the digits of n when represented in base 10. It is easy to see that the sequence of numbers n, f (n), f (f (n)), f (f (f (n))), . . . eventually becomes a single digit number that repeats for- ever. Let this single digit be denoted g(n).
 * 
sample compile command:
    g++ -o gn gn.cpp
sample run command:
    ./gn 47
corresponding output:
    2
 * 
 * ***************************************************/

#include <iostream>

using namespace std;


int main (int input_count, char* input_array[])
{
    string n = input_array[1] ;

// change the code below to solve the problem

    while (n.length() > 1){

        int sum = 0;

        for (int i=0; i<n.length(); i++){
            sum += (int)n[i] - (int)'0';
        }
        n = to_string(sum);

    }

    cout << n << endl;
    
}
****
```