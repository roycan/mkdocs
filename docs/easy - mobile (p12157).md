easy - mobile (p12157)
========================

Ampang Communications & Mobile (ACM) provides telecom services for
various types of users. Since the people of Ampang are quite talkative,
they are always seeking for packages that are best suited for them. To
have an edge over their competitors, ACM provides various packages. Two
of the most popular packages are:

* • Mile
* • Juice

![qownnotes-media-USHQtw](media/qownnotes-media-USHQtw.png)



**Mile** charges every 30 seconds at a rate of 10 cents. That means if you
talk for 29 seconds or less, you will be charged with 10 cents. If you talk
for 30 to 59 seconds, you will be charged with 20 cents and so on.

**Juice** charges every 60 seconds at a rate of 15 cents. That means if you
tailk for 59 seconds or less, you will be charged with 15 cents. Similarly, if
you talk for 60 seconds to 119 seconds, you will be charged with 30 cents
and so on.

Given a list of call durations, can you determine the package that is
cheaper?


## Input

 Each case
starts with a line containing an integer N (0 < N < 20). The next numbers give a list of N call durations
(In second). Each call duration is an integer in the range [1, 2000]. Consecutive integers are separated
by a single space character.

## Output

Output the name of the cheaper package followed by
the corresponding cost in cents. If both package gives the same total cost, then output both the names
(‘Mile’ preceding ‘Juice’) followed by the cost. Look at the output for sample input for details.

### Illustration
> Case 1: Mile(30+10=40) & Juice(30+15=45).

> Case 2: Mile(20+20+20=60) & Juice(15+15+15=45).

> Case 3: Mile(30+30=60) & Juice(30+30=60).

## Sample Input / Output



| Input  | Output |
|---|---|
| 2 61 10 | Mile 40  |
| 3 40 40 40 | Juice 45  |
| 2 60 65 | Mile Juice 60 |
 | 2 29 59 | Mile Juice 30 |

## Codepost Guide

Create a program to solve the problem and rename it to *mobile.cpp*.
Upload the file to *[codepost.io](http://codepost.io)*

sample compile command: 
```
    g++ -o mobile mobile.cpp
```
sample run command:
```
    ./mobile 2 61 10
```
corresponding output:
```
    Mile 40
```
## Working Template Code

```
/***********************************************************************
 * 
 * easy - mobile (p12157)
 * 
 * pick the cheaper mobile plan
 * 
sample compile command:
    g++ -o mobile mobile.cpp
sample run command:
    ./mobile 2 61 10
corresponding output:
    Mile 40
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int count = atoi( input_array[1]);
    int mile_cost = 0;
    int juice_cost = 0;

    for (int i=0; i<count; i++){

        int call = atoi(input_array[i+2]);
        cout << call << " " ;

    }
    cout << endl;



}
```

## Sir Roy's Solution
```
/***********************************************************************
 * 
 * easy - mobile (p12157)
 * 
 * pick the cheaper mobile plan
 * 
sample compile command:
    g++ -o mobile mobile.cpp
sample run command:
    ./mobile 2 61 10
corresponding output:
    Mile 40
 * 
***********************************************************************/

#include <iostream>
using namespace std;

int main(int input_count, char* input_array[]) 
{
// change the following code to solve the problem

    int count = atoi( input_array[1]);
    int mile_cost = 0;
    int juice_cost = 0;

    for (int i=0; i<count; i++){

        int call = atoi(input_array[i+2]);
        // cout << call << " " ;


        ++call ; // make the computation easier

        mile_cost += call/ 30 * 10;
        

        if (call % 30 != 0){
            mile_cost += 10; 
        }

        juice_cost += call/60 * 15;


        if (call % 60 != 0 ){
            juice_cost += 15;
        }

    }


    if (mile_cost < juice_cost){
        cout << "Mile " << mile_cost <<endl;  
    }
    else if (mile_cost > juice_cost){
        cout << "Juice " << juice_cost << endl;
    }
    else{  // case where both have the same cost
        cout << "Mile Juice " << mile_cost << endl;
    }

}
```