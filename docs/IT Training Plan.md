IT Training Plan
========================

## Day 1 - restarting moodle webservers (nginx and php-fpm)
* putty 
* adduser
* usermod -aG sudo 
* ssh user@172.105.217.30
* sudo systemctl restart 
    * nginx
    * php7.1-fpm
    * php7.2-fpm
    * mysqld
* chmod +x 
* restart bash script 

## Day 2 - linux backup basics (tar and rsync)

* I'll create a script to quickly make a folder with a lot of files 
* I'll ask everyone to tarball the folder
* I'll ask everyone to rsync the folder into another folder
* We'll log into the backhub server 
* We'll tarball the important Nephila folders
* We'll rsync pull down to the laptop

### rsync

### tar 


## Day 3

* Present a roy-tutorial on installing moodle on a VPS
* Create nanodes for everyone
* Ask everyone to follow the tutorial and STOP at the Moodle installation web page

    > R :: results we want

    > O :: observations to make along the way

    > W :: why we do it this way 
    
   

## Day 4

* Do the DNS setup in CloudFlare 
* update config.php and sites-available/moodle
* Check for https
* Finish Moodle installation
* Log in as admin

## Day 5

* Create a course
* rsync a large course into the server
* Try to restore a large course
* adjust config.php to increase upload size
* Restore a large course

## Day 6

* install moosh
* sudo -u www-data watch moosh top
* sudo -u www-data moosh course-list
* check database
* sudo -u www-data moosh course-delete
* go over the backup script

## Day 7

* go over the bulk_create_restore script
* install goodls
* get api key
* download .mbz directory
* realpath
* redirection: >
* run the bulk_create_restore script

## Day 8

Try Ruby    