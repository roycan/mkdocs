tar
========================

This is how you make a "zip" file as a SysAd.

`tar cvfz file.tar folder_to_zip`


Extracting a gzip tar Archive *.tar.gz using option -xvzf : This command extracts files from tar archived file.tar.gz files

`$ tar -xvzf file.tar.gz -C folder_to_put_in`



Syntax: 

tar [options] [archive-file] [file or directory to be archived]


Options: 
-c : Creates Archive 
-x : Extract the archive 
-f : creates archive with given filename 
-t : displays or lists files in archived file 
-u : archives and adds to an existing archive file 
-v : Displays Verbose Information 
-A : Concatenates the archive files 
-z : zip, tells tar command that creates tar file using gzip 
-j : filter archive tar file using tbzip 
-W : Verify a archive file 
-r : update or add file or directory in already existed .tar file 




