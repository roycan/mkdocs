I.T. Service Management Elective 
========================

1. Service Strategy 
    1. Service Portfolio Management
    3. Demand Management
2. Service Design 
    1. Service Level Management
    2. Availability Management
    3. Capacity Management
    4. Continuity Management
3. Service Transition 
    1. Change Management 
    2. Release and Deployment Management
    3. Transition Planning and Support
4. Service Operations 
    1. Service Desk
    2. Incident Management
    3. Problem Management
    4. Event Management
    5. Access Management
    6. Request Fulfillment
5. Continuous Service Improvement
    1. Identifying Service Strategies
    2. Defining Metrics
    3. Data Gathering 
    4. Data Processing 
    5. Data Analysis
    6. Data Presentation
    7. Improvement Plan



1. Course Outcomes

Upon completion of the course, students must be able to:

CO 1	
Discuss a conceptual framework of I.T. Service Management and its underlying principles

CO 2	
Apply appropriate tools and techniques for I.T. Risk Assessment and Project Management

CO 3	
Identify the key principles related to each stage of I.T. Service Management

CO 4	
Explain the processes of Demand, Capacity and Availability Monitoring

CO 5	
Identify and plan for managing project risks and opportunities for improvement

