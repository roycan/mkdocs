---
title: Teacher Career Path Evaluation Sheet
author: Roy Vincent Luna Canseco
date: April 2019 onwards
---

Dear Venerable Appointment Committee,

I wish to apply for a Special Science Teacher V post in Philippine Science High School - Main Campus in the Computer Science Unit.

Here are some points of interest for consideration. I am putting self evaluations points on the work that I was able to find the certificates for.

Thank you very much for considering.


Sincerely,

Roy Canseco


---

## Teaching Performance
<to be written>

## Relevant Education (19 points)

Bachelor of Science in Electrical Engineering  -  I got the honor of graduating top of the B.S.E.E. class of 2008.

Master of Science in Electrical Engineering maj. Power Systems  - I got the honor of finishing program in 1 year, instead of 2. My adviser and I believes that I was the first to do so. This can easily be checked by looking at my Bachelor's diploma (year 2008) and comparing it to my Master of Science diploma (year 2009).

Both degrees are from UP Diliman.

I would like to request 19 points.


## Relevant Teaching Experience (5.0 points)

I include my Service Record in UP Diliman, both for teaching in Electrical and Electronics Engineering and in Computer Science. If you add the years/months/days, it would show a few days more than 5 years worth teaching to present.


## Relevant Training (8.9 points)


I ask that the following be credited:

1. <to be written>


## Professional Output / Activities

### *Teacher* Training Involvement

1.  <to be written>

### *Student* Training Involvement (4 points)

1. <to be written>


### Leading a Professional Organization

<to be written>

### Committee Membership (4.0)

* <to be written>

### Administrative Assignments

* <to be written>

### Books Published

none yet (still in the bucket list)

### Research / Professional Articles Published (4.0 points)

<to be written>


### Lab Manual/ Workbook published

none yet (planning to for Web/ Mobile Information System Development)

### Involvement in Research

* <to be written>

### Professional Awards and Recognition (1.0 points)

* <to be written>

### Coaching Co-curricular Activities

none yet (no plans)

### Involvement in Outreach Activities (1.2 points)

* <to be written>

----



Thank you for your kind consideration!
