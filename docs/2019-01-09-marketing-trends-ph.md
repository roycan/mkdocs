# Philippine Digital Marketing Trends 2018

 * Filipinos spend an average of 9 hours on the internet and 4 hours 17 minutes on social media every day, the most in the world  -[source](https://wearesocial.com/special-reports/digital-in-2017-global-overview)

 * About 59% of Filipinos perceive the content they see in Facebook, Instagram and Twitter as reliable. Globally, only 35% trust the content in these channels -[source](http://connectedlife.tnsglobal.com/)

* Smartphone penetration in the Philippines is estimated at 70% -[source](http://business.inquirer.net/204077/smartphone-use-in-ph-seen-rising-to-70-by-18#ixzz52dKVknCT)

* Filipinos use internet on their mobile devices for over 3.5 hours every day [GlobalWebIndex data]

* The Philippines posted the highest year-on-year increase in mobile shopping in Asia-Pacific in 2016 [Mastercard consumer poll]

* Video is the most effective digital marketing format (vs. articles, infographics, e-newsletters) in 2017 [GetCRAFT]

*example:*

<iframe width="565" height="377" src="https://www.youtube.com/embed/RA9ilKx9aqU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* 65% of Filipinos purchase with their smartphones and 41% make mobile payments at least once a week -[source](http://business.inquirer.net/235294/filipinos-show-high-interest-in-cashless-payments-for-security-convenience-survey)

* 81% of consumers check products on the internet before buying and the first page of the google search gets 33% more web traffic. -[source](http://www.adweek.com/digital/81-shoppers-conduct-online-research-making-purchase-infographic/)

* Philippines has a reported 60 million internet users in the 2017 -[source](https://www.entrepreneur.com.ph/news-and-events/ph-now-has-60-million-internet-users-growing-27-in-2016-a36-20170124)

* Smartphones take up 69% of digital media time -[source](http://business.inquirer.net/204077/smartphone-use-in-ph-seen-rising-to-70-by-18)


-----

- [Rappler.com](https://www.rappler.com/technology/features/194338-digital-marketing-in-2018-philippines-trends)

- [TrueLogic.com.ph](https://www.truelogic.com.ph/blog/digital-marketing/digital-marketing-2018-philippines)
