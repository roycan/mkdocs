#!/usr/bin/ruby

# This script gets the images from my Notes and saves to Mkdocs


command = "rsync ../../Dropbox/Notes/media docs/media/ -v -r --progress --append --delete"

command2 = "rsync ../../Dropbox/Notes/*.md docs/ -v -r --progress --append "

if ARGV[0] == '-n'
    command += ' -n'
end

puts command
puts command2

puts "you may put a '-n' option for a dry run"

puts `#{command}`
puts `#{command2}`
